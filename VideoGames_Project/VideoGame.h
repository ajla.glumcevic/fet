// Filename: VideoGame.h
// Author: 
// Date: 02-11-2021
#pragma once
#include "Text.h"
#include <fstream>

class VideoGame{

  private:
    Text* title = nullptr;
    Text* platform = nullptr;
    int year = 0;
    Text* genre = nullptr;
    Text* ageRating = nullptr;
    int userRating = 0;

  public:
    VideoGame(Text*, Text*,int ,Text* ,Text*, int);
    ~VideoGame();
    void printVideoGameDetails()const;
    void printVideoGameDetailsToFile(std::ofstream&)const;
    Text* getVideoGameTitle()const; 
};
