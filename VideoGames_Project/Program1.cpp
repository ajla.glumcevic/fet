// Filename: Program1.cpp
// Author:
// Date: 02-11-2021
#include <iostream>
#include "VideoGameLibrary.h"

// This function displays the menu and returns the choice
// It also checks the range of choice
int menu(VideoGameLibrary* library) {
  int choice = -1;
  std::cout << "\nWhat would you like to do?" << std::endl;
  std::cout << "1. Load video games from file." << std::endl;
  std::cout << "2. Save video games to a file." << std::endl;
  std::cout << "3. Add a video game." << std::endl;
  std::cout << "4. Remove a video game." << std::endl;
  std::cout << "5. Display all video games." << std::endl;
  std::cout << "6. Remove ALL video games from this library and end program."
            << std::endl;
  std::cout << "CHOOSE 1-6: ";
  std::cin >> choice;
  std::cin.clear();
  std::cin.ignore();

  if (choice < 0 || choice > 6) {
    std::cout << "This is not a valid choice." << std::endl;
    choice = -1;
  }
  return choice;
}

int main(void) {
  std::cout << "How many video games can your library hold?" << std::endl;
  int max_num_of_games;
  std::cin >> max_num_of_games;

  // Dinamically allocate a VideoGameLibrary object with max number of games
  VideoGameLibrary* library = new VideoGameLibrary(max_num_of_games);

  // Continue displaying a menu
  while (1) {
    int choice = menu(library);

    // For each choice do as written in description of the program
    if (choice == 1) {
      char filename[100];
      std::cout << "What is the name of the file? (example.txt) ";
      std::cin >> filename;
      // If on Linux comment this line
      fflush(stdin);
      library->loadVideoGamesFromFile(filename);
    } else if (choice == 2) {
      char filename[100];
      std::cout << "What do you want to name the file? (example.txt) ";
      std::cin >> filename;
      // If on Linux comment this line
      fflush(stdin);
      library->saveToFile(filename);
    } else if (choice == 3) {
      library->addVideoGameToArray();
    } else if (choice == 4) {
      library->removeVideoGameFromArray();
    } else if (choice == 5) {
      library->displayVideoGames();
    } else if (choice == 6) {
      // End the program by removing all video games and break the while loop
      delete library;
      std::cout << "GOODBYE!" << std::endl;
      break;
    } else {
      continue;
    }
  }
  return 0;
}
