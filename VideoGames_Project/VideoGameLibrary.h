// Filename: VideoGameLibrary.h
// Author: 
// Date: 02-11-2021
#pragma once
#include "VideoGame.h"


class VideoGameLibrary{

  private:
    VideoGame** videoGamesArray = nullptr;
    size_t maxGames = 0;
    size_t numGames = 0;

  public:
    VideoGameLibrary(const int);
    ~VideoGameLibrary();
    void resizeVideoGameArray();
    void addVideoGameToArray();
    void displayVideoGames()const;
    void displayVideoGameTitles()const;
    void loadVideoGamesFromFile(const char*);
    void removeVideoGameFromArray();
    void saveToFile(const char*)const;
};
