// Filename: VideoGameLibrary.cpp
// Author:
// Date: 02-11-2021
#include "VideoGameLibrary.h"

// Constructor
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Dinamically allocate an array of VideoGame* and initialize other class
// members accordingly
VideoGameLibrary::VideoGameLibrary(const int maxGames_arg) {
  videoGamesArray = new VideoGame*[maxGames_arg];
  maxGames = maxGames_arg;
  numGames = 0;
}

// Destructor
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
// First, deallocate all of the VideoGames from the library
// Then delete the array and null other class members
VideoGameLibrary::~VideoGameLibrary() {
  for (int i = 0; i < numGames; ++i) {
    delete videoGamesArray[i];
  }
  delete[] videoGamesArray;
  videoGamesArray = nullptr;
  maxGames = 0;
  numGames = 0;
  std::cout << "VideoGameLibrary destructor: Released memory for each game in "
               "the video game array and the array itself."
            << std::endl;
}

// Create a temporary array with maxGames*2
// ////////////////////////////////////////////////////////////////////////////////
// Copy the contents, delete the old array and initialize it to the temporary
void VideoGameLibrary::resizeVideoGameArray() {
  VideoGame** tmp_videoGameArray = new VideoGame*[maxGames * 2];
  std::copy(videoGamesArray, videoGamesArray + maxGames, tmp_videoGameArray);
  delete[] videoGamesArray;
  videoGamesArray = tmp_videoGameArray;
  maxGames *= 2;
}

// Adding one VideoGame to this library
// /////////////////////////////////////////////////////////////////////////////////////
void VideoGameLibrary::addVideoGameToArray() {
  // Temporary variables used to create Text objects
  char title[100], platform[100], genre[100], age_rating[100];
  int year, user_rating;

  // Ask the user for VideoGame information
  // Also, use cin.clear and cin.ignore for correct input
  std::cout << "Video Game TITLE: ";
  std::cin.getline(title, 100);
  std::cout << "Video Game PLATFORM: ";
  std::cin.getline(platform, 100);
  std::cout << "Video Game YEAR: ";
  std::cin >> year;
  std::cin.clear();
  std::cin.ignore();
  std::cout << "Video Game GENRE: ";
  std::cin.getline(genre, 100);
  std::cout << "Video Game AGE RATING: ";
  std::cin.getline(age_rating, 100);
  std::cout << "Video Game USER RATING (out of 100): ";
  std::cin >> user_rating;
  // In case of wrong input
  if (user_rating < 0 || user_rating > 100) {
    std::cout << "Invalid input." << std::endl;
    return;
  }

  // Allocate Text objects so we can initialize the new VideoGame
  Text* text_title = new Text(title);
  Text* text_platform = new Text(platform);
  Text* text_genre = new Text(genre);
  Text* text_age_rating = new Text(age_rating);
  VideoGame* new_videoGame =
      new VideoGame(text_title, text_platform, year, text_genre,
                    text_age_rating, user_rating);

  // In case there is not enough space for the new object
  if (numGames == maxGames) resizeVideoGameArray();

  // Add the game and Increment the number of games in the library
  videoGamesArray[numGames] = new_videoGame;
  ++numGames;
}

// For every VideoGame in this library, print its information using its own
// print method /////////////////////////////////////////
void VideoGameLibrary::displayVideoGames() const {
  for (int i = 0; i < numGames; ++i) {
    std::cout << "\t\t\t---------- Video Game " << i + 1 << "----------\n"
              << std::endl;
    videoGamesArray[i]->printVideoGameDetails();
  }
}

// For every VideoGame in this library print its title
// ///////////////////////////////////////////////////////////////////////////
void VideoGameLibrary::displayVideoGameTitles() const {
  for (int i = 0; i < numGames; ++i)
    std::cout << "Video game " << i + 1 << " : "
              << videoGamesArray[i]->getVideoGameTitle()->getText()
              << std::endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void VideoGameLibrary::loadVideoGamesFromFile(const char* filename) {
  FILE* fptr;
  fptr = fopen(filename, "r");
  if (!fptr) {
    std::cout << "Sorry, I was unable to open the file." << std::endl;
    return;
  }

  char title[100], platform[100], genre[100], age_rating[100], year[100],
      user_rating[100];
  while (fscanf(fptr, "%[^\n]", title) != EOF) {
    fgetc(fptr);
    fscanf(fptr, "%[^\n]", platform);
    fgetc(fptr);
    fscanf(fptr, "%[^\n]", year);
    fgetc(fptr);
    fscanf(fptr, "%[^\n]", genre);
    fgetc(fptr);
    fscanf(fptr, "%[^\n]", age_rating);
    fgetc(fptr);
    fscanf(fptr, "%[^\n]", user_rating);
    fgetc(fptr);

    // Allocate Text objects so we can initialize the new VideoGame
    Text* text_title = new Text(title);
    Text* text_platform = new Text(platform);
    Text* text_genre = new Text(genre);
    Text* text_age_rating = new Text(age_rating);
    VideoGame* new_videoGame =
        new VideoGame(text_title, text_platform, std::stoi(year), text_genre,
                      text_age_rating, std::stoi(user_rating));

    // In case there is not enough space for the new object
    if (numGames == maxGames) resizeVideoGameArray();

    // Add the game and Increment the number of games in the library a
    videoGamesArray[numGames] = new_videoGame;
    ++numGames;

    std::cout << title << " was added to the video game library!" << std::endl;
  }

  fclose(fptr);
  std::cout << numGames
            << " video games were read from the file and added to your "
               "VideoGame library."
            << std::endl;
}

// Removing a VideoGame from this library
// /////////////////////////////////////////////////////////////////////////////////////////
void VideoGameLibrary::removeVideoGameFromArray() {
  // If the library's empty just warn the user
  if (!numGames) {
    std::cout << "No VideoGames in the library." << std::endl;
    return;
  }

  // Display the titles of all VideoGames in this library and ask user which one
  // does he want to remove Also, use cin.clear and cin.ignore for correct input
  int choice = -1;
  std::cout << "Choose from the following video games to remove:" << std::endl;
  displayVideoGameTitles();
  std::cout << "Choose a video game to remove between 1 & " << numGames << ":";
  std::cin >> choice;
  std::cin.clear();
  std::cin.ignore();
  // In case of wrong input
  if (choice < 0 || choice > numGames)
    throw std::out_of_range("Invalid input.");

  // Print the name of the game the user is removing
  std::cout << "The video game \""
            << videoGamesArray[choice - 1]->getVideoGameTitle()->getText()
            << "\" has been succesfully deleted." << std::endl;

  // Deallocate this VideoGame from the heap, move all array elements back and
  // decrement numGames
  delete videoGamesArray[choice - 1];
  for (int i = choice; i < numGames; ++i)
    videoGamesArray[i - 1] = videoGamesArray[i];
  --numGames;
}

// Save your work to a file
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
void VideoGameLibrary::saveToFile(const char* filename) const {
  std::ofstream output{filename};
  if (!output.is_open()) {
    std::cout << "Sorry, I was unable to open the file." << std::endl;
    return;
  }

  // For every VideoGame in the array print its information to a file using its
  // own method
  for (int i = 0; i < numGames; ++i)
    videoGamesArray[i]->printVideoGameDetailsToFile(output);

  output.close();
  std::cout << "All video games in your library have been printed to "
            << filename << std::endl;
}
