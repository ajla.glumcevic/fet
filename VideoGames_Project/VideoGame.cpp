// Filename: VideoGame.cpp
// Author: 
// Date: 02-11-2021
#include "VideoGame.h"

// Constructor
// /////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialize all of the class members
VideoGame::VideoGame(Text* title_arg, Text* platform_arg, int year_arg,
                     Text* genre_arg, Text* ageRating_arg, int userRating_arg) {
  title = title_arg;
  platform = platform_arg;
  year = year_arg;
  genre = genre_arg;
  ageRating = ageRating_arg;
  userRating = userRating_arg;
}

// Destructor
// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// Deallocate the Text objects from the heap, null all of the class members
VideoGame::~VideoGame() {
  delete title;
  delete platform;
  delete genre;
  delete ageRating;
  title = nullptr;
  platform = nullptr;
  genre = nullptr;
  ageRating = nullptr;
  year = 0;
  userRating = 0;
  std::cout << "VideoGame destructor: Released memory for title, platform, "
               "genre, & rating."
            << std::endl;
}

// Print the information about this VideoGame
// ///////////////////////////////////////////////////////////////////////
void VideoGame::printVideoGameDetails() const {
  // Another way of doing this
  // std::cout << std::endl;
  // std::cout << "\t\tGame Title:\t" << title->getText() << std::endl;
  // std::cout << "\t\tPlatform:\t" << platform->getText() << std::endl;
  // std::cout << "\t\tYear Released:\t" << year << std::endl;
  // std::cout << "\t\tGenre:\t" << genre->getText() << std::endl;
  // std::cout << "\t\tRating:\t" << ageRating->getText() << std::endl;
  // std::cout << "\t\tNumber of Stars:" << userRating << std::endl;
  // std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "\t\tGame Title:\t";
  title->displayText();
  std::cout << "\t\tPlatform:\t";
  platform->displayText();
  std::cout << "\t\tYear Released:\t" << year << std::endl;
  std::cout << "\t\tGenre:\t\t";
  genre->displayText();
  std::cout << "\t\tRating:\t\t";
  ageRating->displayText();
  std::cout << "\t\tNumber of Stars:" << userRating << std::endl;
  std::cout << std::endl;
}

// Print all the VideoGame information to a file
// //////////////////////////////////////////////////////////////////
void VideoGame::printVideoGameDetailsToFile(std::ofstream& output) const {
  output << title->getText() << '\n'
         << platform->getText() << '\n'
         << year << '\n'
         << genre->getText() << '\n'
         << ageRating->getText() << '\n'
         << userRating << '\n';
}

// Return a title of this VideoGame as a Text*
// /////////////////////////////////////////////////////////////////////
Text* VideoGame::getVideoGameTitle() const { return title; }
