// Filename: Text.cpp
// Author: 
// Date: 02-11-2021
#include "Text.h"

// Constructor ////////////////////////////////////////////////////////////////
// Allocate an array on heap with the size of arr, copy the characters and
// finish it with '\0' Initialize the length accordingly
Text::Text(const char* arr) {
  for (int i = 0; arr[i] != '\0'; ++i) textLength++;
  textArray = new char[textLength + 1];
  std::copy(arr, arr + textLength, textArray);
  textArray[textLength] = '\0';
}

// Destructor ///////////////////////////////////////////////////////////////
// Dallocate the array and null all of the class members
Text::~Text() {
  delete[] textArray;
  textLength = 0;
  textArray = nullptr;
  std::cout << "Text destructor: Released memory for textArray." << std::endl;
}

// Display all the characters(text) ////////////////////////////////////////
void Text::displayText() const {
  for (int i = 0; i < textLength; ++i) std::cout << textArray[i];
  std::cout << std::endl;
}

// Return the array as a const char* ///////////////////////////////////////
const char* Text::getText() const { return textArray; }

// Return the size of the text(string) /////////////////////////////////////
size_t Text::getLength() const { return textLength; }

