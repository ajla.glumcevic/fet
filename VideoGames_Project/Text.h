// Filename: Text.h
// Author: 
// Date: 02-11-2021
#pragma once
#include <algorithm>
#include <iostream>

class Text {
  private:
  // This is not const here because of initialization but methods will return
  // const char* so it is safe
  char* textArray = nullptr;
  size_t textLength = 0;

  public:
  Text(const char* arr);
  ~Text();
  void displayText() const;
  const char* getText() const;
  size_t getLength() const;
};
