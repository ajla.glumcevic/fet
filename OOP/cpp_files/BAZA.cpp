#include <iostream>
#include <string>
#include <vector>

class Worker {
  public:
  std::string name;
  double pay;
  int age;

  friend bool operator>>(std::istream& in, Worker&);
  friend std::ostream& operator<<(std::ostream& out, const Worker&);
};

bool operator==(const Worker& w1, const Worker& w2) {
  if (w1.name == w2.name && w1.pay == w2.pay && w1.age == w2.age)
    return true;
  else
    return false;
}
class Base {
  std::vector<Worker> workers;

  public:
  double avpay() {
    double sum = 0;
    for (const auto& worker : workers) {
      sum += worker.pay;
    }
    return sum / workers.size();
  }
  double avage() {
    double sum = 0;
    for (const auto& worker : workers) {
      sum += worker.age;
    }
    return sum / workers.size();
  }

  void enter() {
    Worker w;
    while (std::cin >> w) {
      for (const auto& worker : workers) {
        if (worker == w) throw std::invalid_argument("POSTOJI");
      }
      workers.push_back(w);
    }
  };

  void print() {
    for (const auto& worker : workers) std::cout << worker << std::endl;
  }

  bool eraseWorker(std::string name) {
    for (auto it = workers.begin(); it != workers.end();) {
      if (it->name == name) {
        workers.erase(it);
        return true;
      } else
        ++it;
    }
    return false;
  }
};

bool operator>>(std::istream& in, Worker& w) {
  in >> w.name >> w.pay >> w.age;
  return (bool)in;
}
std::ostream& operator<<(std::ostream& out, const Worker& w) {
  out << w.name << w.pay << w.age;
  return out;
}

int main(void)
{
  Base b;
  b.enter();
  b.print();
  std:: cout << b.avage() << " " <<b.avpay();

  b.eraseWorker("medina");
  b.print();

  return 0;

}
