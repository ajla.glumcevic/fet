#include "student.hpp"

void Student::dodajOcjenu(std::string naziv, int ocjena) {
  ocjene_[naziv].push_back(ocjena);
}

void Student::ispisiOcjene() const {
  std::map<std::string, std::vector<int>>::const_iterator it = ocjene_.begin();
  while (it != ocjene_.end()) {

    std::cout << it->first << std::endl;

    // auto it_vec_begin = (it->second).begin();
    // auto it_vec_end = (it->second).end();
    //
    // while (it_vec_begin != it_vec_end) {
    //   std::cout << *it_vec_begin << std::endl;
    //   ++it_vec_begin;
    // }
    //
    
    auto ocjene = it -> second;

 
    for(const auto& e: ocjene) std::cout << e << std::endl;
    ++it;
  }
}

