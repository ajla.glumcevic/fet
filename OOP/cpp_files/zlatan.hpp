#pragma once
#include <algorithm>
#include <iostream>

namespace my {
template <class Iterator, class UnaryOpt>
void sort(Iterator start, Iterator end, UnaryOpt predicate) {
  bool isSwap;
  do {
    isSwap = false;
    for (auto it = start; it != end; ++it)
      for (auto jt = it; jt != end; ++jt) {
        if (predicate(*jt, *it)) {
          std::swap(*it, *jt);
          isSwap = true;
        }
      }
  } while (isSwap);
}

template <class Iterator>
void sort(Iterator start, Iterator end) {
  auto predicate = [](auto a, auto b) { return a < b; };
  my::sort(start, end, predicate);
}
}  

