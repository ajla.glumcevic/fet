#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <algorithm>
#include "doctest.h"
#include <vector>
#include <list>
#include "algorithmFunkcije.hpp"
//
// TEST_CASE("accumulate"){
//   std::vector<int>v{1,2,3,4};
//   int a = 0;
//   auto test = myAlgo::accumulate(std::begin(v), std::end(v), a);
//   CHECK(test != a);
// }
//
// TEST_CASE("equal with binary"){
//   std::vector<int>v{1,2,3,4};
//   std::vector<int>v2{1,2,3,4};
//   auto k = [](int a){return a > 0;};
//   auto test = myAlgo::equal(std::begin(v), std::end(v), std::begin(v2), k);
//   CHECK(test == true);
// }
TEST_CASE("includes"){
  std::vector<int>v{1,2,3,5,4};
  std::vector<int>v2{1,2,4};
  auto test = myAlgo::includes(std::begin(v), std::end(v), std::begin(v2), std::end(v2));
  CHECK(test == true);
}
// TEST_CASE("remove_copy"){
//   std::vector<int>v{1,2,3,4};
//   std::vector<int>v2(10);
//   int a = 3;
//   auto test = myAlgo::remove_copy(std::begin(v), std::end(v), std::begin(v2), a );
//   CHECK(*(test -1) == 3);
// }
// TEST_CASE("replace_copy_if"){
//   std::vector<int>v{1,2,3,4};
//   std::vector<int>v2(10);
//   auto k = [](int a){return a > 3;};
//   int a = 3;
//   auto test = myAlgo::replace_copy_if(std::begin(v), std::end(v), std::begin(v2),k, a );
//   CHECK(*(test -1) == 4);
// }
// TEST_CASE("fill_n"){
//   std::vector<int>v{1,2,3,4};
//   int a = 3;
//   myAlgo::fill_n(std::begin(v), a, 1 );
//   CHECK(*(std::begin(v)) == 1 );
// }
// TEST_CASE("inner_product"){
//   std::vector<int>v(2,2);
//
//   std::vector<int>v1(2,2);
//   int a = 0;
//   auto test = myAlgo::inner_product(std::begin(v),std::end(v), std::begin(v1) ,a);
//   CHECK( test == 8 );
// }
// TEST_CASE("partition"){
//   std::vector<int>v{-1,-2,4,5};
// auto k = [](int a){return a%4 ==0;};
//   auto test = myAlgo::partition(std::begin(v),std::end(v), k);
//   for(auto w : v) std::cout << w << std::endl;
//   CHECK(*test < 0);
//   std::cout << *test << std::endl;
// }
// TEST_CASE("adjecent"){
//   std::vector<int>v{-1,-2,4,5};
// auto k = [](int b){return ( b>0);};
// auto test = myAlgo::adjecent(v.begin(), v.end(), k);
//   CHECK(*test == 4);
// }


//  TEST_CASE("set_difference"){
//    std::vector<int>v{1,2,3};
//    std::vector<int>v1{1};
//    std::vector<int>v2(10);
//  auto test = myAlgo::set_difference(v.begin(), v.end(), v1.begin(), v1.end(),v2.begin());
//    CHECK(*test == 0);
//    for(auto e: v2) std::cout << e << std::endl;
//  }


