#include <iostream>

namespace my{
  auto k = [](auto a, auto b){return a<b;};
template<typename InputIt,typename UnaryP=decltype(k)>
void sort(InputIt begin, InputIt end, UnaryP predicate){
  for(auto it = begin; it!=end ; ++it ){
    for(auto jt = it; jt != end ; ++jt){
      if(!predicate(*it,*jt)){
      std::swap(*it,*jt);
      }
    }
}


}

}
