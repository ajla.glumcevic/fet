#include <iostream>
#include <vector>

void inserting(std::vector<int>& vec, int n) {
  int counter = 0;
  int num;
  while (std::cin >> num) {
    ++counter;

    if (counter > n) throw std::string("iznimka");

    vec.push_back(num);
  }
}

template <typename InputIt, typename UnaryPred>
bool all_off(InputIt begin, InputIt end, UnaryPred predicate) {
  while (begin != end) {
    if (predicate(*begin)) return true;

    ++begin;
  }
  return false;
}

int main(void) {
  std::vector<int> v;
  int n = 5;

  try {
    inserting(v, n);
  } catch (std::string& e) {
    std::cout << e << std::endl;
  }

  auto k = [n](int a) { return a > n; };
  auto result = all_off(v.begin(), v.end(), k);
  if (result) std::cout << "ok" << std::endl;

  return 0;
}
