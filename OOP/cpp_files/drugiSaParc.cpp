#include <iostream>
#include <vector>

using namespace std;
typedef char Element;
typedef std::vector<Element> Red;
typedef std::vector<Red> Matrica;

Red ucitajRed(std::istream& input, int numElem) {
  Element el;
  Red red;

  for (auto i = 0; i < numElem; ++i) {
    input >> el;
    red.push_back(el);
  }
  return red;
}

Matrica ucitajMatricu(std::istream& input) {
  Element puta;
  int br_red, br_kol;

  input >> br_red >> puta >> br_kol;

  Matrica mat;
  Red red;

  for (auto i = 0; i < br_red; ++i) {
    red = ucitajRed(cin, br_kol);
    mat.push_back(red);
  }
  if ((mat.size() != br_red) || br_kol <= 0 || br_red <= 0 || puta!= 'x')
    throw string("Greska");

  // input.clear();
  input.ignore();

  return mat;
}



void pisiMatricu(vector<Red> mat){

  for(auto red : mat){
    for(auto el : red){
      std::cout << el << " ";
    }
    std::cout  << std::endl;
  }
}


int main(void) {
  try {
    Matrica mat = ucitajMatricu(cin);
    std::cout << "Unijeli ste matricu:" << std::endl;
    pisiMatricu(mat);
  } catch (std::string& s) {
    std::cout << s << std::endl;
  }
  return 0;
}
