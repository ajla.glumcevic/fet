#include <iostream>
#include <vector>



bool isPrime( int n){

  if(n==0) throw std::string("nula");
  for(auto i = 2; i<n/2;++i){
    if(n%i==0)return false;
  }

  return true;
}


int main(int argc, char *argv[])
{
  try{
  std::cout << std::boolalpha << isPrime(1)<< std::endl;
  }catch(std::string& e){
    std::cout << e << std::endl;
  }
  return 0;
}
