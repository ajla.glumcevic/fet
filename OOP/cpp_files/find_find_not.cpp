#include <algorithm>
#include <iostream>
#include <vector>

template <typename InputIt, typename UnaryPred>
void zamjeni(InputIt begin, InputIt end, UnaryPred pred) {

  InputIt b = begin, e = --end;

  while(b!=e){
    auto it1 = find_if(b,e,pred);
    auto it2 = find_if_not(it1,e,pred);
    if(!(it2 == it1) && !(it2 == e)) {--end;
    std::swap(*it1,*it2);
    }
    b = it2;

  }
}


int main(void)
{
  std::vector<int> v{2,6,3,5,8,3,7,52};
  auto k = [](int a ){return a %2 == 0;};
  zamjeni(v.begin(),v.end(),k);
  for(const auto& e: v) std::cout << e ;
  return 0;
}
