#include <iostream>
#include <map>

class A{

  public: 
  A(){
    std::cout << "def" << std::endl;
  }
  A(const A&){
    std::cout << "copy" << std::endl;
  }
  A& operator=(const A&){
    std::cout << "operator" << std::endl;
  }
  ~A(){
    std::cout << "desk" << std::endl;
  }
};


int main(void)
{
  std::map<int, A> mapa;

  return 0;
}
