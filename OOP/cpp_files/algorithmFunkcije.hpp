#pragma once
#include <iostream>

namespace myAlgo {
template <typename InputIt, typename Init>
auto accumulate(InputIt begin, InputIt end, Init init) -> Init {
  while (begin != end) {
    init += *begin;
    ++begin;
  }

  return init;
}

template <typename InputIt, typename UnaryPred>
bool equal(InputIt begin, InputIt end, InputIt begin2, UnaryPred pred) {
  while (begin != end) {
    if (*begin == *begin2) {
      if (pred(*begin)) {
        ++begin;
        ++begin2;
      } else
        return false;
    }
  }

  return true;
}

template<typename InputIt>
bool includes(InputIt begin, InputIt end, InputIt begin2, InputIt end2){
  bool check;
  while(begin2!=end2){
    InputIt temp = begin;

    while(temp != end){

      check = false;
      if(*begin2 == *temp){
        check = true;
        break;
      }
      ++temp;
    }
    ++begin2;


  }
  return check;
}

template <typename InputIt, typename OutputIt, typename Value>
OutputIt remove_copy(InputIt begin, InputIt end, OutputIt result, Value val) {
  while (begin != end) {
    if (*begin == val) {
      *result = *begin;
      ++result;
    }
    ++begin;
  }
  return result;
}

template <typename InputIt, typename OutputIt, typename Value,
          typename UnaryPred>
OutputIt replace_copy_if(InputIt begin, InputIt end, OutputIt result,
                         UnaryPred pred, Value value) {
  while (begin != end) {
    *result = *begin;

    if (pred(*begin)) {
      *begin = value;
    }
    ++result;
    ++begin;
  }

  return result;
}

template <typename ForwardIt, typename Value, typename Size>
void fill_n(ForwardIt begin, Size n, Value value) {
  for (auto i = 0; i < n; ++i, ++begin) {
    *begin = value;
  }
}

template <typename InputIt, typename Init>
auto inner_product(InputIt begin, InputIt end, InputIt begin1, Init init)
    -> Init {
  Init sum{};

  while (begin != end) {
    Init product = *begin * *begin1;
    sum += product;

    ++begin;
    ++begin1;
  }

  return sum;
}

template <typename BidirectIt, typename UnaryPred>
BidirectIt partition(BidirectIt begin, BidirectIt end, UnaryPred pred) {
  for (auto it = begin; it != end; ++it) {
    for (auto jt = it; jt != end; ++jt) {
      if (pred(*jt)) std::swap(*jt, *it);
    }
  }

  while (pred(*begin)) {
    ++begin;
  }
  return begin;
}
//
// template<typename ForwardIt, typename UnaryPred, typename OutputIt>
// OutputIt adjecent(ForwardIt begin, ForwardIt end, UnaryPred pred){
//   ForwardIt a = begin++;
//   ForwardIt b = begin;
//
//   while(b!=end){
//     if(pred(*a,*b)) return a;
//     ++a;
//     ++b;
//
//   }
//
//   return end;
// }

template <typename InputIt, typename OutputIt>
OutputIt set_difference(InputIt begin, InputIt end, InputIt begin1,
                        InputIt end1, OutputIt result) {
  while (begin != end) {
    InputIt temp = begin1;
    while (temp != end1) {
      if (*begin != *temp) {
        *result = *begin;
        ++result;
      }
      ++temp;
    }
    ++begin;
  }
  return result;
}


}  // namespace myAlgo
