#include <iostream>

template<typename T>
int foo(T a)
{
  static int x;
  std::cout << x++ << std::endl;
}

int main(int argc, char *argv[])
{
  const int x = 10;
  foo(3);
  foo(x);

  int b = 3;
  foo(b);

  foo(&b);
  return 0;
}
