#include <iostream>
using namespace std;

template<class T,class U>
void foo(T x, U y = 5)
{
  std::cout << x << std::endl;
  std::cout << y << std::endl;
}

int main(void)
{

  return 0;
}
