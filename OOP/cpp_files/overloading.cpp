#include <iostream>
#include <string>


template<typename U,typename T>
void foo(U a , T b = 1){
  std::cout << a << b << std::endl;
}

void foo(int a, std::string b){
  std::cout << 1 << std::endl;
}

template<typename T>
void foo(T a, T b = 'a' ){

  std::cout << a << b<< std::endl;
}


void bar(int a, char b){
  std::cout << 1 << std::endl;
}

void bar(char a , char b){
  std::cout << 2 << std::endl;
}

template<typename T>
void fooz(T a){
  a = 4;
  std::cout << 5 << std::endl;
}


int main(int argc, char *argv[])
{
 
  
// foo<int,double>(5);

  
  const int a = 5;
  fooz(a);

  return 0;
}
