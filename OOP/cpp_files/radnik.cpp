#include <iostream>
#include <string>
#include <vector>

class Radnik {
  std::string ime{};
  int godine{};
  double plata{};

  public:
  std::istream& unesiRadnik(std::istream& ulaz) {
    ulaz >> ime >> godine >> plata;
  }

  Radnik() { unesiRadnik(std::cin); }

  std::ostream& ispisRadnik(std::ostream& izlaz) {
    std::cout << "Ispis radnika: " << std::endl;
    izlaz << ime << godine << plata;
  }

  void povecajPlatu(const double& iznos) {
    std::cout << "Uvecavanje plate: " << std::endl;
    plata = plata + iznos;
  }

  void smanjiPlatu(const double& iznos) {
    std::cout << "Smanjivanje plate: " << std::endl;
    plata = iznos > plata ? 0 : plata - iznos;
  }
  int godinaDoPenzije(const int& iznos) {
    std::cout << "Godina do penzije: " << std::endl;
    return godine - iznos;
  }

  const double& getPlata() const { return plata; }
};

bool uporediPoPlati(const Radnik& a, const Radnik& b) {
  return a.getPlata() > b.getPlata();
}

int main(void) {
  Radnik a;
  std::cout << "_________" << std::endl;
  Radnik b;

  a.povecajPlatu(14);
  b.smanjiPlatu(3);

  a.ispisRadnik(std::cout);
  b.ispisRadnik(std::cout);

  return 0;
}
