#include <iostream>
#include <vector>

template<typename InputIt, typename UnaryPred>
void reverse_forr(InputIt begin, InputIt end, UnaryPred predicate){
  if(end - begin < 0) throw std::invalid_argument("iznimka");

  
  while(--end!=begin){
    predicate(*end);
  }

}


struct Koordinata{
  double Lat;
  double Lang;
};

int main(void)
{
 
 std::vector<int> v{1,2,3,4}; 
  reverse_forr(v.begin(), v.end(), [](int& a){ a=1;} );

  for(const auto& e: v) std::cout << e << std::endl;
  return 0;
}

