#include <iostream>
#include <string>


class B {
  private:
  double z = 0.;

  public:
  B() { std::cout << "B default" << std::endl; }
  B(const B&) { std::cout << "B copy" << std::endl; }
  B(int b) : z(b) { std::cout << "B int" << z << std::endl; }
  B& operator=(const B&){ std::cout << "B operator=" << std::endl;}
  ~B() { std::cout << "B destruktor  " << z<< std::endl; }
};


class A {
  private:
  int x = 1;
  // B y{x};

 
  public:
  A() {
    B b{x};
    std::cout << "A default" << std::endl;
  }
  A(const A&) { std::cout << "A copy" << std::endl; }
  A& operator=(const A&) { std::cout << "A operator=" << std::endl; }
   explicit A(int _x)  { std::cout << "A konstruktor uzima B" << x << std::endl; }
  ~A() { std::cout << "A destruktor" << std::endl; }
};

void foo(A a) { std::cout << 1 << std::endl; }
template <typename T>
void foo(T t) {
  std::cout << "t" << std::endl;
}


class D{
  public:
  D(){
    std::cout << "D" << std::endl;
  }
  D(const D&){
    std::cout << 1 << std::endl;
  }
  ~D(){
    std::cout << "D desk" << std::endl;
  }
};

class E{
  public:
  E(){
    std::cout << "E" << std::endl;
  }
  E(const E&){
    std::cout << 2 << std::endl;
  }
  ~E(){
    std::cout << "E desk" << std::endl;
  }
};

class F{
  private:
    D d;
    E e;
   char i;
  public:
    F(char l): i{l}{
      std::cout << "F" << i << std::endl;
    }
   ~F(){
     std::cout << "F desk" << std::endl;
   }
};

void foo(F f){
  std::cout << 100 << std::endl;
}
int main(void) { 
  F a = 5;
foo(a);

  // A x;
  // x = A{x};
  // B s = 10;
  // foo(B{s});

  // A a;
  // B b = 5.6;
  // B bbb = 5.6;
  // // std::cout << "kraj" << std::endl;
  // // std::cout << "kraj" << std::endl;
  // A c = a;
  // A d{'A'};
  // foo(d);
  // foo(bbb);

  // A a;
  // foo(A{});
  // B {};

  return 0; }

