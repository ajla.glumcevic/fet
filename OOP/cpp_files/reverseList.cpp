#include <iostream>
#include <list>
template <typename InputIt>
size_t velicina_(InputIt begin, InputIt end) {
  size_t counter = 0;
  while (begin != end) {
    ++begin;
    ++counter;
  }
  return counter;
}

template <typename InputIt>
void reverse_(InputIt begin, InputIt end) {
  auto velicina = velicina_(begin, end);
  auto sredina = velicina % 2 ? velicina / 2 + 1 : velicina / 2;

  --end;
  for (auto i = 0; i != sredina; ++i,++begin, --end) {
    std::swap(*begin, *end);
  }
}


int main(void)
{
  std::list<int> l(20'000,3);
  reverse_(l.begin(), l.end());
  for(auto e : l) std::cout << e << std::endl;
  return 0;
}
