#include <iostream>
#include <list>

namespace my {

template <typename InputIt, typename UnaryPred>
void max(InputIt begin, InputIt end, UnaryPred predicate) {
  bool check = false;

  while (begin != end) {
    if (predicate(*begin)) {
      check = true;
      *begin = 0;
    }
    ++begin;
  }
  if (!check) throw std::invalid_argument("iznimka");
}
}  // namespace my

int main(void) {
  std::list<int> l;
  try {
    std::cout << "Enter nums: " << std::endl;

    int integer;
    while (std::cin >> integer) {
      l.push_back(integer);
    }
    std::cin.clear();
    std::cin.ignore();

    int test;
    std::cout << "Enter value to test:" << std::endl;
    std::cin >> test;

    auto k = [test](int a) { return a < test; };

    my::max(l.begin(), l.end(), k);

    for(const auto& el : l) std::cout << el << std::endl;
  } catch (std::invalid_argument& e) {
    std::cout << e.what() << std::endl;
  }

  return 0;
}
