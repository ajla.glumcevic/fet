#include <iostream>


struct A {
  private:
  int x = 0;
  int y = 5;

  public:
  A() { std::cout << "A def" << std::endl; }
  A(double a) { std::cout << "A double" << a<< std::endl; }
  A(const A&) { std::cout << "A copy" << std::endl; }
  ~A() { std::cout << "A destruct"  << std::endl; }
  A& operator=(const A&) {
    x = 3;
    y = 6;
    return *this;
  }
};

struct B {
  private:
  int k;
  A x{5.5};

  public:
  B(int a) : x{A{3}} { std::cout << "B int" << std::endl; }
  B() { std::cout << "B default" << std::endl; }
  B(const B&) = default;
  ~B() { std::cout << "B destruct" << std::endl; }
};

struct C {
  private:
  B trick;

  public:
  C() { std::cout << "C default" << std::endl; }
  ~C() { std::cout << "C destruct" << std::endl; }
  C(const C&) { std::cout << "C copy" << std::endl; }
};

void fun(const C& x) { std::cout << "Funkcija fun" << std::endl; }

int main(void) {
  B bar(5.5);
  fun(C{});
  return 0;
}
