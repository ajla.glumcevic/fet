#include <iostream>
#include <vector>

void inserting(std::vector<int>& vec) {
  int num;
  while (std::cin >> num) {
    vec.push_back(num);
  }
  std::cin.ignore();

  if (vec.size() == 0) throw std::string("Iznimka");
}

template <typename InputIt, typename UnaryPred>
bool none_off(InputIt begin, InputIt end, UnaryPred predicate) {
  while (begin != end) {
    if (predicate(*begin)) return false;

    ++begin;
  }
  return true;
}

int main(void) {
  std::vector<int> v;

  try {
    inserting(v);
  } catch (std::string& e) {
    std::cout << e << std::endl;
  }

  auto size = v.size();
  auto k = [size](int a) { return a > size; };
  auto result = none_off(v.begin(), v.end(), k);

  if (result) std::cout << "nijedan element ne zadovoljava lambdu" << std::endl;
  return 0;
}
