#include <iostream>
#include <vector>
#include <fstream>
std::vector<std::string> over25char(const std::vector<std::string>& dict){
  std::vector<std::string> vec;
  // std::vector<std::string>::const_iterator it = dict.begin();
  // while(it!=dict.end() )
  // {
  //   if((*it).size() > 25)
  //     std::cout << *it << std::endl;
  // }
  for(const auto& word: dict){
    if(word.size()>25)
      vec.push_back(word);
  }
  return vec;
}


int main(void)
{
  
std::ifstream dictionaryFile("words.txt");
std::vector<std::string> dictionary;
std::string word;
while (dictionaryFile >> word) 
dictionary.push_back(std::move(word));

std::vector<std::string> vec = over25char(dictionary);
for(const auto& e: vec)
  std::cout << e << std::endl;

return 0;
}

