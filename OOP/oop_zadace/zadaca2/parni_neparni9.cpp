#include <iostream>
#include <list>
#include <functional>



//
// lista parni_neparni(iter it1 , iter it2){
//   lista parni, neparni;
//     for(auto it = it1 ; it!= it2 ; ++it){
//       if((*it)%2 ==0)
//         parni.push_back(*it);
//        else 
//          neparni.push_back(*it);
//     }
//        for(const auto& e: neparni)
//          parni.push_back(e);
//
//        return parni;
//       
// }
//
template<typename InputIt, typename UnaryOpt>
InputIt partitionn(InputIt begin, InputIt end, UnaryOpt predicate){

  InputIt i = begin;
  InputIt j = i;

  for(auto i = begin; i!=end; ++i){
    for(auto j = i; j!= end;++j){
      if(predicate(*j)) std::swap(*j,*i);
    }
  }
while(predicate(*begin)){
  ++begin;
}
return begin;
}
// lista sortiraj(iter it1 , iter it2 , f predicate){
//   lista l, temp;
//   for(auto it = it1 ; it != it2 ; ++it)
//     if(predicate(*it)) l.push_back(*it);
//   else temp.push_back(*it);
//
//   for(const auto& e: temp)
//     l.push_back(e);
//
//   return l;
// }
//
int main(void)
{

  std::list<int> l;
  typedef std::list<int>::iterator iter;
  int broj;
  while(std::cin >> broj)
    l.push_back(broj);

  std::cin.clear();
  std::cin.ignore();

  auto k1 = [](int num){return num>0;};
  auto k2 = [](int num){return !(num%10);};
  auto k3 = [](int num){return std::abs(num)>99;};
  for(const auto& e: l)
    std::cout << e << std::endl;
  iter it1 = l.begin();
  iter it2 = l.end();
  iter nova = partitionn(it1, it2,k1);
  std::cout <<"Sortirana lista:" << std::endl;
  for(const auto& e: l)
    std::cout << e << std::endl;


  return 0;
}
