#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int hamming(const std::string& a, const std::string& b) {
  int counter = 0;
  for (auto i = 0; i < a.size(); ++i) {
    if (a.at(i) != b.at(i)) counter++;
  }
  return counter;
}

int main() {
  std::ifstream dictionaryFile("words.txt");
  std::vector<std::string> dictionary;
  std::string word;
  while (dictionaryFile >> word) dictionary.push_back(std::move(word));

  std::string unos;
  std::cout << "Unos: " << std::endl;
  std::cin >> unos;

  std::vector<std::string> result;

  for (auto i = dictionary.begin(); i != dictionary.end(); ++i){
    if(unos.size() == i ->size()){
    if (hamming(unos, *i) >= 0 && hamming(unos, *i) < 2) {
      result.push_back(*i);
    }
    }
  }

  std::sort(result.begin(), result.end(),
            [unos](std::string a, std::string b) {
              return hamming(unos, a) < hamming(unos, b);
            });


  for(const auto& e: result) std::cout << e << std::endl;

  return 0;
}
