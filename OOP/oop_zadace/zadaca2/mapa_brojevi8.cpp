#include <iostream>
#include <map>

using std::string;
int main(void) {
  std::map<int, string> mapa = {
      {1, "jedan"}, {2, "dva"},   {3, "tri"},  {4, "cetri"}, {5, "pet"},
      {6, "sest"},  {7, "sedam"}, {8, "osam"}, {9, "devet"}, {10, "deset"},
  };
  std::map<int, string>::iterator it = mapa.begin(), it1 = mapa.end();

  try{
  std::cout << "Unesite broj: ";
  int num;
  std::cin >> num;
  if(num > it1 -> first || num < it -> first ) 
    throw string("Broj se ne nalazi u mapi.");
  for (auto i = it; i != it1; ++i){
    if (i->first == num) {
      std::cout << i->second;
      break;
    }
  }
   }catch(string s){
     std::cout << s << std::endl;
   }

  return 0;
}
