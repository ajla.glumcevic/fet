#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef vector<int> kontejner;
kontejner sortiraj(kontejner kont) {
  std::sort(std::begin(kont), std::end(kont));
  return kont;
}
double unos_elem_prosjecna_vrijed(kontejner &kont) {
  int suma = 0;
  std::cout << "Unesite elemente: " << std::endl;
  for (int element; std::cin >> element;) {
    kont.push_back(element);
    suma += element;
  }

  if (kont.size() == 0) {
    std::cin.clear();
    std::cin.ignore();
    throw string("Pogresan unos! ");
  }

  return (double)suma / kont.size();
}
double median(kontejner kont) {
  typedef decltype(kont.size()) velicina;
  velicina br_elem = kont.size();
  if (br_elem == 0) throw string("Prazan niz!");
  kont = sortiraj(kont);
  velicina sredina = br_elem / 2;
  double median = 0;
  if (br_elem % 2 == 0) {
    median = (kont.at(sredina) + kont.at(sredina - 1)) / 2;
  } else {
    median = kont.at(sredina);
  }
  return median;
}

int main(void) {
  kontejner kont;
  while (true) {
    try {
      auto p = unos_elem_prosjecna_vrijed(kont);

      auto m = median(kont);
      std::cout << "Prosjecna vrijednsot: " << p << std::endl;
      std::cout << "Median: " << m << std::endl;
      break;
    } catch (string a) {
      std::cout << a << std::endl;
    }
  }
  return 0;
}
