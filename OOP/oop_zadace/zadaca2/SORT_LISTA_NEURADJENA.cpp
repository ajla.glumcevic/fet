#include <algorithm>
#include <functional>
#include <iostream>
#include <list>

typedef std::list<int>::const_iterator iter;
typedef std::list<int> lista;
typedef std::function<bool(int)> f;

lista parni_neparni(iter it1, iter it2) {
  lista parni, neparni;
  for (auto it = it1; it != it2; ++it) {
    if ((*it) % 2 == 0)
      parni.push_back(*it);
    else
      neparni.push_back(*it);
  }
  for (const auto& e : neparni) parni.push_back(e);

  return parni;
}

lista sorting(iter it1, iter it2, f funk) {
  auto k = funk;
  std::sort(it1, it2, k);
  for (auto i = it1; i != it2; ++i) std::cout << *i << std::endl;
}
int main(void) {
  lista l;
  int broj;
  while (std::cin >> broj) l.push_back(broj);

  std::cin.clear();
  std::cin.ignore();

  for (const auto& e : l) std::cout << e << std::endl;
  iter it1 = l.begin();
  iter it2 = l.end();
  lista nova = parni_neparni(it1, it2);
  std::cout << "Sortirana lista:" << std::endl;
  for (const auto& e : nova) std::cout << e << std::endl;

  auto k1 = [](int num) { return num > 0; };
  auto k2 = [](int num) { return !(num % 10); };
  auto k3 = [](int num) { return std::abs(num) > 99; };
  std::cout << "Sortirano prema  prvom kriteriju: " << std::endl;
  nova = sorting(l.begin(), l.end(), k1);
  std::cout << "Sortirano prema  drugom kriteriju: " << std::endl;
  nova = sorting(l.begin(), l.end(), k2);
  std::cout << "Sortirano prema  trecem kriteriju: " << std::endl;
  nova = sorting(l.begin(), l.end(), k3);

  return 0;
}
