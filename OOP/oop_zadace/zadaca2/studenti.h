#pragma once
#include <iostream>
#include <list>
#include <string>
#include <vector>
struct Predmet{
  std::string naziv;
  std::string odsjek;
  int ocjena;
};


struct Student{
std::string brojIndeksa;
std::string ime;
std::string prezime;
std::string grad;
std::list<Predmet> lPred;
};

typedef std::vector<Student> Studenti;
typedef std::vector<Student>::iterator itStudent;
int meni();
void dodajPredmet(Studenti&);
void dodajStudenta(Studenti&);
void editPolje(Studenti&);
void ispisStudenta(const Studenti&);
void meniEdit();
void brisanjeOcjena(std::list<Predmet>&);
void promjenaOcjena(std::list<Predmet>&);
