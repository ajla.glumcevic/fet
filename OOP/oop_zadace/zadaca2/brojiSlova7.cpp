#include <iostream>
#include <string>
#include <vector>
#include <map>

bool provjeri(const std::string& procitani, const char& slovo){
  for(const auto& e: procitani)
    if(e == slovo) return true;

  return false;
}



void izbroji(const std::string& recenica){

  std::string procitani;

  for(auto i = std::begin(recenica); i != std::end(recenica); ++i){
    
    char slovo = *i;
    if(provjeri(procitani, slovo)) continue;

    int n = 0;
    for(auto j = i; j != std::end(recenica); ++j)
    if(slovo == *j) ++n;

    std::cout << slovo << n << std::endl;

    procitani.push_back(slovo);


  }
  std::cout << procitani << std::endl;

}

void izbroji(std::string::const_iterator begin, std::string::const_iterator end){

  std::string temp;
  
  while(begin!=end){
    char slovo=*begin;
    std::string::const_iterator it = begin;
    if(!provjeri(temp,slovo)){
      int n = 0;
      while(it!=end){
        if(*it == slovo) ++n;
        ++it;
      }
      std::cout << slovo << n << " ";
      temp.push_back(slovo);
    }

    ++begin;
 }
}

int main(void) {
  std::cout << "Unesite recenicu: ";
  std::string recenica;
  std::getline(std::cin, recenica);

 std::cout << recenica << std::endl;
  // izbroji(recenica);
//  izbroji(recenica.cbegin(), recenica.cend());
//

 std::map<char, int> mapa;
 for(const auto& e : recenica){
   mapa[e]++;
 }
 for(auto it = mapa.begin() ; it!=mapa.end(); ++it){
   std::cout << it->first << " " <<it->second << std::endl;
 }
  

  return 0;
}
