#include <iostream>
#include <string>

int hammingFunc(const std::string& st1, const std::string& st2) {
  int n = 0;
  for (auto i = 0; i < st1.size(); ++i)
    if (st1.at(i) != st2.at(i)) n++;

  return n;
}

typedef std::string::const_iterator iter;

iter hamming(iter prvi,iter kraj, iter drugi) {
  int n = 0;
  iter it = prvi;
  while (prvi !=kraj) {
    if (*prvi != *drugi) {
      it = prvi;
      ++n;
    }
      ++drugi;
      ++prvi;
  }
    return it;
}
    int main(void) {
      std::string st1, st2;

      while (true) {
        try {
          std::cout << "First word: ";
          std::cin >> st1;
          std::cout << "Second word: ";
          std::cin >> st2;
          if (st1.size() != st2.size())
            throw std::string("Size must be the same!");
          // int hamming = hammingFunc(st1, st2);
          auto i = hamming(st1.cbegin(), st1.cend(),st2.cbegin());
          std::cout << *i << std::endl;
          break;
        } catch (std::string s) {
          std::cout << s << std::endl;
        }
      }
      return 0;
    }

