#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <string>
#include <vector>

namespace my{
template<typename InputIt, typename UnaryOpt>
InputIt partition(InputIt begin, InputIt end, UnaryOpt predicate){

  InputIt i = begin;
  InputIt j = i;

  for(auto i = begin; i!=end; ++i){
    for(auto j = i; j!= end;++j){
      if(!predicate(*j)) std::swap(*j,*i);
    }
  }
while(!predicate(*begin)){
  ++begin;
}
return begin;
}

}


int main(void)
{
  std::vector<int> v{4,2,7,3,5,4,6};
  auto k =my:: partition(v.begin(),v.end(), [](int  A){return A%2==1;});
  std::cout <<*k  << std::endl;
  for(const auto& e: v) std::cout << e ;
  return 0;
}
