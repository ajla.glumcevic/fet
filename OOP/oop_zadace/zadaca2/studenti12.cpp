#include <iostream>
#include <string>
#include "studenti.h"

int main(void) {
  // Student s;
  Studenti vec;
while(true){
  int opcija = meni();
  if (opcija == 1)
    dodajStudenta(vec);
  else if (opcija == 2)
    dodajPredmet(vec);
  else if (opcija == 3){
    try{
    editPolje(vec);
    }catch(...){
      std::cout << "Editovanje neuspjesno" << std::endl;
    }
  }
  else if (opcija == 4)
    ispisStudenta(vec);

  else
    std::cout << "Pogresan unos." << std::endl;
  std::cout << "\n\n\n" << std::endl;
}
  return 0;
}
