#include <functional>
#include <iostream>
#include <list>
typedef std::list<int> lista;
typedef std::list<int>::iterator iter;
typedef std::list<int>::const_iterator c_iter;
typedef std::function<bool(int)> func;

c_iter sortiraj(c_iter it1, c_iter it2 , func f){
lista l, temp;
while(it1!= it2){
  if(f(*it1)) l.push_back(*it1);
  else temp.push_back(*it1);
  ++it1;
}
for(auto e: temp) l.push_back(e);

return temp.cbegin();

}

int main(void) {
  lista l;
  int broj;
  while (std::cin >> broj) l.push_back(broj);
  std::cin.clear();
  std::cin.ignore();

  std::cout << "Kraj unosa" << std::endl;
  auto k = [](int a) {
    if (a > 100 || a < -100) throw 5;
    return a > 0;
  };

  try{
    auto it = sortiraj(l.cbegin() , l.cend(), k);
  std::cout << *it << std::endl;
  for(auto& e: l) std::cout << e << std::endl;
  }catch(int w){
    std::cout << "Iznimka\n" << std::endl;
    for(auto& e: l ) std::cout << e << std::endl;
  }
  return 0;
}
