#include "studenti.h"
#include <iostream>
#include <list>
#include <string>
#include <vector>
using std::string;
using std::vector;
void meniEdit() {
  std::cout
      << "1. Mijenjanje broja indeksa studenta\n2. Mijenjanje imena "
         "studenta\n3. Mijenjanje prezimena studenta\n4. Mijenjanje grada "
         "studenta\n5. Brisanje svih ocjena\n6. Dodavanje novih ocjena\n"
      << std::endl;
}


int meni() {
  int opcija;
  std::cout << "Izaberite opciju:" << std::endl;
  std::cout << "1. Dodaj studenta" << std::endl;
  std::cout << " 2. Dodaj predmet" << std::endl;
  std::cout << "3.Edituj polje studenta" << std::endl;
  std::cout << "4.Ispisi studente" << std::endl;

  std::cout << "Opcija: ";
  std::cin >> opcija;
  std::cin.clear();
  std::cin.ignore();
  return opcija;
}
/**********************************************************************/
void dodajStudenta(Studenti& vec) {
  Student s;
  ///////////////////////////////////////indeks
  try {
    std::cout << "Indeks:";
    string indeks;
    std::cin >> indeks;
    for (auto i = vec.begin(); i != vec.end(); ++i)
      if (i->brojIndeksa == indeks)
        throw string("Student sa ovim brojem indeksa vec postoji.");

    s.brojIndeksa = indeks;

  } catch (string s) {
    std::cout << s << std::endl;
  };
  ///////////////////////////////////ime
  std::cout << "Ime:";
  string ime;
  std::cin >> ime;
  s.ime = ime;
  ////////////////////////////prezime
  std::cout << "Prezime:";
  string prezime;
  std::cin >> prezime;
  s.prezime = prezime;
  //////////////////////////////grad
  std::cout << "Grad:";
  string grad;
  std::cin >> grad;
  s.grad = grad;

  vec.push_back(s);
}
/***********************************************************************/
void dodajPredmet(Studenti& vec) {
  int brojac = 0;
  for (auto& student : vec) {
    std::cout  << std::endl;
    ++brojac;
    std::cout << "Unos predmeta za studenta broj:" << brojac << std::endl;
    for(int i = 1 ; i < 3 ; ++i){  
    std::cout << "Predmet broj: " << i << std::endl;
      Predmet p;
      string naziv, odsjek;
      int ocjena;
      std::cout << "Unesi naziv predmeta:";
      std::cin >> naziv;
      std::cout << "Unesi odsjek:";
      std::cin >> odsjek;
      std::cout << "Unesi ocjenu: ";
      std::cin >> ocjena;

      p.naziv = naziv;
      p.odsjek = odsjek;
      p.ocjena = ocjena;

      student.lPred.push_back(p);

    }
    std::cin.clear();
    std::cin.ignore();
  }
}
/***********************************************************************************/
void ispisStudenta(const Studenti& vec) {
  for (const auto& student : vec) {
    std::cout << "Broj indeksa:" << student.brojIndeksa << std::endl;
    std::cout << "Ime:" << student.ime << std::endl;
    std::cout << "Prezime:" << student.prezime << std::endl;
    std::cout << "Grad:" << student.grad << std::endl;

    for (const auto& pred : student.lPred) {
      std::cout << "Naziv:" << pred.naziv << std::endl;
      std::cout << "Odsjek:" << pred.odsjek << std::endl;
      std::cout << "Ocjena:" << pred.ocjena << std::endl;
    }
  }
}
/******************************************************************************/
void editPolje(Studenti& vec) {
  Student s;
  try{
  std::cout << "Unesi broj indeksa studenta:";
  string indeks;
  std::cin >> indeks;
  auto it = vec.begin();
  for(;it != vec.end(); ++it){
  if(it == vec.end()) throw string("Ne postoji student sa ovim brojem indeksa.");
    if (it->brojIndeksa == indeks) break;
  }
  meniEdit();

  int opcija;
  std::cin >> opcija;
  if (opcija == 1) {
    string indeks;
    std::cout << "Novi broj indeksa: ";
    std::cin >> indeks;
    it ->brojIndeksa = indeks;
  }

  else if (opcija == 2) {
    string ime;
    std::cout << "Novo ime: ";
    std::cin >> ime;
    it ->ime = ime;
  } else if (opcija == 3) {
    string prezime;
    std::cout << "Novo prezime: ";
    std::cin >> prezime;
    it ->prezime = prezime;
  } else if (opcija == 4) {
    string grad;
    std::cout << "Novi grad: ";
    std::cin >> grad;
    it ->grad = grad;
  } else if (opcija == 5) {
    brisanjeOcjena(it -> lPred);
  } else if (opcija == 6) {
    promjenaOcjena(it -> lPred);
  } else
    std::cout << "Pogresan unos." << std::endl;
  }catch(string s){
    std::cout << s << std::endl;
    throw;
  }
}



/****************************************************************************/
void brisanjeOcjena(std::list<Predmet>& predmeti){

  for(auto& predmet : predmeti){
    predmet.ocjena = 0;
  }
}
/********************************************************************/
void promjenaOcjena(std::list<Predmet>& predmeti){
if(predmeti.size()==0){
 for(int i = 1 ; i < 3 ; ++i){  
    std::cout << "Predmet broj: " << i << std::endl;
      Predmet p;
      string naziv, odsjek;
      int ocjena;
      std::cout << "Unesi naziv predmeta:";
      std::cin >> naziv;
      std::cout << "Unesi odsjek:";
      std::cin >> odsjek;
      std::cout << "Unesi ocjenu: ";
      std::cin >> ocjena;

      p.naziv = naziv;
      p.odsjek = odsjek;
      p.ocjena = ocjena;
  
      predmeti.push_back(p);

    }
    std::cin.clear();
    std::cin.ignore();
  }

else{

  for(auto& predmet: predmeti){
    std::cout << "Unesite novu ocjenu: ";
    int ocjena;
    std::cin >> ocjena;
    predmet.ocjena = ocjena;
  }
}
}

