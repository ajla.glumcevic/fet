#pragma once
#include <iostream>
#include <vector>

template <typename T>
class Matrica {
  private:
  size_t br_redova = 0, br_kolona = 0;
  size_t velicina = br_redova * br_kolona;

  public:
  T* ptr = new T[velicina];

  size_t get_br_redova() const { return br_redova; }
  size_t get_br_kolona() const { return br_kolona; }
  size_t get_velicina() const { return velicina; }

  template <typename U>
  Matrica(const Matrica<U>& druga)
      : br_redova{druga.get_br_redova()},
        br_kolona{druga.get_br_kolona()},
        ptr{new T[velicina]} {
    std::copy(druga.ptr, druga.ptr + velicina, ptr);
  }

  Matrica() : ptr{nullptr} {}
  Matrica(const size_t& m, const size_t& n) : br_redova{m}, br_kolona{n} {}
  Matrica(const std::initializer_list<T>& lista)
      : br_redova{*(std::begin(lista))}, br_kolona{*(std::begin(lista) + 1)} {
    if (lista.size() > velicina + 2)
      throw std::domain_error("veci  broj elemenata");
    std::copy(std::begin(lista) + 2, std::end(lista), ptr);
  }

  Matrica(const Matrica& druga)
      : br_redova{druga.br_redova},
        br_kolona{druga.br_kolona},
        ptr{new T[velicina]} {
    std::copy(druga.ptr, druga.ptr + velicina, ptr);
  }

  Matrica(Matrica&& druga) {
    ptr = druga.ptr;
    br_redova = druga.br_redova;
    br_kolona = druga.br_kolona;
    druga.ptr = nullptr;
    br_redova = 0;
    br_kolona = 0;
  }

  Matrica& operator=(Matrica&& druga) {
    delete[] ptr;
    br_redova = druga.br_redova;
    br_kolona = druga.br_kolona;
    druga.ptr = nullptr;
    br_redova = 0;
    br_kolona = 0;
    return *this;
  }

  Matrica& operator=(const Matrica& druga) {
    if (this != &druga) {
      delete[] ptr;
      std::copy(druga.ptr, druga.ptr + velicina, ptr);
      br_redova = druga.br_redova;
      br_kolona = druga.br_kolona;
      velicina = druga.velicina;
    }
  }

  T& operator()(const size_t& red, const size_t& kolona) const {
    return ptr[(red) * br_kolona + kolona];
  }
  T& operator[](const size_t& pos) const { return ptr[pos]; }
  std::ostream& size(std::ostream& izlaz) {
    izlaz << br_redova << " " << br_kolona << " " << velicina << "\n";
    return izlaz;
  }

  // std::ostream& ispisi(std::ostream& out) {
  //   for (auto i = 0; i < velicina; ++i) {
  //     out << ptr[i];
  //   }
  //   return out;
  // }

  Matrica operator+(const Matrica& druga) const {
    Matrica nova = *this;
    for (size_t i = 0; i < velicina; ++i) {
      nova.ptr[i] = nova.ptr[i] + druga.ptr[i];
    }
    return nova;
  }

  Matrica operator-(const Matrica& druga) const {
    Matrica nova = *this;
    for (size_t i = 0; i < velicina; ++i) {
      nova.ptr[i] = ptr[i] - druga.ptr[i];
    }
    return nova;
  }
  Matrica operator*(const T& mnozitelj) {
    Matrica nova = *this;
    for (size_t i = 0; i < velicina; ++i) {
      nova.ptr[i] = nova.ptr[i] * mnozitelj;
    }
    return nova;
  }
  Matrica operator/(const T& djeljitelj) {
    if (djeljitelj == 0) throw std::invalid_argument("dijeljenje sa nulom");
    Matrica nova = *this;
    for (size_t i = 0; i < velicina; ++i) {
      nova[i] = nova[i] / djeljitelj;
    }
    return nova;
  }
  Matrica& operator*=(const T& mnozitelj) {
    for (size_t i = 0; i < velicina; ++i) {
      ptr[i] = ptr[i] * mnozitelj;
    }
    return *this;
  }
  Matrica& operator*=(const Matrica& druga) {
    if (velicina != druga.velicina)
      throw std::domain_error("nekompatibilne velicine");
    for (size_t i = 0; i < velicina; ++i) {
      ptr[i] = ptr[i] * druga.ptr[i];
    }
    return *this;
  }

  Matrica& operator-=(const Matrica& druga) {
    if (velicina != druga.velicina)
      throw std::domain_error("nekompatibilne velicine");
    for (size_t i = 0; i < velicina; ++i) {
      ptr[i] = ptr[i] - druga.ptr[i];
    }
    return *this;
  }
  Matrica& operator+=(const Matrica& druga) {
    if (velicina != druga.velicina)
      throw std::domain_error("nekompatibilne velicine");
    for (size_t i = 0; i < velicina; ++i) {
      ptr[i] = ptr[i] + druga.ptr[i];
    }
    return *this;
  }

  template <typename A,typename U>
  Matrica<U> operator+(const std::vector<A>& v) {
    if (v.size() != this->get_velicina()) throw std::invalid_argument("..");
   Matrica<U> temp = *this;
    for (auto i = 0; i != v.size(); ++i) {
      temp.ptr[i] =ptr[i]+ v.at(i);
    }
    return temp;
  }
};

template <typename T>
std::ostream& operator<<(std::ostream& out, const Matrica<T>& mat) {
  for (size_t i = 0; i < mat.get_velicina(); ++i) {
    if (i % mat.get_br_kolona() == 0) out << "\n";
    out << mat.ptr[i] << ' ';
  }
  return out;
}

