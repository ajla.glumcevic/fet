#include <iostream>
template<typename T>
void foo(T a){
  static int c = 0;
  std::cout << ++c << std::endl;
}



void foo(const int& a){
  std::cout << "prva" << std::endl;
}
void foo( int& a){
  std::cout << "druga" << std::endl;
}
void foo(int&& a){
  std::cout << "treca" << std::endl;
}


int main(void)
{
  int a = 1;
  const int b = 2;
  const int& c = a;
  foo(a);
  foo(1);
  foo(int{});
  foo(std::move(a));
  foo(std::move(b));
  foo(b);
  foo(c);


  return 0;
}
