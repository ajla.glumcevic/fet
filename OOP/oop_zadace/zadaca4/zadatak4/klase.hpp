#pragma once
#include <iostream>
#include <string>
#include <vector>
struct Student{
  std::string ime;
  double prosjek;
  friend bool operator>>(std::istream& in,Student& s );
  friend std::ostream& operator<<(std::ostream& in,Student& s );
};

  bool operator>>(std::istream& in,Student& s ){
     in >> s.ime >> s.prosjek;
     return (bool)in;

  }
std::ostream& operator<<(std::ostream& out,const Student& s ){
  out << s.ime << " " << s.prosjek;
  return out;
}
class Fakultet{
  protected:
    std::vector<Student>studenti;
  int rating;

  public:
  virtual ~Fakultet(){}
  void upis(){
    Student sinisa;
   while(std::cin >> sinisa){
     studenti.push_back(sinisa);
   }
  }
  void ispis()const{
    for(const auto& student : studenti) std::cout << student << std::endl; 
  }
    

};

class Fet : public Fakultet{

  

};
