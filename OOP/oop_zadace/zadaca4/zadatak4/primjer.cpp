#include <iostream>
#include <string>
using namespace std;
#include "amer.hpp"

int main(void) {
  std::vector<Profesor*> profesori;
  Amer amer;
  amer.jacinaGlasa = 40000;
  amer.prijatelj = {"toka"};
  amer.uzrecice = {"uzgred receno", "ultra korisno",
                   "kontinuitet,pamcenje,trud"};
  amer.prjezici = {"si sarp, java, pajton, rubi,de"};
  amer.relStatus = false;
  profesori.push_back(new Amer);
  *profesori[0] = amer;
  return 0;
}
