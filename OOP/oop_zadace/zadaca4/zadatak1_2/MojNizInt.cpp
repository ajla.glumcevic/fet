#include "MojNizInt.hpp"
int* MojNizInt::begin() { return ptr; }

int* MojNizInt::end() { return ptr + duzina; }

int& MojNizInt::front() { return *ptr; }

int& MojNizInt::back() { return *(ptr + duzina - 1); }

bool MojNizInt::empty() {
  if (duzina != 0)
    return false;
  else
    return true;
}

const size_t& MojNizInt::capacity() const { return kapacitet; }

const size_t& MojNizInt::size() const { return duzina; }

int& MojNizInt::at(size_t pozicija) const {
  if (pozicija >= duzina || pozicija < 0)
    throw std::out_of_range("Indeks van granica");
  return ptr[pozicija];
}

void MojNizInt::pop_back() { --duzina; }

void MojNizInt::push_back(const int& element) {
  if (kapacitet > duzina) {
    ptr[duzina] = element;
    duzina += 1;
  } else {
    int* newPtr = new int[duzina + duzina];
    std::copy(ptr, ptr + duzina, newPtr);
    delete[] ptr;
    ptr = newPtr;
    ptr[duzina] = element;
    kapacitet = duzina * 2;
    ++duzina;
  }
}

MojNizInt MojNizInt::operator+(const MojNizInt& drugi) const {
  if (duzina != drugi.duzina) throw std::invalid_argument("duzine razlicite");
  MojNizInt novi = *this;
  for (int i = 0; i < duzina; ++i) {
    novi[i] = novi[i] + drugi[i];
  }
  return novi;
}
MojNizInt& MojNizInt::operator++() {
  for (int i = 0; i < duzina; ++i) {
    ++ptr[i];
  }
  return *this;
}
MojNizInt MojNizInt::operator++(int) {
  MojNizInt novi = *this;
  for (int i = 0; i < duzina; ++i) {
    ++ptr[i];
  }
  return novi;
}
int& MojNizInt::operator[](const int& pozicija) const { return ptr[pozicija]; }
MojNizInt MojNizInt::operator*(const int& skalar) const {
  MojNizInt novi = *this;
  for (int i = 0; i < duzina; ++i) {
    novi[i] = novi[i] * skalar;
  }
  return novi;
}

