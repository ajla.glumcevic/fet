#include <iostream>

class MojNizInt {
  size_t n_ = 0;
  size_t k_ = 1;
  int* p_ = new int[k_];

  public:
  MojNizInt()=default;
  MojNizInt(const std::initializer_list<int>& a)
      : n_{a.size()}, k_{a.size()}, p_{new int[k_]} {
    std::copy(a.begin(), a.end(), p_);
  };
  MojNizInt(const MojNizInt& o) : n_{o.n_}, k_{o.k_}, p_{new int[k_]} {
    std::copy(o.p_, o.p_ + n_, p_);
  };
  MojNizInt(MojNizInt&& o) : n_{o.n_}, k_{o.k_}, p_{o.p_} {
    o.n_ = 0;
    o.k_ = 0;
    o.p_ = nullptr;
  }
  ~MojNizInt() {
    n_ = 0;
    k_ = 0;
    delete[] p_;
  }
  size_t size() const { return n_; }
  size_t capacity() const { return k_; }
  int& at(int a) const {
    if ((n_ == 0) || (a < 0) || (a > n_)) {
      throw std::out_of_range("nbtno");
    } else {
      return *(p_ + a);
    }
  }
  int& operator[](const int& a) const { return p_[a]; }

  MojNizInt& operator=(MojNizInt& o) {
    n_ = o.n_;
    k_ = o.k_;
    p_ = new int[k_];
    std::copy(o.p_, o.p_ + o.n_, p_);
    return *this;
  }
  MojNizInt& operator=(MojNizInt&& o) {
    n_ = o.n_;
    k_ = o.k_;
    p_ = o.p_;
    o.n_ = 0;
    o.k_ = 0;
    o.p_ = nullptr;
    return *this;
  }
  MojNizInt operator*(int a) const {
    MojNizInt temp;
    temp.n_ = n_;
    temp.k_ = k_;
    temp.p_ = new int[k_];
    for (auto i = 0; i < n_; ++i) *(temp.p_ + i) = *(p_ + i) * a;
    return temp;
  }
  MojNizInt operator+(const MojNizInt& o) const {
    if (n_ != o.n_)
      throw std::invalid_argument("nbtno");
    else {
      MojNizInt temp;
      temp.n_ = n_;
      temp.k_ = k_;
      temp.p_ = new int[k_];
      for (auto i = 0; i < n_; ++i) *(temp.p_ + i) = *(p_ + i) + *(o.p_ + i);
      return temp;
    }
  }
  MojNizInt operator++(int a) const {
    MojNizInt temp;
    temp.n_ = n_;
    temp.k_ = k_;
    temp.p_ = new int[temp.k_];
    std::copy(p_, p_ + n_, temp.p_);
    for (auto i = 0; i < n_; ++i) *(p_ + i) += 1;
    return temp;
  }
  MojNizInt& operator++() {
    for (auto i = 0; i < n_; ++i) *(p_ + i) += 1;
    return *this;
  }
  void push_back(int a) {
      if (k_> n_)
      {
        p_[n_]=a;
        n_=n_+1;
      }
      else{
      int* temp = new int[2*n_];
      std::copy(p_, p_+n_, temp);
      delete [] p_;
      p_ = temp;
      p_[n_] = a;

      k_= 2*n_;
      n_+=1;


      }
  }
  int& front() { return *(p_); }
  int& back() { return *(p_ + n_ - 1); }
  void pop_back() { n_ = n_ - 1; }


};
