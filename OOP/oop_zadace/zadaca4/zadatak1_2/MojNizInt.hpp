#include <initializer_list>
#include <iostream>

class MojNizInt {
  private:
  size_t duzina = 0;
  int* ptr = nullptr;
  size_t kapacitet;

  public:
  int* begin();
  int* end();
  int& front();
  int& back();
  bool empty();
  const size_t& capacity() const;
  const size_t& size() const;
  int& at(size_t pozicija) const;
  void pop_back();
  void push_back(const int& element);

  MojNizInt operator+(const MojNizInt& drugi)const;
  MojNizInt& operator++();
  MojNizInt operator++(int);
  int& operator[](const int& pozicija)const;
  MojNizInt operator*(const int& skalar)const;


  MojNizInt() {
    kapacitet = 1;
    ptr = new int[kapacitet];
  }
  MojNizInt(std::initializer_list<int> l)
      : duzina{l.size()}, ptr{new int[duzina]}, kapacitet{l.size()} {
    std::copy(std::begin(l), std::end(l), ptr);
  };
  MojNizInt(const MojNizInt& novi) : duzina{novi.duzina}, ptr{new int[duzina]} {
    std::copy(novi.ptr, novi.ptr + duzina, ptr);
  }
  MojNizInt& operator=(const MojNizInt& novi) {
    if (this != &novi) {
      delete[] ptr;
      duzina = novi.duzina;
      ptr = new int[duzina];
      std::copy(novi.ptr, novi.ptr + duzina, ptr);
    }
    return *this;
  }
  MojNizInt(MojNizInt&& novi) : duzina{novi.duzina}, ptr{novi.ptr} {
    novi.duzina = 0;
    novi.ptr = nullptr;
  };
  MojNizInt& operator=(MojNizInt&& novi) {
    delete[] ptr;
    duzina = novi.duzina;
    ptr = novi.ptr;
    novi.duzina = 0;
    novi.ptr = nullptr;
    return *this;
  }
  ~MojNizInt() { delete[] ptr; }





};
