#include <iostream>
#include "MojNiz.hpp"

int main(void)
{
  MojNiz<int> b{3};
  b = MojNiz<double>{};
  b.push_front(4);
  b.push_back(5);
  b.push_front(3);
  for (auto i: b) {
    std::cout << i << std::endl;
  }
  return 0;
}
