#pragma once
#include <initializer_list>
#include <iostream>
#include <vector>


template <typename T>
class MojNiz {
  public:
  size_t duzina = 0;
  T* ptr = nullptr;
  size_t kapacitet = 1;

  friend std::ostream& operator<<(std::ostream& out, const MojNiz<T>&);
  

  const bool& erase(){
  delete [] ptr;  
  ptr = new T;
  return true;
  }

  


  template<typename A>
  MojNiz(const std::vector<A>& v):duzina{v.size()},ptr{new T[duzina]}{  
    std::copy(v.begin(), v.end(),ptr);
    }

  void insert(const T& value, const size_t& pos){
    if(pos>= duzina || pos < 0) throw std::invalid_argument("greska");
      auto temp = new T[duzina+1];
      std::copy(ptr,ptr+pos,temp);
      temp[pos] = value;
      std::copy(ptr+pos, ptr+duzina, temp+pos+1);
      delete [] ptr;
      ptr = temp;
      ++duzina;
  }








  T* begin() { return ptr; }
  T* end() { return ptr + duzina; }
  T& front() { return ptr[0]; }
  T& back() { return ptr[duzina - 1]; }
  bool empty() {
    if (duzina == 0)
      return true;
    else
      return false;
  }
  const size_t& capacity() const { return kapacitet; }
  const size_t& size() const { return duzina; }
  T& at(size_t pozicija) const {
    if (pozicija >= duzina || pozicija < 0)
      throw std::out_of_range("Indeks van granica");
    return ptr[pozicija];
  }


  void pop_back() { --duzina; }
  MojNiz& push_back(const T& element) {
    if (kapacitet > duzina) {
      ptr[duzina] = element;
      ++duzina;
    } else {
      T* newPtr = new T[2 * duzina];
      std::copy(ptr, ptr + duzina, newPtr);
      delete[] ptr;
      ptr = newPtr;
      ptr[duzina] = element;
      kapacitet = 2 * duzina;
      ++duzina;
    }
    return *this;
  }

  template <typename A,typename U>
  auto operator+(const MojNiz<A>& drugi) const
      -> MojNiz<decltype(ptr[0] + drugi.ptr[0])> {
    if (duzina != drugi.duzina) throw std::invalid_argument("duzine razlicite");
    using B = decltype(ptr[0] + drugi.ptr[0]);
    MojNiz<U> niz;
    for (auto i = 0; i < duzina; ++i) {
      niz[i] = ptr[i] + drugi[i];
    }
    return niz;
  }

  void push_front(T value) {
    auto temp = new T[duzina + 1];
    std::copy(ptr, ptr + duzina, temp + 1);
    delete[] ptr;
    ptr = temp;
    ptr[0] = value;
    ++duzina;
  }

  MojNiz& operator++() {
    for (int i = 0; i < duzina; ++i) {
      ++ptr[i];
    }
    return *this;
  }
  MojNiz& operator++(int) {
    MojNiz novi = *this;
    for (int i = 0; i < duzina; ++i) {
      ++ptr[i];
    }
    return novi;
  }
  T& operator[](const int& pozicija) const { return ptr[pozicija]; }
  MojNiz operator*(const int& skalar) const {
    MojNiz novi = *this;
    for (int i = 0; i < duzina; ++i) {
      novi[i] = novi[i] * skalar;
    }
    return novi;
  }
  MojNiz() {
    kapacitet = 1;
    ptr = new T[kapacitet];
  }
  MojNiz(std::initializer_list<T> l)
      : duzina{l.size()}, ptr{new T[duzina]}, kapacitet{l.size()} {
    std::copy(std::begin(l), std::end(l), ptr);
  };
  template <typename A>
  MojNiz(const MojNiz<A>& novi)
      : duzina{novi.duzina}, ptr{new T[duzina]}, kapacitet{novi.kapacitet} {
    std::copy(novi.ptr, novi.ptr + duzina, ptr);
  }
  template <typename A>
  MojNiz& operator=(const MojNiz<A>& novi) {
    delete[] ptr;
    duzina = novi.duzina;
    ptr = new T[duzina];
    std::copy(novi.ptr, novi.ptr + duzina, ptr);
    return *this;
  }
  template <typename A>
  MojNiz(MojNiz<A>&& novi)
      : duzina{novi.duzina}, ptr{novi.ptr}, kapacitet{novi.kapacitet} {
    novi.duzina = 0;
    novi.kapacitet = 1;
    novi.ptr = nullptr;
  };

  // template<typename A>
  MojNiz& operator=(MojNiz&& novi) {
    delete[] ptr;
    duzina = novi.duzina;
    ptr = novi.ptr;
    novi.duzina = 0;
    novi.ptr = nullptr;
    return *this;
  }
  ~MojNiz() { delete[] ptr; }
};

template<typename T>
   std::ostream& operator<<(std::ostream& out, const MojNiz<T>& niz){
     for(auto i = 0; i<niz.duzina; ++i)
     out<<niz.ptr[i];
     return out;
   }
