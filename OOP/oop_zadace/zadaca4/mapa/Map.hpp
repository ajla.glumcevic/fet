#include "Pair.hpp"
namespace my{
template <typename T, typename U>
class map {
  using pair = typename my::pair<T, U>;
  pair* pairs;
  size_t size;

  public:
  map() : pairs{new pair} {}
  map(const map& other) : pairs{new pair[size]} {
    std::copy(other.pairs, other.pairs + size, pairs);
  }
  map(map&& other) : pairs{other.pairs} { other.pairs = nullptr; }
  ~map() { delete[] pairs; }
};
}
