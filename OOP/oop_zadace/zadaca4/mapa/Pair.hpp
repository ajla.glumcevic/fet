#pragma once
#include <iostream>

namespace my {
template <typename T, typename U>
class pair {
  public:
  T first;
  U second;

  pair(): first{},second{} {}
  pair(const T& f, const U& s) {
    first = f;
    second = s;
  }
  pair(const pair& other) {
    first = other.first;
    second = other.second;
  }
  pair(pair&& other) : first{other.first}, second{other.second} {
    other.first = T{};
    other.second = U{};
  }
  ~pair() {}

  pair& operator=(const pair& other) {
    first = other.first;
    second = other.second;
    return *this;
  }

  pair& operator=(pair&& other) {
    first = other.first;
    second = other.second;
    other.first = T{};
    other.second = U{};

    return *this;
  }

  const pair& operator*()const{
    return second;
  }

  void operator[](const T& f){
    first = f;
  }


  template <typename A, typename B>
  friend std::ostream& operator<<(std::ostream& out, const pair<A, B>& par);
};
template <typename T, typename U>
std::ostream& operator<<(std::ostream& out, const pair<T, U>& par) {
  out << (par.first) << " " << (par.second);
  return out;
}


}  // namespace my

