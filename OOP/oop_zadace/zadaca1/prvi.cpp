#include <iostream>
#include "nmsp.txt"
using namespace std;

// func: miles to kilometers
void mTokm() {
  double speed;
  std::cout << "Enter speed in miles:" << std::endl;
  std::cin >> speed;
  std::cout << speed << " in miles is " << speed * Speed::mileKmCoeff
            << " kilometers." << std::endl;
}

// func: kilometers to miles
void kmTom() {
  double speed;
  std::cout << "Enter speed in kilometers:" << std::endl;
  std::cin >> speed;
  std::cout << speed << " in kilometers is " << speed / Speed::mileKmCoeff
            << "miles" << std::endl;
}

// func: pounds to kilograms
void lbsTokg() {
  double weight;
  std::cout << "Enter weight in pounds:" << std::endl;
  std::cin >> weight;
  std::cout << weight << " in pounds is " << weight * Weight::lbsKgCoeff
            << "kilograms" << std::endl;
}

// func:kilograms to pounds
void kgTolbs() {
  double weight;
  std::cout << "Enter weight in kilograms:" << std::endl;
  std::cin >> weight;
  std::cout << weight << " in kilograms is " << weight / Weight::lbsKgCoeff
            << "pounds." << std::endl;
}

// func: light years to kilometers
void lyTokm() {
  double length;
  std::cout << "Enter length in light years:" << std::endl;
  std::cin >> length;
  std::cout << length << " in light years is " << length * Length::lyKmCoeff
            << "kilometers." << std::endl;
}
// func: kilometers to light years
void kmTokly() {
  double length;
  std::cout << "Enter length in kilometers:" << std::endl;
  std::cin >> length;
  std::cout << length << " in kilometers is " << length / Length::lyKmCoeff
            << "light years." << std::endl;
}
// func: meters to feet
void metersToFeet() {
  double length;
  std::cout << "Enter length in meters:" << std::endl;
  std::cin >> length;
  std::cout << length << " in meters is " << length / Length::metersFeetCoeff
            << "feet." << std::endl;
}
// func:  feet to meters
void feetToMeters() {
  double length;
  std::cout << "Enter length in feet:" << std::endl;
  std::cin >> length;
  std::cout << length << " in feet is " << length * Length::metersFeetCoeff
            << "meters." << std::endl;
}

// func:  cm to inches
void cmToInches() {
  double length;
  std::cout << "Enter length in cm:" << std::endl;
  std::cin >> length;
  std::cout << length << " in cm  is " << length / Length::cmInchCoeff
            << "inches." << std::endl;
}
// func: inches to cm
void inchesToCm() {
  double length;
  std::cout << "Enter length in inches:" << std::endl;
  std::cin >> length;
  std::cout << length << " in inches  is " << length * Length::cmInchCoeff
            << "cm." << std::endl;
}

// celsius to fahrenheit
void celToFahr() {
  double temp;
  std::cout << "Enter temperature in degrees Celsius:" << std::endl;
  std::cin >> temp;
  if (temp > -Temperature::celsiusKelvinCoeff )
    std::cout << temp << " degrees Celsius is " << temp * 9 / 5 + 32
              << "Fahrenheit" << std::endl;
  else
    std::cout << "Incorrect value!" << std::endl;
}
// fahrenheit to celsius
void fahrToCel() {
  double temp;
  std::cout << "Enter temperature in degrees Fahrenheit:" << std::endl;
  std::cin >> temp;
  std::cout << temp << " degrees Fahrenheit is " << (temp - 32) * 5 / 9
            << "Celsius" << std::endl;
}
// celsius to kelvin
void celToKel() {
  double temp;
  std::cout << "Enter temperature in degrees Celsius:" << std::endl;
  std::cin >> temp;
  if (temp > -Temperature::celsiusKelvinCoeff)
    std::cout << temp << " degrees Celsius is "
              << temp + Temperature::celsiusKelvinCoeff << "Celsius"
              << std::endl;
  else
    std::cout << "Incorrect value!" << std::endl;
}
// kelvin to celsius
void kelToCel() {
  double temp;
  std::cout << "Enter temperature in degrees Kelvin:" << std::endl;
  std::cin >> temp;
  if(temp >= 0) 
  std::cout << temp << " degrees Kelvin is "
            << temp - Temperature::celsiusKelvinCoeff << "Celsius" << std::endl;
   else 
     std::cout << "Negative value!" << std::endl;

}
// fahrenheit to kelvin
void fahrToKel() {
  double temp;
  std::cout << "Enter temperature in degrees Fahrenheit:" << std::endl;
  std::cin >> temp;
  std::cout << temp << " degrees Fahrenheit is " << (temp + 459.67) * 5 / 9
            << "Kelvin" << std::endl;
}

// kelvin to fahrenheit
void kelToFahr() {
  double temp;
  std::cout << "Enter temperature in degrees Kelvin:" << std::endl;
  std::cin >> temp;
  if(temp >= 0)
  std::cout << temp << " degrees Kelvin is " << temp * 9 / 5 - 459.67
            << "Fahrenheit" << std::endl;
  else
    std::cout << "Negative value!" << std::endl;
}

//func: liters per km to miles per gallon
void lpkToMpg() {
  double value;
  std::cout << "Enter value in liters per hour" << std::endl;
  std::cin >> value;
  std::cout << value << " lpk is " << (100* Fuel::lpg) / (value * Fuel::kmpm)
            << "mpg" << std::endl;
}

//func: miles per gallon to liters per km
void mpgToLpk() {
  double value;
  std::cout << "Enter value in miles per gallon" << std::endl;
  std::cin >> value;
  std::cout << value << " mpg is " << (100* Fuel::lpg) / (value * Fuel::kmpm)
            << "lpk" << std::endl;
}


// main//////////////////////////////////////////////////////
int main(void) {
  std::cout
      << "welcome to Unit converter.Please enter one of the following options:"
      << std::endl;
  std::cout << "1.Temperature" << std::endl;
  std::cout << "2.Speed" << std::endl;
  std::cout << "3.Length" << std::endl;
  std::cout << "4.Weight" << std::endl;
  std::cout << "5.Fuel economy" << std::endl;

  // choosing converter
  int option;
  std::cout << "Your choice:" << std::endl;
  std::cin >> option;

  // choice temperature//////////////////////////////////////
  if (option == 1) {
    
    std::cout << "Please choose converter:" << std::endl;
    std::cout << "1.Celsious to Fahrenheit" << std::endl;
    std::cout << "2.Fahrenheit to Celsius" << std::endl;
    std::cout << "3.Celsius to Kelvin" << std::endl;
    std::cout << "4.Kelvin to Celsius" << std::endl;
    std::cout << "5.Fahrenheit to Kelvin" << std::endl;
    std::cout << "6.Kelvin to Fahrenheit" << std::endl;

    int option1;
    std::cin >> option1;
    std::cout << "Your choice:" << option1 << std::endl;

    // celsius to fahrenheit
    if (option1 == 1) {
      celToFahr();
    }
    // fahrenheit to celsius
    else if (option1 == 2) {
      fahrToCel();
    }
    // celsius to kelvin
    else if (option1 == 3) {
      celToKel();

    }
    // kelvin to celsius
    else if (option1 == 4) {
      kelToCel();

    }
    // fahrenheit to kelvin
    else if (option1 == 5) {
      fahrToKel();
    }

    // kelvin to fahrenheit
    else if (option1 == 6) {
      kelToFahr();

    } else {
      std::cout << "You must enter again!" << std::endl;
    }
  }
  // choice speed////////////////////////////////////////////
  else if (option == 2) {
    std::cout << "Please choose converter:" << std::endl;
    std::cout << "1.Mile to kilometer" << std::endl;
    std::cout << "2.Kilometer to mile" << std::endl;

    int option1;
    std::cin >> option1;
    std::cout << "Your choice:" << option1 << std::endl;

    // miles to kilometers
    if (option1 == 1) {
      mTokm();
    }
    // kilometers to  miles
    else if (option1 == 2) {
      kmTom();
    } else {
      std::cout << "Error. Try again!" << std::endl;
    }
  }
  // choice length//////////////////////////////////////////
  else if (option == 3) {
    std::cout << "Please choose converter:" << std::endl;
    std::cout << "1.Light years to kilometers" << std::endl;
    std::cout << "2.Kilometers to lightyears" << std::endl;
    std::cout << "3.Meters to feet" << std::endl;
    std::cout << "4.Feet to meters" << std::endl;
    std::cout << "5.Centimeters to inches" << std::endl;
    std::cout << "6.Inches to centimeters" << std::endl;

    int option1;
    std::cin >> option1;
    std::cout << "Your choice:" << option1 << std::endl;

    // ly to km
    if (option1 == 1) {
      lyTokm();
    }
    // km to ly
    else if (option1 == 2) {
      kmTokly();
    }
    // meters to feet
    else if (option1 == 3) {
      metersToFeet();
    }
    // feet to meters
    else if (option1 == 4) {
      feetToMeters();
    }
    // cm to inches
    else if (option1 == 5) {
      cmToInches();
    }
    // inches to cm
    else if (option1 == 6) {
      inchesToCm();
    }
    // wrong choice
    else {
      std::cout << "Wrong choice" << std::endl;
    }

  }
  // choice weigth////////////////////////////////////////////////
  else if (option == 4) {
    std::cout << "Please choose converter:" << std::endl;
    std::cout << "1.Pounds to kilograms" << std::endl;
    std::cout << "2.Kilograms to pounds" << std::endl;

    int option1;
    std::cin >> option1;
    std::cout << "Your choice:" << option1 << std::endl;

    // pounds to kilograms
    if (option1 == 1) {
      lbsTokg();
    }
    // kilograms to  pounds
    else if (option1 == 2) {
      kgTolbs();
    } else {
      std::cout << "Wrong choice!" << std::endl;
    }
  }
  // choice fuel economy//////////////////////////////////////////////
  else if (option == 5) {
   std::cout << "Please choose converter:" << std::endl;
    std::cout << "1.Lpk to mpg " << std::endl;
    std::cout << "2.Mpg to lpk" << std::endl;

    int option1;
    std::cin >> option1;
    std::cout << "Your choice:" << option1 << std::endl;

    if(option1 == 1){lpkToMpg();}
  else if(option1 == 2){mpgToLpk();}
  // wrong choice
  else {
    std::cout << "Wrong choice!" << std::endl;
  }
  }

  return 0;
}
