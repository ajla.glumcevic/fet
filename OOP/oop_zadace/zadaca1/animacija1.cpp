#include <sys/ioctl.h>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
size_t broj_kolona() {
  winsize sz;
  ioctl(0, TIOCGWINSZ, &sz);
  return sz.ws_col;
}

void pauziraj(unsigned int msec) {
  std::flush(std::cout);
  std::this_thread::sleep_for(std::chrono::milliseconds{msec});
}
int pauza = 100;
int main() {
  while (1) {
    std::vector<char> niz{'\\', '-', '/', '|'};
    int i = 0, j = 3;
    while (i < broj_kolona() && j >= 0) {
      std::cout << "\r" << std::string(broj_kolona(), ' ') << "\r";
      std::cout << std::string(i, ' ');
      std::cout << niz.at(j);
      if (!j) j = 4;
      --j;
      ++i;

      pauziraj(pauza);
    }

    while (--i < broj_kolona() && j >= 0) {
      std::cout << "\r" << std::string(broj_kolona(), ' ') << "\r";
      std::cout << std::string(i, ' ');
      std::cout << niz.at(j);
      if (!j) j = 4;
      --j;

      pauziraj(pauza);
    }
  }
  return 0;
}
