
#include <math.h>
#include <bitset>
#include <iostream>
#include <string>
#include <vector>


unsigned short reg1 = -1, reg2 = 2; //test
void printaj(unsigned short reg) {
  std::bitset<16> bits(reg);
  std::cout << bits << std::endl;
}

int printMenu() {
  std::cout << "1. Print register\n";
  std::cout << "2. Set bit in register\n";
  std::cout << "3. Reset bit in register\n";
  std::cout << "4. Switch values\n";
  std::cout << "5. Exit\n";
  std::cout << "\nEnter option: ";
  int option;
  std::cin >> option;
  return option;
}

unsigned short setuj(int pozicija, unsigned short reg) {
  reg = (1<<pozicija -1) | reg;
  return reg;
}

unsigned short resetuj(int pozicija, unsigned short reg) {
  reg = ~(1<<pozicija -1)&reg;
  return reg;
}

unsigned short switch_values(int regNum) {
  reg1 = reg1 + reg2;
  reg2 = reg1 - reg2;
  reg1 = reg1 - reg2;
  if (regNum == 1)
    return reg1;
  else
    return reg2;
}
///////////////////////////////////////////////////////////////////
int main() {
  while (1) {
    std::cout << "\n\n\n";
    unsigned short reg;
    std::cout << "Izaberi registar 1 ili 2: ";
    int regNum;
    std::cin >> regNum;

    if (regNum == 1)
      reg = reg1;
    else if (regNum == 2)
      reg = reg2;
    else
      return 1;

    int pozicija;
    int opcija = printMenu();
    if (opcija == 1) {
      printaj(reg);
    } else if (opcija == 2) {
      std::cout << "Unesi poziciju: ";
      std::cin >> pozicija;
      reg = setuj(pozicija, reg);
      printaj(reg);
    } else if (opcija == 3) {
      std::cout << "Unesi poziciju: ";
      std::cin >> pozicija;
      reg = resetuj(pozicija, reg);
      printaj(reg);
    } else if (opcija == 4) {
      reg = switch_values(regNum);
      std::cout << "Nova vrijednost: ";
      printaj(reg);
    } else if (opcija == 5)
      return 0;
    else {
      std::cout << "Pogresan unos" << std::endl;
    }
    regNum == 1 ? reg1 = reg : reg2 = reg;
    std::cout << "Vrijednost registra: " << reg << std::endl;
  };
  return 0;
}
