#include <sys/ioctl.h>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

typedef std::vector<double> vektor;
const int velicina = 4;

size_t broj_kolona() {
  winsize sz;
  ioctl(0, TIOCGWINSZ, &sz);
  return sz.ws_col;
}
double const podiok = (broj_kolona() - 2) / 100.;
  unsigned int pauza = 300;

void pauziraj(unsigned int msec) {
  std::flush(std::cout);
  std::this_thread::sleep_for(std::chrono::milliseconds{msec});
}

void ocisti(vektor granice) {
  for (int i = 0; i < velicina; ++i) {
    std::string crta(granice.at(i) * podiok, ' ');
    std::cout << crta;
    pauziraj(pauza);
  }
}

void iscrtaj(vektor granice) {
  for (int i = 0; i < velicina; ++i) {
    std::string crta(granice.at(i) * podiok, '=');
    std::cout << crta;

    pauziraj(pauza);
  }
}

////////////////////////////////////////////////////////////////////////
int main(void) {
  vektor granice(velicina, 0);
  double suma = 0;

  std::cout << "Unesite granice:" << std::endl;
  for (int i = 0; i < velicina; ++i) {
    std::cin >> granice.at(i);
    std::cin.clear();
    std::cin.ignore();
    suma += granice.at(i);
  }
  std::cout <<"Suma iznosi :" << suma << std::endl;

  //validan unos
  if (suma <= 100) {
    while (true) {
      pauziraj(pauza);
      std::cout << "\r"
                << "[";
      iscrtaj(granice);
      std::cout << "]";
      pauziraj(pauza);
      std::cout << "\r"
                << "[";
      pauziraj(pauza);
      ocisti(granice);
      std::cout << "]";
    }
  //pogresan unos
  } else {
    std::cout << "Pogresan unos." << std::endl;
    return 1;
  }
  return 0;
}
