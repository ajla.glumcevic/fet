#include <iostream>
#include <vector>

template<typename InputIt, typename OutputIt>
bool moj_equal(InputIt begin1, InputIt end1, OutputIt begin2){
  while(begin1 != end1){
    // if(*begin1 == *begin2){
    //   ++begin1;
    //   ++begin2;
    // }
    // else return false;
    if(*begin1 != *begin2) return false;
    ++begin1;
    ++begin2;
  }
  return true;
}


int main(void)
{
  std::string word;
  while(std::cin >> word){
    std::string temp;
    for(auto i = word.end() -1 ; i >= word.begin() ; --i) temp.push_back(*i);
 std::cout << temp << std::endl; 
    if(moj_equal(word.cbegin(), word.cend(),temp.cbegin())) std::cout << word << " je palindrom."<< std::endl;
    else std::cout << word << " nije palindrom." << std::endl;
    std::cin.ignore();
  }
  return 0;
}
