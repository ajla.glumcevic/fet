#include <iostream>
#include "baza.hpp"

int main(void) {
  BazaRadnik b;
  while (true) {
    try {
      b.unos(std::cin);

      break;

    } catch (std::domain_error& e) {
      std::cout << e.what() << std::endl;
    }
  }

  b.brisiRadnik("ajla");

  std::cout << "Ispis:" << std::endl;
  b.ispis(std::cout);

  std::cout << "Prosjek plata:" << std::endl;
  std:: cout <<  b.prosjekPlata();
  return 0;
}
