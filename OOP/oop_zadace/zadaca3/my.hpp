#pragma once
#include <algorithm>
#include <functional>
#include <iostream>

namespace my {

auto k = [](auto a , auto b){return a<b;};

template <typename Iterator, 
          class UnaryOpt = decltype(k) >
void sort(Iterator start, Iterator end,
          UnaryOpt predicate = k) {
  for (auto it = start; it != end; ++it) {
    for (auto jt = it; jt != end; ++jt) {
      if (predicate(*jt, *it)) {
        std::swap(*it, *jt);
      }
    }
  }
}
}  // namespace my
