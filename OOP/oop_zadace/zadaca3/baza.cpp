#include "baza.hpp"


void Radnik::setRadnik(std::string& _ime, int& _godine, double& _plata){
  ime = _ime;
  godine = _godine;
  plata = _plata;
}
const std::string& Radnik:: getIme()const{
  return ime;
}
const int& Radnik:: getGodine()const{
  return godine;
}
const double& Radnik::getPlata()const{
  return plata;
}


std::istream& BazaRadnik::unos(std::istream& ulaz){
  Radnik radnik;
  std::string ime;
  int godine;
  double plata;

  while(ulaz >> ime >> godine >> plata){

    for(auto it = radnici.begin(); it!= radnici.end();){

    if(it -> getIme() == ime){ 
      auto it_er = radnici.erase(it);
    // throw std::domain_error("Radnik postoji");
    }
    else ++it;
    }
  radnik.setRadnik(ime, godine, plata);
  radnici.push_back(radnik);
  }
  std::cin.ignore();
   return ulaz; 
}

std::ostream& BazaRadnik::ispis(std::ostream& izlaz){
  for(const auto& radnik : radnici){
    izlaz << radnik.getIme() << radnik.getGodine() << radnik.getPlata();
  }
  return izlaz;
}

bool BazaRadnik::brisiRadnik(std::string ime){
  for(auto it = radnici.begin(); it!= radnici.end(); ){
    if(ime == it -> getIme()){
      auto it_er = radnici.erase(it);
    }
    else ++it;
  }
 }

double BazaRadnik::prosjekPlata(){
  double sum = 0;
  for(const auto& radnik: radnici){
    sum+=radnik.getPlata();
  }
  return sum/radnici.size();
}

