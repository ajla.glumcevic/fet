#pragma once
#include <iostream>
#include <vector>


class Radnik{
  std::string ime;
  int godine;
  double plata;

  public:

  void setRadnik(std::string&, int& , double&);
  const std::string& getIme() const;
  const int& getGodine() const;
  const double& getPlata()const;
};

class BazaRadnik{
   std::vector<Radnik> radnici; 


  public:


  std::istream& unos(std::istream&);
 std::ostream& ispis(std::ostream&);
 double prosjekPlata();
 double prosjekGodine();
 bool brisiRadnik(std::string);
};
