#include "ComplexNumber.hpp"

int main (int argc, char *argv[])
{

    // default
    ComplexNumber c1;
    std::cout << "Default cons: " << c1 << std::endl;

    // with args
    ComplexNumber c2(3,4);
    std::cout << "With args (3,4): " << c2 << std::endl;
    ComplexNumber c_21(3);
    std::cout << "With args (3): " << c_21 << std::endl;
    ComplexNumber c_22(0, 3);
    std::cout << "With args (0,3): " << c_22 << std::endl;
    ComplexNumber c3(-3,-4);
    std::cout << "With args (-3,-4): " << c3 << std::endl;

    // const
    ComplexNumber c4(c3);
    std::cout << "Const cons from (-3-4i): " << c4 << std::endl;

    // operator=
    ComplexNumber c5;
    c5 = c4;
    std::cout << "Operator= from (-3-4j): " << c4 << std::endl;

    // operator+
    ComplexNumber c6 = ComplexNumber(2,3) + ComplexNumber(5,7);
    std::cout << "(2+3i) + (5+7i): " <<  c6 << std::endl;

    // operator-
    ComplexNumber c7 = ComplexNumber(2,3) - ComplexNumber(5,7);
    std::cout << "(2+3i) - (5+7i): " <<  c7 << std::endl;

    // operator*
    ComplexNumber c8 = ComplexNumber(2,3) * ComplexNumber(2,-3);
    std::cout << "(2+3i) * (2-3i): " << c8 << std::endl;
    ComplexNumber c9 = ComplexNumber(2,3) * ComplexNumber(2,3);
    std::cout << "(2+3i) * (2+3i): " << c9 << std::endl;
    ComplexNumber c10 = ComplexNumber(1,1) * ComplexNumber(2,1);
    std::cout << "(1+1i) * (2+1i): " << c10 << std::endl;
    ComplexNumber c11 = ComplexNumber(0,1) * ComplexNumber(2,1);
    std::cout << "i * (2+1i): " << c11 << std::endl;

    //operator+ double
    ComplexNumber c12 = ComplexNumber(5,2) + 10;
    std::cout << "(5+2i) + 10: " << c12 << std::endl;

    //operator- double
    ComplexNumber c13 = ComplexNumber(5,2) - 10;
    std::cout << "(5+2i) - 10: " << c13 << std::endl;

    //operator* double
    ComplexNumber c14 = ComplexNumber(5,2) * 10;
    std::cout << "(5+2i) * 10: " << c14 << std::endl;

    //operator* double
    ComplexNumber c15 = ComplexNumber(5,2) / 10;
    std::cout << "(5+2i) / 10: " << c15 << std::endl;

    //operator+=
    ComplexNumber c16(2,3);
    ComplexNumber c17(3,4);
    c17 += c16;
    std::cout << "z1=2+3i, z2=3+4i, z2+=z1: " << c17 << std::endl;

    //operator-=
    ComplexNumber c18(2,3);
    ComplexNumber c19(3,4);
    c19 -= c18;
    std::cout << "z1=2+3i, z2=3+4i, z2-=z1: " << c19 << std::endl;

    //operator*=
    ComplexNumber c20(2,3);
    ComplexNumber c21(3,4);
    c21 *= c20;
    std::cout << "z1=2+3i, z2=3+4i, z2*=z1: " << c21 << std::endl;

    //operator/=
    ComplexNumber c22(2,3);
    ComplexNumber c23(3,4);
    c23 /= c22;
    std::cout << "z1=2+3i, z2=3+4i, z2/=z1: " << c23 << std::endl;

    //operator ++ prefix
    ComplexNumber c24(2,3);
    std::cout << "++(2+3i): " << ++c24 << std::endl;
    //operator -- prefix
    c24.setRe(2);c24.setIm(3);
    std::cout << "--(2+3i): " << --c24 << std::endl;
    //operator ++ postfix
    c24.setRe(2);c24.setIm(3);
    std::cout << "(2+3i)++: " << c24++ << ", " << c24 << std::endl;
    //operator -- postfix
    c24.setRe(2);c24.setIm(3);
    std::cout << "(2+3i)--: " << c24-- << ", " << c24 << std::endl;

    //oprator ==
    ComplexNumber c25(2,3);
    std::cout << "(2+3i) == (2+3i): " << std::boolalpha << (c25 == c25) << std::endl;
    std::cout << "(2+3i) != (2+3i): " << (c25 != c25) << std::endl;

    //modul
    std::cout << "|2+3i|: " << c25.modul() << std::endl;

    return 0;
}
