#include "ComplexNumber.hpp"

ComplexNumber::ComplexNumber(): re_{0}, im_{0}{}
ComplexNumber::ComplexNumber(const ComplexNumber& obj){
    re_ = obj.re_;
    im_ = obj.im_;
}
ComplexNumber::ComplexNumber(double re, double im): re_{re}, im_{im}{}
ComplexNumber& ComplexNumber::operator=(const ComplexNumber& obj){
    re_ = obj.re_;
    im_ = obj.im_;
    return *this;
}
ComplexNumber ComplexNumber::operator+(const ComplexNumber& obj) const{
    ComplexNumber tmp;;
    tmp.re_ = re_ + obj.re_;
    tmp.im_ = im_ + obj.im_;
    return tmp;
}
ComplexNumber ComplexNumber::operator-(const ComplexNumber& obj) const{
    ComplexNumber tmp;;
    tmp.re_ = re_ - obj.re_;
    tmp.im_ = im_ - obj.im_;
    return tmp;
}
ComplexNumber ComplexNumber::operator*(const ComplexNumber& obj) const{
    ComplexNumber tmp;
    tmp.re_ = re_*obj.re_ - im_*obj.im_;
    tmp.im_ = re_*obj.im_ + im_*obj.re_;
    return tmp;
}
ComplexNumber ComplexNumber::operator+(double broj) const{
    ComplexNumber tmp;
    tmp.re_ = re_ + broj;
    tmp.im_ = im_;
    return tmp;
}
ComplexNumber ComplexNumber::operator-(double broj) const{
    ComplexNumber tmp;
    tmp.re_ = re_ - broj;
    tmp.im_ = im_;
    return tmp;
}
ComplexNumber ComplexNumber::operator*(double broj) const{
    ComplexNumber tmp;
    tmp.re_ = re_ * broj;
    tmp.im_ = im_ * broj;
    return tmp;
}
ComplexNumber ComplexNumber::operator/(double broj) const{
    ComplexNumber tmp;
    tmp.re_ = re_ / broj;
    tmp.im_ = im_ / broj;
    return tmp;
}
ComplexNumber& ComplexNumber::operator+=(const ComplexNumber& obj) {
    re_ += obj.re_;
    im_ += obj.im_;
    return *this;
}
ComplexNumber& ComplexNumber::operator-=(const ComplexNumber& obj){
    re_ -= obj.re_;
    im_ -= obj.im_;
    return *this;
}
ComplexNumber& ComplexNumber::operator*=(const ComplexNumber& obj){
    re_ *= obj.re_;
    im_ *= obj.im_;
    return *this;
}
ComplexNumber& ComplexNumber::operator/=(const ComplexNumber& obj){
    re_ /= obj.re_;
    im_ /= obj.im_;
    return *this;
}
ComplexNumber& ComplexNumber::operator++(){
    ++re_;
    return *this;
}
ComplexNumber ComplexNumber::operator++(int){
    ComplexNumber tmp = *this;
    ++re_;
    return tmp;
}
ComplexNumber& ComplexNumber::operator--(){
    --re_;
    return *this;
}
ComplexNumber ComplexNumber::operator--(int){
    ComplexNumber tmp = *this;
    --re_;
    return tmp;
}
bool ComplexNumber::operator==(const ComplexNumber& obj) const{
    return (re_ == obj.re_) && (im_ == obj.im_);
}
bool ComplexNumber::operator!=(const ComplexNumber& obj) const{
    return !operator==(obj);
}
double ComplexNumber::getRe() const{
    return re_;
}
double ComplexNumber::getIm() const{
    return im_;
}
void ComplexNumber::setRe(double x){
    re_ = x;
}
void ComplexNumber::setIm(double y){
    im_ = y;
}
double ComplexNumber::modul() const{
    return sqrt(re_*re_ + im_*im_);
}
ComplexNumber::~ComplexNumber (){
    // std::cout << "Destructor" << std::endl;
}


std::ostream& operator<<(std::ostream& os, const ComplexNumber& obj){
    if(obj.getIm() == 0){
        return os << obj.getRe();
    }else if(obj.getRe() == 0){
        return os << obj.getIm() << "i";
    }else if(obj.getIm() < 0){
        return os << obj.getRe() << " - " << -obj.getIm() << "i";
    }else{
        return os << obj.getRe() << " + " << obj.getIm() << "i";
    }
}
