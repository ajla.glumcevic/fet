#pragma once
#include <iostream>
#include <ostream>
#include <cmath>

class ComplexNumber {
    public:
        ComplexNumber();
        ComplexNumber(const ComplexNumber& obj);
        ComplexNumber(double re, double im = 0);
        ComplexNumber& operator=(const ComplexNumber& obj);
        ComplexNumber operator+(const ComplexNumber& obj) const;
        ComplexNumber operator-(const ComplexNumber& obj) const;
        ComplexNumber operator*(const ComplexNumber& obj) const;
        ComplexNumber operator+(double broj) const;
        ComplexNumber operator-(double broj) const;
        ComplexNumber operator*(double broj) const;
        ComplexNumber operator/(double broj) const;
        ComplexNumber& operator+=(const ComplexNumber& obj);
        ComplexNumber& operator-=(const ComplexNumber& obj);
        ComplexNumber& operator*=(const ComplexNumber& obj);
        ComplexNumber& operator/=(const ComplexNumber& obj);
        ComplexNumber& operator++();
        ComplexNumber operator++(int);
        ComplexNumber& operator--();
        ComplexNumber operator--(int);
        bool operator==(const ComplexNumber& obj) const;
        bool operator!=(const ComplexNumber& obj) const;
        double getRe() const;
        double getIm() const;
        void setRe(double x);
        void setIm(double y);
        double modul() const;
        ~ComplexNumber();

    private:
        double re_;
        double im_;
};

std::ostream& operator<<(std::ostream& os, const ComplexNumber& obj);
