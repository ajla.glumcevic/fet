#include <iostream>
#include "dna_storage.hpp"

int userInput() {
  std::cout << "Your choice: " ;
  int choice;
  std::cin >> choice;
  if (choice < 1 || choice > 7)
    throw std::string("You must enter option 1-7. Please try again!");
  return choice;
}

int main(void) {
  DNAStorage myDNA;
  std::cout
    << "Welcome to DNA storage. Please enter one of the following options:"
    << std::endl;
  std::cout << "1.Print\n2.Print <pos> <len>\n3.Insert <pos> "
    "<lanac>\n4.Remove <pos> <len>\n5.Store <file>\n6.Load "
    "<file>\n7.Quit  "
    << std::endl;

  while (1) {
    try {
      auto choice = userInput();
      if (choice == 1) {
        myDNA.print(std::cout);
      } else if (choice == 2) {
        int position, len;
        std::cout << "Position: ";
        std::cin >> position;
        std::cout << "Length: ";
        std::cin >> len;
        myDNA.print(std::cout, position, len);
      } else if (choice == 3) {
        int position;
        std::string value;
        std::cout << "Position: ";
        std::cin >> position;
        std::cout << "Value: ";
        std::cin >> value;
        myDNA.insert(position, value);
      } else if (choice == 4) {
        int position, len;
        std::cout << "Position: ";
        std::cin >> position;
        std::cout << "Length: ";
        std::cin >> len;
        int n = myDNA.remove(position, len);
        // std::cout << "Number of deleted molecules: " << n << std::endl;
      } else if (choice == 5) {
        std::string filename;
        std::cout << "File: ";
        std::cin >> filename;
        myDNA.store(filename);
      } else if (choice == 6) {
        std::string filename;
        std::cout << "File: ";
        std::cin >> filename;
        myDNA.load(filename);
      } else {
        return 0;
      }
      // std::cout << "Full length of DNA: " << myDNA.length()<< std::endl;
    } catch (std::string& msg) {
      std::cout << msg << std::endl;
    }
  }
  return 0;
}


