#include "dna_storage.hpp"
// Constructors
DNAStorage::DNAStorage() : length_{0}, molecules{new char[length_]} {}

DNAStorage::DNAStorage(int len, char *mol) : length_{len}, molecules{mol} {}

DNAStorage::DNAStorage(const DNAStorage &other) : length_{other.length_} {
  molecules = new char[length_];
  std::copy(other.molecules, other.molecules + length_, molecules);
}

DNAStorage::DNAStorage(DNAStorage &&other)
    : length_{other.length_}, molecules{other.molecules} {
  other.length_ = 0;
  other.molecules = nullptr;
}
// Operator=
DNAStorage &DNAStorage::operator=(const DNAStorage &other) {
  // if (this != &other) {
  //   length_ = other.length_;
  //   delete[] molecules;
  //   molecules = new char[length_];
  //   std::copy(other.molecules, other.molecules + length_, molecules);
  // }
  // return *this;
  return *this = DNAStorage{other};
}

DNAStorage &DNAStorage::operator=(DNAStorage &&other) {
  if (this != &other) {
    length_ = other.length_;
    molecules = other.molecules;
    other.length_ = 0;
    other.molecules = nullptr;
  }
  return *this;
}

DNAStorage::~DNAStorage() { delete[] molecules; };

// Methods
void DNAStorage::print(std::ostream &out) const {
  if (empty()) throw std::string("0-0: NULL ...DNAStorage is empty!");
  // iterating DNAStorage
  for (int i = 0; i < length_;) {
    std::string printing;
    int j = i;

    if (molecules[i] == 'A' || molecules[i] == 'T' || molecules[i] == 'C' ||
        molecules[i] == 'G') {
      // iterating known sequences
      while ((molecules[i] == 'A' || molecules[i] == 'T' ||
              molecules[i] == 'C' || molecules[i] == 'G') &&
             i < length_) {
        printing.push_back(molecules[i]);
        ++i;
      }
      out << j << "-" << i - 1 << ": " << printing << std::endl;
      j = i;
    } else {
      // iterating unknown sequences
      while (molecules[i] != 'A' && molecules[i] != 'T' &&
             molecules[i] != 'C' && molecules[i] != 'G' && i < length_) {
        ++i;
      }
      out << j << "-" << i - 1 << ": NULL" << std::endl;
    }
  }
  std::cout << std::endl;
  std::cout << "Current length: " << length_ << std::endl;
}

void DNAStorage::print(std::ostream &out, int pos, int len) const {
  // checks if storage is empty or position greater than length
  if (empty()) throw std::string("0-0: NULL ...DNAStorage is empty!");
  if (pos >= length_) throw std::string("Position out of range.");
  if (pos + len > length_) len = length_ - pos;

  int i = pos;
  std::string printing;
  if (molecules[i] == 'A' || molecules[i] == 'T' || molecules[i] == 'C' ||
      molecules[i] == 'G') {
    while (i < pos + len) {
      if (molecules[i] != 'A' && molecules[i] != 'T' && molecules[i] != 'C' &&
          molecules[i] != 'G')
        break;  // break when position is empty/unknown

      printing.push_back(molecules[i]);
      ++i;
    }
    out << pos << "-" << i - 1 << ": " << printing << std::endl;
    std::cout << std::endl;
  }

  std::cout << "Current length: " << length_ << std::endl;
}

void DNAStorage::insert(int pos, std::string lanac) {
  // checks if users input is valid
  for (auto i = 0; i < lanac.size(); ++i) {
    if (lanac.at(i) != 'A' && lanac.at(i) != 'T' && lanac.at(i) != 'C' &&
        lanac.at(i) != 'G')
      throw std::string("Wrong input!");
  }

  if (pos < 0) throw std::string("Position must be a positive number.");
  if (pos + lanac.size() >= length_) resize(pos + lanac.size());

  for (auto i = pos, j = 0; j < lanac.size(); ++i, ++j) {
    molecules[i] = lanac.at(j);
  }
  std::cout << "Current length:" << length_ << std::endl;
}

// returns number of deleted elements
int DNAStorage::remove(int pos, int len) {
  if (pos >= length_) return 0;
  int numOfDeletedMolecules = 0;
  for (auto i = pos, j = 0; i < length_ && j < len; ++i, ++j) {
    if (molecules[i] != 'A' && molecules[i] != 'T' && molecules[i] != 'C' &&
        molecules[i] != 'G')
      break;  // break when position is empty/unknown
    molecules[i] = ' ';
    ++numOfDeletedMolecules;
  }
  // ignore empty positions
  while (molecules[length_ - 1] != 'A' && molecules[length_ - 1] != 'T' &&
         molecules[length_ - 1] != 'C' && molecules[length_ - 1] != 'G' &&
         length_ != 0) {
    --length_;
  }

  std::cout << "Current length:" << length_ << std::endl;
  return numOfDeletedMolecules;
}

void DNAStorage::load(std::string filename) {
  std::ifstream input{filename};
  if (!input.is_open()) throw std::runtime_error("Couldn't open file.");

  delete[] molecules;
  length_ = 0;
  molecules = new char[length_];

  int index;
  std::string mols;
  while (input >> index >> mols) {
    insert(index, mols);
  }
  input.close();
}

void DNAStorage::store(std::string filename) {
  std::ofstream output{filename};
  if (!output.is_open()) throw std::runtime_error("Couldn't open file.");
  // iterating DNAStorage
  for (int i = 0; i < length_;) {
    std::string printing;
    int j = i;

    if (molecules[i] == 'A' || molecules[i] == 'T' || molecules[i] == 'C' ||
        molecules[i] == 'G') {
      // iterating known sequences
      while ((molecules[i] == 'A' || molecules[i] == 'T' ||
              molecules[i] == 'C' || molecules[i] == 'G') &&
             i < length_) {
        printing.push_back(molecules[i]);
        ++i;
      }
      output << j << ' ' << printing << std::endl;
    } else {
      ++i;
    }
  }
  output.close();
}

// Called in insert()
// If newLength is greater than length_ storage is extended,
// otherwise length_ becomes newLength
void DNAStorage::resize(int newLength) {
  if (newLength > length_) {
    auto tmp = new char[newLength];
    std::copy(molecules,molecules+length_, tmp);
    delete[] molecules;
    molecules = tmp;
  }
  length_ = newLength;
}
