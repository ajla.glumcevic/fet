#pragma once

template <typename T>
void ArrayList<T>::insert(const typename ArrayList<T>::iterator& pos, const T &value) {
  auto c = pos - begin();
  insert(c, value);
}

template <typename T>
void ArrayList<T>::insert(const typename ArrayList<T>::const_iterator& pos,
                          const T &value) {
  auto c = pos - cbegin();
  insert(c, value);
}

template <typename T>
void ArrayList<T>::insert(const int &index, const T &value) {
  if( index>=size_ ||index <0) throw std::out_of_range("Pos out of range.");
  if (full()) reserve(capacity_ * 2);
  for (auto i = size_; i > index; --i)
    std::swap(elements_[i], elements_[i - 1]);
  elements_[index] = value;
  ++size_;
}

template <typename T>
typename ArrayList<T>::iterator ArrayList<T>::find(const T &value) {
  return find_if([&value](const T &el) { return value == el; });
}

template <typename T>
typename ArrayList<T>::iterator ArrayList<T>::find_if(
    const std::function<bool(const T &)> &predicate) {
  for (auto it = begin(); it != end(); ++it) {
    if (predicate(*it)) return it;
  }
  return end();  // if no element is found for which predicate returns true
}

template <typename T>
void ArrayList<T>::remove(const T &value) {
  auto it = find(value);
  remove(it);
}

template <typename T>
void ArrayList<T>::remove(const iterator &it) {
  // if (it == end()) return;

  auto pos = it - begin();
  for(auto i = pos; i <size_; ++i)
    elements_[i]=elements_[i+1];
    
  --size_;
}

template <typename T>
void ArrayList<T>::remove_if(const std::function<bool(const T &)> &predicate) {
  auto it = find_if(predicate);
   remove(it);
}

template <typename T>
ArrayList<T> &ArrayList<T>::invert() {
  for (auto i = 0; i < size_ / 2; ++i)
    std::swap(elements_[i], elements_[size_ - i - 1]);
  return *this;
}

/////////////////////////////////////////////////////////////////////////////
template <typename T>
ArrayList<T>::ArrayList(int n) {
  n < 0 ? capacity_ = 100 : capacity_ = n;
  size_ = 0;
  elements_ = new T[capacity_];
}


// Copy cons
template <typename T>
ArrayList<T>::ArrayList(const ArrayList<T> &other) {
  capacity_ = other.capacity();
  size_ = other.size();
  elements_ = new T[capacity_];
  for (int i = 0; i < size_; i++) elements_[i] = other.elements_[i];
}
// Move cons
template <typename T>
ArrayList<T>::ArrayList(ArrayList<T> &&other)
    : size_{other.size_},
      capacity_{other.capacity_},
      elements_{other.elements_} {
  other.size_ = 0;
  other.capacity_ = 100;
  other.elements_ = nullptr;
}
// Cons init list
template <typename T>
ArrayList<T>::ArrayList(const std::initializer_list<T> &l)
    : size_{l.size()}, capacity_{2*size_} {
  elements_ = new T[capacity_];
  std::copy(std::begin(l), std::end(l), elements_);
}
// Copy operator=
template <typename T>
ArrayList<T> &ArrayList<T>::operator=(const ArrayList<T> &other) {
  if (this != &other) {
    delete[] elements_;
    capacity_ = other.capacity();
    size_ = other.size();
    elements_ = new T[capacity_];
    for (int i = 0; i < size_; i++) elements_[i] = other.elements_[i];
  }
  return *this;
}
// Move operator=
template <typename T>
ArrayList<T> &ArrayList<T>::operator=(ArrayList<T> &&other) {
  if (this != &other) {
    delete[] elements_;
    size_ = other.size_;
    capacity_ = other.capacity_;
    elements_ = other.elements_;
    other.size_ = 0;
    other.capacity_ = 100;
    other.elements_ = nullptr;
  }
  return *this;
}

template <typename T>
void ArrayList<T>::reserve(int newCap) {
  if (newCap > capacity_) {
    auto tmp = new T[newCap];
    std::copy(elements_, elements_ + size_, tmp);
    delete[] elements_;
    elements_ = tmp;
    capacity_ = newCap;
  }
}

template <typename T>
bool ArrayList<T>::empty() const {
  return (size_ == 0);
}

template <typename T>
bool ArrayList<T>::full() const {
  return (size_ == capacity_);
}

template <typename T>
void ArrayList<T>::print() const {
  for (int i = 0; i < size_; i++) std::cout << elements_[i] << " ";
  std::cout << std::endl;
}

template <typename T>
const T &ArrayList<T>::at(int index) const {
  if (index < 0 || index >= size_)
    throw std::out_of_range("Index is out of range.");
  else
    return elements_[index];
}

template <typename T>
T &ArrayList<T>::at(int index) {
  if (index < 0 || index >= size_)
    throw std::out_of_range("Index is out of range.");
  else
    return elements_[index];
}

template <typename T>
ArrayList<T> &ArrayList<T>::push_back(const T &value) {
  if (full()) reserve(capacity_ * 2);
  elements_[size_] = value;
  ++size_;

  return *this;
}

template <typename T>
ArrayList<T> &ArrayList<T>::push_front(const T &value) {
  if (full()) reserve(capacity_ * 2);
  for (auto i = size_; i > 0; --i) std::swap(elements_[i], elements_[i - 1]);
  ++size_;
  elements_[0] = value;
  return *this;
}

template <typename T>
ArrayList<T> &ArrayList<T>::pop_back() {
  if (size_ > 0) --size_;
  return *this;
}

template <typename T>
ArrayList<T> &ArrayList<T>::pop_front() {
  for (auto i = 0; i < size_; ++i) std::swap(elements_[i], elements_[i + 1]);
  --size_;
  return *this;
}

template <typename T>
ArrayList<T> &ArrayList<T>::replace(const T &oldValue, const T &newValue) {
  auto it = find(oldValue);
  if (it != end()) *it = newValue;
  return *this;
}
