#pragma once
template <typename T>
class ArrayList<T>::iterator
    : public std::iterator<std::random_access_iterator_tag, T> {
  T* ptr;

  public:
  iterator() : ptr{nullptr} {}
  iterator(T* ptr_) : ptr{ptr_} {}
  T& operator*() { return *ptr; }
  const T& operator*() const { return *ptr; }
  iterator& operator++() {
    ++ptr;
    return *this;
  }
  iterator operator++(int) {
    auto tmp(ptr);
    ++ptr;
    return tmp;
  }
  iterator& operator--() {
    --ptr;
    return *this;
  }
  iterator operator--(int) {
    auto tmp(ptr);
    --ptr;
    return tmp;
  }

  bool operator==(const iterator& other) const { return ptr == other.ptr; }
  bool operator!=(const iterator& other) const { return ptr != other.ptr; }
  bool operator<(const iterator& other) const { return ptr < other.ptr; }
  bool operator>(const iterator& other) const { return ptr > other.ptr; }
  iterator operator+(const int& n) const { return iterator(ptr + n); }
  iterator operator-(const int& n) const { return iterator(ptr - n); }
  iterator& operator+=(const int& n) {
    ptr += n;
    return *this;
  }
  iterator& operator-=(const int& n) {
    ptr -= n;
    return *this;
  }

  int operator-(const iterator& other) const { return ptr - other.ptr; }
};
