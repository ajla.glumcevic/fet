#pragma once
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <functional>
#include "node.hpp"

template <typename T>
class list
{
private:
  Node<T> *head = nullptr;
  Node<T> *tail = nullptr;
  size_t size_ = 0;

public:
  // Constructors
  list() = default;
  list(const list &);
  list(list &&);

  // Ops=
  list &operator=(const list &);
  list &operator=(list &&);

  // Dtor
  ~list() { clear(); }

  // Capacity
  bool empty() const { return size_ == 0; }
  size_t size() const { return size_; }

  // Element access
  T &front() { return head->value; }
  const T &front() const { return head->value; }
  T &back() { return tail->value; }
  const T &back() const { return tail->value; }

  // Iterators
  class iterator;
  class const_iterator;
  iterator begin() { return iterator(head, nullptr); }
  const_iterator cbegin() const { return const_iterator(head, nullptr); }
  iterator end() { return iterator(nullptr, tail); }
  const_iterator cend() const { return const_iterator(nullptr, tail); }

  // Modifiers
  void clear();
  iterator insert(iterator, const T &);
  iterator erase(iterator);
  iterator erase(iterator, iterator);
  void resize(size_t);
  void push_back(const T &);
  void push_front(const T &);
  void pop_back();
  void pop_front();

  // Operations
  void merge(list &);
  void splice(iterator, list &);
  void remove(const T &);
  template <typename UnaryPredicate>
  void remove_if(UnaryPredicate);
  void reverse();
  iterator find(const T &);
  template <typename UnaryPredicate>
  iterator find_if(UnaryPredicate);
  void print() const;
};
#include "iterator.hpp"
#include "list.hxx"
