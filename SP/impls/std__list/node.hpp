template <typename T>
class Node {
  public:
  T value;
  Node *next = nullptr;
  Node *prev = nullptr;

  Node() = default;
  Node(T &&value_, Node *next_ = nullptr, Node *prev_ = nullptr)
      : value{value_}, next{next_}, prev{prev_} {}
  Node(const T &value_, Node *next_ = nullptr, Node *prev_ = nullptr)
      : value{value_}, next{next_}, prev{prev_} {}
};
