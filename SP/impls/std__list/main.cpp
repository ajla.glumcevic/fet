#include <iostream>
#include "list.hpp"
#include <utility>
#include <functional>

int main(void)
{

    list<int> lista;
    lista.push_back(2);
    lista.push_back(4);
    lista.push_back(6);
    lista.push_back(8);


    list<int> other;
    other.push_back(1);
    other.push_back(3);
    other.push_back(5);

    lista.merge(other);


    for(auto it = lista.begin(); it!=lista.end(); ++it) std::cout << *it << std::endl;




    return 0;
}
