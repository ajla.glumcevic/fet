#pragma once

// Copy Constructor
template <typename T>
list<T>::list(const list<T> &other) {
  auto tmp = other.head;
  while (tmp) {
    push_back(tmp->value);
    tmp = tmp->next;
  }
}

// Move Constructor
template <typename T>
list<T>::list(list<T> &&other)
    : head{other.head}, tail{other.tail}, size_{other.size_} {
  other.head = nullptr;
  other.tail = nullptr;
  other.size_ = 0;
}

// Copy assignment
template <typename T>
list<T> &list<T>::operator=(const list &other) {
  if (this != &other) {
    clear();
    auto tmp = other.head;
    while (tmp) {
      push_back(tmp->value);
      tmp = tmp->next;
    }
  }
  return *this;
}

// Move assignment
template <typename T>
list<T> &list<T>::operator=(list &&other) {
  if (this != &other) {
    clear();
    head = other.head;
    tail = other.tail;
    size_ = other.size_;
    other.head = nullptr;
    other.tail = nullptr;
    other.size_ = 0;
  }
  return *this;
}

// Modifiers
template <typename T>
void list<T>::clear() {
  auto tmp = head;
  while (tmp) {
    auto toDelete = tmp;
    tmp = tmp->next;
    delete toDelete;
  }
  head = tail = nullptr;
  size_ = 0;
}

template <typename T>
typename list<T>::iterator list<T>::insert(iterator it, const T &val) {
  if (it.isBegin()) {
    push_front(val);
  } else if (it.isEnd()) {
    push_back(val);
  } else {
    auto newNode = new Node<T>(val);
    newNode->prev = it.previous;
    newNode->next = it.current;
    it.previous->next = newNode;
    it.current->prev = newNode;

    ++size_;
  }
}

template <typename T>
typename list<T>::iterator list<T>::erase(typename list<T>::iterator it) {
  if (it.isBegin())
    pop_front();
  else if (it.isEnd())
    throw std::out_of_range("End!");
  else if (it.current == tail)
    pop_back();
  else {
    it.previous->next = it.current->next;
    it.current->next->prev = it.previous;
    delete it.current;
    --size_;
  }
}

template <typename T>
typename list<T>::iterator list<T>::erase(typename list<T>::iterator it1,
                                          typename list<T>::iterator it2) {
  if (it1.isBegin() && it2.isEnd())
    clear();
  else if (it1.isBegin()) {
    auto tmp = head;
    while (tmp != it2.current->next) {
      tmp = tmp->next;
      delete head;
      --size_;
    }
    head = it2.current;
  } else if (it2.isEnd()) {
    auto tmp = it1.current;
    while (tmp) {
      auto toDelete = tmp;
      tmp = tmp->next;
      delete toDelete;
      --size_;
    }
    tail = it1.previous;
    tail->next = nullptr;
    it2.current = tail;
  } else {
    auto tmp = it1.current;
    it1.previous->next = it2.current;
    it2.current->prev = it1.previous;
    while (tmp != it2.current) {
      auto toDelete = tmp;
      tmp = tmp->next;
      delete toDelete;
      --size_;
    }
  }
  return iterator(
      it2.current,
      it2.current->prev);  // return iterator same position as it1 before erase
}

template <typename T>
void list<T>::resize(size_t newLen) {
  if (newLen < 0)
    throw std::invalid_argument("Negative number!");
  else if (newLen == size_)
    return;
  else if (newLen > size_) {
    auto diff = newLen - size_;
    for (auto i = 0; i != diff; ++i) push_back(T());
  } else {
    auto diff = size_ - newLen;
    for (auto i = 0; i != diff; ++i) pop_back();
  }
}

template <typename T>
void list<T>::push_back(const T &val) {
  auto newNode = new Node<T>(val, nullptr);
  if (empty()) {
    head = tail = newNode;
  } else {
    newNode->prev = tail;
    tail->next = newNode;
    tail = newNode;
  }
  ++size_;
}

template <typename T>
void list<T>::push_front(const T &val) {
  auto newNode = new Node<T>(val);
  if (empty()) {
    head = tail = newNode;
  } else {
    head->prev = newNode;
    newNode->next = head;
    head = newNode;
  }
  ++size_;
}

template <typename T>
void list<T>::pop_back() {
  if (empty())
    throw std::runtime_error("List is empty!");
  else if (size_ == 1) {
    delete head;
    head = tail = nullptr;
  } else {
    auto tmp = tail->prev;
    delete tail;
    tail = tmp;
    tail->next = nullptr;
  }
  --size_;
}

template <typename T>
void list<T>::pop_front() {
  if (empty())
    throw std::runtime_error("List is empty!");
  else if (size_ == 1) {
    head = tail = nullptr;
  } else {
    auto tmp = head->next;
    delete head;
    head = tmp;
    head->prev = nullptr;
  }
  --size_;
}

// Operations
template <typename T>
void list<T>::merge(list &other) {
  iterator begin(head, head->prev);
  iterator end(nullptr, tail);
  iterator o_begin(other.head, other.head->prev);
  iterator o_end(nullptr, other.tail);

  list<T> tmp;

  while (begin != end || o_begin != o_end) {
    if (begin == end) {
      tmp.push_back(*o_begin);
      ++o_begin;
    } else if (o_begin == o_end) {
      tmp.push_back(*begin);
      ++begin;
    } else if (*begin < *o_begin) {
      tmp.push_back(*begin);
      ++begin;
    } else {
      tmp.push_back(*o_begin);
      ++o_begin;
    }
  }

  
  *this = std::move(tmp);


}

template <typename T>
void list<T>::splice(iterator it, list &other) {
  if (other.empty()) return;
  if (it.isBegin()) {
    head->prev = other.tail;
    other.tail->next = head;
    head = other.head;
  } else if (it.isEnd()) {
    tail->next = other.head;
    other.head->prev = tail;
    tail = other.tail;
  } else {
    it.previous->next = other.head;
    other.head->prev = it.previous;
    other.tail->next = it.current;
    it.current->prev = other.tail;
  }

  size_ += other.size_;
  other.head = nullptr;
  other.tail = nullptr;
  other.size_ = 0;
}

template <typename T>
void list<T>::remove(const T &val) {
  if (head->value == val)
    pop_front();
  else if (tail->value == val)
    pop_back();
  else {
    auto it = find(val);
    if (it != end()) erase(it);
  }
}
template <typename T>
template <typename UnaryPredicate>
void list<T>::remove_if(UnaryPredicate predicate) {
  auto it = find_if(predicate);
  if (it != end()) erase(it);
}

template <typename T>
void list<T>::reverse() {
  auto it = begin();
  auto jt = --end();
  for (auto i = 0; i < size_ / 2; ++i, ++it, --jt)
    std::swap(it.current->value, jt.current->value);
}
// for(int i = 0; i < size_/2; ++i){
// auto tmp = head->value;
// head -> value = tail -> value;
// tail->value = tmp -> value;
// head = head->next;
// tail = tail ->prev;
// }

template <typename T>
typename list<T>::iterator list<T>::find(const T &val) {
  return find_if([val](T a) { return a == val; });
}

template <typename T>
template <typename UnaryPredicate>
typename list<T>::iterator list<T>::find_if(UnaryPredicate predicate) {
  auto tmp = head;
  while (tmp) {
    if (predicate(tmp->value)) return iterator(tmp, tmp->prev);
    tmp = tmp->next;
  }
  return end();
}

template <typename T>
void list<T>::print() const {
  auto tmp = head;
  while (tmp) {
    std::cout << tmp->value << " ";
    tmp = tmp->next;
  }
  std::cout << std::endl;
}
