#pragma once
#include <algorithm>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <stdexcept>

template <typename T>
class ArrayList {
  private:
  T *elements_;
  size_t size_;
  size_t capacity_;

  public:
  class iterator;
  class const_iterator;

  ArrayList(int size_ = 100);
  ArrayList(const ArrayList<T> &);
  ArrayList(ArrayList<T> &&);
  ArrayList<T> &operator=(const ArrayList<T> &);
  ArrayList<T> &operator=(ArrayList<T> &&);
  ArrayList(const std::initializer_list<T> &);
  ~ArrayList(void) { delete[] elements_; }

  //  O(n)
  void reserve(int);
  void print() const;
  // O(1)
  bool empty() const;
  bool full() const;
  int size() const { return size_; }
  int capacity() const { return capacity_; }
  T &at(int);
  const T &at(int) const;
  T &front() { return *elements_; }
  const T &front() const { return *elements_; }
  T &back() { return elements_[size_ - 1]; }
  const T &back() const { return elements_[size_ - 1]; }
  T &operator[](int n) { return elements_[n]; }
  const T &operator[](int n) const { return elements_[n]; }

  ArrayList<T> &push_back(const T &);            // O(1)
  ArrayList<T> &pop_back();                      // O(1)
  ArrayList<T> &push_front(const T &);           // O(n)
  ArrayList<T> &pop_front();                     // O(n)
  void insert(const int &, const T &);  // O(n)
  ArrayList<T> &replace(const T &, const T &);   // O(n)

  // Trazeno u zadaci
  // O(1)
  iterator begin() { return iterator(elements_); }
  const_iterator cbegin() const { return const_iterator(elements_); }
  iterator end() { return iterator(elements_ + size_); }
  const_iterator cend() const { return const_iterator(elements_ + size_); }
  // O(n)
  void insert(const const_iterator&, const T &);
  void insert(const iterator&, const T &);
  iterator find(const T &);
  void remove(const T &);
  void remove(const  iterator &);
  iterator find_if(const std::function<bool(const T &)> &);
  void remove_if(const std::function<bool(const T &)> &);
  ArrayList<T> &invert();
};

#include "ArrayList.hxx"
#include "const_iterator.hpp"
#include "iterator.hpp"
