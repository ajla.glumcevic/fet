#pragma once
template <typename T>
class ArrayList<T>::const_iterator
    : public std::iterator<std::random_access_iterator_tag, T> {
  const T* ptr;

  public:
  const_iterator() : ptr{nullptr} {}
  const_iterator(const T* ptr_) : ptr{ptr_} {}
  const T& operator*() const { return *ptr; }
  const_iterator& operator++() {
    ++ptr;
    return *this;
  }
  const_iterator operator++(int) {
    auto tmp(ptr);
    ++ptr;
    return tmp;
  }
  const_iterator& operator--() {
    --ptr;
    return *this;
  }
  const_iterator operator--(int) {
    auto tmp(ptr);
    --ptr;
    return tmp;
  }

  bool operator==(const const_iterator& other) const {
    return ptr == other.ptr;
  }
  bool operator!=(const const_iterator& other) const {
    return ptr != other.ptr;
  }
  bool operator<(const const_iterator& other) const { return ptr < other.ptr; }
  bool operator>(const const_iterator& other) const { return ptr > other.ptr; }
  const_iterator operator+(const int& n) const {
    return const_iterator(ptr + n);
  }
  const_iterator operator-(const int& n) const {
    return const_iterator(ptr - n);
  }
  const_iterator& operator+=(const int& n) {
    ptr += n;
    return *this;
  }
  const_iterator& operator-=(const int& n) {
    ptr -= n;
    return *this;
  }

  int operator-(const const_iterator& other) const { return ptr - other.ptr; }
};
