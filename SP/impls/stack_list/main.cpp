#include <iostream>
#include "stack.hpp"

int main(void) {
  Stack<int> s;
  s.push(1);
  s.push(2);
  s.push(3);

  Stack<int> s1;
  s1 = std::move(s);
  std::cout << s.size() << std::endl;
  std::cout << s1.size() << std::endl;
  std::cout << s1.pop() << std::endl;
  std::cout << s1.pop() << std::endl;
  std::cout << s1.pop() << std::endl;
  return 0;
}
