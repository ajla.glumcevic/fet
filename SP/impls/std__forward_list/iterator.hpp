#pragma once
#include <iterator>
class fList<T>::iterator
{
private:
  Node<T> *current = nullptr;
  Node<T> *prev = nullptr; // for erase()

public:
  friend class fList;

  // Constructors
  iterator() = default;
  iterator(Node<T> &current_, Node<T> &prev_)
      : current{current_}, prev{prev_} {}

  // Forward ops
  T &operator*() const { return current->value; }
  const T &operator*() const { return current->value; }

  iterator &operator++()
  {
    prev = current;
    if (current)
      current = current->next;
    return *this;
  }

  iterator operator++(int)
  {
    iterator tmp = *this;
    ++(*this);
    return tmp;
  }

  bool operator==(const iterator &other) const
  {
    return current == other.current;
  }
  bool operator!=(const iterator &other) const
  {
    return current != other.current;
  }

  // Methods

  bool isBegin(const iterator &it) const { return current == head; }
  bool isEnd(const iterator &it) const { return ++current == nullptr; };
}