#pragma once
#include <cstddef>     // for size_t
#include <functional>  // for comparison function
#include <iostream>
#include <utility>  // for std::move

// #include "iterator.hpp"
#include "node.hpp"
template <typename T>
class fList {
  private:
  Node<T>* head = nullptr;
  Node<T>* tail = nullptr;
  size_t size_ = 0;

  public:
  // Constructors
  fList() = default;
  fList(const fList&);
  fList(fList&&);
  // Ops=
  fList& operator=(const fList&);
  fList& operator=(fList&&);
  // Destructor O(n)
  ~fList() { clear(); }

  // O(1)
  inline size_t size() const { return size_; };
  inline bool empty() const { return size_ == 0; }
  inline T& front() { return head->value; };
  inline const T& front() const { return head->value; };
  inline T& back() { return tail->value; }
  inline const T& back() const { return tail->value; }

  void push_back(const T&);   // O(1)
  void push_front(const T&);  // O(1)
  void pop_back();            // O(n)
  void pop_front();           // O(1)
  void clear();               // O(n)
  void print() const;         // O(n)

  class iterator;
  inline iterator begin() const { return iterator(head, nullptr); } // O(1)
  inline iterator end() const { return iterator(nullptr, tail); } // O(1)

  template <typename U>
  iterator find(U); // O(n)
  iterator find(const T&); // O(n)

  template <typename U>
  void transform(U); // O(n)

  void erase(iterator); // O(1)
  void splice(iterator, fList&); // O(1)
};
//////////////////////////////////////////////////////////////////////
template <typename T>
class fList<T>::iterator {
  private:
  Node<T>* current = nullptr;
  Node<T>* prev = nullptr;  // for erase()

  public:
  friend class fList;

  // Constructors
  iterator() = default;
  iterator(Node<T>* current_ = nullptr, Node<T>* prev_ = nullptr)
      : current{current_}, prev{prev_} {}

  // Forward ops
  T& operator*() { return current->value; }
  const T& operator*() const { return current->value; }

  iterator& operator++() {
    prev = current;
    if (current) current = current->next;
    return *this;
  }

  iterator operator++(int) {
    iterator tmp = *this;
    ++(*this);
    return tmp;
  }

  bool operator==(const iterator& other) const {
    return current == other.current;
  }
  bool operator!=(const iterator& other) const {
    return current != other.current;
  }

  // Methods
  bool isBegin() const { return current != nullptr && prev == nullptr; }
  bool isEnd() const { return current == nullptr && prev !=nullptr; }
};
///////////////////////////////////////////////////////////////////////////

// Copy cons
template <typename T>
fList<T>::fList(const fList& other) {
  auto tmp = other.head;
  while (tmp) {
    push_back(tmp->value);
    tmp = tmp->next;
  }
}

// Move cons
template <typename T>
fList<T>::fList(fList&& other)
    : head{other.head}, tail{other.tail}, size_{other.size_} {
  other.head = nullptr;
  other.tail = nullptr;
  other.size_ = 0;
}
// Copy op=
template <typename T>
fList<T>& fList<T>::operator=(const fList& other) {
  if (this != &other) {
    auto tmp = other.head;
    while (tmp) {
      push_back(tmp->value);
      tmp = tmp->next;
    }
  }
  return *this;
}
// Move op=
template <typename T>
fList<T>& fList<T>::operator=(fList&& other) {
  if (this != &other) {
    head = other.head;
    tail = other.tail;
    size_ = other.size_;
    other.head = nullptr;
    other.tail = nullptr;
    other.size_ = 0;
  }
  return *this;
}

template <typename T>
void fList<T>::push_back(const T& val) {
  auto newNode = new Node<T>(val);
  if (empty()) {
    head = tail = newNode;
  } else {
    tail->next = newNode;
    tail = newNode;
  }
  ++size_;
}

template <typename T>
void fList<T>::push_front(const T& val) {
  auto newNode = new Node<T>(val, head);
  if (empty()) {
    head = tail = newNode;
  } else {
    head = newNode;
  }
  ++size_;
}

template <typename T>
void fList<T>::pop_back() {
  if (empty())
    return;
  else if (size_ == 1) {
    delete tail;
    head = tail = nullptr;
  } else {
    auto tmp = head;
    while (tmp->next != tail) {
      tmp = tmp->next;
    }
    delete tail;
    tmp->next = nullptr;
    tail = tmp;
  }
  --size_;
}

template <typename T>
void fList<T>::pop_front() {
  if (empty())
    return;
  else if (size_ == 1) {
    delete head;
    head = tail = nullptr;
  } else {
    auto tmp = head->next;
    delete head;
    head = tmp;
  }
  --size_;
}

template <typename T>
void fList<T>::clear() {
  auto tmp = head;
  while (tmp) {
    auto toDelete = tmp;
    tmp = tmp->next;
    delete toDelete;
  }
  head = tail = nullptr;
  size_ = 0;
}

template <typename T>
template <typename U>
typename fList<T>::iterator fList<T>::find(U predicate) {
  // Version 1 iterator
  for (auto it = begin(); it != end(); ++it)
    if (predicate(*it)) return it;
  // Version 2 cannot convert node int* to nullptr in assignment
  // auto tmp = head;
  // auto prevTmp = nullptr;
  // while (tmp) {
  //   if (predicate(tmp->value)) return iterator(tmp, prevTmp);
  //   prevTmp = tmp;
  //   tmp = tmp->next;
  // }
  return end();
}

template <typename T>
typename fList<T>::iterator fList<T>::find(const T& val) {
  for (auto it = begin(); it != end(); ++it)
    if (*it == val) return it;

  return end();
}

template <typename T>
template <typename U>
void fList<T>::transform(U predicate) {
  for (auto it = begin(); it != end(); ++it) predicate(*it);
}

template <typename T>
void fList<T>::erase(typename fList<T>::iterator it) {
  if (it.isEnd() || empty())
    return;
  else if (it.isBegin())
    pop_front();
  else if (it.current == tail)
    pop_back();
  else {
    it.prev->next = it.current->next;
    delete it.current;
    --size_;
  }
}

template <typename T>
void fList<T>::splice(typename fList<T>::iterator it, fList<T>& other) {
  if (other.empty()) return;
  if (it.isBegin()) {
    other.tail->next = head;
    head = other.head;
  } else if (it.isEnd()) {
    tail->next = other.head;
    tail = other.tail;
  } else {
    it.prev->next = other.head;
    other.tail->next = it.current;
  }
  size_  = size_+ other.size_;
  other.head = nullptr;
  other.tail = nullptr;
  other.size_ = 0;
}

template <typename T>
void fList<T>::print() const {
  auto tmp = head;
  while (tmp) {
    std::cout << tmp->value << " ";
    tmp = tmp->next;
  }
  std::cout << std::endl;
}
