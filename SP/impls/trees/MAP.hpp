#include <algorithm>
#include <initializer_list>
#include <iostream>
#include <queue>
template <typename Key, typename Value>
class pair {
  public:
  Key key{};
  Value value{};
  pair<Key, Value>* leftChild = nullptr;
  pair<Key, Value>* rightChild = nullptr;
  pair(const Key& k, const Value& v)
      : key{k}, value{v}, leftChild{nullptr}, rightChild{nullptr} {}
};

template <typename Key, typename Value>
class map {
  private:
  pair<Key, Value>* root = nullptr;
  size_t size_ = 0;

  template <typename U>
  bool insertPair(pair<Key, Value>*& root, U&& p) {
    if (root == nullptr) {
      root = new pair<Key, Value>(std::forward<U>(p));
      ++size_;
      return true;
    }
    if (root->key == p.key) {
      return false;
    }
    if (root->key < p.key)
      return insertPair(root->rightChild, p);
    else
      return insertPair(root->leftChild, p);
  }

  void printInorder(pair<Key, Value>* root) const {
    if (root == nullptr) return;
    printInorder(root->leftChild);
    std::cout << root->key << " " << root->value << std::endl;
    printInorder(root->rightChild);
  }
  void printPostorder(pair<Key, Value>* root) const {
    if (root == nullptr) return;
    printPostorder(root->leftChild);
    printPostorder(root->rightChild);
    std::cout << root->key << " ";
  }
  void printPreorder(pair<Key, Value>* root) const {
    if (root == nullptr) return;
    std::cout << root->key << " ";
    printPreorder(root->leftChild);
    printPreorder(root->rightChild);
  }

  // O(logn)
  pair<Key, Value>* findPair(const Key& k) {
    while (root) {
      if (root->key == k)
        return root;
      else if (root->key < k)
        root = root->rightChild;
      else
        root = root->leftChild;
    }
    return root;
  }

  // O(n)
  void copy(pair<Key, Value>*& root, pair<Key, Value>* other_root) {
    if (other_root == nullptr) return;
    root = new pair<Key, Value>(other_root->key, other_root->value);
    copy(root->leftChild, other_root->leftChild);
    copy(root->rightChild, other_root->rightChild);
  }

  void clear(pair<Key, Value>*& root) {
    if (root == nullptr) return;
    clear(root->leftChild);
    clear(root->rightChild);
    delete root;
  }

  //////////////////////////////////////////////////////////////////////////////
  public:
  map(const std::initializer_list<Key>& l) {
    for (auto i = std::begin(l); i != std::end(l); ++i) {
      auto newPair = pair<Key, Value>(*i, Value{});
      insert(newPair);
    }
  }

  map() = default;
  // O(n)
  map(const map& other) : size_{other.size_} { copy(other); }
  // O(1)
  map(map&& other) : root{other.root}, size_{other.size_} {
    other.root = nullptr;
    other.size_ = 0;
  }
  // O(n)
  map& operator=(const map& other) {
    if (this != &other) clear();
    return *this = map<Key, Value>(other);
  }
  // O(1)
  map& operator=(map&& other) {
    if (this != &other) {
      clear();
      root = other.root;
      size_ = other.size_;
      other.root = nullptr;
      other.size_ = 0;
    }
    return *this;
  }
  // O(n)
  ~map() { clear(); }
  // O(logn) ////////////////////////////////////////////////////////////////
  template <typename U>
  bool insert(U&& p) {
    return insertPair(root, p);
  }
  // O(n) ///////////////////////////////////////////////////////////////////
  void clear() {
    clear(root);
    size_ = 0;
    root = nullptr;
  }
  // O(logn) ////////////////////////////////////////////////////////////
  Value& find(const Key& k) {
    auto pairPtr = findPair(k);
    if (pairPtr) return pairPtr->value;
    throw std::runtime_error("Pair not found.");
  }
  // O(1) ///////////////////////////////////////////////////////////
  bool empty() const { return root == nullptr; }
  size_t size() const { return size_; }
  // O(n) ///////////////////////////////////////////////////////////////
  void print() const {
    std::cout << "Inorder: " << std::endl;
    printInorder(root);
    std::cout << std::endl;
    std::cout << "Preorder: " << std::endl;
    printPreorder(root);
    std::cout << std::endl;
    std::cout << "Postorder: " << std::endl;
    printPostorder(root);
    std::cout << std::endl;
  }
  // O(logn) ////////////////////////////////////////////////////////////
  Value& operator[](const Key& k) {
    auto pairPtr = findPair(k);
    if (pairPtr) return pairPtr->value;

    insert(pair<Key, Value>(k, Value{}));
    return find(k);
  }
  // O(n) //////////////////////////////////////////////////////////////
  void copy(const map& other) { copy(root, other.root); }
  // O(logn) ///////////////////////////////////////////////////////////
  bool erase(const Key& k) {
    // find pair
    auto current = root;
    auto parent = root;
    while (current) {
      if (current->key == k) break;
      parent = current;
      if (current->key < k)
        current = current->rightChild;
      else
        current = current->leftChild;
    }
    if (current == nullptr) return false;

    // has 2 children
    if (current->leftChild && current->rightChild) {
      auto npp = current->leftChild;
      parent = current;
      while (npp->rightChild) {
        parent = npp;
        npp = npp->rightChild;
      }
      current->key = npp->key;
      current->value = std::move(npp->value);
      current = npp;
    }

    // has leftChild
    if (current->leftChild && current->rightChild == nullptr) {
      if (parent->leftChild == current)
        parent->leftChild = current->leftChild;
      else
        parent->rightChild = current->leftChild;
      // has rightChild
    } else if (current->leftChild == nullptr && current->rightChild) {
      if (parent->leftChild == current)
        parent->leftChild = current->rightChild;
      else
        parent->rightChild = current->rightChild;
      // has 0 children
    } else {
      if (parent->leftChild == current)
        // current->leftChild jer radim sa npp
        parent->leftChild = current->leftChild;
      else
        parent->rightChild = current->leftChild;
    }
    delete current;
    --size_;
    return true;
  }

  // BROJI KOLIKO IMA LISTOVA//////////////////////
  // public
  size_t countLeafs() const { return countLeafs(root); }
  // privatni metodi
  size_t countLeafs(pair<Key, Value>* root, size_t count = 0) const {
    if (root == nullptr) return count;
    if (isLeaf(root)) return count + 1;
    return countLeafs(root->leftChild, count) +
           countLeafs(root->rightChild, count);
  }

  bool isLeaf(pair<Key, Value>* root) const {
    if (root->leftChild == nullptr && root->rightChild == nullptr)
      return true;
    else
      return false;
  }
  ///////////////////////////////////////////////////

  // DA LI JE CVOT S OVIM KLJUCEM LIST ///////////////////////
  bool isLeaf(const Key& k) {
    auto tmp = root;
    while (tmp) {
      if (k == tmp->key) {
        if (tmp->leftChild == nullptr && tmp->rightChild == nullptr)
          return true;
        else
          return false;
      }
      if (tmp->key < k)
        tmp = tmp->rightChild;
      else
        tmp = tmp->leftChild;
    }
    return false;
  }
  //////////////////////////////////////////////

  // BREADTH TRAVERSAL //////////////////////////////////////////
  void breadthPrint() const { breadth(root); }
  void breadth(pair<Key, Value>* root) const {
    std::cout << "Breadth: " << std::endl;
    std::queue<pair<Key, Value>*> q;
    if (root == nullptr) return;
    q.push(root);
    while (!q.empty()) {
      auto tmp = q.front();
      std::cout << tmp->key << " ";
      q.pop();
      if (tmp->leftChild != nullptr) q.push(tmp->leftChild);
      if (tmp->rightChild != nullptr) q.push(tmp->rightChild);
    }
  }
  /////////////////////////////////////////////////////

  int sumaParnih() { return sumaParnih(root); }

  int sumaParnih(pair<Key, Value>* root) {
    if (root == nullptr) return 0;
    if ((root->value) % 2 == 0)
      return root->key + sumaParnih(root->leftChild) +
             sumaParnih(root->rightChild);
    return sumaParnih(root->leftChild) + sumaParnih(root->rightChild);
  }
};
