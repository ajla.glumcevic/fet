#pragma once
#include <initializer_list>
#include <iostream>
template <typename T>
class heap {
  private:
  size_t capacity_;
  size_t size_;
  size_t first;
  T* elements;

  void bottomUp(size_t index) {
    for (auto i = index; i > first; i /= 2) {
      if (elements[i] > elements[i / 2])
        std::swap(elements[i], elements[i / 2]);
      print();
    }
  }

  void topDown(size_t index) {
    for (auto i = index; i * 2 <= size_;) {
      if (elements[i * 2] > elements[i]) {
        if (i * 2 + 1 <= size_ && elements[i * 2 + 1] > elements[i * 2]) {
          std::swap(elements[i], elements[i * 2 + 1]);
          i = i * 2 + 1;
        } else {
          std::swap(elements[i], elements[i * 2]);
          i = i * 2;
        }
      } else if (i * 2 + 1 <= size_ && elements[i * 2 + 1] > elements[i * 2]) {
        std::swap(elements[i], elements[i * 2 + 1]);
        i = i * 2 + 1;
      } else
        break;
      print();
    }
  }

  void makeheap() {
    for (int i = size_ / 2; i >= 1; --i) {
      print();
      topDown(i);
    }
  }

  public:
  heap(const std::initializer_list<T>& l)
      : capacity_{100}, size_{l.size()}, elements{new T[l.size()]} {
    std::copy(std::begin(l), std::end(l), elements + 1);
    makeheap();
  }
  heap() : capacity_{100}, size_{0}, first{1}, elements{new T[capacity_]} {}
  heap(const size_t& cap)
      : capacity_{cap}, size_{0}, first{1}, elements{new T[capacity_]} {}
  ~heap() { delete[] elements; }

  bool empty() const { return size_ == 0; }
  bool full() const { return size_ + 1 == capacity_; }
  size_t size() const { return size_; }

  // O(logn)
  template <typename U>
  bool insert(U&& el) {
    if (full()) throw std::runtime_error("Full");
    elements[++size_] = std::forward<U>(el);
    print();
    bottomUp(size_);
  }

  void clear() {
    for (auto i = 1; size() != 0; ++i) {
      erase();
    }
  }

  // O(logn)
  T erase() {
    if (empty()) throw std::runtime_error("Empty");
    print();
    std::swap(elements[1], elements[size_]);
    print();
    size_--;
    topDown(1);

    return elements[size_ + 1];
  }

  void merge(const heap<T>& other) {
    for (auto i = 1; i <= other.size(); ++i) insert(other.elements[i]);
  }

  void print() const {
    for (int i = 1; i <= size_; ++i) std::cout << elements[i] << " ";
    std::cout << std::endl;
  }
};
