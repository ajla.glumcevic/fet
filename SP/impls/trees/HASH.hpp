#pragma once
#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

template <typename TypeKey, typename TypeVal>
class hash {
  private:
  using pair = std::pair<TypeKey, TypeVal>;
  using bucket = std::list<pair>;
  using vector = std::vector<bucket>;

  const size_t size_ = 1000;
  vector buckets;

  public:
  hash() { buckets.resize(size_); }

  void insert(const pair& p) {
    auto index = std::hash<TypeKey>()(p.first);
    auto& bucket = buckets[index];
    bucket.push_back(p);
  }

  bool erase(const TypeKey& k) {
    auto index = std::hash<TypeKey>()(k);
    auto& bucket = buckets[index];
    auto pairPtr = std::find_if(std::begin(bucket), std::end(bucket),
                                [k](const pair& p) { return p.first == k; });
    if (pairPtr != std::end(bucket)) {
      bucket.erase(pairPtr);
      return true;
    }
    return false;
  }

  TypeVal* find(const TypeKey& k) {
    auto index = std::hash<TypeKey>()(k);
    auto& bucket = buckets[index];
    auto pairPtr = std::find_if(std::begin(bucket), std::end(bucket),
                                [k](const pair& p) { return p.first == k; });
    if (pairPtr != std::end(bucket)) return &pairPtr->second;
    return nullptr;
  }

  TypeVal& operator[](const TypeKey& k) {
    auto index = std::hash<TypeKey>()(k);
    auto& bucket = buckets[index];
    auto pairPtr = std::find_if(std::begin(bucket), std::end(bucket),
                                [k](const pair& p) { return p.first == k; });
    if (pairPtr != std::end(bucket)) return pairPtr->second;
  }
};

