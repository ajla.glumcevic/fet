#pragma once
#include "sorted_list.hpp"
#include "to_do_task.hpp"
typedef sorted_list<ToDoTask> list;

void newTask(list &);
void showAllTasks(const list &);
void showMaxPriority(const list &);
void showMinPriority(const list &);
void markAsCompleted(list &);
int printMenu();
