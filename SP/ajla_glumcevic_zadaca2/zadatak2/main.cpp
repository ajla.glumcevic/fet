#include <iostream>
#include <string>
#include "functions.hpp"
#include "sorted_list.hpp"
#include "to_do_task.hpp"

int main(void) {
  list taskList;

  while (1) {
    try {
      auto myChoice = printMenu();

      if (myChoice == 1) {
        newTask(taskList);
      } else if (myChoice == 2) {
        showAllTasks(taskList);
      } else if (myChoice == 3) {
        markAsCompleted(taskList);
      } else if (myChoice == 4) {
        showMaxPriority(taskList);
      } else if (myChoice == 5) {
        showMinPriority(taskList);
      } else {
        return 0;
      }
    } catch (std::string& s) {
      std::cout << s << std::endl;
    }
  }

  return 0;
}

