#include "to_do_task.hpp"

int generateId()
{
  static int counter = 0;
  return ++counter;
}

ToDoTask::ToDoTask()
{
  std::cout << "\nEnter title: ";
  std::getline(std::cin, title);
  std::cout << "\nEnter description:";
  std::getline(std::cin, description);
  std::cout << "\nEnter priority:";
  std::cin >> priority;
  id = std::to_string(generateId());
}

ToDoTask::ToDoTask(const std::string &id_) : id{id_} {}


bool ToDoTask::operator<(const ToDoTask &other) const { return priority < other.priority; }
bool ToDoTask::operator>(const ToDoTask &other) const { return priority > other.priority; }
bool ToDoTask::operator==(const ToDoTask &other) const { return id==other.id; }

std::ostream &operator<<(std::ostream &out, const ToDoTask &task)
{
  out << "\nID: " << task.id << "\nTitle: " << task.title
      << "\nDescription: " << task.description
      << "\nPriority: " << task.priority;
  return out;
}
std::istream &operator>>(std::istream &in, ToDoTask &task)
{
  in >> task.id >> task.title >> task.description >> task.priority;
  return in;
}
