#pragma once

#include <string>
#include <iostream>

struct ToDoTask
{
  std::string id;
  std::string title;
  std::string description;
  int priority;

  ToDoTask();
  ToDoTask(const std::string &);
  bool operator<(const ToDoTask &) const;
  bool operator>(const ToDoTask &) const;
  bool operator==(const ToDoTask &) const;
};
std::ostream &operator<<(std::ostream &, const ToDoTask &);
std::istream &operator>>(std::istream &, ToDoTask &);
