#pragma once
template <typename T>
class sorted_list<T>::const_iterator
    : public std::iterator<std::bidirectional_iterator_tag, T> {
  private:
  const Node<T> *current = nullptr;
  const Node<T> *previous = nullptr;

  public:
  friend class sorted_list;

  const_iterator() = default;
  const_iterator(Node<T> *current_ = nullptr, Node<T> *previous_ = nullptr)
      : current{current_}, previous{previous_} {}

  const T &operator*() const { return current->value; }

  const_iterator &operator++() {
    previous = current;
    if (current) current = current->next;
    return *this;
  }
  const_iterator operator++(int) {
    const_iterator tmp = *this;
    ++(*this);
    return tmp;
  }
  const_iterator &operator--() {
    current = previous;
    if (current) {
      previous = current->prev;
    }
    return *this;
  }
  const_iterator operator--(int) {
    const_iterator tmp = *this;
    --(*this);
    return tmp;
  }

  bool operator==(const const_iterator &other) const {
    return current == other.current;
  }
  bool operator!=(const const_iterator &other) const {
    return current != other.current;
  }

  bool isBegin() const { return current != nullptr && previous == nullptr; }
  bool isEnd() const { return current == nullptr && previous != nullptr; }
};
