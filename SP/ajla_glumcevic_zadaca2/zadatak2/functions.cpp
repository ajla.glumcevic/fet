#include "functions.hpp"

int printMenu()
{
  std::cout << "Enter choice:" << std::endl;
  std::cout << "1.  New task" << std::endl;
  std::cout << "2.  Show all tasks" << std::endl;
  std::cout << "3.  Mark task as completed" << std::endl;
  std::cout << "4.  Show tasks with max priority" << std::endl;
  std::cout << "5.  Show tasks with min priority" << std::endl;
  std::cout << "6.  Quit" << std::endl;
  std::cout << "Your choice:";
  int choice;
  std::cin >> choice;
  std::cin.ignore();
  if (choice < 1 || choice > 6)
    throw std::string("Wrong input, please try again!");

  return choice;
}

void newTask(list &taskList)
{
  ToDoTask newTask;
  taskList.add(newTask);
}

void showAllTasks(const list &taskList)
{
  if (taskList.empty())
    std::cout << "You don't have any tasks on the list." << std::endl;
  for (auto it = taskList.cbegin(); it != taskList.cend(); ++it)
    std::cout << *it << std::endl;
  std::cout << std::endl;
}

void showMinPriority(const list &taskList)
{
  if (taskList.size() < 3)
    showAllTasks(taskList);
  else
  {
    auto it = --taskList.cend();
    for (auto i = 0; i < 3; ++i, --it)
      std::cout << *it << std::endl;
    std::cout << std::endl;
  }
}

void showMaxPriority(const list &taskList)
{
  if (taskList.size() < 3)
    showAllTasks(taskList);
  else
  {
    auto it = taskList.cbegin();
    for (auto i = 0; i < 3; ++i, ++it)
      std::cout << *it << std::endl;
    std::cout << std::endl;
  }
}

void markAsCompleted(list &taskList)
{
  std::string taskID;
  std::cout << "Enter task ID:";
  std::cin >> taskID;
  // Koristeci find_if
  // auto result =
  //     taskList.find_if([taskID](ToDoTask task) { return task.id == taskID; });

  // Koristeci find i konstruktor ToDoTask(const std::string&);
  auto result = taskList.find(ToDoTask{taskID});
  taskList.remove(result);
}
