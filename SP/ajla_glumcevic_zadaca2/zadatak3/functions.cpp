#include "functions.hpp"

auto enterLocation(Vector_Station &stations) {
  std::cout << "Enter location where station is placed: ";
  std::string location;
  std::getline(std::cin, location);
  // if not found in stations vector, returns stations.end()
  auto it = std::find_if(
      stations.begin(), stations.end(),
      [location](const Station &s) { return s.stationName == location; });

  return it;
}

size_t windowColumn() {
  winsize sz;
  ioctl(0, TIOCGWINSZ, &sz);
  return sz.ws_col;
}

int printMenu() {
  std::string title = " Historical temperature information ";
  const std::string stars =
      std::string((windowColumn() - title.size()) / 2, '*');
  std::cout << stars << title << stars << std::endl;
  std::cout << "\t1. Get information for entire year" << std::endl;
  std::cout << "\t2. Get information for specific month" << std::endl;
  std::cout << "\t3. Insert new historical temperature information"
            << std::endl;
  std::cout << "\t4. Exit" << std::endl;
  std::cout << "Your choice: ";
  int choice;
  std::cin >> choice;
  std::cin.clear();
  std::cin.ignore();

  if (choice < 1 || choice > 4) throw std::string("Wrong input, try again!");
  return choice;
}

void loadTemperatures(const std::string &filename, List_Date &dates) {
  std::ifstream input(filename);
  if (!input.is_open()) throw std::string("Couldn't open file!");

  std::string string_, token;
  std::getline(input, string_, '\n');  // first line

  while (std::getline(input, string_, '\n')) {
    std::vector<std::string> vectorInfo;
    Station::Date d;
    std::stringstream ss(string_);

    while (std::getline(ss, token, ',')) vectorInfo.push_back(token);

    auto date = vectorInfo[1];
    auto pos = date.find('-');
    d.year = std::stoi(date.substr(0, pos));
    d.month = std::stoi(date.erase(0, pos + 1));

    // check if there is max temp for this date
    // otherwise default value is "N\\A"
    if (vectorInfo[2] != "") d.Tmax = vectorInfo[2];

    // check if there is min temp for this date
    // otherwise default value is "N\\A"
    if (vectorInfo.size() == 4) d.Tmin = vectorInfo[3];

    dates.push_back(d);
  }

  input.close();
}

void loadStationsFile(Vector_Station &stations) {
  std::ifstream input("stations.csv");
  if (!input.is_open()) throw std::string("Couldn't open file!");

  std::string string_, token;
  std::getline(input, string_, '\n');  // first line

  while (std::getline(input, string_, '\n')) {
    std::vector<std::string> vectorInfo;
    Station s;
    std::stringstream ss(string_);

    while (std::getline(ss, token, ',')) vectorInfo.push_back(token);

    s.id = vectorInfo[0];
    s.latitude = std::stod(vectorInfo[1]);
    s.longitude = std::stod(vectorInfo[2]);
    s.elevation = std::stod(vectorInfo[3]);
    s.stationName = vectorInfo[4];

    stations.push_back(s);
  }

  input.close();
}

void printYear(List_Date::iterator &it) {
  std::vector<std::string> vectorMonths{"Jan",  "Feb", "Mar", "Apr",
                                        "May",  "Jun", "Jul", "Aug",
                                        "Sept", "Oct", "Nov", "Dec"};
  std::cout << "Month\t\tMax temp\tMin temp" << std::endl;
  for (auto i = 0; i < vectorMonths.size(); ++i) {
    if (it->month != i + 1)
      std::cout << vectorMonths[i] << "\t\tN\\A\t\tN\\A" << std::endl;
    else {
      std::cout << vectorMonths[i] << "\t\t" << it->Tmax << "\t\t" << it->Tmin
                << std::endl;
      ++it;
    }
  }
}

void option1(Vector_Station &stations) {
  auto foundStationIt = enterLocation(stations);
  if (foundStationIt == stations.end()) return;

  foundStationIt->printStationInfo();  // printing information

  // only load from file if not loaded before
  if (foundStationIt->dates.empty())
    loadTemperatures(foundStationIt->stationName + ".csv",
                     foundStationIt->dates);

  std::cout << "\nEnter year: ";
  size_t year;
  std::cin >> year;
  std::cin.clear();
  std::cin.ignore();

  auto dateIt =
      std::find_if(foundStationIt->dates.begin(), foundStationIt->dates.end(),
                   [year](const Station::Date &d) { return d.year == year; });
  if (dateIt == foundStationIt->dates.end())
    std::cout << "Year not avaliable." << std::endl;
  else
    printYear(dateIt);
}

void option2(Vector_Station &stations) {
  auto foundStationIt = enterLocation(stations);
  if (foundStationIt == stations.end()) return;

  foundStationIt->printStationInfo();

  // only load from file if not loaded before
  if (foundStationIt->dates.empty())
    loadTemperatures(foundStationIt->stationName + ".csv",
                     foundStationIt->dates);

  std::cout << "\nEnter year: ";
  size_t year;
  std::cin >> year;
  std::cout << "Enter month: ";
  size_t month;
  std::cin >> month;
  std::cin.clear();
  std::cin.ignore();
  auto dateIt =
      std::find_if(foundStationIt->dates.begin(), foundStationIt->dates.end(),
                   [year, month](const Station::Date &d) {
                     return d.year == year && d.month == month;
                   });
  if (dateIt == foundStationIt->dates.end())
    std::cout << "Max temp\tMin Temp\nN\\A\t\tN\\A" << std::endl;
  else
    dateIt->printMonth();
}

void option3(Vector_Station &stations) {
  std::string id, tmax, tmin;
  size_t year, month;

  std::cout << "Enter stationID: ";
  std::cin >> id;

  // if not found in file, returns to main menu
  auto foundStationIt =
      std::find_if(stations.begin(), stations.end(),
                   [id](const Station &s) { return s.id == id; });
  if (foundStationIt == stations.end()) return;

  if (foundStationIt->dates.empty())
    loadTemperatures(foundStationIt->stationName + ".csv",
                     foundStationIt->dates);

  std::cout << "Enter year: ";
  std::cin >> year;
  std::cout << "Enter month: ";
  std::cin >> month;
  std::cout << "Enter MAX temperature: ";
  std::cin >> tmax;
  std::cout << "Enter MIN temperature: ";
  std::cin >> tmin;

  // search for date
  auto dateIt =
      std::find_if(foundStationIt->dates.begin(), foundStationIt->dates.end(),
                   [year, month](const Station::Date &d) {
                     return (d.year == year && d.month == month);
                   });

  // if date isn't found in dates
  if (dateIt == foundStationIt->dates.end()) {
    // create new Date
    Station::Date newDate{year, month, tmax, tmin};
    // find first Date with the same year
    auto newDateIt = std::find_if(
        foundStationIt->dates.begin(), foundStationIt->dates.end(),
        [year](const Station::Date &d) { return (d.year == year); });

    // do at least 12 iterations to find where to insert newDate
    for (auto i = 0; i < 12; ++i) {
      // corner case if newDate month is December
      if (newDateIt->year > year) {
        (foundStationIt->dates).insert(--newDateIt, newDate);
        // for other months insert when newDate month greater than one in list
      } else if (newDateIt->month > month) {
        (foundStationIt->dates).insert(newDateIt, newDate);
        break;
      } else
        ++newDateIt;
    }
    // if found in dates, change max and min temps
  } else {
    dateIt->Tmax = tmax;
    dateIt->Tmin = tmin;
  }
}
