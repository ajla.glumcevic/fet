#include <iostream>
#include <string>
#include <vector>
#include "functions.hpp"

int main(void) {
  std::vector<Station> stations;
  loadStationsFile(stations);

  while (1) {
    try {
      auto choice = printMenu();

      if (choice == 1)
        option1(stations);
      else if (choice == 2)
        option2(stations);
      else if (choice == 3)
        option3(stations);
      else
        return 0;
    } catch (std::string& s) {
      std::cout << s << std::endl;
    }
  }

  return 0;
}
