
#include <sys/ioctl.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>
#include <string>
#include <vector>
#include "structures.hpp"

typedef std::vector<Station> Vector_Station;
typedef std::list<Station::Date> List_Date;

auto enterLocation(Vector_Station&);
void setMonthsNames(List_Date&);
size_t windowColumn();
int printMenu();
void loadTemperatures(const std::string&, List_Date&);
void loadStationsFile(Vector_Station&);
void printYear(List_Date::iterator&);
void option1(Vector_Station&);
void option2(Vector_Station&);
void option3(Vector_Station&);
