#include <iostream>
#include <list>
#include <string>


struct Station {
  struct Date {
    size_t year;
    size_t month;
    std::string Tmax = "N\\A";
    std::string Tmin = "N\\A";

    inline void printMonth() {
      std::cout << "Max temp\tMin temp" << std::endl;
      std::cout << Tmax << "\t\t" << Tmin << std::endl;
    }
  };

  std::string id;
  double latitude;
  double longitude;
  double elevation;
  std::string stationName;
  std::list<Date> dates; // using std::list for insert O(1)

  inline void printStationInfo() const {
    std::cout << "StationID: " << id << std::endl;
    std::cout << "Lattitude: " << latitude << std::endl;
    std::cout << "Longitude: " << longitude << std::endl;
    std::cout << "Elevation: " << elevation << std::endl;
  }
};
