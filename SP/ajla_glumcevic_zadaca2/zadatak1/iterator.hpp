#pragma once
template <typename T>
class sorted_list<T>::iterator : public std::iterator<std::bidirectional_iterator_tag, T>
{
private:
    Node<T> *current = nullptr;
    Node<T> *previous = nullptr;

public:
    friend class sorted_list;

    iterator() = default;
    iterator(Node<T> *current_ = nullptr, Node<T> *previous_ = nullptr)
        : current{current_}, previous{previous_} {}

    T &operator*() { return current->value; }

    iterator &operator++()
    {
        previous = current;
        if (current)
            current = current->next;
        return *this;
    }
    iterator operator++(int)
    {
        iterator tmp = *this;
        ++(*this);
        return tmp;
    }
    iterator &operator--()
    {
        current = previous;
        if (current)
        {
            previous = current->prev;
        }
        return *this;
    }
    iterator operator--(int)
    {
        iterator tmp = *this;
        --(*this);
        return tmp;
    }

    bool operator==(const iterator &other) const { return current == other.current; }
    bool operator!=(const iterator &other) const { return current != other.current; }

    bool isBegin() const { return current != nullptr && previous == nullptr; }
    bool isEnd() const { return current == nullptr && previous != nullptr; }
};
