#pragma once

// Copy Constructor
template <typename T>
sorted_list<T>::sorted_list(const sorted_list<T> &other)
{
  auto tmp = other.head;
  while (tmp)
  {
    push_back(tmp->value);
    tmp = tmp->next;
  }
}

// Move Constructor
template <typename T>
sorted_list<T>::sorted_list(sorted_list<T> &&other)
    : head{other.head}, tail{other.tail}, size_{other.size_}
{
  other.head = nullptr;
  other.tail = nullptr;
  other.size_ = 0;
}

// Copy assignment
template <typename T>
sorted_list<T> &sorted_list<T>::operator=(const sorted_list &other)
{
  if (this != &other)
  {
    clear();
    auto tmp = other.head;
    while (tmp)
    {
      push_back(tmp->value);
      tmp = tmp->next;
    }
  }
  return *this;
}

// Move assignment
template <typename T>
sorted_list<T> &sorted_list<T>::operator=(sorted_list &&other)
{
  if (this != &other)
  {
    clear();
    head = other.head;
    tail = other.tail;
    size_ = other.size_;
    other.head = nullptr;
    other.tail = nullptr;
    other.size_ = 0;
  }
  return *this;
}
/*************************************************************************/
// Metodi trazeni u zadaci

template <typename T>
template <typename U>
void sorted_list<T>::add(U &&val)
{
  if (empty())
  {
    push_front(std::forward<U>(val));
  }
  else
  {
    auto iter = find_if([val](const T &a) { return a > val; });
    insert(iter, std::forward<U>(val));
  }
}

template <typename T>
typename sorted_list<T>::iterator sorted_list<T>::find(const T &val)
{
  return find_if([val](const T &a) { return a == val; });
}

template <typename T>
template <typename UnaryPredicate>
typename sorted_list<T>::iterator sorted_list<T>::find_if(
    UnaryPredicate predicate)
{
  auto tmp = head;
  while (tmp)
  {
    if (predicate(tmp->value))
      return iterator(tmp, tmp->prev);
    tmp = tmp->next;
  }
  return end();
}

template <typename T>
void sorted_list<T>::remove(typename sorted_list<T>::iterator it)
{

  if (it.isEnd())
    throw std::string("Removing end - invalid operation!");
  else if (it.current == nullptr)
    throw std::out_of_range("Removing nullptr - invalid operation!");
  if (it.isBegin())
    pop_front();
  else if (it.current == tail)
    pop_back();
  else
  {
    it.previous->next = it.current->next;
    it.current->next->prev = it.previous;
    delete it.current;
    --size_;
  }
}

template <typename T>
void sorted_list<T>::clear()
{
  auto tmp = head;
  while (tmp)
  {
    auto toDelete = tmp;
    tmp = tmp->next;
    delete toDelete;
  }
  head = tail = nullptr;
  size_ = 0;
}

/******************************************************************/
// Ostali metodi

template <typename T>
void sorted_list<T>::insert(iterator it, const T &val)
{
  if (it.isBegin())
    push_front(val);
  else if (it.isEnd())
    push_back(val);
  else
  {
    auto newNode = new Node<T>(val);
    newNode->prev = it.previous;
    newNode->next = it.current;
    it.previous->next = newNode;
    it.current->prev = newNode;

    ++size_;
  }
}


template <typename T>
void sorted_list<T>::push_back(const T &val)
{
  auto newNode = new Node<T>(val, nullptr);
  if (empty())
    head = tail = newNode;
  else
  {
    newNode->prev = tail;
    tail->next = newNode;
    tail = newNode;
  }
  ++size_;
}

template <typename T>
void sorted_list<T>::push_front(const T &val)
{
  auto newNode = new Node<T>(val);
  if (empty())
    head = tail = newNode;
  else
  {
    head->prev = newNode;
    newNode->next = head;
    head = newNode;
  }
  ++size_;
}

template <typename T>
void sorted_list<T>::pop_back()
{
  if (empty())
    throw std::runtime_error("List is empty!");
  else if (size_ == 1)
  {
    delete head;
    head = tail = nullptr;
  }
  else
  {
    auto tmp = tail->prev;
    delete tail;
    tail = tmp;
    tail->next = nullptr;
  }
  --size_;
}

template <typename T>
void sorted_list<T>::pop_front()
{
  if (empty())
    throw std::runtime_error("List is empty!");
  else if (size_ == 1)
  {
    delete head;
    head = tail = nullptr;
  }
  else
  {
    auto tmp = head->next;
    delete head;
    head = tmp;
    head->prev = nullptr;
  }
  --size_;
}


