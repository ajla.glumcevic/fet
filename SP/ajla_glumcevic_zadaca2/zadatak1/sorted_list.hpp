#pragma once
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <functional>
#include "node.hpp"

template <typename T>
class sorted_list
{
private:
  Node<T> *head = nullptr;
  Node<T> *tail = nullptr;
  size_t size_ = 0;

public:
  // Constructors
  sorted_list() = default;
  sorted_list(const sorted_list &);
  sorted_list(sorted_list &&);

  // Ops=
  sorted_list &operator=(const sorted_list &);
  sorted_list &operator=(sorted_list &&);

  // Dtor
  ~sorted_list() { clear(); }


  // Element access
  T &front() { return head->value; }
  const T &front() const { return head->value; }
  T &back() { return tail->value; }
  const T &back() const { return tail->value; }

  // Iterators
  class iterator;
  class const_iterator;
  iterator begin() { return iterator(head, nullptr); }
  const_iterator cbegin() const { return const_iterator(head, nullptr); }
  iterator end() { return iterator(nullptr, tail); }
  const_iterator cend() const { return const_iterator(nullptr, tail); }

  // Metodi trazeni u zadaci
  template<typename U>
    void add(U&&);

  iterator find(const T &);
  template <typename UnaryPredicate>
    iterator find_if(UnaryPredicate);
  void remove(iterator);
  void clear();
  // Capacity
  bool empty() const { return size_ == 0; }
  size_t size() const { return size_; }

  // Ostali metodi
  void insert(iterator, const T &);
  void push_back(const T &);
  void push_front(const T &);
  void pop_back();
  void pop_front();
};
#include "iterator.hpp"
#include "const_iterator.hpp"
#include "sorted_list.hxx"
