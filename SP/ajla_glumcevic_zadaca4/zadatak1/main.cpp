#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

void analyseText(const std::string& filename) {
  std::map<std::string, std::vector<size_t>> words;

  std::ifstream input{filename};
  if (!input.is_open()) throw std::invalid_argument("Couldn't open file!");

  std::string word;
  size_t position = 0;
  while (input >> word) {
    words[word].push_back(position);
    ++position;
  }

  for (auto it = words.begin(); it != words.end(); ++it) {
      std::cout << "Rijec je " << it->first
                << " i nalazi se na sljedecim pozicijama:" << std::endl;
      for (const auto& position : it->second) std::cout << position << " ";
      std::cout << std::endl;
  }

  input.close();
}

int main(int argc, char *argv[])
{
  
  analyseText("story.txt");
  return 0;
}
