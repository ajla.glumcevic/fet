#pragma once
#include <iostream>
#include "student.hpp"
#include <string>
#include <map>

class App {

  std::map<std::string,Student> students; 
  int print_menu()const;
  void add_student();
  void delete_student();
  void print_all()const;
  void print_student()const;

  public:
  void open();
   };
