#include "app.hpp"

void App::open() {
  while (1) {
    try {
      auto choice = print_menu();
      if (choice == 1)
        add_student();
      else if (choice == 2)
        delete_student();
      else if (choice == 3)
        print_all();
      else if (choice == 4)
        print_student();
      else
        return;
    } catch (std::invalid_argument& s) {
      std::cout << s.what() << std::endl;
    }
  }
}
int App::print_menu() const {
  std::cout << "\n--------------STUDENT APP------------\n" << std::endl;
  std::cout << "1. Add student" << std::endl;
  std::cout << "2. Delete student" << std::endl;
  std::cout << "3. Print all students" << std::endl;
  std::cout << "4. Print student" << std::endl;
  std::cout << "5. Close app" << std::endl;
  int choice;
  std::cout << "Your choice: ";
  std::cin >> choice;
  std::cin.ignore();
  if (choice < 1 || choice > 5) throw std::invalid_argument("Wrong input.");
  return choice;
}

void App::add_student() {
  std::cout << "\nADDING STUDENT" << std::endl;
  Student newStudent;
  std::cout << "Full name: " << std::endl;
  std::getline(std::cin, newStudent.full_name);
  std::cout << "Index number: ";
  std::cin >> newStudent.index;
  std::cout << "Average grade: ";
  std::cin >> newStudent.av_grade;
  std::cin.ignore();

  if (students.find(newStudent.index) != students.end())
    throw std::invalid_argument("Student with index " + newStudent.index +
                                " already added.");

  students[newStudent.index] = newStudent;
}
void App::delete_student() {
  std::cout << "\nDELETING STUDENT" << std::endl;
  std::cout << "Enter index: ";
  std::string index;
  std::cin >> index;
  auto it = students.find(index);
  if (it != students.end()) {
    it->second.print_student();
    char choice = 'a';
    while (choice != 'y' && choice != 'n') {
      std::cout << "Are you sure you want to delete student? y/n" << std::endl;
      std::cin >> choice;
    }
    if (choice == 'y') students.erase(it);
  } else {
    throw std::invalid_argument("Student doesn't exist.");
  }
}
void App::print_all() const {
  std::cout << "\nPRINTING ALL STUDENTS" << std::endl;
  if (students.empty()) std::cout << "No students in base." << std::endl;
  for (auto it = students.cbegin(); it != students.cend(); ++it)
    it->second.print_student();
}
void App::print_student() const {
  std::cout << "\nPRINTING STUDENT" << std::endl;
  std::cout << "1. Search by index: " << std::endl;
  std::cout << "2. Search by full name: " << std::endl;
  int choice;
  std::cout << "Your choice: ";
  std::cin >> choice;
  std::cin.ignore();
  if (choice != 1 && choice != 2) throw std::invalid_argument("Wrong input!");

  std::cout << "Enter info: ";
  std::string info;
  std::getline(std::cin, info);
  if (choice == 1) {
    auto it = students.find(info);
    if (it == students.end())
      throw std::invalid_argument("Student doesn't exist.");
    else {
      it->second.print_student();
    }
  } else {
    auto it = students.cbegin();
    auto i = 0;
    while (it != students.cend()) {
      if (it->second.full_name == info) {
        it->second.print_student();
        ++i;
      }
      ++it;
    }
    if (i == 0) throw std::invalid_argument("Student doesn't exist.");
  }
}
