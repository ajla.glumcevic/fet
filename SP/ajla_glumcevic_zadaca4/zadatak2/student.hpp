#pragma once
#include <iostream>
#include <string>

class Student {
  std::string index;
  std::string full_name;
  double av_grade;


	public:
  friend class App;
  void print_student() const {
    std::cout << std::endl;
    std::cout << "Full name: " << full_name << std::endl;
    std::cout << "Index number: " << index << std::endl;
    std::cout << "Average grade: " << av_grade << std::endl;
    std::cout << std::endl;
  }
};
