#include "BankAccount.hpp"
#include <sys/ioctl.h>


size_t windowColumn() {
  winsize sz;
  ioctl(0, TIOCGWINSZ, &sz);
  return sz.ws_col;
}

int printMenu() {

  std::cout << std::string(windowColumn(), '-') << std::endl;
  std::cout << "Actions:" << std::endl;
  std::cout << "\t1. New transaction" << std::endl;
  std::cout << "\t2. Apply transaction" << std::endl;
  std::cout << "\t3. Pending transaction" << std::endl;
  std::cout << "\t4. Discard pending transaction" << std::endl;
  std::cout << "\t5. Balance" << std::endl;
  std::cout << "\t6. Quit" << std::endl;
  std::cout << "\nYour choice?  ";
  int choice;
  std::cin >> choice;

  if (choice < 1 || choice > 6) throw std::string("Wrong input!");
  return choice;
}
// Get current date and time
std::string getCurrentDateTime() {
  time_t rawtime;
  struct tm* timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", timeinfo);
  std::string str(buffer);
  return buffer;
}
// Make new transaction and add it to the queue
void BankAccount::addNewTransaction() {
  Transaction newTrans;
  std::cout << "\t\tENTER VALUE: ";
  double inputValue;
  std::cin >> inputValue;
  newTrans.setValue(inputValue);

  auto time = getCurrentDateTime();
  auto pos = time.find(' ');
  newTrans.setDate(time.substr(0, pos));
  newTrans.setTime(time.erase(0, pos + 1));

  transactionsToApply.push(newTrans);
}


// Add the value of the first transaction to the current balance
void BankAccount::applyPendingTransaction() {
  if (transactionsToApply.empty())
    throw std::string(
        "\t\tYOU HAVE NO MORE TRANSACTIONS TO BE APPLIED OR DISCARDED!");

  Transaction t = transactionsToApply.pop();
  balance += t.getValue();
  std::cout << "\t\tTRANSACTION APPLIED" << std::endl;
  std::cout << t << std::endl;
}


// Print the first transaction made by the user
const Transaction BankAccount::printPendingTransaction() const {
  if (transactionsToApply.empty())
    throw std::string(
        "\t\tYOU HAVE NO MORE TRANSACTIONS TO BE APPLIED OR DISCARDED!");
  return transactionsToApply.front();
}


// Check and discard the first transaction made by the user
void BankAccount::discardTransaction() {
  if (transactionsToApply.empty())
    throw std::string(
        "\t\tYOU HAVE NO MORE TRANSACTIONS TO BE APPLIED OR DISCARDED!");

  char choice;
  while (1) {
    std::cout << "Are you sure you want to discard transaction: y/n? ";
    std::cin >> choice;
    if (choice == 'y') {
      transactionsToApply.pop();
      std::cout << "\t\tTRANSACTION DISCARDED" << std::endl;
      return;
    }
    if (choice == 'n')
      return;
    else
      continue;
  }
}

std::ostream& operator<<(std::ostream& out, const Transaction& t) {
  out << "\t\tTRANSACTION VALUE: " << t.getValue()
      << "\t\tDATE AND TIME: " << t.getDate() << " "
      << t.getTime();

  return out;
}
