#pragma once
#include <iostream>
#include "Transaction.hpp"

class BankAccount {
  Queue<Transaction> transactionsToApply;
  double balance = 0;

  public:
  // Make new transaction and add it to the queue
  void addNewTransaction();

  // Return current balance of the account
  const double getBalance() const { return balance; }

  // Add the value of the first transaction to the current balance
  void applyPendingTransaction();

  // Print the first transaction made by the user
  const Transaction printPendingTransaction() const;

  // Check and discard the first transaction made by the user
  void discardTransaction();
};
