#include <sys/ioctl.h>
#include <iostream>
#include "BankAccount.hpp"

size_t windowColumn();
int printMenu();


int main(void) {
  BankAccount account;

  while (1) {
    try {
      auto choice = printMenu();

      if (choice == 1) {
        account.addNewTransaction();
      } else if (choice == 2) {
        account.applyPendingTransaction();
      } else if (choice == 3) {
        std::cout << account.printPendingTransaction() << std::endl;
      } else if (choice == 4) {
        account.discardTransaction();
      } else if (choice == 5) {
        std::cout << "\n\t\tBALANCE: " << account.getBalance() << std::endl;
      } else {
        std::cout << "\t\tEXITED" << std::endl;
        return 0;
      }
    } catch (std::string& s) {
      std::cout << s << std::endl;
    }
  }
  return 0;
}



