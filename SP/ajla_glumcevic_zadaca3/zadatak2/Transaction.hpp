#pragma once
#include <iostream>
#include "queue.hpp"

class Transaction {
  double value;
  std::string date;
  std::string time;

  public:
  void setValue(const double& value_) { value = value_; }
  void setDate(const std::string& date_) { date = date_; }
  void setTime(const std::string& time_) { time = time_; }
  const double getValue() const { return value; }
  const std::string getDate() const { return date; }
  const std::string getTime() const { return time; };

  friend std::ostream& operator<<(std::ostream&, const Transaction&);
};

