#include <iostream>

template <typename T>
class Queue {
  private:
  size_t capacity_ = 5;
  size_t size_ = 0;
  int first = 0;
  int last = -1;
  T* elements_ = nullptr;

  public:
  Queue(const size_t& maxCap)
      : capacity_{maxCap}, elements_{new T[capacity_]} {}

  Queue(const Queue& other)
      : capacity_{other.capacity_},
        size_{other.size_},
        first{other.first},
        last{other.last} {
    elements_ = new T[capacity_];
    for (auto i = 0; i < size_; ++i) elements_[i] = other.elements_[i];
  }
  Queue(Queue&& other)
      : capacity_{other.capacity_},
        size_{other.size_},
        first{other.first},
        last{other.last},
        elements_{other.elements_} {
    other.size_ = 0;
    other.capacity_ = 100;
    other.elements_ = nullptr;
    other.first = 0;
    other.last = -1;
  }

  Queue& operator=(const Queue& other) {
    if (this != &other) {
      delete[] elements_;
      capacity_ = other.capacity_;
      size_ = other.size_;
      first = other.first;
      last = other.last;
      elements_ = new T[capacity_];
      std::copy(other.elements_, other.elements_ + size_, elements_);
    }
    return *this;
  }

  Queue& operator=(Queue&& other) {
    if (this != &other) {
      delete[] elements_;
      capacity_ = other.capacity_;
      size_ = other.size_;
      first = other.first;
      last = other.last;
      elements_ = other.elements_;

      other.elements_ = nullptr;
      other.size_ = 0;
      other.capacity_ = 100;
      other.first = 0;
      other.last = -1;
    }
    return *this;
  }

  ~Queue() { delete[] elements_; }
  bool empty() const { return size_ == 0; }
  size_t size() const { return size_; }
  bool capacity() const { return capacity_; }

  T& front() { return elements_[first]; }
  const T& front() const { return elements_[first]; }
  T& back() { return elements_[last]; }
  const T& back() const { return elements_[last]; }

  template <typename U>
  void push(U&& val) {
    if (size_ == capacity_) throw std::out_of_range("Nije moguce dodati elem.");

    last = (last + 1) % capacity_;
    elements_[last] = std::forward<U>(val);
    ++size_;
  }

  T pop() {
    if (empty()) throw std::string("Queue empty!");
    auto toReturn = std::move(elements_[first]);
    first = (first + 1) % capacity_;
    --size_;
    return toReturn;
  }

  // void reallocate(const size_t& newCap) {
  //   auto newElem = new T[newCap];
  //   std::copy(elements_ + first, elements_ + capacity_, newElem);
  //   if (last < first)
  //     std::copy(elements_, elements_ + last + 1, newElem + capacity_ -
  //     first);
  //   delete[] elements_;
  //   elements_ = newElem;
  //   first = 0;
  //   last = size_ - 1;
  //   capacity_ = newCap;
  // }
};
