#include <iostream>
#include "queue.hpp"

int main(void) {
  Queue<int> q(10);
  for (int i = 0; i < 10; i++) q.push(i);
  std::cout << std::boolalpha << "Empty: " << q.empty() << std::endl;
  std::cout << "Front: " << q.front() << std::endl;
  std::cout << "Back: " << q.back() << std::endl;
  {
    std::cout << "Copy cons" << std::endl;
    Queue<int> q(10);
    for (int i = 0; i < 10; i++) q.push(i);
    std::cout << q.size() << std::endl;
    Queue<int> q1 = q;
    std::cout << q1.size() << std::endl;
    std::cout << q.size() << std::endl;
    for (int i = 0; i < 10; i++) std::cout << q1.pop();
    std::cout << std::endl;
  }
  {
    std::cout << "Copy=" << std::endl;
    Queue<int> q(10);
    for (int i = 0; i < 10; i++) q.push(i);
    std::cout << q.size() << std::endl;
    Queue<int> q1(0);
    q1 = q;
    std::cout << q1.size() << std::endl;
    std::cout << q.size() << std::endl;
    for (int i = 0; i < 10; i++) std::cout << q1.pop();
    std::cout << std::endl;
  }
  {
    std::cout << "Move cons" << std::endl;
    Queue<int> q(10);
    for (int i = 0; i < 10; i++) q.push(i);
    std::cout << q.size() << std::endl;
    Queue<int> q1 = std::move(q);
    std::cout << q1.size() << std::endl;
    std::cout << q.size() << std::endl;
    for (int i = 0; i < 10; i++) std::cout << q1.pop();
    std::cout << std::endl;
  }
  {
    std::cout << "Move=" << std::endl;
    Queue<int> q(10);
    for (int i = 0; i < 10; i++) q.push(i);
    std::cout << q.size() << std::endl;
    Queue<int> q1(0);
    q1 = std::move(q);
    std::cout << q1.size() << std::endl;
    std::cout << q.size() << std::endl;
    for (int i = 0; i < 10; i++) std::cout << q1.pop();
    std::cout << std::endl;
  }
  return 0;
}
