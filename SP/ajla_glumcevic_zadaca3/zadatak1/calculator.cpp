#include "calculator.hpp"
#include <sys/ioctl.h>
size_t windowColumn() {
  winsize sz;
  ioctl(0, TIOCGWINSZ, &sz);
  return sz.ws_col;
}

int PostfixCalc::start() {
  std::cout << std::string(windowColumn(), '-') << std::endl;
  std::string expression;
  std::cout << "\n\t\tYOUR INPUT: ";
  std::getline(std::cin, expression);
  if (expression == "Q")
    return -1;
  else {
    try {
      auto result = evaluate(expression);
      std::cout << "\t\tRESULT: " << result << std::endl;
      operands.clear();
    } catch (std::string& s) {
      std::cout << s << std::endl;
      return -1;
    }
  }
  return 1;
}

int PostfixCalc::evaluate(const std::string& expression) {
  std::string value;
  std::vector<std::string> values;
  std::stringstream ss(expression);

  while (std::getline(ss, value, ' ')) values.push_back(value);

  auto result = 0;

  for (int i = 0; i < values.size(); ++i) {
    auto value = values[i];

    if (isOperand(value)) {
      operands.push(std::stoi(value));
    } else if (isOperator(value)) {
      if (operands.size() == 1 || operands.empty())
        throw std::string("\n\t\tWRONG FORMAT OF INPUT! TRY AGAIN!");

      int y = operands.pop();
      int x = operands.pop();

      if (!operands.empty() && i == values.size() - 1)
        throw std::string("\n\t\tWRONG FORMAT OF INPUT! TRY AGAIN!");

      if (value == "+") result = add(x, y);
      if (value == "-") result = sub(x, y);
      if (value == "*") result = multiply(x, y);
      if (value == "/") result = divide(x, y);
      operands.push(result);
    } else {
      throw std::string("\n\t\tINVALID INPUT: " + values[i]);
    }
  }

  return result;
}

bool PostfixCalc::isOperator(const std::string& op) {
  if (op == "+" || op == "-" || op == "*" || op == "/")
    return true;
  else
    return false;
}
bool PostfixCalc::isOperand(const std::string& op) {
  for (int i = 0; i < op.size(); ++i) {
    if (op[i] < '0' || op[i] > '9') return false;
  }

  return true;
}

int PostfixCalc::sub(const int& a, const int& b) { return a - b; }
int PostfixCalc::multiply(const int& a, const int& b) { return a * b; }
int PostfixCalc::divide(const int& a, const int& b) {
  if (b == 0) throw std::string("\t\tCannot divide by 0.");
  return a / b;
}
int PostfixCalc::add(const int& a, const int& b) { return a + b; }
