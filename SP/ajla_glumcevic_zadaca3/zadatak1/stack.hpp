#include <iostream>

template <typename T>
class Node {
  public:
  T value;
  Node *next = nullptr;
  Node *prev = nullptr;
  Node(const T &val, Node *n = nullptr, Node *p = nullptr)
      : value{val}, next{n}, prev{p} {}
};

template <typename T>
class Stack {
  size_t size_ = 0;
  Node<T> *head = nullptr;
  Node<T> *tail = nullptr;

  public:
  Stack() = default;
  Stack(const size_t &);
  Stack(const Stack &);
  Stack(Stack &&);
  Stack &operator=(const Stack &);
  Stack &operator=(Stack &&);
  ~Stack();

  // Member functions
  // void reallocate(const size_t &);
  T &top();
  const T &top() const;

  bool empty() const;
  size_t size() const;

  template <typename U>
  void push(U &&);
  T pop();
  void clear();
};

template <typename T>
void Stack<T>::clear() {
  while (head) {
    auto toDelete = head;
    head = head->next;
    delete toDelete;
  }
  head = tail = nullptr;
  size_ = 0;
}

template <typename T>
Stack<T>::Stack(const Stack<T> &other) : size_{other.size_} {
  if (!other.empty()) {
    auto newNode = new Node<T>(other.head->value);
    head = tail = newNode;
    if (size() > 1) {
      auto previous = head;
      auto tmp = other.head->next;

      while (tmp) {
        auto newNode = new Node<T>(tmp->value);
        previous->next = newNode;
        newNode->prev = previous;
        previous = newNode;
        tmp = tmp->next;
      }
      tail = previous;
    }
  }
}

template <typename T>
Stack<T>::Stack(Stack<T> &&other)
    : size_{other.size_}, head{other.head}, tail{other.tail} {
  other.size_ = 0;
  other.head = nullptr;
  other.tail = nullptr;
}
// template <typename T>
// Stack<T> &Stack<T>::operator=(const Stack<T> &other) {
// return *this = Stack<T>{other};
// }
template <typename T>
Stack<T> &Stack<T>::operator=(Stack<T> &&other) {
  size_ = other.size_;
  head = other.head;
  tail = other.tail;
  other.size_ = 0;
  other.head = nullptr;
  other.tail = nullptr;
  return *this;
}

template <typename T>
Stack<T>::~Stack() {
  auto tmp = head;
  while (head) {
    tmp = head->next;
    delete head;
    head = tmp;
  }
}

template <typename T>
T &Stack<T>::top() {
  if (empty()) throw std::string("Empty stack.");
  return head->value;
}
template <typename T>
const T &Stack<T>::top() const {
  if (empty()) throw std::string("Empty stack.");
  return head->value;
}

template <typename T>
bool Stack<T>::empty() const {
  return size_ == 0;
}
template <typename T>
size_t Stack<T>::size() const {
  return size_;
};

template <typename T>
template <typename U>
void Stack<T>::push(U &&value) {
  auto newNode = new Node<T>(std::forward<U>(value));
  if (empty())
    head = tail = newNode;
  else {
    newNode->next = head;
    head->prev = newNode;
    head = newNode;
  }
  ++size_;
}
template <typename T>
T Stack<T>::pop() {
  if (empty()) throw std::string("Empty stack.");
  auto poping = head->value;
  if (size() == 1) {
    delete head;
    head = tail = nullptr;
  } else {
    auto tmp = head;
    head = head->next;
    delete tmp;
    head->prev = nullptr;
  }
  --size_;
  return poping;
}
// // koji je fazon sa emplace

