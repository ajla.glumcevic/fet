#pragma once
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "stack.hpp"
class PostfixCalc {
  private:
  Stack<int> operands;

  bool isOperator(const std::string&);
  bool isOperand(const std::string&);
  int add(const int&, const int&);
  int sub(const int&, const int&);
  int multiply(const int&, const int&);
  int divide(const int&, const int&);
  int evaluate(const std::string&);

  public:
  PostfixCalc() {
    std::cout << "\t\tREVERSE POLISH NOTATION BASED CALCULATOR" << std::endl;
    std::cout << "\t\t(example of correct format of input: 3 4 +): " << std::endl;
    std::cout << "\t\tPRESS 'Q' TO QUIT." << std::endl;
  }

  int start();
};
