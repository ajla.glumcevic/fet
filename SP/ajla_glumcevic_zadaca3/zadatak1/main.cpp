#include <iostream>
#include "calculator.hpp"

int main(void) {
  PostfixCalc calculator;
  while (1) {
    try {
      auto q = calculator.start();
      if (q == -1) return 0;
    } catch (std::string& s) {
      std::cout << s << std::endl;
    }
  }
  return 0;
}
