#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>

// C
// A sa obrnutim redoslijedom
// B
template <typename T>
void transform(std::vector<T>& v, size_t a, size_t b, size_t c) {
  std::stack<T> s;
  std::queue<T> q;

  for (auto i = 0; i < a; ++i) {
    s.push(std::move(v[i]));
  }
  for (auto i = a + b; i < a + b + c; ++i) {
    q.push(std::move(v[i]));
  }
  for (auto i = a; i < a + b; ++i) {
    q.push(std::move(v[i]));
  }
  for (auto i = 0; i < c; ++i) {
    v[i] = q.front();
    q.pop();
  }
  for (auto i = c; i < c + a; ++i) {
    v[i] = s.top();
    s.pop();
  }
  for (auto i = c + a; i < c + a + b; ++i) {
    v[i] = q.front();
    q.pop();
  }
}

int main(void) {
  std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8};
  transform(v, 3, 3, 2);
  for (const auto& e : v) std::cout << e << " ";
  return 0;
}
