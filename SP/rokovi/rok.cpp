#include <fstream>
#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>

int main(void) {
  std::string filename;
  std::cout << "Unesi ime file: ";
  std::cin >> filename;
  std::ifstream input{filename};
  if (!input.is_open())
    throw std::runtime_error("Can not open file " + filename);
  std::unordered_map<std::string, std::vector<size_t>> words;
  // std::unordered_map<size_t, std::string> positionsInFile;
  std::vector<std::string> positionsInFile;
  while (1) {
    std::cout << "1. Ucitaj podatke" << std::endl;
    std::cout << "2. Rijec i koliko puta se ponavlja" << std::endl;
    std::cout << "3. Rijec i prva pozicija" << std::endl;
    std::cout << "4. Rijec i zadnja pozicija" << std::endl;
    std::cout << "5. Ispisi od x do x pozicije" << std::endl;
    int choice = 0;
    std::cout << "Vas izbor: ";
    std::cin >> choice;
    if (choice < 1 || choice > 5) throw std::out_of_range("Wrong input");

    if (choice == 1) {
      std::string word;
      size_t position = 0;
      while (input >> word) {
        words[word].push_back(position);
        // positionsInFile[position] = word;
        positionsInFile.push_back(word);
        position++;
      }
    } else if (choice == 2) {
      std::string findWord;
      std::cout << "Trazena rijec: ";
      std::cin >> findWord;
      auto it = words.find(findWord);
      if (it != words.end())
        std::cout << it->first << " " << it->second.size() << std::endl;
    } else if (choice == 3) {
      std::string findWord;
      std::cout << "Trazena rijec: ";
      std::cin >> findWord;
      auto it = words.find(findWord);
      if (it != words.end())
        std::cout << it->first << " " << it->second.front() << std::endl;
    } else if (choice == 4) {
      std::string findWord;
      std::cout << "Trazena rijec: ";
      std::cin >> findWord;
      auto it = words.find(findWord);
      if (it != words.end()) {
        auto position = it->second.back();
        std::cout << it->first << " " << position << std::endl;
        if (position > 0)
          std::cout << "Rijec prije te pozicije: "
                    << positionsInFile[position - 1];
        if (position < words.size() - 1)
          std::cout << "Rijec poslije te pozicije: "
                    << positionsInFile[position + 1];
      }
    } else if (choice == 5) {
      size_t first, last;
      std::cout << "Unesite prvu i zadnju poziciju: ";
      std::cin >> first >> last;
      if (first >= 0 && last < positionsInFile.size()) {
        for (int i = first; i < last; ++i)
          std::cout << "Pozicija: " << i << "\tRijec: " << positionsInFile[i]
                    << std::endl;
      }
    } else
      return 0;
  }
  return 0;
}
