#include <fstream>
#include <iostream>
#include <unordered_map>
#include <vector>

std::unordered_map<std::string, std::vector<size_t>> words;
void ucitaj(const std::string& filename) {
  std::ifstream input{filename};
  if (!input.is_open()) throw std::runtime_error("can not open file");
  std::string word;
  size_t pos = 0;
  while (input >> word) {
    words[word].push_back(pos);
    ++pos;
  }
  input.close();
}

std::string pronadji() {
  std::string word;
  std::cout << "Rijec: ";
  std::cin >> word;
  auto it = words.find(word);
  if (it != words.end()) {
    for (const auto& pos : it->second) {
      std::cout << pos << " ";
      word = word + ' ' + std::to_string(pos);
    }
    return word;
  } else {
    return word + " nije pronadjena";
  }
}

int main(void) {
  std::string filename = "text.txt";
  std::string zadnjaPretraga = "nema prijasnjih pretraga";

  while (1) {
    std::cout << "l ucitaj" << std::endl;
    std::cout << "f pronadji" << std::endl;
    std::cout << "s zadnja pretraga" << std::endl;
    std::cout << "q quit" << std::endl;
    std::cout << "Izbor:" << std::endl;

    char izbor;
    std::cin >> izbor;
    //provjera izbora

    if (izbor == 'l')
      ucitaj(filename);
    else if (izbor == 'f')
      zadnjaPretraga = pronadji();
    else if (izbor == 's')
      std::cout << zadnjaPretraga << std::endl;
    else
      return 0;
  }
  return 0;
}
