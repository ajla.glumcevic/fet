#pragma once
#include "Finger.hpp"

GLfloat palmFingerAngle = 0.;
GLfloat palmThumbAngle = 0.;
GLfloat jointAngle = 0.;
extern GLfloat rotateSceneAngle;

class Hand
{
    class Palm
    {
       const float scaleX = 1, scaleY = 1, scaleZ = 0.3;

    public:
        void draw(GLdouble size)
        {
            glPushMatrix();
            glScalef(scaleX, scaleY, scaleZ);
            glutSolidCube(size);
            glPopMatrix();
        }
    };

    Material material;
    Palm palm;
    Finger fingers[5];

public:
    void draw()
    {
        glLoadIdentity();
        glRotatef(rotateSceneAngle, 0, 1, 0);
        setMaterial();

        palm.draw(6);
        fingers[0].drawThumb(4);
        fingers[1].draw(-2.5);
        fingers[2].draw(-0.8);
        fingers[3].draw(0.8);
        fingers[4].draw(2.5);
    }

    void
    closeHand()
    {
        if (palmFingerAngle < 70)
            palmFingerAngle += 1;
        if (palmThumbAngle < 50)
            palmThumbAngle += 1;
        if (jointAngle < 60)
            jointAngle += 1;
    }

    void openHand()
    {
        if (palmFingerAngle > 0)
            palmFingerAngle -= 1;
        if (palmThumbAngle > 0)
            palmThumbAngle -= 1;
        if (jointAngle > 0)
            jointAngle -= 1;
    }

    void setMaterial()
    {
        material.setWhiteMaterial();
        material.setTexture();
    }
};