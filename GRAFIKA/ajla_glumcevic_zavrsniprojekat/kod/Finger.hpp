#pragma once
#include "Material.hpp"

extern GLfloat palmFingerAngle, jointAngle, palmThumbAngle;

class Finger
{

    class Joint
    {
        const float scaleX = 2, scaleY = 1, scaleZ = 2;
        const float radius = 0.25;

    public:
        void draw(float distanceY)
        {
            glPushMatrix();
            glScalef(scaleX, scaleY, scaleZ);
            glTranslatef(0, distanceY, 0);
            glutSolidSphere(radius, 10, 10);
            glPopMatrix();
        }
    };

    Joint joints[3];
    const float cubeSize = 1;
    const float scaleX = 1, scaleY = 2, scaleZ = 1;

    const float palmFingerSpacingY = 2.2;
    const float jointSpacingY = 1.2;
    const float jointSpacingX = 1.2;
    const float thumbAtY = -1;

    const float palmFingerTranslateFactorY = 0.01;
    const float palmFingerTranslateFactorZ = 0.01;
    const float palmThumbTranslateFactorX = 0.007;
    const float jointTranslateFactorX = 0.007;
    const float jointTranslateFactorY = 0.007;
    const float jointTranslateFactorZ = 0.007;

public:
    void draw(GLfloat fingerAtX)
    {
        glPushMatrix();
        glScalef(scaleX, scaleY, scaleZ);

        glTranslatef(fingerAtX, palmFingerSpacingY, 0);
        joints[0].draw(-0.6);
        glRotatef(palmFingerAngle, 1, 0, 0);
        glTranslatef(0, palmFingerAngle * palmFingerTranslateFactorY, palmFingerAngle * palmFingerTranslateFactorZ);
        glutSolidCube(cubeSize);

        joints[1].draw(0.6);
        glTranslatef(0, jointSpacingY, 0.0);
        glRotatef(jointAngle, 1, 0, 0);
        glTranslatef(0, jointAngle * jointTranslateFactorY, jointAngle * jointTranslateFactorZ);
        glutSolidCube(cubeSize);

        joints[2].draw(0.6);
        glTranslatef(0, jointSpacingY, 0);
        glRotatef(jointAngle, 1, 0, 0);
        glTranslatef(0, jointAngle * jointTranslateFactorY, jointAngle * jointTranslateFactorZ);
        glutSolidCube(cubeSize);

        glPopMatrix();
    }

    void drawThumb(GLfloat palmThumbSpacingX)
    {
        glPushMatrix();
        glScalef(scaleX, scaleY, scaleZ);

        glTranslatef(palmThumbSpacingX, thumbAtY, 0);
        glRotatef(-palmThumbAngle, 0, 1, 0);
        glTranslatef(0, 0, palmThumbAngle * palmThumbTranslateFactorX);
        glutSolidCube(cubeSize);

        glTranslatef(jointSpacingX, 0, 0);
        glutSolidCube(cubeSize);

        glPopMatrix();
    }
};
