#include <GL/glut.h>
class Camera
{
    GLdouble left, right, bottom, top, nearClip, farClip;
    GLdouble *eye;
    GLdouble *center;
    GLdouble *up;

public:
    Camera()
    {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
    }

    void setFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble nearClip, GLdouble farClip)
    {
        this->left = left;
        this->right = right;
        this->bottom = bottom;
        this->top = top;
        this->nearClip = nearClip;
        this->farClip = farClip;
        glFrustum(left, right, bottom, top, nearClip, farClip);
    }
    void setLookAt(GLdouble *eye, GLdouble *center, GLdouble *up)
    {
        this->eye = eye;
        this->center = center;
        this->up = up;
        gluLookAt(eye[0], eye[1], eye[2], center[0], center[1], center[2], up[0], up[1], up[2]);
    }
};
