#pragma once
#include <GL/glut.h>
#include "lowQuality.c"

class Material
{
    GLfloat *mat_ambient;
    GLfloat *mat_diffuse;
    GLfloat *mat_specular;
    GLfloat *mat_shine;

public:
    void setMaterial(GLfloat *ambient, GLfloat *diffuse, GLfloat *specular, GLfloat *shine)
    {
        this -> mat_ambient = ambient;
        this -> mat_diffuse = diffuse;
        this -> mat_specular = specular;
        this -> mat_shine = shine;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shine);
    }

    void setTexture()
    {
        GLuint texture = 0;
        glGenTextures(1, &texture);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, gimp_image.width, gimp_image.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, gimp_image.pixel_data);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }

    void
    setWhiteMaterial()
    {
        GLfloat ambient[] = {0.25f, 0.20725f, 0.20725f, 0.922f};
        GLfloat diffuse[] = {1.0f, 0.829f, 0.829f, 0.922f};
        GLfloat specular[] = {1, 0.296648f, 0.296648f, 1.0};
        GLfloat shine[] = {0.0};
        setMaterial(ambient, diffuse, specular, shine);
    }
};