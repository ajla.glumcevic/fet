#include <GL/glut.h>
#include <iostream>
#include "Hand.hpp"
#include "Light.hpp"
#include "Camera.hpp"

extern Hand hand;
bool rotate = false;
GLfloat rotateSceneAngle = 0.;

void updateAngle(GLfloat increment)
{
    rotateSceneAngle = (rotateSceneAngle > 360) ? 0 : rotateSceneAngle + increment;
}

void init()
{
    glClearColor(0, 0, 0, 0);

    Camera camera;
    camera.setFrustum(-5.0, 5.0, -5.0, 5.0, 5, 30.0);
    GLdouble eye[] = {0, 5, 12};
    GLdouble center[] = {0, 4, 0};
    GLdouble up[] = {0, 1, 0};
    camera.setLookAt(eye, center, up);

    Light light0;
    GLfloat ambient0[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat position0[] = {1.0, 0.0, 1.0, 0.0};
    GLfloat value0[] = {1.0, 1.0, 1.0, 1.0};
    light0.setup(ambient0, position0, value0, GL_LIGHT0);

    Light light1;
    GLfloat ambient1[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat position1[] = {-1.0, 0.0, 1.0, 0.0};
    GLfloat value1[] = {1.0, 1.0, 1.0, 1.0};
    light1.setup(ambient1, position1, value1, GL_LIGHT1);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    hand.draw();
    glFlush();
}

void keyboardCallback(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 'o':
        hand.openHand();
        break;
    case 'c':
        hand.closeHand();
        break;
    case 'r':
        rotate = !rotate;
        break;
    }
    glutPostRedisplay();
}

void specialKeys(int key, int x, int y)
{
    switch (key)
    {
    case GLUT_KEY_LEFT:
        updateAngle(5);
        break;
    case GLUT_KEY_RIGHT:
        updateAngle(-5);
        break;
    }

    glutPostRedisplay();
}

void idle()
{
    if (rotate)
    {
        updateAngle(0.02);
        glutPostRedisplay();
    }
}
