#include <GL/glut.h>
class Light
{
    GLfloat *lmodel_ambient;
    GLfloat *light_position;
    GLfloat *light_value;

public:
    void setup(GLfloat *ambient, GLfloat *position, GLfloat *value, GLenum num)
    {
        this -> lmodel_ambient = ambient;
        this -> light_position = position;
        this -> light_value = value;
        const GLfloat specular_light[] = {0.0, 0.0, 0.0, 1.0};

        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
        glEnable(GL_LIGHTING);
        glEnable(GL_DEPTH_TEST);

        glLightfv(num, GL_POSITION, light_position);
        glLightfv(num, GL_DIFFUSE, light_value);
        glLightfv(num, GL_SPECULAR, specular_light);
        glEnable(num);
    }
};
