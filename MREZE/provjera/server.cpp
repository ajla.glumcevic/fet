#include <arpa/inet.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <string_view>
#include <thread>

#define BUFFSIZE 1024

void die(std::string reason) {
  std::cout << reason << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

void runServer(int receivingPort, int maxReconnection, int ipv = AF_INET) {
  std::cout << "server" << std::endl;
  std::cout << "Receiving port: " << receivingPort << std::endl;

  // kreiraj UDP socket
  const auto sock = socket(ipv, SOCK_DGRAM, 0);
  if (sock < 0) die("Goodbye cruel world");

  // postavi info za server
  sockaddr_in serverData;
  serverData.sin_family = ipv;
  serverData.sin_port = htons(receivingPort);
  serverData.sin_addr.s_addr = htonl(INADDR_ANY);
  memset(serverData.sin_zero, 0, sizeof(serverData.sin_zero));

  // bindaj socket na receiving port
  if (bind(sock, reinterpret_cast<sockaddr *>(&serverData),
           sizeof(serverData)) < 0) {
    die("Couldn't bind socket.");
  }

  // za storiranje info od klijenta
  size_t bread = 0;
  char incomingData[BUFFSIZE + 1];
  const std::string serverMsg = "Message received.";

  // za info od klijenta
  sockaddr_in destinationData;
  socklen_t destinationDataSize = sizeof(destinationData);

  // prima poruku od klijenta
  bread = recvfrom(sock, incomingData, BUFFSIZE, 0,
                   reinterpret_cast<sockaddr *>(&destinationData), &destinationDataSize);
  if (bread < 0) die("Error while reading from socket.");
  incomingData[bread] = '\0';

  // procitaj port klijenta
  std::cout << "Vrijednost porta clienta: " << ntohs(destinationData.sin_port)
            << std::endl;

  // server odgovara klijentu
  sendto(sock, serverMsg.c_str(), serverMsg.size(), 0,
         reinterpret_cast<sockaddr *>(&destinationData), destinationDataSize);
  //
  //
  //
  //
  // probaj se conncetovat sa drugim hostom
  while (true) {
    if (maxReconnection == 0) exit(0);
    if (connect(sock, reinterpret_cast<sockaddr *>(&destinationData),
                destinationDataSize) < 0) {
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      --maxReconnection;
      continue;
    } else {
      std::cout << "connected" << std::endl;
      break;
    }
  }

  // procitaj poruku od drugog hosta
  bread = recv(sock, incomingData, BUFFSIZE, 0);
  incomingData[bread] = '\0';
  std::cout << incomingData << std::endl;
  return;
}

int main(int argc, char *argv[]) {
  if (argc == 3) {
    runServer(std::atoi(argv[1]), std::atoi(argv[2]));

  } else {
    die("Wrong input.");
  }
  return 0;
}

