#include <arpa/inet.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <chrono>
#include <iostream>
#include <string_view>
#include <thread>
#define BUFFSIZE 1024

void die(std::string reason) {
  std::cout << reason << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

void runClient(const char *serverip, int serverport, int listeningport,
               int ipv = AF_INET) {
  std::cout << "client" << std::endl;
  std::cout << "serverport: " << serverport << std::endl;
  std::cout << "listeningport: " << listeningport << std::endl;
  const auto sock = socket(ipv, SOCK_DGRAM, 0);
  if (sock < 0) die("sock fail.");

  // destination data
  sockaddr_in serverData;
  serverData.sin_family = ipv;
  serverData.sin_port = htons(serverport);
  inet_pton(ipv, serverip, &serverData.sin_addr.s_addr);
  memset(serverData.sin_zero, 0, sizeof(serverData.sin_zero));

  // za slanje poruke serveru
  size_t bread = 0;
  std::string inputBuffer;
  inputBuffer = std::to_string(listeningport);
  // za poruku od servera
  char incomingData[BUFFSIZE + 1];

  // posalji listeningport serveru
  if (sendto(sock, inputBuffer.c_str(), inputBuffer.size(), 0,
             reinterpret_cast<sockaddr *>(&serverData),
             sizeof(serverData)) < 0) {
    die("Couldn't send data");
  }
  //odgovor 
  bread = recvfrom(sock, incomingData, BUFFSIZE, 0, nullptr, nullptr);
  incomingData[bread] = '\0';
  std::cout << incomingData << std::endl;
  close(sock);
  //
  //
  //
  // 
  //  
  sockaddr_in newConnectionData;
  socklen_t newConnectionSize = sizeof(newConnectionData);
  char newConnectionIp[INET_ADDRSTRLEN];
  std::string clientMsg = "closing conn.";

  // client info sa listeningportom
  sockaddr_in clientData;
  clientData.sin_family = ipv;
  clientData.sin_port = htons(listeningport);
  inet_pton(ipv, "127.0.0.1", &clientData.sin_addr.s_addr);
  memset(clientData.sin_zero, 0, sizeof(clientData.sin_zero));

  const auto tcpsock = socket(ipv, SOCK_STREAM, 0);
  if (tcpsock < 0) die("tcp sock fail.");
  // bindam na listeningport
  if (bind(tcpsock, reinterpret_cast<sockaddr *>(&clientData),
           sizeof(clientData)) < 0) {
    die("Couldn't bind tcp socket.");
  }

  if (listen(tcpsock, 1) < 0) die("Couldn't listen tcp spock.");
  const auto peer_sock =
      accept(tcpsock, reinterpret_cast<sockaddr *>(&newConnectionData),
             &newConnectionSize);
  if (peer_sock < 0) die("Failed to accept connection.");
  
  // ispisi ip od konkecije 
  std::cout << inet_ntop(ipv, &newConnectionData.sin_addr.s_addr,
                         newConnectionIp, INET_ADDRSTRLEN)
            << std::endl;
  send(peer_sock, clientMsg.c_str(), clientMsg.size(), 0);

  close(peer_sock);
  close(tcpsock);
  return;
}

int main(int argc, char *argv[]) {
  if (argc == 4) {
    runClient(argv[1], std::atoi(argv[2]), std::atoi(argv[3]));

  } else {
    die("wrong input");
  }
  return 0;
}

