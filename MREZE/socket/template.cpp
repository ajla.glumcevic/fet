#include <arpa/inet.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string_view>

#define BUFFSIZE 1024

void die(std::string reason) {
  std::cout << reason << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

void runServer(int port, int ipv) {
  std::cout << "server" << std::endl;
  std::cout << "Listening on port: " << ntohs(port) << std::endl;

  const auto sock = socket(ipv, SOCK_STREAM, 0);
  if (sock < 0) die("Goodbye cruel world");

  sockaddr_in serverData;
  serverData.sin_family = ipv;
  serverData.sin_port = port;
  serverData.sin_addr.s_addr = INADDR_ANY;
  memset(serverData.sin_zero, 0, sizeof(serverData.sin_zero));

  if (bind(sock, reinterpret_cast<sockaddr *>(&serverData),
           sizeof(serverData)) < 0) {
    die("Couldn't bind socket.");
  }

  size_t bread = 0;
  char incomingData[BUFFSIZE + 1];
  const std::string msg = "Message from server.";

  if (listen(sock, 10) < 0) die("Couldn't listen on socket");

  while (true) {
    const auto peerSock = accept(sock, nullptr, nullptr);
    if (peerSock < 0) die("Failed to accept connection.");

    if (const auto pid = fork(); pid < 0) {
      die("Failed to fork.");
    } else if (pid == 0) {
      close(sock);
      while ((bread = recv(peerSock, incomingData, BUFFSIZE, 0))) {
        if (bread < 0) die("Error while reading from socket.");
        incomingData[bread] = '\0';
        std::cout << incomingData << std::endl;
        //

        send(peerSock, msg.c_str(), msg.size(), 0);
      }

      close(peerSock);
      return;
    }
    close(peerSock);
  }
  close(sock);
  return;
}

void runClient(int port, const char *ipaddr, int ipv) {
  std::cout << "client" << std::endl;

  const auto sock = socket(ipv, SOCK_STREAM, 0);
  if (sock < 0) die("Goodbye cruel world");

  // destination server
  sockaddr_in serverData;
  serverData.sin_family = ipv;
  serverData.sin_port = port;
  inet_pton(AF_INET, ipaddr, &serverData.sin_addr.s_addr);
  memset(serverData.sin_zero, 0, sizeof(serverData.sin_zero));

  if (connect(sock, reinterpret_cast<sockaddr *>(&serverData),
              sizeof(serverData)) < 0)
    die("Failed to connect to server");

  std::string inputBuffer;
  size_t readb = 0;
  char incomingData[BUFFSIZE + 1];

  while (std::getline(std::cin, inputBuffer)) {
    //
    if (send(sock, inputBuffer.c_str(), inputBuffer.size(), 0) < 0)
      die("Couldn't send data.");
    if ((readb = recv(sock, incomingData, BUFFSIZE, 0)) < 0)
      std::cout << "Couldn't recieve data but its ok." << std::endl;
    incomingData[readb] = '\0';
    std::cout << incomingData << std::endl;
    //
  }
}

int main(int argc, char *argv[]) {
  if (argv[1][1] == 's') {
    int port;
    if (argc == 3) {
      port = htons(std::atoi(argv[2]));
      runServer(port, AF_INET);
    } else {
      port = htons(std::atoi(argv[3]));
      runServer(port, AF_INET6);
    }

  } else if (argv[1][1] == 'c') {
    int port;
    if (argc == 4) {
      port = htons(std::atoi(argv[3]));
      runClient(port, argv[2], AF_INET);
    } else {
      port = htons(std::atoi(argv[4]));
      runClient(port, argv[3], AF_INET6);
    }

  } else {
    die("Wrong input.");
  }
  return 0;
}

