#include <arpa/inet.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <iostream>
#include <string_view>

#define BUFF_SIZE 1024

void die(std::string_view reason) {
  std::cout << reason << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

int main(int argc, char *argv[]) {
  const auto sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) die("Goodbye cruel world");

  sockaddr_in data;
  data.sin_family = AF_INET;
  data.sin_port = htons(10000);
  inet_pton(AF_INET, "127.0.0.1", &data.sin_addr.s_addr);
  memset(data.sin_zero, 0, sizeof(data.sin_zero));

  if (connect(sock, reinterpret_cast<sockaddr *>(&data), sizeof(data)) < 0)
    die("Failed to connect to server");

  std::string input_buffer;
  char buffer[BUFF_SIZE + 1];
  size_t readb = 0;
  while (std::getline(std::cin, input_buffer)) {
    if (send(sock, input_buffer.c_str(), input_buffer.size(), 0) < 0)
      die("Couldn't send it");
    if ((readb = recv(sock, buffer, BUFF_SIZE, 0)) < 0) die("Couldn't receive");
    buffer[readb] = '\0';
    std::cout << buffer << std::endl;
  }
  return 0;
}
