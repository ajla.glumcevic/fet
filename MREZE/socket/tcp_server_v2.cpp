#include <arpa/inet.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <string_view>

#define BUFF_SIZE 1024

void die(std::string_view reason) {
  std::cout << reason << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

int main(int argc, char *argv[]) {
  const auto sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) die("Goodbye cruel world");

  sockaddr_in data;
  data.sin_family = AF_INET;
  data.sin_addr.s_addr = INADDR_ANY;
  data.sin_port = htons(10000);
  memset(data.sin_zero, 0, sizeof(data.sin_zero));

  if (bind(sock, reinterpret_cast<sockaddr *>(&data), sizeof(data)) < 0)
    die("Couldn't bind socket");
  if (listen(sock, 10) < 0) die("Couldn't listen on socket");

  char buffer[BUFF_SIZE + 1];
  size_t bread = 0;
  const std::string msg = "Message from server";
  while (true) {
    const auto peer_sock = accept(sock, nullptr, nullptr);
    if (peer_sock < 0) die("Failed to accept connection");

    if (const auto pid = fork(); pid < 0) {
      die("Failed to fork");
    } else if (pid == 0) {
      close(sock);
        while ((bread = recv(peer_sock, buffer, BUFF_SIZE, 0))) {
        if (bread < 0) die("Error while reading from socket");
        buffer[bread] = '\0';
        std::cout << buffer << std::endl;
        send(peer_sock, msg.c_str(), msg.size(), 0);
      }
      close(peer_sock);
      return 0;
    }
    close(peer_sock);
  }
  close(sock);
  return 0;
}
