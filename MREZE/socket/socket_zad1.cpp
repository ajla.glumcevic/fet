#include <arpa/inet.h>
#include <netinet/ip.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <chrono>
#include <iostream>
#include <string_view>
#include <thread>

#define BUFF_SIZE 1024

void die(std::string_view reason) {
  std::cout << reason << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

int main(int argc, char *argv[]) {

  if(argc<3) {
  std::cout << "Missing arguments!" << std::endl;
  return 1;
  }

  const auto sock_udp = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock_udp < 0) die("umro udp soket");

  sockaddr_in data;
  data.sin_family = AF_INET;
  data.sin_addr.s_addr = INADDR_ANY;
  data.sin_port = htons(atoi(argv[1]));
  memset(data.sin_zero, 0, sizeof(data.sin_zero));


  if (bind(sock_udp, reinterpret_cast<sockaddr *>(&data),
           sizeof(data)) < 0)
    die("Couldn't bind socket for udp");


  char udp_buffer[BUFF_SIZE + 1];
  ssize_t rec = 0;
  sockaddr peer_data;
  socklen_t sz = sizeof(peer_data);
  rec = recvfrom(sock_udp, udp_buffer, BUFF_SIZE, 0, reinterpret_cast<sockaddr*>(&peer_data), &sz);
  if(rec<0) die("problem oko udp"); 
  udp_buffer[rec] = '\0';
  std::cout << udp_buffer << std::endl;
    std::string msg = "Message from server";
 sendto(sock_udp, &msg[0], msg.length(), 0, &peer_data, sizeof(peer_data));
      //tcp konekcija

  const auto sock_tcp = socket(AF_INET, SOCK_STREAM, 0);
  sockaddr_in data_tcp;
  data_tcp.sin_family = AF_INET;
  data_tcp.sin_addr.s_addr = INADDR_ANY;
  data_tcp.sin_port = htons(atoi(udp_buffer));
  memset(data.sin_zero, 0, sizeof(data.sin_zero));
  //
  if (bind(sock_tcp, reinterpret_cast<sockaddr *>(&data_tcp), sizeof(data_tcp)) < 0)
    die("Couldn't bind socket for tcp");

  std::cout << "here" << std::endl;

  if (listen(sock_tcp, 10) < 0) die("Couldn't listen on socket");

  char buffer[BUFF_SIZE + 1];
  size_t bread = 0;
  while (true) {
    int attempts = 0;
    auto peer_sock = accept(sock_tcp, nullptr, nullptr);
    while (attempts < atoi(argv[2]) && peer_sock < 0) {
      ++attempts;
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      peer_sock = accept(sock_tcp, nullptr, nullptr);
    }
  //
    if (attempts == atoi(argv[2])) die("previse pokusaja oko tcp");

    while ((bread = recv(peer_sock, buffer, BUFF_SIZE, 0))) {
      if (bread < 0) die("Error while reading from socket");
      buffer[bread] = '\0';
      std::cout << buffer << std::endl;
    }
  }
  close(sock_tcp);
  return 0;
}
