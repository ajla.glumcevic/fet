#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <iostream>
#include <string_view>

#define BUFF_SIZE 1024

void die(std::string_view msg) {
  std::cout << msg << std::endl;
  std::cout << strerror(errno) << std::endl;
  exit(1);
}

int main(int argc, char *argv[]) {
  const auto sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0) die("Couldn't create socket");

  sockaddr_in server_data;
  server_data.sin_family = AF_INET;
  server_data.sin_addr.s_addr = htonl(INADDR_ANY);
  server_data.sin_port = htons(10000);
  memset(server_data.sin_zero, 0, sizeof(server_data.sin_zero));

  if (bind(sock, reinterpret_cast<sockaddr *>(&server_data),
           sizeof(server_data)) < 0)
    die("Couldn't bind socket");

  char data_buffer[BUFF_SIZE + 1];
  char ip[INET_ADDRSTRLEN];
  ssize_t rec = 0;
  sockaddr_in peer_data;
  socklen_t sz = sizeof(peer_data);
  while ((rec = recvfrom(sock, data_buffer, BUFF_SIZE, 0, reinterpret_cast<sockaddr*>(&peer_data), &sz))) {
    data_buffer[rec] = '\0';
    inet_ntop(AF_INET, &peer_data.sin_addr.s_addr, ip, INET_ADDRSTRLEN); 
    std::cout << data_buffer << '@' << ip << ':' << ntohs(peer_data.sin_port) << std::endl;
  }
  return 0;
}
