#!/bin/bash

VM_NAME=stretch

set_up() {
#######################################################################
set +e
cloonix_cli nemo kil
set -e
echo waiting 5 sec
echo
sleep 5 
cloonix_net nemo
#----------------------------------------------------------------------

#######################################################################
cloonix_gui nemo
#----------------------------------------------------------------------

#######################################################################
cloonix_cli nemo add kvm router 200 2 3 0 ${VM_NAME}.qcow2 &
cloonix_cli nemo add kvm one 200 2 1 0 ${VM_NAME}.qcow2 &
cloonix_cli nemo add kvm two 200 2 1 0 ${VM_NAME}.qcow2 &
cloonix_cli nemo add kvm three 200 2 1 0 ${VM_NAME}.qcow2 &
cloonix_cli nemo add kvm four 200 2 1 0 ${VM_NAME}.qcow2 &
# cloonix_cli nemo add kvm five 200 2 1 0 ${VM_NAME}.qcow2 &
#----------------------------------------------------------------------

#######################################################################
for i in {1..3}; do 
  cloonix_cli nemo add snf "sniffer$i"
done
#----------------------------------------------------------------------

sleep 10

#######################################################################
cloonix_cli nemo add lan one 0 lan1
cloonix_cli nemo add lan router 0 lan1
#----------------------------------------------------------------------

#######################################################################
cloonix_cli nemo add lan two 0 lan2
cloonix_cli nemo add lan three 0 lan2
cloonix_cli nemo add lan router 1 lan2
#----------------------------------------------------------------------

#######################################################################
cloonix_cli nemo add lan four 0 lan3
cloonix_cli nemo add lan router 2 lan3
#----------------------------------------------------------------------

#######################################################################
# cloonix_cli nemo add lan five 0 lan4 
# cloonix_cli nemo add lan router 3 lan4
#----------------------------------------------------------------------

#######################################################################
for i in {1..3}; do 
  cloonix_cli nemo add lan "sniffer$i" 0 "lan$i"
done
#----------------------------------------------------------------------

#######################################################################
set +e
for i in one two three four router; do
  while ! cloonix_ssh nemo ${i} "echo" 2>/dev/null; do
    echo ${i} not ready, waiting 5 sec
    sleep 5
  done
done
set -e
#----------------------------------------------------------------------

#######################################################################
for i in one two three four router; do
  cloonix_ssh nemo ${i} "sysctl -w net.ipv6.conf.all.disable_ipv6=1"
  cloonix_ssh nemo ${i} "sysctl -w net.ipv6.conf.default.disable_ipv6=1"
  cloonix_ssh nemo ${i} "hostnamectl set-hostname ${i}"
  cloonix_ssh nemo ${i} "service systemd-timesyncd stop"
  cloonix_ssh nemo ${i} "rm /var/lib/dhcp/*"
  cloonix_ssh nemo ${i} "ip l set dev eth0 up"
done

cloonix_ssh nemo router "ip l set dev eth1 up"
cloonix_ssh nemo router "ip l set dev eth2 up"
# cloonix_ssh nemo router "ip l set dev eth3 up"
cloonix_ssh nemo router "sysctl net.ipv4.ip_forward=1"
#----------------------------------------------------------------------

#######################################################################
cloonix_ssh nemo router "ip a add 1.$1.$2.1/24 dev eth0"
cloonix_ssh nemo router "ip a add 2.$3.$4.1/24 dev eth1"
cloonix_ssh nemo router "ip a add 3.$5.$6.1/24 dev eth2"
# cloonix_ssh nemo router "ip a add 192.168.0.1/24 dev eth3"
# cloonix_ssh nemo five "ip a add 192.168.0.2/24 dev eth0"
# cloonix_ssh nemo five "ip r add default via 192.168.0.1 dev eth0"
cloonix_ssh nemo one "ip a add 1.$1.$2.2/24 dev eth0"
cloonix_ssh nemo one "ip r add default via 1.$1.$2.1 dev eth0"
cloonix_ssh nemo four "ip a add 3.$5.$6.2/24 dev eth0"
cloonix_ssh nemo four "ip r add default via 3.$5.$6.1 dev eth0"
#----------------------------------------------------------------------

#######################################################################
DHCP_CONF=$(cat <<-END
local=/fet.ba/
expand-hosts
listen-address=127.0.0.1,1.$1.$2.1,2.$3.$4.1,3.$5.$6.1
END
)
cloonix_ssh nemo router "echo \"$DHCP_CONF\" > /etc/dnsmasq.conf"
cloonix_ssh nemo router "echo '192.168.0.2 five' >> /etc/hosts"
cloonix_ssh nemo router "echo '1.$1.$2.2 one' >> /etc/hosts"
cloonix_ssh nemo router "echo '10.$5.$6.2 four' >> /etc/hosts"
# cloonix_ssh nemo five "echo 'nameserver 192.168.0.1' > /etc/resolv.conf"
cloonix_ssh nemo one "echo 'nameserver 1.$1.$2.1' > /etc/resolv.conf"
#----------------------------------------------------------------------
}

set_up $((RANDOM % 256)) $((RANDOM % 256)) $((RANDOM % 256)) $((RANDOM % 256)) $((RANDOM % 256)) $((RANDOM % 256))

#######################################################################
#----------------------------------------------------------------------







