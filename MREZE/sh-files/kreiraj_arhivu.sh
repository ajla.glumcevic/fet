#!/bin/bash

if [ -z "$1" ]
then
  echo "Proslijedite ime i prezime u formatu ime_prezime"
  exit 1
fi

NAME="$1_2"

rm -f -r "$NAME/"
rm -f "NAME.tar.gz"

for i in one two three four router; do
  echo "Extracting from $i"
  mkdir -p "$NAME/log/$i/"
  cloonix_ssh nemo $i "ip a" | tee "$NAME/log/$i/ipa.txt" > /dev/null
  cloonix_ssh nemo $i "ip r" | tee "$NAME/log/$i/ipr.txt" > /dev/null
  cloonix_ssh nemo $i "sysctl net.ipv4.ip_forward" | tee "$NAME/log/$i/fw.txt" > /dev/null
  echo $NAME >> "$NAME/log/$i/fw.txt"
done


mkdir -p "$NAME/router"
cloonix_ssh nemo router "cat /etc/dnsmasq.conf" | tee "$NAME/router/dnsmasq.conf" > /dev/null
cloonix_ssh nemo router "cat /var/lib/misc/dnsmasq.leases" | tee "$NAME/router/dnsmasq.leases" > /dev/null
cloonix_ssh nemo router "cat /etc/hosts" | tee "$NAME/router/hosts" > /dev/null
cloonix_ssh nemo router "iptables -S" | tee "$NAME/router/filter.txt" > /dev/null
cloonix_ssh nemo router "iptables -t nat -S" | tee "$NAME/router/nat.txt" > /dev/null

mkdir "$NAME/two/" "$NAME/three" "$NAME/four"
cloonix_ssh nemo two "cat /var/lib/dhcp/dhclient.leases" | tee "$NAME/two/dhclient.leases" > /dev/null
cloonix_ssh nemo three "cat /var/lib/dhcp/dhclient.leases" | tee "$NAME/three/dhclient.leases" > /dev/null
cloonix_ssh nemo four "cat /etc/resolv.conf" | tee "$NAME/four/resolv.conf" > /dev/null

for i in {1..3}; do 
  cloonix_cli nemo cnf snf off "sniffer$i" 
  cloonix_cli nemo cnf snf on "sniffer$i" 
done


one_ip=$(cloonix_ssh nemo one "ip a show dev eth0" | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b")
# cloonix_ssh nemo five "echo "" > /dev/udp/four/9000" 
cloonix_ssh nemo one "ping -c1 four" | tee -a "$NAME/ping.txt" > /dev/null
cloonix_ssh nemo four "ping -c1 $one_ip" | tee -a "$NAME/ping.txt" > /dev/null

mkdir "$NAME/pcap/"
cp /opt1/cloonix_data/nemo/snf/*.pcap "$NAME/pcap/."

tar -zcf "$NAME.tar.gz" $NAME
rm -r "$NAME"
echo "Kreiranu arhivu $NAME.tar.gz predati na classroom"

