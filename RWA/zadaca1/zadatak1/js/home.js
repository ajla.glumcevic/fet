const data = [
    new VideoInfo(0, "Thor Ragnarok", "ue80QwXMRHg", 0, 0, 'thorragnarok.jpg'),
    new VideoInfo(1, "Doctor Strange", "UffVhIixpLA", 0, 0, 'doctorstrange.jpg'),
    new VideoInfo(2, "Black Panther", "xjDjIWPwcPU", 0, 0, 'blackpanther.jpg'),
    new VideoInfo(3, "Captain America", "7SlILk2WMTI", 0, 0, 'captainamerica.jpg'),
    new VideoInfo(4, "Iron Man", "8ugaeA-nMTc", 0, 0, 'ironman.jpg'),
    new VideoInfo(5, "Black Widow", "ybji16u608U", 0, 0, 'blackwidow.jpg'),
    new VideoInfo(6, "Spiderman", "JfVOs4VSpmA", 0, 0, 'spiderman.jpg'),
    new VideoInfo(7, "Venom", "u9Mv98Gr5pY", 0, 0, 'venom.jpg'),
    new VideoInfo(8, "Guardians of the Galaxy", "d96cjJhvlMA", 0, 0, 'guardians.jpg'),
    new VideoInfo(9, "Captain Marvel", "Z1BCujX3pw8", 0, 0, 'captainmarvel.jpg'),
];
localStorage.setItem('data', data);
let sortedData = [];
let totalVotes = 0;

const slider = document.getElementsByClassName('scrolling-container')[0];
const top5 = document.getElementsByClassName('top5-container')[0];
const top5Rows = document.getElementsByClassName('rows')[0];


window.onload = function(event) {
    showCards(); 
    showRows();
}

function sortDataAsc() {
    sortedData = JSON.parse(JSON.stringify(data));
    sortedData.sort((video1,video2) => {return video2.votes - video1.votes});
}

function showCards() {
    data.forEach(video => createCard(video)); 
}

function showRows() {
    sortDataAsc();
    removeChildren(top5Rows);
    sortedData.forEach((video, index)  => { 
        video.rank = index + 1;
        if(index < 5) {
            createRow(video) 
        } 
    }); 
}

// Event handlers

function onBtnVoteClicked(video) {
    ++video.votes;
    ++totalVotes;
    sortDataAsc();
    showRows();
}

function onBtnRefreshClicked() {
    window.location.reload();
}

// DOM 

function removeChildren(element) {
    while(element.firstChild) {
        element.removeChild(element.firstChild)
    }
}

function createCard(video) {
    const div = document.createElement('div');
    div.setAttribute('class', 'card');
    div.setAttribute('id', `video${video.id}`);
    const h3 = document.createElement('h3');
    h3.setAttribute('class', 'video-title');
    h3.textContent = video.title;
    const iframe = document.createElement('iframe');
    iframe.src = `https:/www.youtube.com/embed/${video.urlId}`;
    const btn = document.createElement('button');
    btn.setAttribute('class', 'btn-vote');
    btn.textContent = "VOTE";
    btn.addEventListener('click', onBtnVoteClicked.bind(null, video));

    div.appendChild(h3);
    div.appendChild(iframe);
    div.appendChild(btn);
    slider.appendChild(div);
}

function createRow(video) {
    const div = document.createElement('div');
    div.setAttribute('class', 'row');
    const img = document.createElement('img');
    img.src = `./images/${video.image}`
    const span1 = document.createElement('div');
    span1.setAttribute('class', 'title-span');
    span1.textContent = video.title;
    const span2 = document.createElement('div');
    span2.setAttribute('class', 'votes-span');
    span2.textContent = video.votes + '/' + totalVotes;
    const span3 = document.createElement('div');
    span3.setAttribute('class', 'rank-span');
    span3.textContent = video.rank;

    div.appendChild(img);
    div.appendChild(span1);
    div.appendChild(span2);
    div.appendChild(span3);
    top5Rows.appendChild(div);
}


document.getElementById('btn-refresh').onclick = onBtnRefreshClicked;
















function VideoInfo(id, title, urlId, votes, rank, image) {
    this.id = id;
    this.title = title;
    this.urlId = urlId;
    this.votes = votes;
    this.rank = rank;
    this.image = image;
}


function randomizeArray(arr) {
    const randomized = JSON.parse(JSON.stringify(arr));
    randomized.sort((obj1, obj2) => 0.5 - Math.random());
    return randomized;
}

