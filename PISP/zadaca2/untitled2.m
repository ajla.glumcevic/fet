s = struct('x',{4 3 2 1 5 6},'y',{1 2 3 2 5 4})
[o p] = zadatakt(s)

function y= zadatakt(A)

if nargin~=1
    error ('Funckija prima samo jednu strukturu')
end
if length(A)<6
    error ('Struktura mora imati najmanje 6 tacaka')
end
min1=A(1).x;
min2=A(1).y;
max1=A(1).x; 
max2=A(1).y;
for g=1:length(A)
    if A(g).x<min1 
        min1=A(g).x;
    end
    if A(g).y<min2
        min2=A(g).y;
    end
    if A(g).x>max1
        max1=A(g).x;
    end
    if A(g).y>max2
        max2=A(g).y;
    end
end
if (max1-min1<=max2-min2)
max2=max1;
min2=min1;
else 
    max1=max2;
    min1=min2;
end

disp (['Gornja desna tacka: (',num2str(max1),',',num2str(max2),')']) 
disp (['Donja desna tacka: (',num2str(min1),',',num2str(min2),')']) 


for i=1:length(A)
plot(A(i).x, A(i).y,'ko');
axis ([-10 10 -10 10])
hold on
end 
B=[min1,min1,max1,max1,min1];
C=[max2,min2,min2,max2,max2];
plot(B,C,'g--');

P=zeros(1,2);
P(1,1)=4*(max1-min1);
P(1,2)=(max1-min1)*(max1-min1);
y=P;

end

