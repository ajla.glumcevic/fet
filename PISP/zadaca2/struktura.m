% struktura.m
s = struct('Oznaka',{'A' 'B' 'C' 'D' 'E', 'F'},'x',{-1, 3 , 1, 0 , 5 -2},'y',{2,3,4,-2,6,0})
x = struktur(s)

function C = struktur(S)
if length(S) < 5
error('Potrebno min pet tacaka.');
end

maxX = S(1).x;
maxY = S(1).y;
minX = S(1).x;
minY = S(1).y;

for i=1:length(S)
if(maxX < S(i).x) maxX = S(i).x;
elseif(minX > S(i).x) minX = S(i).x;
end

if(maxY < S(i).y ) maxY = S(i).y;
elseif(minY > S(i).y ) minY = S(i).y;
end
end

disp(['Gornja lijeva tacka: (' num2str(minX) ',' num2str(maxY) ')']);
disp(['Donja desna tacka: (' num2str(maxX) ',' num2str(minY) ')']);

fprintf("Tacke u pravougaoniku: ")
for i = 1:length(S)
if S(i).x < maxX && S(i).x> minX && S(i).y < maxY && S(i).y > minY
fprintf("%c", S(i).Oznaka);
end
end
fprintf("\n");

fprintf("Tacke na rubu pravougaonika: ")
for i = 1:length(S)
if (S(i).x == maxX || S(i).x == minX)&&(S(i).y~= maxY && S(i).y~=minY) || (S(i).y == maxY...
|| S(i).y == minY)&&(S(i).x~= maxX && S(i).x~=minX)
fprintf("%c", S(i).Oznaka);
end
end

fprintf("\n");
fprintf("Tacke na tjemenu pravougaonika: ")
for i = 1:length(S)
if (S(i).x == maxX && S(i).y == maxY) || (S(i).x == maxX && S(i).y == minY) || (S(i).x ==...
minX && S(i).y == minY) || (S(i).x == minX && S(i).y == maxY)
fprintf("%c", S(i).Oznaka);
end
end

fprintf("\n")
a = maxX - minX;
b = maxY - minY;
obim = 2*a + 2*b;
povrsina = a*b;
dijagonala = sqrt( a^2 + b^2 );

C = cell(1,3);
C = {obim, povrsina, dijagonala};

X = [minX minX maxX maxX minX]
Y = [maxY minY minY maxY maxY]

plot(X, Y, 'r--');
axis([-10 10 -10 10 ]);
grid on;
hold on;

for i = 1: length(S)
plot(S(i).x, S(i).y, 'bo', 'MarkerSize', 5);
hold on;
end
end
