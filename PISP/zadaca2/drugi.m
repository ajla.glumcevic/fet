syms R1 R2 R3 R4 R5 R6 V1 V2 Is real;
R = [R2 1 2;
     R3 2 3;
     R4 2 0;
     R5 3 0;
     R1 3 4;
     R6 3 4;];
 V = [V1 2 3;
      V2 1 0];
  I = [Is 0 4];
  
  kolo.R = R;
  kolo.V = V;
  kolo.I = I;
  kolo.simb = 1;
  
  [G,B,C,D,A] = amatrica(kolo);
  Z = zmatrica(kolo);
  x = A\Z;
  disp('Napon na krajevima R1:');
  pretty(x(3)-x(4));
  disp('Napon na krajevima R4:');
  pretty(x(2));
  disp('Struja kroz V1:');
  pretty(x(5));

