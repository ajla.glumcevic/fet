%polinomi.m

function R = polinomi(P, Q)


if nargout>1
    error('Funkcija ima jedan  izlazni arg. ili ga nema');
end

if nargin>2 
    error('Funkcija ima jedan ili dva ulazna argumenta.');
end

if nargin==1
   
    sP = size(P);
    if sP(1) > 1
       error('Argument mora biti vektor.');
    end
    
   x = -8: 0.4 : 8;
   P1 = polyval(P,x);
   P1 = P1 + randn(size(P1));
   plot(x, P1, 'gs');
   hold on;
   red = length(P)-1;
   P2 = polyfit(x, P1, red);
   plot(x, polyval(P2,x), 'r');
   hold on;
   title(['Aproksimacija polinomom ' int2str(red) '.reda']);
   xlabel('x osa');
   ylabel('y osa');
   grid on;
   
   R = roots(P);
   R = R(find(imag(R)==0));
   plot(R, 0, 'ko', 'MarkerSize', 12);
    
end


if nargin==2
    
    sP = size(P);
    sQ = size(Q);
    if(sP(1) || sQ(1)) > 1
       error('Argument mora biti vektor.');
    end
    
    duzi = P;
    kraci = Q;
    dodajNule = 0;
       
  if length(P)> length(Q)
      dodajNule = length(P) - length(Q);
      duzi = P;
      kraci = Q;
  else
      dodajNule = length(Q) - length(P);
      duzi = Q;
      kraci = P;
  end
      % sabiranje
      kraci_ = [zeros(1, dodajNule ), kraci];
      disp(['Zbir= ' num2str(duzi + kraci_)]);
    
    % oduzimanje
      disp(['Razlika= ' num2str(duzi - kraci_)]);
  
    % mnozenje
    disp(['Proizvod= ' num2str(conv(P,Q))]);
    
    % dijeljenje
    [kolicnik ostatak] = deconv(duzi, kraci);
    disp(['Kolicnik= ' num2str(kolicnik)]);
    disp(['Ostatak= ' num2str(ostatak)]);
    
  
    
     R = -1;
    
    end
    
 
        
end