clear all 
close all 
clc


f = figure('Tag', 'Figure1', 'menubar', 'none','Name', 'zlatan', 'Position', [700 700 400 400]);

g = axes('Position', [0.1 0.1 0.8 0.7], 'parent', f);

title = uicontrol('Style', 'text', 'Units', 'normalized'...
    , 'Position', [0.1 0.92 0.8 0.05]...
    ,'String', 'APROKSIMACIJA PODATAKA SUMA', 'parent', f);

tekst =  uicontrol('Style' , 'text', 'Units', 'normalized'...
    ,'Position',[0.1 0.83 0.2 0.07], 'String', 'Polinom i interval'...
    ,'parent', f, 'FontSize', 8);

polinom  =  uicontrol('Style' , 'edit', 'Units', 'normalized'...
    ,'Position',[0.31 0.83 0.17 0.05]...
    ,'parent', f);

interval  =  uicontrol('Style' , 'edit', 'Units', 'normalized'...
    ,'Position',[0.52 0.83 0.17 0.05]...
    ,'parent', f);

popup = uicontrol('Style', 'popup', 'String'...
    ,'|+|*|o|x','Units', 'normalized'...
    , 'parent', f, 'Position', [0.75 0.83 0.17 0.05]...
    ,'Callback', 'zlaja');









