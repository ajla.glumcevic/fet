%math.h



function math(znak)

if nargin~=1
    error('Funkcija ima jedan ulazni arg.');
end

if nargout~=0
    error('Funkcija nema izlaznih arg');
end

if(znak ~= 'a' & znak~='b' & znak~='c')
    error('Pogresan input');
end

if znak=='a'
    syms x;
    brojnik = 2 + log(x);
    nazivnik = 2* sqrt(x);
    f = brojnik/nazivnik;
    
    I = int(f, -4, 4);
    pretty(I)
    
    y = dsolve('2*D3y - 3*D1y = sin(3*x)', 'y(0)=1', 'Dy(0)=1', 'x');
    pretty(y)
    
end    

if znak=='b'
    syms x;
    brojnik = log(x) + 2;
    nazivnik = sqrt(x);
    f = brojnik / nazivnik;
    fig = figure;
    ezplot(f, [-1 10 -4 4], fig);
    hold on;
    grid on;
    
    nula = solve(brojnik);
    prekid = solve(nazivnik);
    
    plot(nula, 0, 'rs');
    
    hlimit = limit(f, inf);
    vlimit = solve(x==0);
    limit(f, x, vlimit, 'right');
    plot([-1 10] ,[hlimit hlimit], 'g');
    hold on;
    plot([vlimit vlimit] , [-4 4], 'g');
    hold on;
    
    fprim = diff(f);
    x0 = solve(fprim);
    y0  = subs(f, {x}, x0);
    plot(x0, y0, 'ko');
    
end

if znak=='c'
   
    
R = [110 2 0;
     100 3 0;
     90 1 2;
     80 2 3;
     70 3 4;
];

V = [12 1 0];
I = [2  0 4];

kolo.R = R;
kolo.V = V;
kolo.I = I;
kolo.simb = 0;

[G,B,C,D,A] =  amatrica(kolo);
z = zmatrica(kolo);
x = A\z;
disp(['Napon na krajevima otpornika R4: ' num2str(x(3)-x(2)) 'V']);
disp(['Vrijednost struje kroz naponski izvor V1: ' num2str(x(5)) 'A']);

    
end
