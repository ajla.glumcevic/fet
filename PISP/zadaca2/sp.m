
clear all
close all
xx = -4:0.1:4;
[n r maxred o maxO] = polinomi([2 3 0 -6],xx )

function [N  R maxRed O maxOznaka]= polinomi(P, X)

if nargin~=2
    error('e')
end

if nargout~=5
    error('e')
end


drugi = 0;
treci = 0;
x = 0;
plus = 0;
krug = 0;
i = 1;
fnum = 1;

while i == 1

    red = input('red: ');
    oznaka = input('oznaka: ');
    
    if red~=[2,3]
        break;
    end
    if oznaka~=['x','+','o']
        break;
    end
    
    if oznaka == 'x'
        x = x+1;
    elseif oznaka == '+'
        plus = plus + 1;
    else
        krug = krug + 1;
    end
        
     if red == 2
         drugi = drugi +1;
     else 
         treci = treci + 1;
     end
    
y = polyval(P, X);
y = y + randn(size(y));
y1 = polyfit(X, y, red);

figure(fnum)
plot(X, polyval(y1, X));
hold on;
plot(X, y, oznaka);

fnum = fnum + 1;
end
 
N = fnum;
redovi = [0 drugi treci];
[R maxRed] = max(redovi);
oznake = [x, plus, krug];
[O maxOznaka] = max(oznake);

if maxOznaka == 1
    O = 'x';
elseif maxOznaka == 2
    O = '+';
else
    O = 'o';
end

end

