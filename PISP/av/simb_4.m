x = sym('x', 'positive');
y = sym('y', 'positive');
f = 8/x + x/y + y;

fx = diff(f, x);
fy = diff(f, y);
[x01 x02] = solve(fx, fy); %nule

%odredjivanje lokal min i max
fxx = diff(fx, x);
fyy = diff(fy, y);
fxy = diff(fx, y);

D = fxx*fyy - fxy*fxy;
eks = subs(D, [x y], [x01 x02]);
A = subs(fxx, [x y], [x01 x02]);
ekstrem = subs(f, [x y], [x01 x02]); %y01 i y02

if(eks < 0) disp('Funkcija nema ekstrem') 
else
    if A > 0 disp('Min funkcije: ')
        [x01 x02], ekstrem
    else disp('Max funkcije: ')
        [x01 x02], ekstream
    end
end



