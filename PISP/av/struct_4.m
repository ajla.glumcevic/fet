

R = [ 70 1 2;
      80 2 3;
      50 3 4;
      100 2 0;
      120 3 0;
      ];
  V = [12 1 0];
  I = [1 0 4];
  
  kolo.V = V;
  kolo.I = I;
  kolo.R = R;
  
  kolo.simb = 0;
  
  [G B C D A] = amatrica(kolo);
  z = zmatrica(kolo);
  
  x = A\z;
  
  fprintf('struja kroz V1: %f\n ', x(5));
  fprintf('pad napona na R4: %f\n', x(3)-x(2));
  