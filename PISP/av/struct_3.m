function D = z3


if nargin ~= 0
error('Funkcija nema ulaznih argumenata')
end
% if nargout ~= 1
% error('Funkcija ima jedan izlazni argument')
% end
n=input('Koliko tačaka želite upisati? ');
if n<0
error('Pogrešan unos')
end

D = 0;
maxx = 0;
maxy = 0;


for i=1:n
disp(['Unos podataka ' num2str(i) '. tačke:'])
T(i).x=input('Unesite x koordinatu tačke: ');
T(i).y=input('Unesite y koordinatu tačke: ');

d = sqrt(T(i).x*T(i).x + T(i).y*T(i).y);

if(d>D) D = d; maxx = T(i).x ; maxy = T(i).y; 
end

end

tx=[0 T(1:n).x];
ty=[0 T(1:n).y];
plot(tx,ty,'bo','MarkerSize',12);

a=[0 maxx];
b=[0 maxy];


plot(a,b,'r');
axis([-10 10 -10 10]);
grid on;

