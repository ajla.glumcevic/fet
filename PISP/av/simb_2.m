syms x;
y = 2*x.^3 + 3*x.^2-36.*x - 10;

yprim = diff(y); %prvi izvod
x0 = solve(yprim); %nule izvoda
y0 = subs(y,{x}, x0); %u izrazu y sve ikseve zamjeni sa x0 da dobijem y0

yprim2 = diff(yprim); %drugi izvod
e = subs(yprim2, {x}, x0); %odredjivanje prirode ekstrema

a = find(e<0);
b = find(e>0);
lokalni_maksimum = [x0(a) y0(a)];
lokalni_minimum = [x0(b) y0(b)];

ezplot(y);
plot(x0, y0, 'rs');
grid on;