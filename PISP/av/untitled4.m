%uzorak2.m

x = [1 2 3 4];
n = length(x);
y = zeros(n, 2*n);
y(1,1:n) = fliplr(x);
y(n,1:n) = x;
y(: ,n ) = x';

y(1:n,n+1:2*n) = fliplr(diag(x))
y(:,2*n) = x';