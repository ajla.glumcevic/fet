P1 = [2 5 0 -4];
r2 = [-1 2 6];
r1 = roots(P1);
P2 = poly(r2);
P = conv(P1, P2);
x = -5:0.1:5;

y = polyval(P, x);
y = y + randn(size(y));

figure(1)
plot(x, y, 'd');
hold on;
Y1 = polyfit(x, y , 2);
Y2 = polyfit(x, y, 3);

plot(x, polyval(Y1, x), 'r');
hold on;
plot(x, polyval(Y2, x), 'b--');
title('Sum i aproksimacije');
xlabel('x osa');
ylabel('y osa');
grid on;

