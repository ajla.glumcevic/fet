syms x;
%plotanje funkcije
f = log(x)/x;
fig = figure;
ezplot(f, [-1,6,-6,1],fig);
grid on;
hold on;

%odredjivanje nule
nula = solve(log(x));
plot(nula, 0, 'ko'); %plotanje nule tj y=0

%asimptote
horizontalna = limit(f,inf);
vertikalna = solve(x==0);
limit(f,x,vertikalna,'right');
plot([-1 6], [horizontalna horizontalna], 'r');
plot([vertikalna vertikalna], [-6 1],'g');

%ekstremi
fprim = diff(f);
x0 = solve(fprim);
y0 = subs(f,{x}, x0);
plot(x0, y0, 'ks');
