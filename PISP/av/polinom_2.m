
P1 = [1 0 -3 4];
r1 = roots(P1);
r2 = [1 -2 3];
P2 = poly(r2);

x = -4:0.1:4;
P = conv(P1, P2);
y = polyval(P,x);

y = y + randn(size(y));

Y1 = polyfit(x, y , 2);
Y2 = polyfit(x, y, 3);

figure(1)
plot(x, y, 'ko');
hold on
plot(x , polyval(Y1,x), 'r--');

figure(2)
plot(x, y, 'gd');
hold on;
plot(x, polyval(Y2,x), 'r');