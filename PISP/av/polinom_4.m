function z2(znak)

if nargin~=1
    error('Funkcija ima jedan ulazni argument');
end

if nargout ~=0
    error('Funkcija nema izlaznih argumenata');
end

if znak~=a || znak~=b
    error('Pogreaan input');
end

if znak=='a'
    
    x = [-pi:0.01:pi];
    y = [-pi:0.01: pi];
    z = 3.*sin(x).*cos(y);

    [X Y ] = meshgrid(x, y);
    Z = 3.*sin(X).*cos(Y);

    
    surf(X,Y,Z);
    shading interp;
    colormap(gray);
    title('Povrsina funkcije')

end

if znak=='b'
    
    P = [2 3 0 -6];
    x = -4:0.1:4;
    y = polyval(P,x);
    y = y + randn(size(y));
    
    
    plot(x, y, 'd');
    hold on;
    Y = polyfit(x, y, 2);
    plot(x, polyval(Y, x), 'r');
    title('Aproksimacija polinomom')
    grid on;
end

end

