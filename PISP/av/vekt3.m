n=input('Upisi prvi broj ');
m=input('Upisi drugi broj ');

if (n<=0 || m<=0)
    error('n i m moraju biti >0')
end
y = zeros(n);
y1 = ones(1,n);
y1(find(y1==1))=2*n;
y1 = fliplr(diag(y1));
y = y+y1;
y(find(y==0))=3*m^2;
y

if (mod(n,2)==0)
    y([round(n/2), round(n/2-1)],:)=[];
else
    y(round(n,2),:)=[];
end
y

