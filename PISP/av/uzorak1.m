%uzorak1.m

x = [1,2,3,4];
n = length(x);
y = zeros(n, 2*n);
y(1:n,1:n) = diag(x);
y(1:n,n+1:2*n) = fliplr(diag(x))