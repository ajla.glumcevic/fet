function N = z3


if nargin ~= 0
error('Funkcija nema ulaznih argumenata')
end
% if nargout ~= 1
% error('Funkcija ima jedan izlazni argument')
% end
n=input('Koliko tačaka želite upisati? ');
if n<0
error('Pogrešan unos')
end
n1=0; n2=0; n3=0; n4=0;



for i=1:n
disp(['Unos podataka ' num2str(i) '. tačke:'])
T(i).x=input('Unesite x koordinatu tačke: ');
T(i).y=input('Unesite y koordinatu tačke: ');
if ( T(i).x >=0 & T(i).y >=0 ) n1=n1+1;
elseif ( T(i).x <0 & T(i).y >=0 ) n2=n2+1;
elseif ( T(i).x <0 & T(i).y <0 ) n3=n3+1;
elseif ( T(i).x >=0 & T(i).y <0 ) n4=n4+1;
else disp('Pogrešan unos!'), break
end
end
disp('Broj tačaka u prvom kvadrantu: '), n1
disp('Broj tačaka u drugom kvadrantu: '),n2
disp('Broj tačaka u trećem kvadrantu: '),n3
disp('Broj tačaka u četvrtom kvadrantu: '), n4
tx=[0 T(1:n).x];
ty=[0 T(1:n).y];
plot(tx,ty,'ro','MarkerSize',12);
[maxVal,maxInd]=max([n1,n2,n3,n4])
N=maxInd;