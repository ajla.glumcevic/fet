
sym x;
brojnik = 3*x^3 - x^2 -3*x+1;
nazivnik = x^2 + 4*x+4;
f = brojnik/nazivnik;
nule = solve(brojnik);
prekid = solve(nazivnik);

sym x;
f = x.^2*sin(2.*x);
ezplot(f);
I = int(f, -1, 4);

sym x;
Y = dsolve('2*D2y + 3*Dy + 4*y = cos(5*x)', 'y(0)=0', 'Dy(0)=1', 'x');
ezplot(Y)

sym x;
jed1 = 'x.^2+ y.^2 - 5 = 0';
jed2 = '-2*x + y -2 = 0';
S = solve(jed1, jed2);
ezplot(jed1);
hold on;
ezplot(jed2);
plot(S.x, S.y , 'yd');

