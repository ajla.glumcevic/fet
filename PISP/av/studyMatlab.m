%helloWorld
start=clock;
size(start);
startString=datestr(start);

disp('Ja cu nauciti MATLAB');
disp(['Poceo sam uciti Matlab ' startString]);
secPerDay =60*60*24;
tau=1.5*secPerDay;
endOfClass=5*secPerDay;
knowledgeAtEnd=1-exp(-endOfClass/tau);
disp(['Na kraju PISP-a ja cu znati ' num2str(knowledgeAtEnd*100) 'Matlaba.']);