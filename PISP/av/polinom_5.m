% function [N, red, oznaka, br_red, br_oznaka] = polinomi(P,X)
P = [3 4 2 -1];
X = -5:0.1:5;
% if nargin~=2 error('Dva su ulazna argumenta')end
% if nargout~=5 error('Pet je izlaznih argumenata')end

N = 0;
drugi_red = 0;
treci_red =0;
iks = 0;
plus = 0;
kruzic = 0;

i=1;

Y = polyval(P,X);
Y = Y + randn(size(Y));

while (i>0)

    n = input('Unesite red polinoma drugi ili treci ');
    oznaka = input('Unesite oznaku x + ili kruzic ');
     if n>3 
         disp('Pogresan red polinoma');
         break;
     end
    
      if oznaka~=['x' '+' 'o'] 
         disp('Pogresna oznaka.');
         break;
     end
   
    
    if n==2
        drugi_red = drugi_red+1;
    else
        treci_red = treci_red +1;
    end
    
    if oznaka=='x'
        iks = iks+1;
    elseif oznaka=='+'
        plus = plus + 1;
    else 
        kruzic = kruzic + 1;
    end
    
    
     N = N +1;

pn = polyfit(X, Y, n);
figure(N+1);
plot(X, Y);
hold on;
plot(X, polyval(pn, X));
grid on;


red = [0 drugi_red treci_red];
[broj_red red] = max(red);

oznaka = [iks plus kruzic];
[broj_oznaka oznaka] =max(oznaka);

if oznaka==1 oznaka='x';
elseif oznaka==2 oznaka='+';
else oznaka='o';
end

end

N
red
broj_red
oznaka
broj_oznaka

% end





