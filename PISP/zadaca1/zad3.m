%z3.m
%x=[1 2 3 4];
function [A B C ] = z3(x)
n = length(x);
if n<4
    error('Duzina vektora x mora biti minimalo 4.')
end
if nargin~=1
        error('Funkcija ima jedan ulazni argument.')
end
if nargout~=3
    error('Funkcija ima tri izlazna argumenta.')
end

A=zeros(n, 3*n);

A(1:n, 1:n)= diag(x);
A(end:-1:1, 2*n)= x';
A(n, 2*n:-1:n+1)=x;
A([1 n],2*n+1:3*n)=[x ;x];
A(:,[1 n n+1])=[x' x' x'];


    B = zeros(2*n-1);
    for i = 1:n
        B(i:2*n-i,i:2*n-i)=i;
    end
    B
    
    %A = [1 2 3 4]
    %n = length(A)
    %B = zeros(2*n-1);
    %for i = 1:n
        %B(i:2*n-i,i:2*n-i) = A(n-i+1);
    %end
B
    C = zeros(n);
   % for i=n:-1:1
   for i = 1:n
       % C(i, 1:n-i+1)=x(n-i+1:-1:1);
        %C(n-i+1, i+1:n) = x(2:n-i+1);
        C(i, 1:n-i) = x(n-i+1 :-1: 2) ;
        C(i, n-i+1:n)=x(1:i);
        
    end
    C
    
    