function z = fun(x,y)
z = sin(x).*cos(y);
end