

%z4.m

function z4(x,y, fun)
z = feval(fun,x,y);
[X Y] = meshgrid(x,y);
Z = feval(fun, X,Y);



figure;
subplot(3,1,1);
surf(X,Y,Z);
title('3D povrsina');
xlabel('x-osa');
ylabel('y-osa');
zlabel('z-osa');


subplot(3,1,2);
plot3(x,y,z,'kd--');

title('3D linijski grafikon');
xlabel('x-osa');
ylabel('y-osa');
zlabel('z-osa');
axis tight;

subplot(3,1,3)
imagesc(Z);
colorbar;
colormap(gray);
title('Slika matrice Z');

end
