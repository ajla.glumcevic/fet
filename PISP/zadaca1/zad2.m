%vektorizacija.m

%a
n = input('Unesi broj(n>4 i n je neparno)');
if(n<5) || mod(n,2)==0
    error('error')
end


A = ones(n).*(2*n+1)
B = round(rand(n-2)*5+4)
b1 = diag(diag(B))
c = diag(ones(1,n-2).*(n-3))
B = B-b1 +c
A(2:end-1, 2:end-1) = B

%b
x1 = sum(sum(A(2:end-1, 2:end-1)));
x2 = length(find(mod(A,2) & (A>n)));
x3 = length(  find(A([2 3],:)  < A(ceil(n*n/2)))  );
X = [x1 x2 x3]