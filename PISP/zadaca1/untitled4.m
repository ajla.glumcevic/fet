function [A B] = z3(x)
n = length(x);
if n < 3 
error('Velicina mora biti minimalno 3')
end
if nargin~=1
error('Funkcija ima jedan ulazni arg.')
end
if nargout~=2
error('Funkcija ima dva izlazna arg.')
end

A=zeros(n,3*n);
A(end:-1:1,n+1:2*n)=diag(x);
A(:,2*n+1:3*n)=diag(x);
A(:,[1,2*n+1,3*n])=[x' x' x'];
A(n,1:n)=x(end:-1:1);
A([1,n],n+1:2*n)=[x; x];

B = zeros(2*n-1);
for i = 1:n
   B(i:2*n-i, i:2*n-i) =x(n-i+1); 
end

end
