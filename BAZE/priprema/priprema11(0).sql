use stusluAG;

delimiter //
create procedure azurDiplom()
begin

declare done int default false;
declare ocjena_odbrane, mbr int;
declare datumprijave date;

declare no_more_rows condition for sqlstate '02000';
declare cur cursor for select mbrStud, datPrijava from diplom;
declare continue handler for no_more_rows
	set done = true;

set transaction isolation level repeatable read;
start transaction;
open cur;

read_loop: loop
if done then leave read_loop;
 end if;
fetch cur into mbr, datumprijave;
set ocjena_odbrane = ocjObrana(mbr, datumprijava);
update diplom set ukupOcjena = ocjena_odbrane
where mbrstud = mbr and datprijava = datumprijave;
end loop;

close cur;
commit;
end;//

delimiter ;
select * from diplom;