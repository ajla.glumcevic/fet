DROP DATABASE ag18135;
CREATE DATABASE ag18135 CHARACTER SET ="utf8" COLLATE = "utf8_croatian_ci";

USE ag18135;

CREATE TABLE stud(
mbrStud NCHAR(8) NOT NULL,
prezStud NCHAR(20) NOT NULL,
imeStud NCHAR(20) NOT NULL,
datRodStud DATE,
pbrRodStud INTEGER,
adrStud NCHAR(40),
prosjOcjena DECIMAL(3.2) NOT NULl
);
CREATE UNIQUE INDEX studUnique ON stud (mbrStud);
INSERT INTO stud VALUES
	("1","Glum", "Ajla",DATE("1999-12-24"),  75000, "Vakufska 17", 3.2),
    ("2", "Hadzic", "Melina",DATE("1999-07-12"), 75000, "Hadzic 17", 4.5),
    ("3", "Radovanovic", "Zlatan",DATE("1999-08-19"), 75000, "Stupine 17", 4.1);
-- SELECT * FROM stud;
UPDATE stud SET prezStud="Glumcevic" WHERE prezStud="Glum";
SELECT * FROM stud;
SELECT * FROM stud INTO OUTFILE "/var/lib/mysql-files/tmp/studenti.unl" 
-- 	FIELDS TERMINATED BY "#" LINES TERMINATED BY "\n";
-- DELETE FROM stud;
-- SELECT * FROM stud;
-- LOAD DATA INFILE "/var/lib/mysql-files/tmp/studenti.unl"
-- 	INTO TABLE stud 
--     FIELDS TERMINATED BY "#"
--     LINES STARTING BY "\n" TERMINATED BY "#\r"; 
-- SELECT * FROM stud;


    