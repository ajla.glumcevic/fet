use stusluAG;

#1 azurDiplom je iskoristen vec
select mbrstud, datprijava, ukupocjena from diplom;
select mbrstud, datprijava, ukupocjena from diplom
where ukupocjena > 1;
select avg(ukupocjena) from diplom 
where ukupocjena > 1;


drop function brojNepristup;
drop function prosjekOcjena;
#2
delimiter //
create function prosjekOcjena()
returns float
begin
declare res float;
	if @p1 is null then 
	select avg(ocjena) into res from ispit where ocjena > 1;
    set @p1 = 1;
    else
	set res = null;
    end if;
return res;
end
//
create function brojNepristup()
returns int
begin
declare res int;
	if @p2 is null then 
    select count(mbrstud) into res from stud
    where mbrstud not in (select mbrstud from ispit);
    set @p2 = 1;
    else
    set res = null;
    end if;
return res;
end
//

create function vrijemePrvogPoziva()
returns text
begin
declare res text;
	if @v is null then
    set @prvovrijeme = SYSDATE();
    end if;
    select concat(@prvovrijeme,' ', SYSDATE()) into res;
return res;
end
//

create procedure najboljih_n(in n int)
begin
	select stud.mbrstud,imestud,prezstud,avg(ocjena) from stud
    inner join ispit on ispit.mbrstud=stud.mbrstud
    group by 1,2,3
    order by 4 desc, 3, 2
    limit n;
end
//
drop procedure najboljih_n//
delimiter ;
call najboljih_n(25);

set @v = null;
select vrijemePrvogPoziva();

set @p1=null, @p2=null;
select prosjekOcjena();
select brojNepristup();



