USE stusluAG;

-- 1. Ispis šifri, imena, prezimena i koeficijenata za platu svih nastavnika
select sifNastavnik, imeNastavnik, prezNastavnik, koef from nastavnik;

-- 2. Ispis svih podataka (vrijednosti svih atributa) o predmetima
select * from pred;

-- 3. Lista različitih imena studenata (svako ime se u listi treba pojaviti samo jednom, bez obzira koliko
-- studenata ima jednako ime)
select distinct imeStud from stud;

-- 4. Ispis šifri nastavnika koji su bar jednom negativno ocijenili studente na ispitu iz predmeta sa šifrom 146.
-- Svaka se šifra takvih nastavnika u listi treba pojaviti samo jednom
select distinct sifNastavnik from ispit 
	where ocjena =1 and sifPred=146;
    
-- 5. Ispis matičnih brojeva studenata koji su POLOŽILI ispit iz predmeta sa šifrom 262
select mbrStud from ispit where ocjena>1 and sifPred=262;

-- 6. Ispis prezimena i imena nastavnika i iznosa plaće, ako se plaća nastavnika dobije množenjem broja 800
-- s koeficijentom koji je prije toga uvećan za 0.4
select prezNastavnik, imeNastavnik ,(koef+0.4)*800 from nastavnik; 

-- 7. Ispis prezimena nastavnika čija je plaća izračunata na gornji način manja od 8000 i veća od 3500
select prezNastavnik from nastavnik 
	where (koef+0.4)*800 between 3500 and 8000; 
    
-- 8. Ispis prezimena i prvog slova imena za sve nastavnike
select prezNastavnik, substring(imeNastavnik, 1,1) 
	as prvoSlovo from nastavnik;
select concat(prezNastavnik, ' ',substring(imeNastavnik, 1,1))
	from nastavnik;
    
--9. Ispis prezimena i imena, te inicijala studenata. Za svakog studenta se, pored imena i prezimena, ispisuje
-- jedan niz znakova sastavljen iz prvog slova imena, tačke, prvog slova prezimena i tačke, npr. za
-- studenta Andrija Bošnjak ispisuje se A.B.
select imeStud, prezStud , concat(substring(imeStud,1,1),
	'.',substring(prezStud,1,1),'.') as inicijali from stud;
    
-- 10. Ispis svih podataka o ispitima koji su obavljeni u julu bilo koje godine
select * from ispit where month(datIspit)=7;

-- 11. Ispis svih podataka o ispitima koji su obavljeni neke srijede u augustu bilo koje godine
select * from ispit where month(datIspit)=8 and weekday(datIspit)=2;

-- 12. Ispis matičnog broja studenta, ocjene, datuma ispita, današnjeg datuma te broja dana koji su protekli
-- nakon održavanja ispita.
select mbrStud, ocjena, datIspit, current_date, 
	datediff(current_date,datIspit) as brDana from ispit;
    
-- 13. Ispis podataka o studentima koji su rođeni na današnji dan prije tačno 25 godina (napomena: ne prije
-- 25*365 dana, nego na današnji dan prije 25 godina). Upit mora biti neovisan o datumu izvođenja.
-- Analizirajte što će se dogoditi ukoliko upit izvedemo na dan 29.2.2009. godine
select * from stud 
	where datRodStud=subdate(current_date, interval 25 year);
select * from stud 
	where datRodStud=subdate('2009-02-29', interval 25 year);
    
-- 14. Za svaki ispit ispisati šifru nastavika, matični broj studenta, ocjenu i cijeli broj koji pokazuje koliko je dana
-- proteklo između ispita i datuma 1.1.2009.
select sifNastavnik, mbrStud, ocjena, 
	abs(datediff(datIspit, '2009-01-01')) as brojDana from ispit;
    
-- 15. Za svaki ispit ispisati matični broj studenta, šifru predmeta, ocjenu i datum na koji će proći tačno 3800
-- dana od održavanja ispita. Ispisati samo one ispite za koje taj dan nije već prošao. Upit mora biti
-- neovisan o datumu izvođenja
select mbrStud, sifPred, ocjena, 
	adddate(datIspit, interval 3800 day) as noviDatum from ispit
    	 where adddate(datIspit, interval 3800 day)  >= current_date;
    
-- 16. Za svaki ispit ispisati matični broj studenta, šifru predmeta, ocjenu i datum održavanja i datum na koji će
-- se navršiti 3 godine od održavanja ispita. Analizirajte što će se dogoditi s upitom ukoliko je ispit održan
-- 29.2.1980. godine
select mbrStud, sifPred, ocjena, datIspit,
	adddate(datIspit, interval 3 year) from ispit;
select mbrStud, sifPred, ocjena, datIspit,
	adddate(datIspit, interval 3 year) from ispit
	where datIspit = '1980-02-29';
    
-- 17. Ispis svih podataka o rezervacijama, s time da se kao naslov za atribut sifPred u izlaznoj listi pojavljuje
-- tekst "predmet".
select oznDvorana, oznVrstaDan, sat, sifPred
	as predmet from rezervacija; 
    
-- 18. Obaviti projekciju relacije stud po atributima poštanski broj rođenja i poštanski broj stanovanja
select distinct pbrRod, pbrStan from stud;
