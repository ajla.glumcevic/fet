use stusluAG;
-- 1 Ispisati inicijal imena i prezime studenta, naziv predmeta (prvih 25 znakova) i datum ispita za svaki ispit
-- ocijenjen ocjenom 5 (21 zapis)
select concat(substr(imeStud,1,1), '.', prezStud), substr(nazPred,1,25), datIspit from stud
inner join ispit on stud.mbrStud=ispit.mbrStud
inner join pred on ispit.sifPred=pred.sifPred
where ocjena=5;
SELECT imeStud AS Student FROM stud WHERE Student LIKE 'A%';
-- Ukoliko stvarno hocu samo jednom da se pojave onda stavi distinct mada tad bude 4 rezultata
-- 2 Ispisati šifre i nazive organizacijskih jedinica čije neposredno podređene organizacijske jedinice imaju
-- naziv koji u sebi sadrži riječ "matem". Svaka se organizacijska jedinica u tom popisu smije naći najviše
-- jednom (5 zapisa)
select noj.siforgjed as NOJ, noj.nazorgjed, poj.siforgjed as POJ from orgjed as noj
inner join orgjed as poj on noj.siforgjed=poj.sifnadorgjed
where poj.nazorgjed like "%matem%";



-- 3 Ispisati nazive i kratice predemta koji pripadaju organizacijskim jedinicama čija neposredno nadređena
-- organizacijska jedinica ima naziv koji sadrži riječ "tehnike" (182 zapisa)
select nazPred , kratPred from pred
inner join orgjed as oj on pred.siforgjed=oj.siforgjed
inner join orgjed as noj on oj.sifnadorgjed=noj.siforgjed
where noj.nazOrgjed like "%tehnike%";

-- Oni su pogrijesili, radili su za mjesec maj
-- 4 Ispisati nazive organizacijskih jedinica onih predmeta koje su u aprilu 2000. godine ispitivali nastavnici
-- čije je mjesto stanovanja u županiji koja se naziva "Dubrovačko-neretvanska" (2 zapisa)
select nazOrgjed from orgjed
inner join nastavnik on nastavnik.siforgjed=orgjed.siforgjed
inner join ispit on ispit.sifnastavnik=nastavnik.sifnastavnik
inner join mjesto on nastavnik.pbrstan=mjesto.pbr
inner join zupanija on mjesto.sifzupanija=zupanija.sifzupanija
where month(datispit)=5 and year(datispit)=2000 
and zupanija.nazzupanija = "Dubrovacko-neretvanska";



-- 5 Ispisati podatke o studentima koje su na ispitu srušili nastavnici koji stanuju u županiji pod nazivom
-- "Osječko-baranjska". Svaki takav student se u popisu smije naći samo jednom (2 zapisa)
select distinct stud.* from stud
inner join ispit on ispit.mbrstud=stud.mbrstud
inner join nastavnik on nastavnik.sifnastavnik=ispit.sifnastavnik
inner join mjesto on nastavnik.pbrstan=mjesto.pbr
inner join zupanija on mjesto.sifzupanija=zupanija.sifzupanija
WHERE ocjena = 1 AND nazZupanija = 'Osječko-baranjska';


-- 6 Koliki je ukupni broj studenata koji su upisali predmete koji se predaju na organizacijskim jedinicama
-- čija je neposredno nadređena organizacijska jedinica “Fakultet elektrotehnike i računarstva” (2080)
select sum(upisanostud) from pred
inner join orgjed as poj on pred.siforgjed=poj.siforgjed
inner join orgjed as noj on poj.sifnadorgjed=noj.siforgjed
where noj.nazorgjed="Fakultet elektrotehnike i računarstva";

-- 7 Ispisati ime i prezime nastavnika, te naziv organizacijske jedinice u kojoj je zaposlen svaki nastavnik (98
-- zapisa)
select imenastavnik, preznastavnik, nazorgjed from nastavnik
inner join orgjed on orgjed.siforgjed=nastavnik.siforgjed;

-- 8 Ispisati ime, prezime, naziv mjesta u kojem stanuje, te naziv mjesta rođenja i naziv županije rođenja
-- svakog studenta (302 zapisa)
select imestud,prezstud,mjs.nazmjesto, mjr.nazmjesto, nazzupanija from stud
inner join mjesto as mjs on stud.pbrstan=mjs.pbr
inner join mjesto as mjr on stud.pbrrod=mjr.pbr
inner join zupanija on zupanija.sifzupanija=mjr.sifzupanija;

-- 9 Koliko negativnih ocjena su dodijelili nastavnici koji stanuju u mjestu čiji naziv počinje slovom "Z" (104)
select count(ocjena) from ispit
inner join nastavnik on ispit.sifnastavnik=nastavnik.sifnastavnik
inner join mjesto on nastavnik.pbrstan=mjesto.pbr
where nazmjesto rlike "^Z" and ocjena=1;

-- 10 Odredite broj različitih nastavnika sa organizacione jedinice „Zavod za primijenjenu matematiku“ kod
-- kojih su studenti polagali ispite (7)
select count(distinct ispit.sifnastavnik)from ispit
inner join nastavnik on nastavnik.sifnastavnik=ispit.sifnastavnik
inner join orgjed on nastavnik.siforgjed=orgjed.siforgjed
where nazorgjed="Zavod za primijenjenu matematiku";

-- 11 Ispisati oznake i kapacitet dvorana koje su rezervisane za predmet „Energetska elektronika-izabrana
-- poglavlja” ponedjeljkom, srijedom ili petkom (u bilo koje vrijeme) (5 zapisa)
select dvorana.oznDvorana, dvorana.kapacitet from dvorana
inner join rezervacija on dvorana.oznDvorana=rezervacija.oznDvorana
inner join pred on pred.sifpred=rezervacija.sifpred
where nazpred="Energetska elektronika-izabrana poglavlja" and oznvrstaDan in ("PO", "SR", "PE");


-- 12 Ispisati sve nazive organizacionih jedinica u kojima je zaposlen barem jedan nastavnik. Naziv svake
-- takve organizacione jedinice treba se u popisu pojaviti samo jednom (13 zapisa)
select distinct orgjed.nazorgjed from nastavnik
inner join orgjed on nastavnik.siforgjed=orgjed.siforgjed; 

-- 13 Iz koliko različitih predmeta se obavlja nastava u dvoranama čiji kapacitet nije preko 60 studenata (52)
select count(distinct rezervacija.sifpred) from rezervacija
inner join dvorana on rezervacija.ozndvorana=dvorana.ozndvorana
where kapacitet<=60;

-- 14 Ispisati nazive mjesta u kojima postoji barem jedan student koji je u tom mjestu i rođen i stanuje. Nazivi
-- takvih mjesta se u popisu trebaju naći najviše jednom (26 zapisa)
select distinct mjr.nazmjesto from mjesto as mjr
inner join stud on stud.pbrrod=mjr.pbr
inner join mjesto as mjstan on stud.pbrstan=mjstan.pbr
where mjr.nazmjesto=mjstan.nazmjesto;

-- 15 Ispisati ime, prezime, te naziv mjesta rođenja i naziv mjesta stanovanja svih studenata koji su položili
-- ispit iz predmeta pod nazivom "Elektronički sklopovi" (18 zapisa)
select imestud, prezstud, mjr.nazmjesto, mjstan.nazmjesto from stud
inner join mjesto as mjr on mjr.pbr=stud.pbrrod
inner join mjesto as mjstan on mjstan.pbr = stud.pbrstan
inner join ispit on ispit.mbrstud=stud.mbrstud
inner join pred on pred.sifpred= ispit.sifpred
where ocjena>1 and nazpred = "Elektronicki sklopovi";

-- 16 Ispisati sve podatke o studentima koji ne stanuju u istom mjestu u kojem su rođeni, ali im se mjesto
-- rođenja i mjesto stanovanja nalaze u istoj županiji (3 zapisa)
select * from stud
inner join mjesto as mjr on mjr.pbr=stud.pbrrod
inner join mjesto as mjstan on mjstan.pbr = stud.pbrstan
where mjr.nazmjesto<>mjstan.nazmjesto and mjr.sifzupanija=mjstan.sifzupanija;


-- no ig treba 179
-- 17 Ispisati podatke o nastavnicima koji su ispitivali studente koji stanuju u istoj županiji u kojoj stanuje i
-- nastavnik (24 zapisa)
select nastavnik.* from nastavnik
inner join ispit on ispit.sifnastavnik=nastavnik.sifnastavnik
inner join stud on stud.mbrstud=ispit.mbrstud
inner join mjesto as mjestostud on stud.pbrstan=mjestostud.pbr
inner join mjesto as mjestonast on nastavnik.pbrstan=mjestonast.pbr
where mjestonast.sifzupanija=mjestostud.sifzupanija
;



-- ZAPAMTI: on ide po prvoj tabeli niz redove i spaja sa prvom ntorkom 
-- u drugoj tabeli zato si isla obrnuto ispisivanje tj prvo iz druge pa onda prve tabele
-- 18 Studenti čije i ime i prezime započinje slovom K natječu se na šahovskom turniru. Svaki takav student
-- treba odigrati dvije partije sa svakim drugim studentom. Treba ispisati sve moguće parove i to tako da
-- se svaki student pojavi u listi dva puta - jednom je u paru ispisan na prvom mjestu, drugi put je ispisan
-- na drugom mjestu. Dovoljno je ispisati samo prezimena i imena studenata. Npr. ako na turniru sudjeluju
-- tri studenta, S1, S2, S3, treba ispisati: (6 zapisa)
select stud2.imestud, stud2.prezstud , stud1.imestud, stud1.prezstud 
from stud as stud1
inner join stud as stud2
where stud1.imestud rlike "^K" and stud1.prezstud rlike "^K"
and stud2.imestud rlike "^K" and stud2.prezstud rlike "^K"
and stud1.imestud<>stud2.imestud and stud1.prezstud<>stud2.prezstud;



