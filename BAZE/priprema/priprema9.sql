use stusluAG;


delimiter //
create procedure orgjedNast(in mbr int)
begin
select concat(oj.nazorgjed,' ',noj.nazorgjed) from orgjed oj
inner join orgjed noj on oj.sifNadorgjed=noj.siforgjed
inner join nastavnik on oj.siforgjed=nastavnik.sifOrgjed
where sifnastavnik = mbr;
end
//
drop procedure orgjedNast//

create function noviKoef(mbr int)
returns decimal(3,2)
begin
declare res decimal(3,2);
declare pozispiti, negispiti int;
declare avgpoz, avguk, starikoef, novikoef decimal(3,2);

select koef into starikoef from nastavnik where 
sifnastavnik=mbr;
select count(ocjena) into pozispiti from ispit where ocjena > 1
and sifnastavnik=mbr;
select count(ocjena) into negispiti from ispit where ocjena = 1
and sifnastavnik=mbr;
select avg(ocjena) into avgpoz from ispit where ocjena > 1
and sifnastavnik=mbr;
select avg(ocjena) into avguk from ispit where ocjena >1;

if pozispiti > negispiti and avgpoz > avguk then
set novikoef = starikoef*1.1;
elseif pozispiti < negispiti and avgpoz < avguk then
set novikoef = starikoef*0.9;
else set novikoef=starikoef;
end if;

return novikoef;
end
//
drop function noviKoef//

create function ocjObrane(mbr int, dat date)
returns int
begin
declare ocjena, imaprijava, mentor, ispitivaci int;

-- provjeri je li ima prijava > 0
select count(mbrstud) into imaprijava from diplom
where exists(select mbrstud, datprijava from diplom
where mbr=mbrstud and dat=datprijava);
	if imaprijava =0 then return null; end if;
    
-- selektuj ocjenu mentora, ako je 1 vrati 1
select ocjenaRad into mentor from diplom where
mbrstud=mbr and datPrijava=dat;
	if mentor = 1 then return 1; end if;
 
 -- provjeri da li imaju sve 3 ocjene ispitivaca, ako ne vrati 0
 select count(ocjenausm) into ispitivaci from dipkom
 inner join diplom on diplom.mbrStud=dipkom.mbrStud
 and diplom.datprijava=dipkom.datprijava
 where diplom.mbrstud=mbr and diplom.datprijava=dat;
	if ispitivaci < 3 then return 0; end if;
    
-- provjeri je li postoji ijedna 1 od ispitivaca, ako da vrati 1
 select count(ocjenausm) into ispitivaci from dipkom
 inner join diplom on diplom.mbrStud=dipkom.mbrStud
 and diplom.datprijava=dipkom.datprijava
 where diplom.mbrstud=mbr and diplom.datprijava=dat and ocjenausm=1;
	if ispitivaci > 0 then return 1; end if;
	
-- izracunaj prosjek
 select ROUND(avg(ocjenausm) ,0) into ocjena from dipkom
 inner join diplom on diplom.mbrStud=dipkom.mbrStud
 and diplom.datprijava=dipkom.datprijava
 where diplom.mbrstud=mbr and diplom.datprijava=dat;

return ocjena;
end
//
drop function ocjObrane//
delimiter ;
#1
call orgjedNast(244) ;
#2
select noviKoef(469);
#2
SELECT ocjObrane(1127, '2045-01-21');
SELECT *, ocjObrane(mbrStud, datPrijava) FROM diplom
ORDER BY mbrStud, datPrijava;