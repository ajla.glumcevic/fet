use stusluAG;

delimiter //
create procedure azurDiplom()
begin
declare done int default 0;
declare mbr int;
declare prijava datetime;

declare ex condition for sqlstate '02000';

declare cur cursor for select mbrstud, datprijava from diplom;

declare continue handler for ex
 set done = 1;
 
 set transaction isolation level repeatable read;
 start transaction;
 
 open cur;
 l:loop
	if done = 1 then leave l;
    end if;
    
	fetch cur into mbr, prijava;
	update diplom set ukupOcjena = ocjObrane(mbr,prijava)
    where mbrstud=mbr and datprijava=prijava;	
end loop;
close cur;

commit;
end
//

drop procedure azurDiplom//
delimiter ;
call azurDiplom;
set sql_safe_updates=0;
select * from diplom;