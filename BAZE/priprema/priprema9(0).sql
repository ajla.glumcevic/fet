use stusluAG;
CREATE TABLE diplom (
mbrStud INTEGER NOT NULL
, datPrijava DATE NOT NULL 
, sifMentor INTEGER
, ocjenaRad SMALLINT
, datObrana DATE
, ukupOcjena SMALLINT
, PRIMARY KEY (mbrStud, datPrijava)
, FOREIGN KEY (mbrStud) REFERENCES stud(mbrstud)
, FOREIGN KEY (sifMentor) REFERENCES nastavnik (sifNastavnik)
);

CREATE TABLE dipkom (
mbrStud INTEGER NOT NULL
, datPrijava DATE NOT NULL
, sifNastavnik INTEGER NOT NULL
, oznUloga CHAR(1)
, ocjenaUsm SMALLINT
, PRIMARY KEY (mbrStud, datPrijava, sifNastavnik)
, FOREIGN KEY (mbrStud, datPrijava) REFERENCES diplom (mbrStud,
datPrijava)
, FOREIGN KEY (mbrStud)
REFERENCES stud (mbrStud)
, FOREIGN KEY (sifNastavnik) REFERENCES nastavnik (sifNastavnik)
);


LOAD DATA INFILE '/var/lib/mysql-files/tmp/diplom.unl' INTO TABLE diplom FIELDS TERMINATED BY
'#' LINES STARTING BY '\n' TERMINATED BY '#\r';

LOAD DATA INFILE '/var/lib/mysql-files/tmp/dipkom.unl' INTO TABLE dipkom FIELDS TERMINATED BY
'#' LINES STARTING BY '\n' TERMINATED BY '#\r';

SELECT * FROM diplom;
SELECT * FROM  dipkom;

-- zadatak 1
-- DROP PROCEDURE orgjedNast; 
DELIMITER //
CREATE PROCEDURE orgjedNast(IN mbrNastavnik INTEGER )
BEGIN
	SELECT concat(imeNastavnik, ' ', prezNastavnik, ':',
    oj.nazorgjed, ' ', noj.nazorgjed) AS info FROM nastavnik
    INNER JOIN orgjed AS oj ON oj.siforgjed=nastavnik.sifOrgjed
    INNER JOIN orgjed AS noj ON oj.sifNadorgjed=noj.siforgjed
    WHERE sifnastavnik=mbrNastavnik;
END;
//
call orgjedNast(244);

-- zadatak 2
-- DROP FUNCTION noviKoef;
DELIMITER //
CREATE FUNCTION noviKoef(sifNast INTEGER)
RETURNS decimal(3,2)
BEGIN
	DECLARE stari_koef, novi_koef, avgp, avgn, avguk decimal(3,2);
	DECLARE poz, ne integer;
    
    select count(ocjena), avg(ocjena) into poz,avgp from ispit
    where ocjena > 1 and sifNast=ispit.sifNastavnik;
    
	select count(ocjena), avg(ocjena) into ne,avgn from ispit
    where ocjena = 1 and sifNast=ispit.sifNastavnik;
    
    select koef into stari_koef from nastavnik
    where sifNast=nastavnik.sifNastavnik;
    
    select avg(ocjena) into avguk from ispit
    where ocjena > 1;
     
     if poz > ne and avgp > avguk then
		set novi_koef=stari_koef*1.1;
	elseif ne > poz and avgp < avguk then
		set novi_koef=stari_koef*0.9;
	else
		set novi_koef=stari_koef;
	end if;
    
    RETURN novi_koef;
END;
//

select noviKoef(460);

-- zadatak 3
drop function ocjObrane;

delimiter //
create function ocjObrane(mbrs int, datp date)
returns integer
begin
	DECLARE UOcjena SMALLINT;
    
	-- ako ne postoji prijava ocjena je NULL
	IF NOT EXISTS (SELECT mbrstud FROM diplom
				WHERE mbrstud=mbrs AND datPrijava=datP) THEN
		SET UOcjena = NULL;
	ELSE
		SELECT ocjenarad INTO UOcjena FROM diplom
		WHERE mbrstud=mbrs AND datPrijava=datP;
        
        -- ako je ocjena mentora NULL
		IF UOcjena IS NULL THEN SET UOcjena=0;
        ELSEIF UOcjena = 1 THEN SET UOcjena = 1;
		ELSEIF UOcjena <> 1 THEN
				-- ako je ocjena barem jednog ispitivaca 1
				IF (SELECT COUNT(*) FROM dipkom WHERE mbrstud=mbrs
				AND datPrijava=datP AND ocjenaUsm=1) > 0 THEN
					SET UOcjena=1;
				-- ako neki ispitivac nije ispitao (ima ih 3)
				ELSEIF (SELECT COUNT(*) FROM dipkom
				WHERE mbrstud=mbrs AND datPrijava=datP) < 3 THEN
					SET UOcjena=0;
				ELSE
					-- inace 
					SELECT AVG(ocjenaUsm) INTO UOcjena FROM dipkom
					WHERE mbrstud=mbrs AND datPrijava=datP;
					SET UOcjena = ROUND(UOcjena, 0);
				END IF;
		END IF;
	END IF;
    return UOcjena;
end;
//

SELECT *, ocjObrane(mbrStud, datPrijava) FROM diplom
ORDER BY mbrStud, datPrijava;