use stusluAG;
START TRANSACTION;
SET autocommit=0;
ALTER TABLE nastavnik MODIFY sifOrgjed INTEGER;
ALTER TABLE nastavnik MODIFY pbrStan INTEGER;
ALTER TABLE nastavnik ADD CONSTRAINT FOREIGN KEY (sifOrgjed) REFERENCES orgjed (sifOrgjed);
ALTER TABLE nastavnik ADD CONSTRAINT FOREIGN KEY (pbrStan) REFERENCES mjesto (pbr);

SET SQL_SAFE_UPDATES = 0;
#1
update nastavnik set siforgjed=NULL 
where siforgjed in
(select siforgjed from orgjed where nazorgjed = 'Zavod za primijenjenu fiziku');

update pred set siforgjed=NULL 
where siforgjed in
(select siforgjed from orgjed where nazorgjed = 'Zavod za primijenjenu fiziku');

update orgjed set sifnadorgjed=NULL 
where nazorgjed="Zavod za primijenjenu fiziku";

delete from orgjed where nazorgjed = "Zavod za primijenjenu fiziku";

#2
update nastavnik set pbrstan=null 
where preznastavnik like "J%" and imenastavnik like "D%";

#3
select count(*) from nastavnik 
where pbrstan is null or siforgjed is null;

#4
#a
select count(sifpred) from pred where siforgjed=100002;
#b
select count(*) from pred where siforgjed<>100002;
#c
select count(*) from pred;

select * from pred where siforgjed is null; #12

#5
select * from pred where siforgjed is not null;

#6
select * from pred where siforgjed is null;

#7
select count(sifpred) from pred where siforgjed is not null;

#8
select count(distinct siforgjed) from nastavnik;

#9
select brojsatitjedno, siforgjed , count(sifpred) from pred
group by 1,2 ;

#10
select distinct * from stud where pbrstan not in
(select pbrrod from stud);

#11
select sifnastavnik, preznastavnik, imenastavnik, nastavnik.siforgjed, 
nazorgjed from nastavnik
left outer join orgjed on nastavnik.siforgjed=orgjed.siforgjed;

#12
select sifnastavnik, preznastavnik, imenastavnik, nastavnik.siforgjed, 
oj.nazorgjed, noj.nazorgjed from nastavnik
left outer join 
(orgjed oj inner join orgjed noj on oj.sifnadorgjed=noj.siforgjed)
on nastavnik.siforgjed=oj.siforgjed; 
#radili bi left outer join


#13
select sifnastavnik, preznastavnik, imenastavnik, pbrstan, nazmjesto,
nastavnik.siforgjed, nazorgjed from nastavnik
left outer join mjesto on mjesto.pbr=nastavnik.pbrstan
left outer join orgjed on nastavnik.siforgjed=orgjed.siforgjed
order by nazmjesto, preznastavnik;

#14
select sifnastavnik, preznastavnik, imenastavnik, pbrstan, nazmjesto,
siforgjed, nazzupanija from nastavnik left outer join 
(mjesto inner join zupanija on mjesto.sifzupanija=zupanija.sifzupanija)
on nastavnik.pbrstan=mjesto.pbr;


#15
select nazpred,kratpred,pred.siforgjed, nazorgjed from pred
left outer join orgjed on orgjed.siforgjed=pred.siforgjed
where nazpred like "F%";

#16
select * from pred 
left outer join orgjed on orgjed.siforgjed=pred.siforgjed
where nazorgjed like "Z%" or nazorgjed is null;

rollback work;

#17
/*
a)			b)				c)				d)				e)
 
a			a  		null	a	null		prazan skup		c	b
c			b		c		b	c							b	c
b			null    null	null null
							c	b
                            a	b
                            null b

 */
 
 #18
 /*
 a)
 A		B		C		D
 
null	null    c		null
null	b    	k		d
null    b		k		f
 
 
 b)
 A		B		C		C   	D
 
 a		null	null    null   null
 b     c        m       null   null
 null  null     c        c     null
 null  b        k        k      d
 null  b        k        k      f
 
 c)
 
  A		B		C		C   	D		D		E
  
a      null     null    null    null    null    null
b      c        m       null    null    null    null  
null   null		c		c	    null    null    null
null    b       k        k       d       d       null	
null    b       k        k       f       null    null
  
  
  
 */
 
















