use stusluAG;
-- 1 Za svaki predmet ispisati prosjek pozitivnih ocjena koje su dodijeljene na ispitima iz tog
-- predmeta. Ispisivati kraticu, naziv predmeta i prosječnu ocjenu. Predmeti iz kojih nisu
-- dodjeljene pozitivne ocjene niti na jednom ispitu ne trebaju se pojaviti u listi rezultata
-- (67 zapisa)


-- Moras ovdje staviti grupisanje i po sifri predmeta jer 
-- FZ sifpred -> nazpred,kratpred ne postoji.. nigdje veze al et
select nazpred, kratpred, avg(ocjena) from pred
	inner join ispit on ispit.sifpred=pred.sifpred
    where ocjena >1
    group by nazpred, kratpred, pred.sifpred;
    
-- 2 Za svaku županiju ispisati broj ispita koje su negativno ocijenili nastavnici koji stanuju u
-- toj županiji. Ispisivati šifru županije i broj negativno ocijenjenih ispita. Županije u kojima
-- ne stanuje niti jedan nastavnik ne trebaju se pojaviti u listi rezultata (7 zapisa)

select sifzupanija, count(ocjena) from ispit
	inner join nastavnik on nastavnik.sifnastavnik=ispit.sifnastavnik
    inner join mjesto on mjesto.pbr=nastavnik.pbrstan
    where ocjena=1
    group by sifzupanija;
    
-- 3 Za svakog nastavnika ispisati njegovo ime i broj ispita na kojima je dodijelio pozitivnu
-- ocjenu. Nastavnici koji nisu dali niti jednu pozitivnu ocjenu na ispitu ne trebaju se
-- pojaviti u listi rezultata. Napomena: voditi računa o tome da više nastavnika ima
-- jednako ime (37 zapisa)

select imenastavnik, count(ocjena) from ispit
	inner join nastavnik on nastavnik.sifnastavnik = ispit.sifnastavnik
    where ocjena >1
    group by imenastavnik, nastavnik.sifnastavnik;
    
-- 4 Za svaku organizacijsku jedinicu ispisati njenu šifru, njen naziv i broj njenih podređenih
-- organizacijskih jedinica. Organizacijske jedinice koje nemaju podređenih
-- organizacijskih jedinica ne trebaju se pojaviti u listi rezultata (18 zapisa)

select n.siforgjed, n.nazorgjed, count(p.siforgjed) from orgjed as p
	inner join orgjed as n on n.siforgjed=p.sifnadorgjed
    group by n.siforgjed, n.nazorgjed;
    
--  5 Ispisati sve podatke o studentima čije su prosječne ocjene pozitivno ocijenjenih ispita
-- veće od 3. Uz podatke ispisati i prosječnu ocjenu studenta (40 zapisa)

select stud.* , avg(ocjena) from stud
	inner join ispit on ispit.mbrstud=stud.mbrstud
    where ocjena >1
    group by 1,2,3,4,5,6,7
    having avg(ocjena)>3;

-- 6 Za svako mjesto u kojem je rođeno više od 5 studenata ispisati poštanski broj, naziv
-- mjesta i broj studenata koji su rođeni u tom mjestu. (11 zapisa)

select mjesto.pbr, nazmjesto, count(pbrrod) from mjesto
	inner join stud on stud.pbrrod=mjesto.pbr
    group by 1,2
    having count(pbrrod) > 5;
    
-- 7 Ispisati matični broj, prezime i inicijale (u obliku P.I.) studenata koji su 3 ili više puta
-- pali na ispitu iz istog predmeta (19 zapisa)

select stud.mbrstud, prezstud, concat(substr(prezstud, 1,1),'.',substr(imestud,1,1),'.') from stud
	inner join ispit on ispit.mbrstud=stud.mbrstud
    where ocjena =1
    group by 1,2,ispit.sifpred
    having count(ocjena)>2;

-- 8 Ispitati postojanje sljedećih funkcijskih zavisnosti
-- a) mbrStud, ocjena → datIspit
-- u relaciji ispit		NE VRIJEDI

select mbrstud, ocjena from ispit
	group by 1,2 
    having count(distinct datispit)>1;
    
-- b) pbr, sifZupanija → nazMjesto
-- u relaciji mjesto	VRIJEDI

select pbr, sifzupanija from mjesto
	group by 1,2
    having count(distinct nazmjesto)>1;
    
-- c) imeNastavnik → prezNastavnik
-- u relaciji nastavnik		NE VRIJEDI

select imenastavnik from nastavnik
	group by 1
    having count(distinct preznastavnik) >1;
    
-- d) sifPred, datIspit, ocjena → mbrStud, sifNastavnik u relaciji ispit
		-- VRIJEDI

select sifpred, datispit, ocjena from ispit
	group by 1,2,3
    having count(distinct mbrstud)>1
    and count(distinct sifnastavnik)>1;
    
-- 9 Za svako mjesto ispisati poštanski broj, naziv mjesta i broj ispita koje su položili
-- studenti koji stanuju u tom mjestu. Pronađene zapise poredati po nazivu mjesta.
-- Mjesta u kojima stanuju studenti koji nisu položili niti jedan ispit ne trebaju se pojaviti u
-- listi rezultata (21 zapisa)

select mjesto.pbr, nazmjesto, count(ocjena) from mjesto
	inner join stud on stud.pbrstan=mjesto.pbr
    inner join ispit on stud.mbrstud=ispit.mbrstud
    where ocjena >1
    group by mjesto.pbr, nazmjesto
    order by nazmjesto;
   
-- 10 Ispisati rang listu studenata po broju položenih ispita. U rang listi se nalazi ime i
-- prezime studenta te broj položenih ispita, poredano tako da su na početku rang liste
-- studenti s više položenih ispita. Studenti koji imaju jednaki broj položenih ispita,
-- poredani su po prezimenu i imenu. Studenti koji nisu položili niti jedan ispit ne trebaju
-- se pojaviti u listi rezultata (109 zapisa)

select imestud, prezstud, count(ocjena) from stud
	inner join ispit on ispit.mbrstud=stud.mbrstud
    where ocjena > 1
    group by 1,2
    order by 3 desc, prezstud, imestud;
    
 -- 11 Uporabom SELECT naredbe kreirajte i napunite privremenu relaciju mjestoTmp čija je
-- šema jednaka šemi relacije mjesto uz dodatak atributa brojNast koji sadrži broj
-- nastavnika koji stanuju u dotičnom mjestu. Mjesta u kojima nema nastavnika koji u
-- njima stanuju, ne trebaju se pojaviti u rezultatu (Izvođenje naredbe: SELECT * FROM
-- mjestoTmp; vraća ukupno 24 zapisa)

create temporary table mjestoTmp as
	select mjesto.*, count(sifnastavnik) as brojNast from mjesto
    inner join nastavnik on nastavnik.pbrstan=mjesto.pbr
    group by 1,2,3;
select * from mjestoTmp;

-- 12 Za svaki mjesec u godini, ispisati koliko je studenata rođeno u tom mjesecu. Poredati
-- rezultate prema broju rođenih studenata, u padajućem poretku (13 zapisa)

select monthname(datrodstud) , count(mbrstud) from stud
	group by 1
    order by count(mbrstud) desc;
    
-- 13 Ispisati sve atribute studenata čiji je prosjek pozitivnih ocjena veći od prosjeka ocjena
-- ostalih studenata ili je broj pozitivno ocijenjenih ispita veći od prosječnog broja
-- pozitivno ocijenjenih ispita ostalih studenata (85 zapisa)

create temporary table newtable as
select stud.*, count(ocjena) as pozitivni_ispiti from stud
inner join ispit on ispit.mbrstud=stud.mbrstud
where ocjena > 1 
group by 1,2,3,4,5,6,7;

select s1.* from stud s1
	inner join ispit on ispit.mbrstud=s1.mbrstud
    where ocjena > 1
    group by 1,2,3,4,5,6,7 
    having avg(ocjena) > (select avg(ocjena) from stud s2
							inner join ispit on ispit.mbrstud=s2.mbrstud
                            where s1.mbrstud<>s2.mbrstud)
		or count(ocjena) > (select avg(pozitivni_ispiti) from newtable
							where newtable.mbrstud<>s1.mbrstud);
                            


    



























