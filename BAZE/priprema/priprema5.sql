use stusluAG;

-- 1. Ispisati sve podatke o studentima koji nisu negativno ocijenjeni niti na jedanom ispitu (250
-- zapisa)

select * from stud
	where mbrstud not in 
    (select mbrstud from ispit 
    where ocjena=1);


-- 2 Ispisati naziv svakog mjesta u kojem stanuje barem jedan nastavnik, a u kojem nije rođen
-- niti jedan student (7 zapisa)

select nazmjesto from mjesto where pbr in
	(select pbrstan from nastavnik)-- ovo mozes i sa join
    and pbr not in -- NEKAD stavljaj distinct zbog manjeg broja poredjenja al i to je skupa operacija..
    (select pbrrod from stud);

-- MORAS U PODUPITIMA PAZIT DA JE SVE DRUGOG IMENA, DAJ ALIAS SVEMU
-- 3 Ispisati nazive županija u kojima niti jedan student ne stanuje, a barem jedan student je
-- rođen (1 zapis)

select zup1.nazzupanija from zupanija zup1
	where zup1.sifzupanija not in
    (select zup2.sifzupanija from zupanija zup2
    inner join mjesto on mjesto.sifzupanija=zup2.sifzupanija
    inner join stud on stud.pbrstan=mjesto.pbr)
    and zup1.sifzupanija in
    (select zup3.sifzupanija from zupanija zup3
    inner join mjesto on mjesto.sifzupanija=zup3.sifzupanija
    inner join stud on stud.pbrrod=mjesto.pbr);
    



-- 4 Ispisati šifru, ime i prezime nastavnika koji u toku prva dva mjeseca tekuće godine nisu
-- pozitivno ocijenili niti jedan ispit. Upit mora biti neovisan o datumu izvođenja (0 zapisa, a za
-- 2000. godinu 74 zapisa)

select sifnastavnik, imenastavnik, preznastavnik from nastavnik
	where sifnastavnik not in
    (select distinct sifnastavnik from ispit
    where year(datispit)=2000 and month(datispit) in (1,2) and ocjena>1);
    

-- 5 Ispisati ime, prezime i datum rođenja studenata koji su bar 200 dana stariji od najmlađeg
-- studenta (204 zapisa)

select imestud, prezstud, datrodstud from stud
	where date_add(datrodstud , interval 200 day) <=
    (select max(datrodstud) from stud);

-- 6 Ispisati sve podatke o predmetima koji pripadaju organizacionim jedinicama u kojima su
-- zaposleni nastavnici čije prezime počinje slovom "O" (25 zapisa)

select * from pred
	where siforgjed in
    (select distinct siforgjed from nastavnik
    where preznastavnik rlike "^O");
    
    -- Wtf ajla
select pred.* from pred
	where pred.siforgjed in
    (select siforgjed from orgjed
    where orgjed.siforgjed in
    (select siforgjed from nastavnik
    where nastavnik.preznastavnik rlike "^O")
    );
 

    
-- 7 Ispisati oznaku dvorane i kapacitet za sve dvorane čiji je kapacitet veći od broja predmeta za
-- koje je ta dvorana rezervisana u toku jednog tjedna (25 zapisa)

select ozndvorana , kapacitet from dvorana
	where kapacitet > 
    (select count(sifpred) from rezervacija
    where dvorana.ozndvorana=rezervacija.ozndvorana
    );
    
-- 8 Ispisati nastavnike čiji je koeficijent za plaću manji od prosječnog koeficijenta svih ostalih
-- nastavnika koji stanuju u istoj županiji. Ispisati šifru, ime, prezime, koeficijent i naziv mjesta
-- stanovanja takvih nastavnika (51 zapis)

select * from nastavnik prvi
inner join mjesto prvo on prvo.pbr=prvi.pbrstan
	where prvi.koef < 
    (select avg(drugi.koef) from nastavnik drugi
    inner join mjesto drugo on drugo.pbr=drugi.pbrstan
    where drugi.sifnastavnik<>prvi.sifnastavnik
    and drugo.sifzupanija=prvo.sifzupanija);

-- 9 Ispisati matični broj, ime i prezime studenata koji niti jedan ispit nisu polagali u danu u
-- sedmici u kojem su rođeni. Npr. ako je student rođen u utorak, niti na jednom ispitu nije bio
-- utorkom (264 zapisa)

select mbrstud, imestud, prezstud from stud
	where mbrstud not in
    (select mbrstud from ispit
    where weekday(datispit)=weekday(stud.datrodstud));

-- ZAPAMTI OVO ANY KAD PODUPIT VRACA VISE N-TORKI
-- 10 Ispisati šifru, ime i prezime za nastavnike koji stanuju u Dubrovačko-neretvanskoj županiji a
-- koeficijent za platu im je veći od barem jednog nastavnika koji stanuje u Splitsko-
-- dalmatinskoj županiji (4 zapisa)

select sifnastavnik, imenastavnik,preznastavnik from nastavnik
	inner join mjesto on mjesto.pbr=nastavnik.pbrstan
    inner join zupanija on zupanija.sifzupanija=mjesto.sifzupanija
    where zupanija.nazzupanija="Dubrovacko-neretvanska"
    and koef > any
    (select koef from nastavnik druginast
    inner join mjesto drugomj on drugomj.pbr=druginast.pbrstan
    inner join zupanija drugazup on drugazup.sifzupanija=drugomj.sifzupanija
    where drugazup.nazzupanija="Splitsko-dalmatinska");
    

-- 11 Ispisati matične brojeve, imena i prezimena studenata koji su položili ispit kod nekog
-- nastavnka koji niti jednom drugom studentu nije dao pozitivnu ocjenu (5 zapisa)

select stud.mbrstud, imestud, prezstud from stud
inner join ispit on ispit.mbrstud=stud.mbrstud
where ocjena>1 
and sifnastavnik not in
(select sifnastavnik from ispit
where ocjena>1
and ispit.mbrstud<>stud.mbrstud);
	
-- 12 
-- a
select distinct b, c from r1
where exists (select distinct b, c from r3
			where r1.b=r3.b and r1.c=r3.c);
            
-- b
select distinct b from r1
where b not in(select distinct b from r3);


-- c
select distinct r3.e from r2
inner join r3 on r2.e=r3.e and r2.d=r3.d
where not exists (select distinct r3.e from r3 where d>7);

-- d
select * from r1 inner join r2
where not exists 
(select r1.*, r3.d, r3.e from r1 
inner join r3 on r1.b=r3.b and r1.c=r3.c);

-- e
select distinct r1.b, r1.c from r1
where b>'d' and not c='m' and
exists (select distinct b, c from r2
		inner join r3 on r2.b=r3.b and r2.c=r3.b);