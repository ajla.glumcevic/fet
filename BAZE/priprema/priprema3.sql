
use stusluAG;

-- 1. Ispisati različite šifre županija za mjesta čiji naziv počinje slovom Z
select distinct sifZupanija from zupanija where nazZupanija rlike "^Z";

-- 2. Ispis svih podataka o studentima koji su rođeni između 1.5. i 1.9.1982 i prezime im počinje
-- slovom "R" ili slovom "P" ili slovom "S" ili slovom "V". Koristiti operator LIKE
select * from stud where
	(datRodStud between str_to_date("1 5 1982" , "%d %m %Y") and str_to_date("1 9 1982" , "%d %m %Y"))
	 and (prezStud like "R%" or prezStud like "%P" or prezStud like "S%" or prezStud like "V%");

select * from stud where
	datRodstud between '1982-05-01' and '1982-09-01';

-- 3. Ispis svih studenata koji su rođeni između 1.5. i 1.9.1982 i prezime im počinje slovom "R" ili
-- slovom "P" ili slovom "S" ili slovom "V". Koristiti operator RLIKE
select * from stud where 
	(datRodStud between str_to_date("1 5 1982" , "%d %m %Y") and str_to_date("1 9 1982" , "%d %m %Y"))
    and prezStud rlike "^[RSPV]";
    
-- 4. Ispis svih mjesta kojima naziv počinje i završava samoglasnikom. Koristiti operator LIKE


-- 5. Ispis svih mjesta kojima naziv počinje i završava bilo kojim znakom osim samoglasnikom.
-- Koristiti operator RLIKE
select nazMjesto from mjesto where nazMjesto not rlike "^[AEIOU]" and nazMjesto not rlike "[aeiou]$";
 
-- 6. Ispis svih mjesta kojima naziv počinje ili završava bilo kojim znakom osim samoglasnikom.
-- Koristiti operator RLIKE
select nazMjesto from mjesto where nazMjesto not rlike "^[AEIOU]" or nazMjesto not rlike "[AEIOU]$";

-- 7. JMBG (jedinstveni matični broj građana) bi trebao biti sastavljen tako da prvih 7 znakova
-- označava datum rođenja. Ispišite sve studente kojima JMBG (prvih sedam znakova) ne
-- odgovara datumu rođenja
select * from stud where
	str_to_date(concat(substr(jmbgStud,1,2)," ", substr(jmbgStud,3,2)," ", concat("1",substr(jmbgStud,5,3))),"%d %m %Y")<>datRodStud; 

-- 8. Ispisati prosječni koeficijent za platu nastavnika koji stanuju u mjestu sa poštanskim brojem
-- 10000 (5.69)
select avg(koef) from nastavnik where pbrStan=10000;

-- 9. Ispisati datume kada su održani prvi i posljednji ispit. (11.01.1999 13.02.2001)
select min(datIspit) as prviIspit , max(datIspit)as posljednjiIspit from ispit;

-- 10. U koliko različitih organizacionih jedinica su zaposleni nastavnici kojima je poštanski broj
-- mjesta stanovanja 10000 (13)
select count(distinct sifOrgJed) as razliciteOrgJed from nastavnik where pbrStan=10000;

-- 11. Ispisati kolika je razlika u danima između najmlađeg i najstarijeg studenta (1484)
select datediff(max(datRodStud), min(datRodStud)) as razlikaUDanima from stud;

-- 12. Ispisati prosječnu ocjenu ispita koji su obavljeni u 1999. godini. U prosjek ulaze samo pozitivne
-- ocjene (2.94)
select avg(ocjena) as prosjecnaOcjena from ispit where year(datIspit)=1999 and ocjena<>1;

-- 13. Ispisati broj položenih ispita i prosječnu ocjenu položenih ispita iz predmeta sa šifrom 146 (4
-- 3.25)
select count(ocjena) as polozeniIspiti , avg(ocjena) as prosjecnaOcjena from ispit where
	ocjena<>1 and sifPred=146;

-- 14. Ispisati prosječnu starost studenata u danima za sve studente koji su rođeni u mjestu sa
-- poštanskim brojem 10000, a ime im počinje ili završava samoglasnikom.
select  avg(datediff(current_date, datRodStud)) as prosStarost
	from stud where pbrStan=10000 and 
	imeStud rlike "^[AEIOU]" or imeStud rlike"[aeiou]$";

-- 15. Ispisati iz koliko se različitih predmeta izvodi nastava u dvoranama u zgradi B (oznaka
-- dvorane počinje slovom B) utorkom i srijedom u bilo koje vrijeme.
select count(distinct sifPred) from rezervacija
	where oznDvorana rlike "^B" and (oznVrstaDan="UT" or oznVrstaDan="SR");

-- 16. Ispisati broj studenata i ukupnu prosječnu ocjenu za sve studente koji su polagali ispite bilo
-- kojeg petka u julu mjesecu prije najmanje 5 godina.
select count(mbrStud) , avg(ocjena) from ispit where
	day(datIspit) = 4 and month(datIspit)=7 and year(datIspit)<year(current_date ) - 5; 

-- 17. Ispisati ukupan broj upisanih studenata na svim predmetima koji imaju više od 3 časa
-- sedmično, a naziv im sadrži riječ ‘tehnike’.
select sum(upisanoStud) from pred where
	brojSatiTjedno > 3 and nazPred like "%elektron%";
