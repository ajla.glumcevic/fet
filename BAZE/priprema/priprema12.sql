use stusluAG;

# TEMA 1 ZADATAK 1

delimiter //
CREATE PROCEDURE test1 ()
BEGIN
DECLARE kod CHAR(5);
DECLARE poruka TEXT;
DECLARE rezultat TEXT;
DECLARE exit HANDLER FOR SQLEXCEPTION
BEGIN
GET DIAGNOSTICS CONDITION 1
kod = RETURNED_SQLSTATE, poruka = MESSAGE_TEXT;
SET rezultat = CONCAT('greska = ',kod,', poruka = ',poruka);
END;
BEGIN
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Pogreska 1000',
MYSQL_ERRNO = 1000;
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Pogreska 1001',
MYSQL_ERRNO = 1001;
END;
SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Pogreska 1002',
MYSQL_ERRNO = 1002;
SELECT rezultat;
END//
drop procedure test1//
call test1//




CREATE PROCEDURE test2 ()
BEGIN
DECLARE kod CHAR(5);
DECLARE poruka TEXT;
DECLARE rezultat TEXT;
BEGIN
	DECLARE continue HANDLER FOR SQLEXCEPTION
	BEGIN
	GET DIAGNOSTICS CONDITION 1
	kod = RETURNED_SQLSTATE, poruka = MESSAGE_TEXT;
	SET rezultat = CONCAT('greska = ',kod,', poruka = ',poruka);
	END;
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Pogreska 1000',
	MYSQL_ERRNO = 1000;
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Pogreska 1001',
	MYSQL_ERRNO = 1001;
END;
SELECT rezultat;
END//
drop procedure test2//
call test2//



CREATE PROCEDURE test3 ()
BEGIN
DECLARE kod CHAR(5);
DECLARE poruka TEXT;
DECLARE rezultat TEXT;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '10003'
	BEGIN
		GET DIAGNOSTICS CONDITION 1
		kod = RETURNED_SQLSTATE;
		SET rezultat = CONCAT('greska = ',kod,', poruka = Druga');
	END;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1
		kod = RETURNED_SQLSTATE;
		SET rezultat = CONCAT('greska = ',kod,', poruka = Prva');
	END;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '10001'
	BEGIN
		GET DIAGNOSTICS CONDITION 1
		kod = RETURNED_SQLSTATE;
		SET rezultat = CONCAT('greska = ',kod,', poruka = Treca');
	END;
		BEGIN
			DECLARE CONTINUE HANDLER FOR SQLSTATE '10002'
			BEGIN
				SIGNAL SQLSTATE '10003';
			END;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '10001'
			BEGIN
				SIGNAL SQLSTATE '10002';
			END;
			SIGNAL SQLSTATE '10001';
		END;
SELECT rezultat;
END//
drop procedure test3//
delimiter ;
call test3();

delimiter //
CREATE PROCEDURE test4 ()
BEGIN
DECLARE kod CHAR(5);
DECLARE poruka TEXT;
DECLARE rezultat TEXT;
DECLARE CONTINUE HANDLER FOR SQLSTATE '10003'
BEGIN
GET DIAGNOSTICS CONDITION 1
kod = RETURNED_SQLSTATE;
SET rezultat = CONCAT('greska = ',kod,', poruka = Druga');
END;
DECLARE CONTINUE HANDLER FOR SQLSTATE '10002'
BEGIN
GET DIAGNOSTICS CONDITION 1
kod = RETURNED_SQLSTATE;
SET rezultat = CONCAT('greska = ',kod,', poruka = Prva');
END;
DECLARE CONTINUE HANDLER FOR SQLSTATE '10001'
BEGIN
GET DIAGNOSTICS CONDITION 1
kod = RETURNED_SQLSTATE;
SET rezultat = CONCAT('greska = ',kod,', poruka = Treca');
END;
BEGIN
DECLARE CONTINUE HANDLER FOR SQLSTATE '10001'
BEGIN
SIGNAL SQLSTATE '10002';
END;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
BEGIN
SIGNAL SQLSTATE '10003';
END;
SIGNAL SQLSTATE '10001';
END;
SELECT rezultat;
END//

call test4//

# TEMA 2 ZADATAK 1
#a 
grant select on mjesto to 'haso'@'%';

#b
grant select(mbrstud,imestud,prezstud,pbrrod,pbrstan,jmbgstud) on stud to 'haso'@'%';

#c
create view pozOcjene as
select * from ispit where ocjena > 1;
grant select on pozOcjene to 'haso'@'%';

#d
create view negOcjene as
select sifPred, count(*) from ispit where ocjena = 1 group by 1;
grant select on negOcjene to 'haso'@'%';

#e
grant execute on function ocjObrana to 'haso'@'%';

#f 
grant insert, update, delete on mjesto to 'haso'@'%';

#g
grant update(nazZupanija) on zupanija to 'haso'@'%';

#h
create view nast as
select * from nastavnik where siforgjed = 100006;
grant insert, update, delete on nast to 'haso'@'%';


# ZADATAK 2
delimiter //
CREATE PROCEDURE boljaAzurDiplom()
BEGIN
DECLARE done INT DEFAULT FALSE;
DECLARE mbrS INTEGER; DECLARE datP DATE; DECLARE uko SMALLINT;
declare poruka char(255);
declare kod int;
-- postavlja se READ LOCK na diplom
DECLARE kursor CURSOR FOR SELECT mbrstud, datPrijava FROM diplom;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

-- handler koji ce catchat gresku prilikom postavljanje kljuceva
declare exit handler for sqlexception
begin
	get diagnostics condition 1
	kod = returned_sqlstate;
    rollback;
    if kod in (1027,1099, 1205, 1213, 1223, 1614, 1689) then
		signal sqlstate '45000' set message_text = poruka;
	else 
		resignal;
    end if;
end;

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
START TRANSACTION;
	OPEN kursor;
	petlja: LOOP
	FETCH kursor INTO mbrs, datP;
		IF done THEN
		LEAVE petlja;
		END IF;
        -- postavlja se READ LOCK na diplom i dipkom, 
        -- set odgovaraju poruku da bi se ona iskoristila u handleru
	set poruka = "Relacija diplom/dipkom: READ ili PROMOTABLE LOCK nije odobren";
	SET uko = ocjObrane(mbrs, datP);
		-- postavlja se WRITE LOCK na diplom
	set poruka = "Relacija diplom: WRITE LOCK nije odobren";
	UPDATE diplom SET ukupOcjena = uko
	WHERE mbrStud = mbrs AND datPrijava = datP;
	END LOOP;
	CLOSE kursor;
COMMIT;
END//


