
use stusluAG;

-- zad1
select mbrstud,datprijava , ocjObrane(mbrstud,datprijava) as ukOcjena
 from diplom;
 
 select diplom.mbrstud, imestud, prezstud,
 ocjObrane(stud.mbrstud, datprijava) from stud
 inner join diplom on diplom.mbrstud=stud.mbrstud
 where  ocjObrane(stud.mbrstud, datprijava)>1;
 
select avg(ocjObrane(mbrstud, datprijava)) from diplom
where ocjObrane(mbrstud, datprijava)>1;
 
-- zad2
-- drop function prosjekOcjena;
delimiter //
create function prosjekOcjena()
returns decimal(3,2)
begin
	declare prosjek decimal(3,2);
	if @posjeceno is null then
		set @posjeceno = 0;
	end if;
    
    set @posjeceno = @posjeceno + 1;
    
    if @posjeceno > 1 then
		set prosjek = null;
	else 
		select avg(ocjena) into prosjek from ispit
        where ocjena > 1;
	end if;
		
return prosjek;
end;
//

--  drop function brojNepristup;
delimiter //
create function brojNepristup()
returns integer
begin
	declare broj integer;
	if @posjeceno is null then
		set @posjeceno = 0;
	end if;
    
    set @posjeceno = @posjeceno + 1;
    
    if @posjeceno > 1 then
		set broj = null;
	else 
		select count(mbrstud) into broj from stud
        where mbrstud not in
        (select mbrstud from diplom where stud.mbrstud=mbrstud);
	end if;
		
return broj;
end;
//

set @posjeceno := null;
select prosjekOcjena();
select brojNepristup();


delimiter //
CREATE FUNCTION vrijemePrvogPoziva()
RETURNS CHAR(100)
BEGIN
	DECLARE rez CHAR(100);
	DECLARE trenutno DATETIME;
	IF @prvo IS NULL THEN
		SET @prvo = SYSDATE();
	END IF;
	SET trenutno = SYSDATE();
	SET rez = CONCAT(@prvo,', ',trenutno);
RETURN rez;
END//
SELECT vrijemePrvogPoziva();


-- zad3

CREATE PROCEDURE najboljih_n(n INTEGER)
BEGIN
SELECT stud.mbrstud, prezstud, imestud, AVG(ocjena)
FROM stud INNER JOIN ispit
ON stud.mbrstud = ispit.mbrstud
GROUP BY 1, 2, 3
ORDER BY 4 DESC, 2, 3
LIMIT n;
END//

