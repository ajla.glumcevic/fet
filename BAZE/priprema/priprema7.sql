use stusluAG;
-- 1 Uz pomoć indeksa osigurajte da dva predmeta unutar iste organizacijske jedinice ne mogu
-- imati jednak naziv. Isprobajte što će se dogoditi ukoliko pomoću INSERT ili UPDATE naredbe
-- pokušate narušiti to pravilo. (Greska -371)

create unique index uniqueNazPred on pred(nazpred, siforgjed);

-- 2 Upotrebom naredbe CREATE TEMPORARY TABLE kreirajte privremene relacije orgjedPred
-- i orgjedNast koje imaju jednaku strukturu kao relacija orgjed. Nad tim relacijama kreirajte
-- indekse kojima ćete osigurati jedinstvenost vrijednosti ključa. Napunite relaciju orgjedPred sa
-- svim organizacijskim jedinicama kojima pripada bar jedan predmet, te relaciju orgjedNast sa
-- svim organizacijskim jedinicama u kojima je zaposlen bar jedan nastavnik (10 i 13 zapisa)

create temporary table orgjedPred (
sifOrgjed int(11) not null,
nazOrgjed char(60) not null,
sifNadorgjed int(11)
);
create temporary table orgjedNast (
sifOrgjed int(11) not null,
nazOrgjed char(60) not null,
sifNadorgjed int(11)
);
create unique index uniqueOrgjedPred on orgjedPred(siforgjed);
create unique index uniqueOrgjedNast on orgjedNast(siforgjed);

insert into orgjedPred
select * from orgjed
where siforgjed in (select siforgjed from pred );

insert into orgjedNast
select * from orgjed
where siforgjed in (select siforgjed from nastavnik);

-- 3 Kreirajte relaciju koja sadrži atribut sa SERIAL tipom podatka. Ispitati kako se generiraju
-- vrijednosti prilikom upisa n-torki (ako se upiše vrijednost 0, ako se upiše vrijednost različita od
-- 0, ako se upiše postojeća vrijednost). Na koji način biste spriječili pojavu duplikata među
-- vrijednostima za atribut sa SERIAL tipom podatka?

-- Sa 0 se upisuje broj veci za jedan od max broja, sa razlicitim brojem od 0 se upisuje taj broj, a za postojecu javlja duplicate entry 

create temporary table test(
atr1 serial,
atr2 char(10)
);
insert into test values(0, 'test1');
insert into test values(599, 'test2');
select * from test;

-- 5 Kreirajte privremenu relaciju nastTemp koja sadrži sve podatke kao relacija nastavnik, ali uz
-- dodani atribut rbrNast (tipa SERIAL). Napunite relaciju nastTemp podacima iz relacije
-- nastavnik tako da za vrijeme punjenja relacije svaki nastavnik dobije svoj redni broj.
-- Redoslijed kojim će redni brojevi biti pridjeljeni nastavnicima nije važan.

create temporary table nastTemp
(rbrNast SERIAL
, sifNastavnik INTEGER NOT NULL
, imeNastavnik NCHAR(25) NOT NULL
, prezNastavnik NCHAR(25) NOT NULL
, pbrStan INTEGER NOT NULL
, sifOrgjed INTEGER NOT NULL
, koef DECIMAL(3,2) NOT NULL
);

insert into nastTemp
select 0, nastavnik.* from nastavnik;

select * from nastTemp;

CREATE TEMPORARY TABLE studT AS SELECT * FROM stud;
CREATE TEMPORARY TABLE predT AS SELECT * FROM pred;
CREATE TEMPORARY TABLE nasT AS SELECT * FROM nastavnik;
CREATE TEMPORARY TABLE ispitT AS SELECT * FROM ispit;

-- 5 Svim predmetima promijeniti naziv tako da im se ispred imena doda kratica predmeta i crtica.
-- Npr. naziv 'Matematika II' treba za predmet sa kraticom MAT2 promijeniti u 'MAT2-Matematika
-- II' (182 zapisa)
SET SQL_SAFE_UPDATES = 0;
update predT set nazpred = concat(trim(kratpred), ' ', trim(nazpred));

-- 6 Nastavnicima koji imaju prosjek pozitivnih ocjena dodijeljenih na ispitima manji od 2.2 ili su na
-- ispitima dali više od 8 negativnih ocjena, umanjiti koficijent za 10% (9 zapisa)

update nasT set koef = koef*0.09
where sifnastavnik in 
(select sifnastavnik from ispit
where ocjena > 1
group by 1
having avg(ocjena) < 2.2)
or sifnastavnik in
(select sifnastavnik from ispit
where ocjena=1
group by 1
having count(ocjena) > 8);


-- 7 Svim predmetima kojima je broj sati tjedno između 1 i 4, a u posljednjih 15 godina je na
-- ispitima iz tih predmeta dodijeljeno više od 10 negativnih ocjena povećati broj sati tjedno za 1.
-- (1 zapis)

update predT set brojsatitjedno = brojsatitjedno + 1
where brojsatitjedno between 1 and 4
and sifpred in
(select sifpred from ispit
where ocjena =1
and datispit between date_sub(current_date, interval 15 year) and current_date
group by 1
having count(ocjena) > 10);



-- 8 Predmetima na kojima je dodijeljeno najviše ocjena 5 na ispitima, smanjiti broj sati tjedno za 1.
-- (3 zapisa)

update predT set brojsatitjedno = brojsatitjedno - 1
where sifpred in
(select sifpred from ispit
where ocjena = 5
group by 1
having count(ocjena) >= all 
(select count(ocjena) from ispit
where ocjena =5 
group by sifpred));


-- 9 Datoteka konverzija.unl sadrži sljedeće podatke za svakog studenta: stari matični broj
-- studenta, novi matični broj studenta, novi poštanski broj rođenja i novi poštanski broj
-- stanovanja (npr. 1234#4321#10000#21000#). Kreirajte privremenu relaciju u koju ćete učitati
-- podatke iz konverzija.unl, te koristeći te podatke zamijeniti matične brojeve studenata i
-- poštanske brojeve rođenja i stanovanja u relaciji studT. Možete li to obaviti jednom UPDATE
-- naredbom? Nakon tog sljedećom UPDATE naredbom zamijenite matične brojeve studenata u
-- relaciji ispitT.

-- Javlja gresku Cant reopen temporary table tako da ocigledno ne moze sve u jednoj 
 
create temporary table conv(
stariMbr int(11) not null,
noviMbr int(11) not null,
noviPbrrod int(11),
noviPbrstan int(11)
);

LOAD DATA INFILE '/var/lib/mysql-files/tmp/konverzija.unl' INTO TABLE conv FIELDS TERMINATED BY
'#' LINES STARTING BY '\n' TERMINATED BY '#\r';

update studT set mbrstud = (select noviMbr from conv where conv.stariMbr=studT.mbrstud);
update studT set pbrstan = (select noviPbrstan from conv where conv.stariMbr=studT.mbrstud);
update studT set pbrrod = (select noviPbrrod from conv where conv.stariMbr=studT.mbrstud);


-- 10 Izbrisati sve studente čije prezime započinje samoglasnikom. (11 zapisa)

delete from studT where prezstud rlike "^[AEIUO]";

-- 11 Izbrisati podatke o svim nastavnicima koji su zaposleni u organizacijskoj jedinici čiji naziv
-- sadrži riječ ‘fiziku’. Prije toga obrisati podatke o ispitima za sve nastavnike koji će biti obrisani.
-- (42 i 8 zapisa)

delete from ispitT where exists
(select sifnastavnik from nastT
inner join nastT on nastT.siforgjed=orgjed.siforgjed
where nazorgjed like "%fiziku%");

delete from nastT where exists
(select siforgjed from orgjed 
where nazorgjed like "%fiziku%"); 

-- 12 Na temelju datoteke init.sql napraviti datoteku initRef.sql čije će naredbe za kreiranje relacija
-- sadržavati definicije primarnih i stranih ključeva. Indekse koji su do sada korišteni za
-- osiguravanje jedinstvenosti ključa više ne treba kreirati.
-- Napisati INSERT, UPDATE ili DELETE naredbe kojim bi se, radi testiranja, narušila neko od
-- definisanih pravila integriteta.dvorana
 
 -- ok

