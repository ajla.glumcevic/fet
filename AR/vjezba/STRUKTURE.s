.rodata
str1: .string "Foo1 %f\n"
str2: .string "addFoo %d\n"
str3: .string "Bar %f\n"
str4: .string "Baz %d %d %d\n"


.text
.globl main
main:


addiu $sp, $sp, -64
sw $ra, 60($sp)


# Foo{ char c; int i ; double d;} size = 16
addiu $a0, $sp, 0
jal loadFoo
lw $a3, 8($a0)
lw $a2, 12($a0)
#lw $a1, 4($sp)
la $a0, str1
jal printf

lb $a0, ($sp)
lw $a1, 4($sp)
lw $a3 , 12($sp)
lw $a2, 8($sp)
jal addFoo


# Bar{ float f; double d; int i; char c;} size = 24
addiu $a0,  $sp, 24
jal loadBar
lw $a2, 8($a0)
lw $a3, 12($a0)
la $a0, str3
jal printf

# Baz { short s1; int i; short s2;} size = 12
addiu $a0, $sp, 48
li $a1, 1
li $a2, 2
li $a3 , 3
jal loadBaz
lh $a1, ($a0)
lw $a2, 4($a0)
lh $a3, 8($a0)
la $a0, str4
jal printf


lw $ra, 60($sp)
addiu $sp, $sp, 64
jr $ra

#########################################################
# void addFoo(Foo)
addFoo:
addiu $sp, $sp, -24
sw $ra, 20($sp)
addiu $a1,$a1, 100 
la $a0, str2
jal printf
lw $ra, 20($sp)
addiu $sp, $sp, 24
jr $ra


# Baz loadBaz(short s , int i , short s)
loadBaz:
sh $a1, ($a0)
sw $a2, 4($a0)
sh $a3, 8($a0)

jr $ra

# Bar loadBar()
loadBar:

# f = 6.7
# d = 50.
# i = d-f
# c = 'B'

li.s $f4, 6.7
s.s $f4,($a0) 

li.d $f6, 50.
s.d $f6, 8($a0)

cvt.d.s $f4, $f4
sub.d $f8, $f6, $f4
trunc.w.d $f8, $f8
mfc1 $t0, $f8
sw $t0, 16($a0)

li $t0, 'B'
sb $t0, 20($a0)

jr $ra



# Foo loadFoo()
loadFoo:

# c = 'A'
# i = -60
# d = 23.56

li $t0, 'A'
sb $t0,($a0) 

li $t0, -60
sw $t0, 4($a0)
li $t0, 0x40378F5C
li $t1, 0x28F5C28F 
sw $t0, 8($a0)
sw $t1, 12($a0)
#li.d $f4, 23.56
#s.d $f4, 8($a0) 

jr $ra
