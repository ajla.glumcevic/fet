#include <stdio.h>

static double res;

double func(float a, float* b, int c);
/* if (a < c) */
/*   *b = a + c; */
/* else */
/*   *b = a * c; */
/*  */
/* return *b; */
/* } */
int main(void) {
  float a = 9.;
  float b = 7.;
  int c = 2;
  double d = func(a, &b, c);

  printf("d=%f ", b);

  return 0;
}
