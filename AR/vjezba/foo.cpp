#include <iostream>
struct Foo
{
    // short s1;
    // char c1;
    // int i1;
    // double d1;
    char c;
    short s1;
    short s2;
    int i;
    double d;
};

double foo(Foo f);
// {
//     return f.c1 + f.s1 + f.d1 + f.i1;
// }

// Foo bar(short s, char c, int i, double d)
// {
//     struct Foo f;
//
//     f.c1 = c;
//     f.d1 = d;
//     f.s1 = s;
//     f.i1 = i;
//     return f;
// }

int main()
{
    // Foo f = bar(1, 'a', 3, 10.);
    // printf("%f\n", foo(f));
    printf("%lu\n", sizeof(Foo));
    return 0;
}
