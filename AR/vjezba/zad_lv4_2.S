.section .rodata
str: .asciiz "abCdEfgzh"
.section .data
res: .space 40
.section .text
.globl main
main:
  la $s0, str
  la $s1, res
  addiu $t0, $0, 0 #i=0

petlja:
  addu $t5,$s0, $t0 #adresa itog slova str 
  addu $t6,$s1, $t0 #adresa itog slova res
  addiu $t0, $t0, 1 #++i
  lb $t1, ($t5) #slovo iz str
  beq $t1,$0, kraj #slovo==\0

  slti $t9, $t1, 123 #slovo<123
  beq $t9, $0, petlja
  slti $t9, $t1, 97 #slovo<97
  bne $t9, $0, petlja
  addiu $t1, $t1,-32 #slovo-32 -->veliko

  sw $t1, ($t6) #result
  j petlja


kraj:
lw $t0, 6($s1)
addiu $v0, $0, 0
jr $ra
