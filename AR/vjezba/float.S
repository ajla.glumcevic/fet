.rodata
str1: .asciiz "d=%f"
str2: .asciiz "d=%f"
.data
res: .double 

.text
.globl main
main:

addiu $sp, $sp, -64
sw $ra, 60($sp)

# f12 ..a
# a1 ...pb
#a2 ...c

lui $t0, 0x40A0
ori $t0, $t0, 0x0000
sw $t0, 56($sp)

lui $t0, 0x40E0
ori $t0, $t0, 0x0000
sw $t0, 52($sp)

addiu $t0, $t0, 2
sw $t0, 48($sp)


l.s $f12, 56($sp)
addiu $a1 , $sp, 52
lw $a2, 48($sp)
jal func

mfc1 $a2, $f0
mfc1 $a3, $f1
la $a0, str1
jal printf

la $a0, str2
la $t0, res
l.d $f4,($t0)
mfc1 $a2, $f4
mfc1 $a3, $f5
jal printf




lw $ra, 60($sp)
addiu $sp, $sp, 64
jr $ra

