#include <iostream>

struct Bar
{
  short s;
  char l;
  int i;
};

std::ostream &operator<<(std::ostream &o, const Bar &b)
{
  std::cout << "{"
            << b.l << ","
            << b.s << ","
            << b.i << "}" << std::endl;
  return o;
}

extern "C" Bar bar();

int main()
{
  std::cout << bar() << std::endl;
  std::cout << sizeof(Bar) << std::endl;
  return 0;
}
