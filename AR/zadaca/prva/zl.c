#include <stdio.h>
#include <inttypes.h>
void swap(void*,void*,int32_t);

int n1[10] = {1,3,5,7,9};
int n2[10] = {2,4,6,8,10}; 
int main(void) {
  swap(n1,n2,5*sizeof(int));
  int i;
  for(i = 0; i < 5; ++i)
    printf("%d %d",n1[i], n2[i] );
  return 0;
}
