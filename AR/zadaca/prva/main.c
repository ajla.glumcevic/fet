#include <stdio.h>
#include <inttypes.h>

int n1[] = {1,2,3};
int n2[] = {7,8,9};
int16_t exchange(int16_t* a, int16_t b);
void swap(void*, void*, int);
int main(void)
{
  swap(n1, n2, 10*sizeof(int));
  int i = 0;
  for(i =0; i < 3; ++i)
    printf("%d %d", n1[i], n2[i]);

  return 0;
}

