#include <stdio.h>
#include <stdlib.h>

/* static char *d; */

void print_comb(int maxr); 
void combinations_impls(char *chars, int maxr, int size, int r);
void combinations(char *chars, int size, int r);

int main(int argc, char *argv[]) {
  char set[] = {'a', 'b', 'c', 'd'};
  combinations(set, 4, 3);
  return 0;
}
