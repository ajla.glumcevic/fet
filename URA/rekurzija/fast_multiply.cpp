#include <iostream>

int fast_multiply(int a, int b) {
  if (b == 1) return a;
  auto result = fast_multiply(a + a, b / 2);
  if (b & 1) result += a;
  return result;
}

int main(int argc, char *argv[]) {
  std::cout << fast_multiply(7, 8) << std::endl;
  return 0;
}
