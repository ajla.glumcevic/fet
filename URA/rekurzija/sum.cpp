#include <iostream>

int sum(int a, int b) {
  if (b == 0)
    return a;
  else
    return sum(a + 1, b - 1);
}

int main(void) {
  std::cout << sum(40, 36) << std::endl;
  return 0;
}
