
#include <stdio.h>

int konverzija(int n)
{
 if(n==0)
   return 0;
 return n%10 + 2*konverzija(n/10);
}

int main()
{
  int n = 1011;
  printf("%d", konverzija(n));
  return 0;
}
