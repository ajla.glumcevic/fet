#include<iostream>

using namespace std;

int najmanji(int *niz, int pocetak, int kraj) {
  if(kraj <= pocetak)
    return niz[pocetak];
  int srednji = (pocetak + kraj ) / 2;
  int pom1 = najmanji(niz, pocetak, srednji);
  int pom2 = najmanji(niz, srednji+1, kraj);

  if(pom1 < pom2)
    return pom1;
  else
    return pom2;
}

int pronadjiNajmanji(int *niz, int n){
  return  najmanji(niz,0,n);
}

int Binary(int *niz, int pocetak, int kraj, int vrijednost) {
  if(kraj < pocetak)
    return -1;
  int srednji = (pocetak + kraj ) / 2;
  if(vrijednost < niz[srednji])
    return Binary(niz, pocetak, srednji-1, vrijednost);
  else if(vrijednost > niz[srednji])
    return Binary(niz, srednji+1, kraj, vrijednost);
  else
    return 1;
}

int main() {
  int niz[5] = {2,-7,-9,5,6};
  cout << "Najmanji je "  << pronadjiNajmanji(niz, 5) << endl;
  cout <<"Trazim 5" << endl;
  if(Binary(niz, 0, 5, 5) == 1)
    cout << "Nadjeno" << endl;
  return 0;
}
