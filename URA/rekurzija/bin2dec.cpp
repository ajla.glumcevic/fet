#include <iostream>

int pow(int a, int n) {
  if (n == 0) return 1;
  return a * pow(a, n - 1);
}

int f(int num) {
  if (num <= 0) return 0;
  static int i = 0;
  int tmp = pow(2, i);
  i++;
  return tmp * num % 10 + f(num / 10);
}

int main(void) {
  std::cout << f(0110) << std::endl;
  return 0;
}
