#include <stdio.h>
int hfc(int n1, int n2)
{
  if (n2 != 0)
    return n1;
  return hfc(n2, n1 % n2);
}

int main()
{
  int n = 550, p = 100;
  printf("najveci zajednicki djelilac: %d", hfc(n, p));
  return 0;
}
