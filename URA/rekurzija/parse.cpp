/*
 *
 *  Napisati rekurzivnu funkciju prototipa
 *
 *  void parse(string tekst, vector<string>& parsirano)
 *
 *  koja vrši parsiranje zadanog niza karaktera tekst. Ovaj parameter treba biti
 *  kontinualna sekvenca slova (bez praznih mjesta, brojeva ili specijalnih
 *  karaktera), a vector parsirano će sadržavati sve riječi iz zadanog stringa koje
 *  se nalaze u unaprijed definisanom rječniku (skupu riječi koje algoritam može
 *  prepoznati). Naprimjer, za parametar
 *
 *  tekst = ”sutrajesubota”
 *
 *  funkcija treba da vector parsirano napuni stringovima “sutra”, “je” i 
 *  “subota” (uz pretpostavku da se sve navedene riječi nalaze u rječniku). Napisati 
 *  program koji testira navedenu funkcionalnost.
 *
 *
 */

#include <iostream>
#include <vector>

using namespace std;

vector<string> rjecnik;

bool postojiRijec(string rijec){
    for(int i=0; i<rjecnik.size(); i++){
        if(rijec==rjecnik[i])
            return true;
    }
    return false;
}

void parse(string tekst, vector<string>& parsirano){
    if(postojiRijec(tekst)){
        parsirano.push_back(tekst);
        return;
    }
    for(int i=0; i<tekst.size(); i++){
        string rijec=tekst.substr(0, i);
        string ostatak=tekst.substr(i);
        if(postojiRijec(rijec)){
            parsirano.push_back(rijec);
            parse(ostatak, parsirano);
            return;
        }
    }
}

int main() {
    
    rjecnik={"ponedjeljak", "utorak", "srijeda", "četvrtak", "petak", "subota", "nedjelja", "danas", "sutra", "je", "dok"};
    
    vector<string> parsirano;
    parse("danasječetvrtakdokjesutrapetak", parsirano);
    for(int i=0; i<parsirano.size(); i++){
        cout<<parsirano[i]<<endl;
    }
    return 0;
}
