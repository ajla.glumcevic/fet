#include <iostream>
#include <vector>

int binary_search( int* niz, int low,  int high, int num){

  if(high>=1){
    int m = low + (high - low)/2;

    if(niz[m]==num) return m;
    if(niz[m] > num) return binary_search(niz, low, m-1, num);
    else return binary_search(niz, m+1, high, num);
  }
    return -1;


}


int main(void)
{
  int niz[]={3 , 2, 1, 4, 5};
  std::cout << binary_search(niz, 0,4, 3) << std::endl;
  return 0;
}
