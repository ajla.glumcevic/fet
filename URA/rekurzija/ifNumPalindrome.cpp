#include <iostream>

bool f(int num) {
  if (num <= 0) return true;

  int tmp = num;
  int a;
  int i = 1;
  while (tmp) {
    a = tmp % 10;
    tmp /= 10;
    i *= 10;
  }

  if (a != num % 10) return false;
  return f(num % (i / 10) / 10);
}

int main(void) {
  int num = 3113;

  std::cout << std::boolalpha << f(num) << std::endl;
  return 0;
}
