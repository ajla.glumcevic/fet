#include <iostream>

int foo(int num){

  if(num == 0) return 0;
  else return num%2 + 10*foo(num/2);

}

int bar(int num){

  if(num == 0) return 0;
  else return num%10 + 2*bar(num/10);

}


int main(void)
{
  std::cout << foo(14) << std::endl;
  std::cout << bar(1110) << std::endl;
  return 0;
}
