#include "hanoj.hpp"
#include <chrono>
#include <thread>

Hanoj::Hanoj(int vel) : n{vel} {
  for (int i = n; i > 0; --i) a.push_back(i);
}

void Hanoj::rijesi() { prebaci(a, b, c, n); }

void Hanoj::prebaci(Stub& A, Stub& B, Stub& C, int N) {
  if (N == 1) {
    prebaci_jedan(A, B);
    return;
  }

  prebaci(A, C, B, N - 1);
  prebaci_jedan(A, B);
  prebaci(C, B, A, N - 1);
}

void Hanoj::prebaci_jedan(Stub& A, Stub& B){
  B.push_back(A.back());
  A.pop_back();
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  std::cout <<std::string(50, '\n') ;
  print();
}

void Hanoj::print() const {
  auto printed = [=](const Stub& s, int i) {
    std::cout << '|';
    if (s.size() >= i)
      std::cout << std::string(s[i-1], '=') << std::string(n + 1 - s[i-1], ' ');
    else
      std::cout << std::string(n + 1, ' ');
  };

  for (int i = n; i > 0; --i) {
    printed(a, i);
    printed(b, i);
    printed(c, i);
    std::cout << std::endl;
  }
  std::cout << std::string(3 * n + 3, '-') << std::endl;
}
