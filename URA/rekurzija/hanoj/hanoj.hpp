#pragma once
#include <iostream>
#include <vector>

class Hanoj {
  typedef std::vector<int> Stub;
  Stub a, b, c;
  int n;
  void prebaci(Stub&, Stub&, Stub&, int);
  void prebaci_jedan(Stub&, Stub&);

  public:
  Hanoj(int);
  void print() const;
  void rijesi();
};







