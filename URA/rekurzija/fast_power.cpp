#include <iostream>

int fast_power(int base, int exp) {
  if (exp == 1) return base;
  auto result = fast_power(base * base, exp / 2);
  if (exp & 1) result *= base;
  return result;
}

int main(void) {
  std::cout << fast_power(2, 8) << std::endl;
  return 0;
}
