#include <iostream>

int f(int n) {
  static int i = 0;
  i++;
  if (n <= 0) return 0;
  return n % 2 + 10 * f(n / 2);
}

int main(void) {
  std::cout << f(13) << std::endl;
  return 0;
}
