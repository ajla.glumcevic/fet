#include <iostream>

int f(int num){

  if(num<=0) return 0;
  
  return num%10 + f(num/10);

}


int main(int argc, char *argv[])
{
  std::cout << f(1239) << std::endl;
  return 0;
}
