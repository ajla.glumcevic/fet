#include <iostream>

int foo(int arr[], int size) {
  if (size == 0) return 1;
  if (size == 1) return *arr;

  auto l = foo(arr, size/2);
  auto r = foo(arr + size/2 + 1, size/2);
  arr[size/2] = l*r;
  
 }

int main(void) { 
  
 int arr[] = {2, 4, 6}; 
foo(arr, 3);
for(int i = 0; i < 3; ++i) std::cout << arr[i] << std::endl;
  return 0; }
