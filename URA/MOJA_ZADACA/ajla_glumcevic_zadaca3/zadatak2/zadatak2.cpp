#include <algorithm>
#include <iostream>
#include <vector>
int nums[] = {1, 3, 4};
// recursion
int combinations(int n, int currentIndex = 0) {
  if (n == 0) return 1;
  if (n < 0) return 0;

  int result = 0;
  for (auto i = currentIndex; i < 3; ++i)
    result += combinations(n - nums[i], i);

  return result;
}

// mem
int combinations_mem_impl(int n, std::vector<int> table, int currentIndex = 0) {
  if (n < 0) return 0;
  if (table[n] != -1) return table[n];
  if (n == 0) {
    table[0] = 1;
    return 1;
  }

  for (auto i = currentIndex; i < 3; ++i)
    table[n] += combinations(n - nums[i], i);

  return table[n] + 1;
}
int combinations_mem(int n) {
  std::vector<int> table(n + 1, -1);
  return combinations_mem_impl(n, table);
}

// dp
int combinations_dp(int n) {
  // initialize table
  int table[n + 1][3];
  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < n + 1; ++j) {
      if (i == 0)
        table[i][j] = 1;
      else
        table[i][j] = 0;
    }
  }

  // solve
  for (int i = 1; i < n + 1; ++i) {
    for (int j = 0; j < 3; ++j) {
      int x = 0, y = 0;

      if (i - nums[j] >= 0) x = table[i - nums[j]][j];
      if (j >= 1) y = table[i][j - 1];
      table[i][j] = x + y;
    }
  }
  // // print
  // for (int i = 0; i < n + 1; ++i) {
  //   for (int j = 0; j < 3; ++j) {
  //     std::cout << table[i][j] << " ";
  //   }
  //   std::cout << std::endl;
  // }
  return table[n][2];
}

int main(void) {
  int n = 5;
  std::cout << "Broj nacina: " << std::endl;
  std::cout << combinations(n) << std::endl;
  std::cout << combinations_mem(n) << std::endl;
  std::cout << combinations_dp(n) << std::endl;
  std::cout << combsimpl(n) << std::endl;
  return 0;
}
