#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int foo(int n)
{
    if (n == 1)
        return n;
    return 1 + foo(n - foo(n - 1));
}


int lookupTable[100];
int foo2(int n) {
  if (lookupTable[n] != 0)
        return lookupTable[n];
  if (n == 1) {
        lookupTable[n] = 1;
        return lookupTable[n];
  } else {
    lookupTable[n] = 1 + foo2(n - foo2(n - 1));
    return lookupTable[n];
  }
}


int foo3(int n) {

  std::vector<int> lookupTable(n + 1);

  lookupTable[1] = 1;
  for (int i = 2; i <= n; ++i) 
    lookupTable[i] = 1 + lookupTable[i - lookupTable[i - 1]];
  
  return lookupTable[n];
}
void funkcija(std::string binary, int ones, int zeros, int n){
    if(n==0){
        std::cout<<binary << " ";
        return;
    }
    funkcija(binary+"1", ones+1, zeros, n-1);
    if(ones>zeros){
        funkcija(binary+"0", ones, zeros+1, n-1);
    }
}

void fun(int n){
    std::string binary;
    funkcija(binary+"1", 1, 0, n-1);
}
int main(void)
{
  
  int n = 19;
  fun(4);
  std::cout << foo(n) << std::endl;
  std::cout << foo2(n) << std::endl;
  std::cout << foo3(n) << std::endl;
  return 0;
}
