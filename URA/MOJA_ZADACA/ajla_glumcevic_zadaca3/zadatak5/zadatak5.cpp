#include <iostream>
#include <vector>
#define N 8

// initialization of the board 8x8
int board[N][N];

bool is_free(int x, int y) {
  return (board[x][y] == 0 && x >= 0 && y >= 0 && x < 8 && y < 8);
}
void place(int x, int y, int n) { board[x][y] = n; }
void remove(int x, int y) { board[x][y] = 0; }

void print_board() {
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      printf("%d\t", board[i][j]);
    }
    printf("\n\n");
  }
  printf("\n\n");
}

int solve_impl(int x, int y, int n) {
  if (n == 64) {
    print_board();
    return 65;
  }

  int result = -1;


  if (result == -1 && is_free(x + 2, y + 1)) {
    place(x + 2, y + 1, n + 1);
    result = solve_impl(x + 2, y + 1, n + 1);
    if (result == -1) {
      remove(x + 2, y + 1);
    }
  }
  if (result == -1 && is_free(x + 1, y + 2)) {
    place(x + 1, y + 2, n + 1);
    result = solve_impl(x + 1, y + 2, n + 1);
    if (result == -1) {
      remove(x + 1, y + 2);
    }
  }
  if (result == -1 && is_free(x - 1, y + 2)) {
    place(x - 1, y + 2, n + 1);
    result = solve_impl(x - 1, y + 2, n + 1);
    if (result == -1) {
      remove(x - 1, y + 2);
    }
  }
  if (result == -1 && is_free(x - 2, y + 1)) {
    place(x - 2, y + 1, n + 1);
    result = solve_impl(x - 2, y + 1, n + 1);
    if (result == -1) {
      remove(x - 2, y + 1);
    }
  }
  if (result == -1 && is_free(x - 2, y - 1)) {
    place(x - 2, y - 1, n + 1);
    result = solve_impl(x - 2, y - 1, n + 1);
    if (result == -1) {
      remove(x - 2, y - 1);
    }
  }
  if (result == -1 && is_free(x - 1, y - 2)) {
    place(x - 1, y - 2, n + 1);
    result = solve_impl(x - 1, y - 2, n + 1);
    if (result == -1) {
      remove(x - 1, y - 2);
    }
  }
  if (result == -1 && is_free(x + 1, y - 2)) {
    place(x + 1, y - 2, n + 1);
    result = solve_impl(x + 1, y - 2, n + 1);
    if (result == -1) {
      remove(x + 1, y - 2);
    }
  }
  if (result == -1 && is_free(x + 2, y - 1)) {
    place(x + 2, y - 1, n + 1);
    result = solve_impl(x + 2, y - 1, n + 1);
    if (result == -1) {
      remove(x + 2, y - 1);
    }
  }

  return result;
}

void solve_board() {
  board[0][0] = 1;
  solve_impl(0, 0, 1);
}

int main(void) {
  solve_board();

  return 0;
}
