#include <iostream>
#include <vector>

typedef unsigned long long ull;
// rekurzija
ull a(int n) {
  if (n == 0) return 2;
  if (n == 1) return 4;
  if (n == 2) return 12;

  return ull(3 * a(n - 3)) + ull(2 * a(n - 2)) + ull(a(n - 3));
}

// memoizacija
ull a_mem_impl(int n, std::vector<ull>& table) {
  if (table[n] != 0) return table[n];

  if (n == 0) {
    table[n] = 2;
    return 2;
  }
  if (n == 1) {
    table[1] = 4;
    return 4;
  }
  if (n == 2) {
    table[n] = 12;
    return 12;
  }

  table[n] = 3 * a_mem_impl(n - 3, table) + 2 * a_mem_impl(n - 2, table) +
             a_mem_impl(n - 3, table);

  return table[n];
}

ull a_mem(int n) {
  std::vector<ull> table;
  table.resize(n + 1);
  return a_mem_impl(n, table);
}

// dinamicko prog
ull a_din(int n) {
  std::vector<ull> table;
  table.resize(n + 1);

  table[0] = 2;
  table[1] = 4;
  table[2] = 12;

  for (auto i = 3; i <= n; ++i) {
    table[i] = 3 * table[i - 3] + 2 * table[i - 2] + table[i - 3];
  }

  return table[n];
}

int main(void) {

  int n = 8 ;
  std::cout << a(n) << std::endl;
  std::cout << a_mem(n) << std::endl;
  std::cout << a_din(n) << std::endl;

  return 0;
}
