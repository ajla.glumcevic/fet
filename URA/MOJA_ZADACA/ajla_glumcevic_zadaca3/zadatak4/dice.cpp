#include <iostream>
#include <vector>

const int nums[] = {1, 2, 3, 4, 5, 6};

void print_combs(const std::vector<int>& v) {
  for (const auto& el : v) std::cout << el << " ";
  std::cout << std::endl;
}

void dices_impl(std::vector<int>& v, int S, int sum, int n, int i) {
  if (n == 0) {
    print_combs(v);
    return;
  }

  for (const auto& num : nums) {
    if (sum + num < S) {
      v[i] = num;
      sum += num;
      dices_impl(v, S, sum, n - 1, i + 1);
      sum-=num;
    } else {
      return;
    }
  }
}
void dices(int S, int n) {
  std::vector<int> v;
  v.resize(n);
  dices_impl(v, S, 0, n, 0);
}

int main(void) {
  int S = 8;
  int n = 5;
  std::cout << "n = " << n << std::endl;
  std::cout << "Limit: " << S << std::endl;
  dices(S, n);
  return 0;
}
