#include <iostream>
#include <vector>

struct Ride {
  int rideID;
  int cost;
  int minutes;
};
Ride rides[] = {{6, 1, 10}, {5, 3, 4},  {4, 2, 2},
                {3, 4, 35}, {2, 5, 18}, {1, 10, 40}};

int N = 6;


int knapsack(int money) {
  int K[N + 1][money + 1];

  for (int i = 0; i <= N; ++i) {
    for (int j = 0; j <= money; ++j) {
      if (j == 0 || i == 0)
        K[i][j] = 0;
      else {
        if (j < rides[i - 1].cost)
          K[i][j] = K[i - 1][j];
        else {
          auto newValue =
              rides[i - 1].minutes + K[i - 1][j - rides[i - 1].cost];
          K[i][j] = std::max(newValue, K[i - 1][j]);
        }
      }
    }
  }
  // std::cout << "Table: " << std::endl;
  // for (int i = 0; i < N + 1; ++i) {
  //   for (int j = 0; j <= money; ++j) {
  //     std::cout << K[i][j] << "\t";
  //   }
  //   std::cout << std::endl;
  // }

  for (int i = N, j = money; i && j;) {
    if (K[i][j] == K[i - 1][j])
      --i;
    else {
      std::cout << "Ride:  " << rides[i - 1].rideID
                << "\tCost: " << rides[i - 1].cost
                << "\tMinutes: " << rides[i - 1].minutes << std::endl;
      j = j - rides[i - 1].cost;
      --i;
    }
  }

  std::cout << "Time: ";
  return K[N][money];
}
int main(void) {
  int money = 10;
  std::cout << knapsack(money) << std::endl;
  return 0;
}
