#include <chrono>
#include <iostream>
#include <vector>
#include "sa_indeksima.hpp"
#include "sa_iteratorima.hpp"

int main(void) {
  int arraySize = 1000000;
  std::cout << "ArraySize: " << arraySize << std::endl;
  {
    std::vector<int> v;
    for (int i = 0; i < arraySize; ++i) v.push_back(i);
    std::random_shuffle(v.begin(), v.end());
    // std::cout << "\nOriginal:" << std::endl;
    // for (const auto& el : v) std::cout << el << " ";
    // std::cout << std::endl;

    auto now = std::chrono::steady_clock::now();
    my::quicksort(v.begin(), v.end());
    auto elapsed = std::chrono::steady_clock::now() - now;
    // std::cout << "\nSorted:" << std::endl;
    // for (const auto& el : v) std::cout << el << " ";
    // std::cout << std::endl;

    std::cout << "\nQuick sort koristenjem iteratora: "
              << elapsed.count() / 1000000 << "ms" << std::endl;
    std::cout << "First element after sort:" << v.front() << std::endl;
    std::cout << "Last element after sort:" << v.back() << std::endl;
    std::cout << "Median: " << v[v.size() / 2] << std::endl;
  }

  {
    std::vector<int> v;
    for (int i = 0; i < arraySize; ++i) v.push_back(i);
    std::random_shuffle(v.begin(), v.end());
    // std::cout << "\nOriginal:" << std::endl;
    // for (const auto& el : v) std::cout << el << " ";
    // std::cout << std::endl;

    auto now = std::chrono::steady_clock::now();
    my::quicksort(v, 0, v.size() - 1);
    auto elapsed = std::chrono::steady_clock::now() - now;
    // std::cout << "\nSorted:" << std::endl;
    // for (const auto& el : v) std::cout << el << " ";
    // std::cout << std::endl;

    std::cout << "\nQuick sort koristenjem indeksa: "
              << elapsed.count() / 1000000 << "ms" << std::endl;
    std::cout << "First element after sort:" << v.front() << std::endl;
    std::cout << "Last element after sort:" << v.back() << std::endl;
    std::cout << "Median: " << v[v.size() / 2] << std::endl;
  }

  return 0;
}
