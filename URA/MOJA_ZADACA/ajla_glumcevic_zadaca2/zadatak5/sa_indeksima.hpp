#include <algorithm>
#include <iostream>

namespace my {
template <typename Container>
void insertionsort(Container& cont, int first, int last) {

  for (auto i = first+1; i <= last; ++i)
    for (auto j = i; j > first && cont[j] < cont[j - 1]; --j)
      std::swap(cont[j], cont[j - 1]);
}
template <typename Container>
int partition(Container& cont, int first, int last) {
  auto pivotPos = last;
  auto last_ = last - 1;

  while (first <= last_) {
    if (cont[first] < cont[pivotPos])
      ++first;
    else {
      if (cont[last_] < cont[pivotPos]) {
        std::swap(cont[first], cont[last_]);
        ++first;
        --last_;
      } else {
        --last_;
      }
    }
  }
  std::swap(cont[first], cont[pivotPos]);
  return first;
}

template <typename Container>
void quicksort(Container& cont, int first, int last) {
  if (first >= last) return;

  auto pivotPos = my::partition(cont, first, last);
  quicksort(cont, first, pivotPos - 1);
  quicksort(cont, pivotPos + 1, last);
}


template <typename Container>
void quicksort_10(Container& cont, int first, int last) {
  if (last - first < 10) {
    my::insertionsort(cont, first, last);
    return;
  }

  auto pivotPos = my::partition(cont, first, last);
  quicksort_10(cont, first, pivotPos - 1);
  quicksort_10(cont, pivotPos + 1, last);
}

template <typename Container>
void quicksort_20(Container& cont, int first, int last) {
  if (last - first < 20) {
    my::insertionsort(cont, first, last);
    return;
  }

  auto pivotPos = my::partition(cont, first, last);
  quicksort_20(cont, first, pivotPos - 1);
  quicksort_20(cont, pivotPos + 1, last);
}

}  // namespace my

