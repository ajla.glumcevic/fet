#include <algorithm>
#include <iostream>



namespace my {


template <typename It>
void insertionsort(It begin, It end) {
  auto b = begin + 1;

  while (b != end) {
    auto checkIt = b;
    while (checkIt != begin && *checkIt < *(checkIt - 1)) {
      std::swap(*checkIt, *(checkIt - 1));
      --checkIt;
    }
    ++b;
  }
}

template <typename It >
It partition(It begin, It end) {
  auto pivotIt = end - 1;
  end = end - 2;

  while (begin <= end) {
    if (*begin<*pivotIt)
      ++begin;
    else {
      if (*end< *pivotIt) {
        std::swap(*end, *begin);
        ++begin;
        --end;
      } else {
        --end;
      }
    }
  }
  std::swap(*begin, *pivotIt);
  return begin;
}

template <typename It>
void quicksort(It begin, It end) {
  if (begin>=end) return;

  auto pivotIt = my::partition(begin, end);

  quicksort(begin, pivotIt);
  quicksort(pivotIt + 1, end);
}

}
