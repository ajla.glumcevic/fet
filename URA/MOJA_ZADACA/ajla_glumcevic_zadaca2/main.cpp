#include <iostream>
#include "shell_sort_iterator.hpp"
#include <algorithm>
#include <vector>

int main(void)
{
  
  std::vector<int> v;
  for(int i = 0; i < 50; ++i) v.push_back(i); 
  std::random_shuffle(v.begin(), v.end());

  for(auto el: v) std::cout << el << " ";
  shell_sort(v.begin(), v.end());
  std::cout  << std::endl;

  for(auto el: v) std::cout << el << " ";
  return 0;
}
