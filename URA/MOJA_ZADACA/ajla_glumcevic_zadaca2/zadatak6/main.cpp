#include <algorithm>
#include <iostream>
#include <string>
#include <chrono>
#include <vector>
#include "my.hpp"

void sort_teams(std::vector<Tim>& timovi) {
  my::quicksort(timovi.begin(), timovi.end(), [](const Tim& t1, const Tim& t2) {
    return t1.naziv < t2.naziv;
  });
  my::insertionsort(timovi, [](const Tim& t1, const Tim& t2) {
    return t1.postignutiGolovi > t2.postignutiGolovi;
  });
  my::insertionsort(timovi, [](const Tim& t1, const Tim& t2) {
    return t1.postignutiGolovi - t1.primljeniGolovi >
           t2.postignutiGolovi - t2.primljeniGolovi;
  });
  my::insertionsort(timovi, [](const Tim& t1, const Tim& t2) {
    return t1.bodovi > t2.bodovi;
  });
}

// prvo sortiram po nazivu pomocu quicksorta zbog njegove brzine
// u tom trenutku nije bitna stabilnost
// nakon toga za sve timove sa istom gol razlikom itd koristim insertion da bi bio inplace
// i radim u obrnutom smjeru 
//
int main(int argc, char* argv[]) {
  std::vector<Tim> timovi;
  timovi.push_back(Tim{"Tim X", 100, 10, 5});
  timovi.push_back(Tim{"Tim T", 200, 10, 5});
  timovi.push_back(Tim{"Tim Z", 100, 5, 10});
  timovi.push_back(Tim{"Tim M", 100, 0, 5});
  timovi.push_back(Tim{"Tim C", 200, 20, 18});
  timovi.push_back(Tim{"Tim I", 200, 20, 18});
  timovi.push_back(Tim{"Tim F", 200, 20, 18});
  timovi.push_back(Tim{"Tim S", 200, 18, 20});
  timovi.push_back(Tim{"Tim J", 200, 32, 30});
  timovi.push_back(Tim{"Tim A", 200, 32, 30});
  timovi.push_back(Tim{"Tim H", 100, 30, 5});
  sort_teams(timovi);

  std::cout << "Naziv\tBodovi\tPrimljeni\tPostignuti" << std::endl;
  for (int i = 0; i < timovi.size(); ++i)
    std::cout << timovi[i].naziv << "\t" << timovi[i].bodovi << "\t"
              << timovi[i].primljeniGolovi << "\t\t"
              << timovi[i].postignutiGolovi << std::endl;


  return 0;
}
