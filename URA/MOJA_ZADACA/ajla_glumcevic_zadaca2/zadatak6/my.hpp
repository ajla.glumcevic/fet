#include <algorithm>
#include <iostream>

struct Tim {
  std::string naziv;
  int bodovi;
  int postignutiGolovi;
  int primljeniGolovi;

  bool operator<(const Tim& other) { return naziv < other.naziv; }
};

namespace my {
template <typename Container, typename Predicate>
void insertionsort(Container& cont, const Predicate& p) {
  auto n = cont.size();

  for (auto i = 1; i < n; ++i)
    for (auto j = i; j > 0 && p(cont[j], cont[j - 1]); --j)
      std::swap(cont[j], cont[j - 1]);
}


template <typename It, typename Predicate>
It partition(It begin, It end, const Predicate& p) {
  auto pivotIt = end - 1;
  end = end - 2;

  while (begin <= end) {
    if (p(*begin, *pivotIt))
      ++begin;
    else {
      if (p(*end, *pivotIt)) {
        std::swap(*end, *begin);
        ++begin;
        --end;
      } else {
        --end;
      }
    }
  }
  std::swap(*begin, *pivotIt);
  return begin;
}

template <typename It, typename Predicate>
void quicksort(It begin, It end, const Predicate& p) {
  if (begin>=end) return;

  auto pivotIt = my::partition(begin, end, p);

  quicksort(begin, pivotIt, p);
  quicksort(pivotIt + 1, end, p);
}

}  // namespace my
