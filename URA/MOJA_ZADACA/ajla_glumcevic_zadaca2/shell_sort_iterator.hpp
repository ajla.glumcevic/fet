#include <iostream>

template <typename It>
void shell_sort(It begin, It end) {
  int size = end - begin;

  for (auto k = size; k != 0; k /= 2) {
    for (auto jt = begin + k; jt < end; jt += k) {
      for (auto kt = jt; kt >= (begin + k) && *(kt) < *(kt - k); kt -= k) {
        std::swap(*kt, *(kt - k));
      }
    }
  }
}
