#include <iostream>

int findSmallest(int* array, int size) {

  auto middle_index = size / 2;
  if (array[middle_index] < array[middle_index - 1]) return array[middle_index];
  if (array[middle_index + 1] < array[middle_index])
    return array[middle_index + 1];

  if (array[0] < array[size - 1])
    return array[0];
  else {
    if (array[middle_index] < array[0])
      return findSmallest(array, middle_index);
    else
      return findSmallest(array + middle_index, size - middle_index);
  }
}

int main(void) {
  int A[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,-3,  0};
  std::cout << findSmallest(A, 16) << std::endl;

  return 0;
}
