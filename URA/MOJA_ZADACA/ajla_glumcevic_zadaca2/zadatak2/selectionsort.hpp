#pragma once
#include <iostream>

namespace my {

template <typename It>
void selectionsort(It begin, It end) {
  while (begin != end) {

    auto minIt = begin;
    auto b = begin;

    while (b != end) {
      if (*b < *minIt) minIt = b;
      ++b;
    }
    if (*minIt != *begin) std::swap(*minIt, *begin);

    ++begin;
  }
}

}  // namespace my
