#include <iostream>
#include "bubblesort.hpp"
#include <vector>
#include <algorithm>
#include <chrono>

int main(void)
{
 
  int arraySize = 20;
  std::vector<int> v;
    for (int i = 0; i < arraySize; ++i) v.push_back(i);
    std::random_shuffle(v.begin(), v.end());
    std::cout << "\nOriginal:" << std::endl;
    for (const auto& el : v) std::cout << el << " ";
    std::cout << std::endl;

    auto now = std::chrono::steady_clock::now();
    my::bubblesort(v.begin(),v.end());
    auto elapsed = std::chrono::steady_clock::now() - now;
    std::cout << "\nSorted:" << std::endl;
    for (const auto& el : v) std::cout << el << " ";
    std::cout << std::endl;

    std::cout << "\nBubble sort: " << elapsed.count() << "ns"
              << std::endl;
    std::cout << "First element after sort:" << v.front() << std::endl;
    std::cout << "Last element after sort:" << v.back() << std::endl;
    std::cout << "Median: " << v[v.size()/2] << std::endl;


  return 0;
}
