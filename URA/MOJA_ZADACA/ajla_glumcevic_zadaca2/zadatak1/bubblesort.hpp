#pragma once
#include <iostream>

namespace my {

template <typename It>
void bubblesort(It begin, It end) {
  auto e = end - 1;
  auto b = begin;

  while (e != begin) {
    b = begin;
    while (b != e) {
      if (*b > *(b + 1)) std::swap(*b, *(b + 1));
      ++b;
    }
    --e;
  }
}
}  // namespace my
