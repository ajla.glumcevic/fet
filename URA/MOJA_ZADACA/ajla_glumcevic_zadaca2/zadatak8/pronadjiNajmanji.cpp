#include <iostream>

int pronadjiNajmanji(int* niz, int size) {
  if (size == 1) return *niz;

  int leftEl = pronadjiNajmanji(niz, size / 2);
  int rightEl = 0;
  if (size & 1)
    rightEl = pronadjiNajmanji(niz + size / 2, size / 2 + 1);
  else
    rightEl = pronadjiNajmanji(niz + size / 2, size / 2);
  if (leftEl < rightEl)
    return leftEl;
  else
    return rightEl;
}

int main(void) {
  int niz[] = {3, 1, 2, 4, 5, -6};
  std::cout << pronadjiNajmanji(niz, 6) << std::endl;
  return 0;
}
