#pragma once
#include <iostream>

namespace my {

template <typename It>
void insertionsort(It begin, It end) {
  auto b = begin + 1;

  while (b != end) {
    auto checkIt = b;
    while (checkIt != begin && *checkIt < *(checkIt - 1)) {
      std::swap(*checkIt, *(checkIt - 1));
      --checkIt;
    }
    ++b;
  }
}

}  // namespace my
