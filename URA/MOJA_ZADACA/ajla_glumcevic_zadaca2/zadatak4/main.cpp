#include <algorithm>
#include <chrono>
#include <iostream>
#include <vector>
#include "mergesort.hpp"

int main(void) {

     int arraySize = 10000;
  {

      std::vector<int> v;
        for (int i = 0; i < arraySize; ++i) v.push_back(i);
        std::random_shuffle(v.begin(), v.end());
        // std::cout << "\nOriginal:" << std::endl;
        // for (const auto& el : v) std::cout << el << " ";
        // std::cout << std::endl;

        auto now = std::chrono::steady_clock::now();
        my::mergesort1(v.begin(),v.end());
        auto elapsed = std::chrono::steady_clock::now() - now;
        // std::cout << "\nSorted:" << std::endl;
        // for (const auto& el : v) std::cout << el << " ";
        // std::cout << std::endl;

        std::cout << "\nNormal Merge sort: " << elapsed.count() /1000000<< "ms"
                  << std::endl;
        std::cout << "First element after sort:" << v.front() << std::endl;
        std::cout << "Last element after sort:" << v.back() << std::endl;
        std::cout << "Median: " << v[v.size()/2] << std::endl;
  }
  {
      std::vector<int> v;
        for (int i = 0; i < arraySize; ++i) v.push_back(i);
        std::random_shuffle(v.begin(), v.end());
        // std::cout << "\nOriginal:" << std::endl;
        // for (const auto& el : v) std::cout << el << " ";
        // std::cout << std::endl;

        auto now = std::chrono::steady_clock::now();
        my::mergesort(v.begin(),v.end());
        auto elapsed = std::chrono::steady_clock::now() - now;
        // std::cout << "\nSorted:" << std::endl;
        // for (const auto& el : v) std::cout << el << " ";
        // std::cout << std::endl;

        std::cout << "\nInplace Merge sort (O(n^2)): " << elapsed.count()/1000000 << "ms"
                  << std::endl;
        std::cout << "First element after sort:" << v.front() << std::endl;
        std::cout << "Last element after sort:" << v.back() << std::endl;
        std::cout << "Median: " << v[v.size()/2] << std::endl;
  }

  return 0;
}
