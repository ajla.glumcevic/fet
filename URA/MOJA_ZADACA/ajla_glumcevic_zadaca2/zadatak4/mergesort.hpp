#pragma once
#include <iostream>
#include <vector>

namespace my {

  template <typename It>
    void merge(It begin1, It end1, It begin2, It end2, It dest) {
      while (begin1 != end1) {
        if (*begin1 <= *begin2) {
          ++begin1;
        } else {
          std::swap(*begin1, *begin2);
          for (auto it = begin2; it != end2 - 1 && *it > *(it + 1); ++it) {
            std::swap(*it, *(it + 1));
          }
          ++begin1;
        }
      }
    }
  template <typename It>
    void mergesort(It begin, It end) {
      if ((end - begin) <= 1) return;

      auto middle = begin + (end - begin) / 2;
      mergesort(begin, middle);
      mergesort(middle, end);

      merge(begin, middle, middle, end, begin);
    }

template <typename It>
void merge1(It begin1, It end1, It begin2, It end2, It dest) {
  while (begin1 != end1 || begin2 != end2) {
    if (begin1 == end1) {
      *dest++ = *begin2++;
    } else if (begin2 == end2) {
      *dest++ = *begin1++;
    } else if (*begin1 <= *begin2) {
      *dest++ = *begin1++;
    } else if (*begin2 < *begin1) {
      *dest++ = *begin2++;
    }
  }
}
template <typename It>
void mergesort1(It begin, It end) {
  if ((end - begin) <= 1) return;

  auto middle = begin + (end - begin) / 2;
  mergesort1(begin, middle);
  mergesort1(middle, end);

  std::vector<typename It::value_type> temp;
  temp.resize(end - begin);
  merge1(begin, middle, middle, end, temp.begin());
  std::copy(temp.begin(), temp.end(), begin);
}

}  // namespace my
