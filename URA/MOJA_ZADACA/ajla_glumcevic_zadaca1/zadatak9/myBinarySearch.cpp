#include <algorithm>
#include <iostream>
#include <vector>
#include <string>

template <typename T>
bool myBinarySearch(std::vector<T>& v, const T& el) {
  auto l = 0;
  auto h = v.size() - 1;
  bool found = false;


  while (l <= h) {
    auto m = (l + h) / 2;

    if (v[m] == el) {
      return true;
    } else if (v[m] < el) {
      l = m + 1;
    } else {
      h = m - 1;
    }
  }

  return found;
}

//
// int main(void)
// {
//   std::vector<std::string> v{"slovo", "dzak", "stolica", "ajla"};
//   std::sort(v.begin(), v.end());
//   std::cout << "Input: ";
//   std::string input;
//   std::cin >> input;
//   std::cout << std::boolalpha << myBinarySearch(v, input) << std::endl;
//   return 0;
// }
//
