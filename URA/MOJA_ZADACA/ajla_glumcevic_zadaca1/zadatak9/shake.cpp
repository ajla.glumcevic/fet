#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <vector>
#include "myBinarySearch.cpp"
#include "myUnique.hpp"

int main(void) {
  std::vector<std::string> words;
  std::ifstream fin{"shakespeare.txt"};
  std::string input;
  while (fin >> input) {
    words.push_back(input);
  }
  std::sort(words.begin(), words.end());
  // brisanje duplikata
  auto it = myUnique(words.begin(), words.end());
  words.erase(it, words.end());

  std::cout << "Ucitano je: " << words.size() << std::endl;

  std::cout << "Loading the file has finished." << std::endl;
  std::cout << "Enter word:" << std::endl;

  while (std::cin >> input) {
    std::cout << "Word: " << input << std::endl;

    auto now = std::chrono::system_clock::now();

	// binarno pretrazivanje
    auto found = myBinarySearch(words, input);

    auto elapsed = std::chrono::system_clock::now() - now;

    if (found) {
      std::cout << "Words exists in dictionary" << std::endl;
    } else {
      std::cout << "Words does not exists in dictionary" << std::endl;
    }
    std::cout << "Duration: " << elapsed.count() << " ns" << std::endl;
    std::cout << "Duration: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed)
                     .count()
              << " ms" << std::endl;
  }
  return 0;
}
