#include <iostream>

bool palindrome(const char* s, int n) {
  if(n<0) return false;
  if (n<=1) return true;
  if (*s != s[n - 1]) return false;
  return palindrome(s + 1, n - 2);
}

int main(void) {
  std::cout << std::boolalpha << palindrome("rottor", 6) << std::endl;
  std::cout << std::boolalpha << palindrome("rotor", 5) << std::endl;
  std::cout << std::boolalpha << palindrome("rotyor", 6) << std::endl;
  std::cout << std::boolalpha << palindrome("rotir", 5) << std::endl;
  std::cout << std::boolalpha << palindrome("ror", 3) << std::endl;
  return 0;
}
