#include <algorithm>
#include <iostream>
#include <vector>
// O(n) --> sekvencijalno pretrazivanje
bool push_unique(std::vector<int>& v, int el) {
  for (auto i = 0; i < v.size(); ++i) {
    if (v.at(i) == el) return false;
  }
  v.push_back(el);
  return true;
}

int main(void) {
  std::vector<int> vec{ 7, 34, 56, 0, 3, 2, 4, 17, 134};
  std::cout << "push_unique: 4\nExpected: false\tActual: ";
  std::cout << std::boolalpha << push_unique(vec, 4) << std::endl;
  std::cout << "push_unique: 6\nExpected: true\tActual: ";
  std::cout << std::boolalpha << push_unique(vec, 6) << std::endl;

  return 0;
}
