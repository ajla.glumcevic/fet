#include <algorithm>
#include <iostream>
#include <vector>

// binarno pretrazivanje
std::vector<int>::iterator sorted_position(std::vector<int>& v, const int& el) {
  auto l = 0;
  auto h = v.size() - 1;
  int pos;

  if (el < v[l])
    return std::vector<int>::iterator(v.begin() + l);
  else if (el > v[h])
    return std::vector<int>::iterator(v.begin() + v.size());

  while (l <= h) {
    auto m = (l + h) / 2;

    if (v[m] == el) {
      return std::vector<int>::iterator(v.begin() + m);
    } else if (v[m] < el) {
      l = m + 1;
    } else {
      h = m - 1;
    }
    pos = l;
  }

  return std::vector<int>::iterator(v.begin() + pos);
}

int main(void) {
  std::vector<int> v{4, 2, 0, -2, -4};
  std::sort(v.begin(), v.end());
  std::cout << "Before:" << std::endl;
  for (auto i : v) std::cout << i << " ";
  std::cout << std::endl;

  int inputEl = -1;

  auto it = sorted_position(v, inputEl);
  v.insert(it, inputEl);
  std::cout << "After inserting " << inputEl << std::endl;
  for (const auto& el : v) std::cout << el << " ";
  std::cout << std::endl;
  return 0;
}
