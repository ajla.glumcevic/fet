#include <iostream>
#include <vector>

namespace my {


template <typename Iter, typename P>
Iter partition(Iter begin, Iter end, const P& p) {
  auto b = begin;
  auto e = end;
  auto b_ = b;

  while (b != e) {
    if (b_ == e) break;
    if (p(*b)) {
      ++b;
      ++b_;
    } else {
      while (b_ != e) {
        if (p(*b_)) {
          std::swap(*b, *b_);
          ++b;
          break;
        } else
          ++b_;
      }
    }
  }
  return b;
}

}  // namespace my
