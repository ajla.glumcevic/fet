#include "partition.hpp"
#include <forward_list>
#include <iostream>
 
using namespace my;
int main(void) {
  std::cout << "Primjer 1 (parni)" << std::endl;
  std::forward_list<int> v{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  auto it =
      partition(v.begin(), v.end(), [](int elem) { return elem % 2 == 0; });
  for (auto i : v) {
    std::cout << i << " ";
  }
  std::cout << std::endl;
  std::cout << "Vracen iterator na: " << *it << std::endl;

  std::cout << "Primjer 2 (<4)" << std::endl;
  std::forward_list<int> v2{2, 1, 3, 8, 4, 5, 6, 9, 7};
  auto it2 =
      partition(v2.begin(), v2.end(), [](const int& elem) { return elem < 4; });
  for (auto i : v2) {
    std::cout << i << " ";
  }
  std::cout << std::endl;
  std::cout << "Vracen iterator na: " << *it2 << std::endl;

  std::cout << "Primjer 3 (==-9)" << std::endl;
  std::forward_list<int> v3{-3, -5, 8, 1, 34, -9, 1, -1};
  ;
  auto it3 = partition(v3.begin(), v3.end(),
                       [](const int& elem) { return elem == -9; });
  for (auto i : v3) {
    std::cout << i << " ";
  }
  std::cout << std::endl;
  std::cout << "Vracen iterator na: " << *it3 << std::endl;
  return 0;
}

