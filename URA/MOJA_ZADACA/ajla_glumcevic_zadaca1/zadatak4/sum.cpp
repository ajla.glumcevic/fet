#include <iostream>

int sum(const int* array, int n) {
  if (n == 1) return array[n - 1];
  return array[n - 1] + sum(array, n - 1);
}

int main(void) {
  int arr[] = {-100, 5, 140, 4};
  printf("%d", sum(arr, 4));
  return 0;
}
