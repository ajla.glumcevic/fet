#include <iostream>
#include <vector>
using namespace std;

void aggregate_count(const vector<int> a) {

  for (int i = 0; i < a.size();) {
    auto element = a[i];
    auto count = 0;

    while (a[i] == element) {
      count++;
      ++i;
    }

    cout << count << " ";
  }
}
int main(void) {
  std::vector<int> v{10, 11,12, 13, 15, 15, 16, 17};
  aggregate_count(v);

  return 0;
}
