#include <iostream>

int divide(int x, int y) {
  if (y == 0) throw std::string("Dijeljenje nulom - nedefinisana operacija.");
  if (x == y) return 1;
  if (x < y) return 0;
  if (x <= 0) return 0;
  return 1 + divide(x - y, y);
}

// divide(12, 3)
// 1 + divide(9,3)
// 1+ 1 + divide(6, 3)
// 1 + 1 + 1 + divide(3, 3)
// 1 + 1 + 1 + 1
// 1 + 1 + 2
// 1 + 3
// 4

int main(void) {
  try {
    std::cout << divide(24, 9) << std::endl;
  } catch (std::string& s) {
    std::cout << s << std::endl;
  }
  return 0;
}

