#include <iostream>

int recursion(int p, int y, int mxx) {
  if (p < 0)
    return 0;
  else if (y == mxx)
    return (1 << p);
  else if (y < mxx)
    return (1 << p) + recursion(p - 1, y >> 1, mxx - y);
  else
    return recursion(p - 1, y >> 1, mxx);
}

int fast_divide(int x, int y) {
  if (y == 0) throw std::string("Dijeljenje sa 0 je nedefinisana operacija!");
  int p = 0;
  int b = y;
  while (b <= x) {
    ++p;
    b = b << 1;
  }  // find power
  --p;
  int mxx = x;
  return recursion(p, y << p, mxx);
}

int divide(int x, int y) {
  if (x >= 0 && y >= 0)
    return fast_divide(x, y);
  else if (x <= 0 && y <= 0)
    return fast_divide(0 - x, 0 - y);
  else if (x >= 0 && y <= 0)
    return 0 - fast_divide(x, 0 - y);
  else
    return 0 - fast_divide(0 - x, y);
}
//

int main() {
  std::cout << divide(126, 9) << std::endl;
  std::cout << divide(12, 450) << std::endl;
  std::cout << divide(12, 3) << std::endl;
  std::cout << divide(12, 12) << std::endl;
  std::cout << divide(181, 111) << std::endl;
  std::cout << divide(126, 0) << std::endl;

  return 0;
}
