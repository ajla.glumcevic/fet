#include <iostream>

typedef unsigned long long ull;
ull multEven(int n) {
  // ukoliko je broj neparan smanji za jedan
  if (n & 1) --n;
  // base case ukoliko dodje do 0
  if (n == 0) return 1;
  return (ull)n * multEven((ull)n - 2);
}

int main(void) {
  int n;
  std::cout << "Unesite prirodan broj: " << std::endl;
  std::cin >> n;
  std::cout << multEven(n) << std::endl;
  return 0;
}
