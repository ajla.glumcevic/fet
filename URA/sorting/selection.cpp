#include <iostream>
#include <vector>

template <typename Container>
void selection(Container& cont) {
  auto n = cont.size();
  for (auto i = 0; i < n; ++i) {
    auto min = i;
    for (auto j = i + 1; j < n; ++j)
      if (cont[j] < cont[min]) min = j;

    if (min != i) std::swap(cont[i], cont[min]);
  }
}

int main(void) {
  std::vector<int> v{0,5, 4, 3, 2, 1};
  selection(v);
  for (const auto& e : v) std::cout << e << std::endl;
  return 0;
}
