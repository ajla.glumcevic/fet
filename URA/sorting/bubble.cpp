#include <iostream>
#include <list>
#include <vector>

template <typename Container>
void bubble(Container& cont) {
  for (auto i = cont.size(); i > 1; --i)
    for (auto j = 1; j < i; ++j)
      if (cont[j] < cont[j - 1]) std::swap(cont[j], cont[j - 1]);
}

int main(void) {
  std::vector<int> v{3, 2, 7, 8, 1, 5};

  bubble(v);

  for (const auto& e : v) std::cout << e << " ";
  std::cout << std::endl;
  return 0;
}
