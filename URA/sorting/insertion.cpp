#include <iostream>
#include <vector>

template <typename Container>
void insertion(Container& cont) {
  auto n = cont.size();

  for(auto i = 1; i < n; ++i)
    for(auto j = i; j > 0 && cont[j] < cont[j-1]; --j)
      std::swap(cont[j], cont[j-1]);


}

int main(void) {
  std::vector<int> v{0,5, 4, 3, 2, 1};
  insertion(v);
  for (const auto& e : v) std::cout << e << std::endl;
  return 0;
}
