


int main(void) {
  std::vector<int> v{3, 5, 1, 2, 6, 0};
  merge(v);

  for(const auto& el: v) std::cout << el << std::endl;
  return 0;
}
