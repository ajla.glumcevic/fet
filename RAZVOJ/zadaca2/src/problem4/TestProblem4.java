package problem4;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class TestProblem4 {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
    }
}

class MainFrame extends JFrame {
    private ButtonPanel buttonPanel = new ButtonPanel();
    private DrawPanel drawPanel = new DrawPanel();
    private TextFieldPanel textFieldPanel = new TextFieldPanel();

    private static int WIDTH = 400;
    private static int HEIGHT = 400;
    private static int ROW_HEIGHT = WIDTH / 12;


    public MainFrame() {
        setSize(new Dimension(WIDTH, HEIGHT));
        setLocation(WIDTH / 2, HEIGHT / 2);
        setVisible(true);
        setTitle("Crtanje oblika");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        add(buttonPanel, BorderLayout.NORTH);
        add(drawPanel, BorderLayout.CENTER);
        add(textFieldPanel, BorderLayout.SOUTH);
    }

    /****************************************************************/
    class ButtonPanel extends JPanel {
        private JButton circleBtn = new JButton("Krug");
        private JButton squareBtn = new JButton("Kvadrat");

        public ButtonPanel() {
            circleBtn.setPreferredSize(new Dimension(MainFrame.WIDTH / 3, MainFrame.ROW_HEIGHT));
            squareBtn.setPreferredSize(new Dimension(MainFrame.WIDTH / 3, MainFrame.ROW_HEIGHT));
            circleBtn.addActionListener(new DrawShapeListener());
            squareBtn.addActionListener(new DrawShapeListener());
            add(circleBtn);
            add(squareBtn);
        }

        class DrawShapeListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == circleBtn) drawPanel.shape = 'c';
                else if (e.getSource() == squareBtn) drawPanel.shape = 's';
                drawPanel.repaint();
            }
        }

    }

    /****************************************************************/
    class DrawPanel extends JPanel {
        private char shape = '\\';
        int shapeDimension = 0;

        public DrawPanel() {
            setLayout(new BorderLayout());

            JLabel label = new JLabel("Oblik:");
            label.setPreferredSize(new Dimension(MainFrame.WIDTH, MainFrame.ROW_HEIGHT));
            label.setHorizontalAlignment(JLabel.CENTER);
            add(label, BorderLayout.NORTH);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;

            if (shape == '\\') return;

            int leftX = MainFrame.WIDTH / 2 - shapeDimension / 2;
            int leftY = MainFrame.HEIGHT / 2 - shapeDimension / 2 - 2 * ROW_HEIGHT;
            Rectangle2D rectangle = new Rectangle2D.Double(leftX, leftY, shapeDimension, shapeDimension);

            if (shape == 'c') {
                Ellipse2D circle = new Ellipse2D.Double();
                circle.setFrame(rectangle);
                g2d.setPaint(Color.BLUE);
                g2d.fillOval(leftX, leftY, shapeDimension, shapeDimension);
                g2d.draw(circle);
            } else {
                g2d.setPaint(Color.RED);
                g2d.fillRect(leftX, leftY, shapeDimension, shapeDimension);
                g2d.draw(rectangle);
            }
        }
    }


    /****************************************************************/
    class TextFieldPanel extends JPanel {
        private JTextField dimension = new JTextField("0");

        public TextFieldPanel() {
            dimension.setPreferredSize(new Dimension(MainFrame.WIDTH * 2 / 3, MainFrame.ROW_HEIGHT));
            dimension.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    updateDimension();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    updateDimension();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {

                }
            });
            add(new JLabel("Dimenzija"));
            add(dimension);
        }

        private void updateDimension() {
            String insertedDimension = dimension.getText().trim();
            if (insertedDimension.equals("")) drawPanel.shapeDimension = 0;
            else drawPanel.shapeDimension = Integer.parseInt(insertedDimension);

        }
    }
}