package problem3;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class TestProblem3 {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
    }
}

class MainFrame extends JFrame {
    private final ButtonPanel buttonPanel = new ButtonPanel();
    private final ListPanel listPanel = new ListPanel();
    private final TextFieldPanel textFieldPanel = new TextFieldPanel();

    private final static int WIDTH = 600;
    private final static int HEIGHT = 400;
    private final static int ROW_HEIGHT = HEIGHT / 12;


    public MainFrame() {
        setSize(new Dimension(WIDTH, HEIGHT));
        setLocation(WIDTH / 2, HEIGHT / 2);
        setVisible(true);
        setTitle("Kreiranje liste");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        add(buttonPanel, BorderLayout.NORTH);
        add(listPanel, BorderLayout.CENTER);
        add(textFieldPanel, BorderLayout.SOUTH);
    }

    class ButtonPanel extends JPanel {
        private final JButton inputBtn = new JButton("Unesi ime");
        private final JButton deleteBtn = new JButton("Obrisi listu");

        public ButtonPanel() {
            inputBtn.addActionListener(new ListActionListener());
            deleteBtn.addActionListener(new ListActionListener());
            add(inputBtn);
            add(deleteBtn);
        }

        class ListActionListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == inputBtn) {
                    listPanel.addNewEntry();
                } else if (e.getSource() == deleteBtn) {
                    listPanel.deleteAllEntries();
                }
            }
        }
    }


    class ListPanel extends JPanel {

        private ArrayList<String> entries = new ArrayList<>();
        JLabel label = new JLabel("Lista: ");

        public ListPanel() {
            setLayout(new BorderLayout());
            label.setPreferredSize(new Dimension(MainFrame.WIDTH, MainFrame.ROW_HEIGHT));
            label.setHorizontalAlignment(JLabel.CENTER);
            add(label, BorderLayout.NORTH);

            setLayout(new GridLayout(5,1));

            refreshList();
        }

        public void addNewEntry() {
            if (entries.size() > 5) return;
            if (textFieldPanel.firstName != null && textFieldPanel.lastName != null) {
                String newEntry = (entries.size() + 1) + ".\t" + textFieldPanel.currentFirstName + " " + textFieldPanel.currentLastName;
                entries.add(newEntry);
                System.out.println(newEntry);
            }
            refreshList();
        }

        public void deleteAllEntries() {
            entries.clear();
            System.out.println("clear");
            refreshList();
        }

        public void refreshList() {
            for (String entry : entries) {
                add(new EntryPanel(entry));
            }
            revalidate();
        }

        class EntryPanel extends JPanel {
            public EntryPanel(String entry) {
                setLayout(new BorderLayout());
                JLabel label = new JLabel(entry);
                Border border = BorderFactory.createLineBorder(Color.GRAY);
                setBorder(border);
                setBackground(Color.WHITE);
                setForeground(Color.GRAY);
                add(label);
            }
        }
    }

    class TextFieldPanel extends JPanel {
        private JTextField firstName = new JTextField("", 20);
        private JTextField lastName = new JTextField("", 20);

        private String currentFirstName = null;
        private String currentLastName = null;

        public TextFieldPanel() {
            firstName.setPreferredSize(new Dimension(MainFrame.WIDTH / 4, MainFrame.ROW_HEIGHT));
            lastName.setPreferredSize(new Dimension(MainFrame.WIDTH / 4, MainFrame.ROW_HEIGHT));
            firstName.getDocument().addDocumentListener(new NameTextFieldListener());
            lastName.getDocument().addDocumentListener(new NameTextFieldListener());
            add(new JLabel("Ime:"));
            add(firstName);
            add(new JLabel("Prezime:"));
            add(lastName);
        }


        class NameTextFieldListener implements DocumentListener {

            private void updateNames(DocumentEvent e) {
                if (e.getDocument() == firstName.getDocument())
                    currentFirstName = firstName.getText().trim();
                else if (e.getDocument() == lastName.getDocument())
                    currentLastName = lastName.getText().trim();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateNames(e);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateNames(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        }
    }

}