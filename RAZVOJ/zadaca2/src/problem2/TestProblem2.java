package problem2;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class TestProblem2 {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
    }
}

class MainFrame extends JFrame {
    private JLabel message = new JLabel("Slijedeca karta je");
    private ButtonPanel buttonPanel = new ButtonPanel();
    private CardsPanel cardsPanel = new CardsPanel();

    private static int WIDTH = 600;
    private static int HEIGHT = 400;

    public MainFrame() {
        setSize(new Dimension(WIDTH, HEIGHT));
        setLocation(WIDTH / 2, HEIGHT / 2);
        setVisible(true);
        setTitle("Igra vece\\manje");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());


//        add(message, BorderLayout.NORTH);
        add(cardsPanel, BorderLayout.CENTER);
//        add(buttonPanel, BorderLayout.SOUTH);
    }

    class CardsPanel extends JPanel {
        int xgD1, xgD2, xgD3, xgD4, ygD1, ygD2, ygD3, ygD4;
        int xdD1, xdD2, xdD3, xdD4, ydD1, ydD2, ydD3, ydD4;
        int xgI1, xgI2, xgI3, xgI4, ygI1, ygI2, ygI3, ygI4;
        int xdI1, xdI2, xdI3, xdI4, ydI1, ydI2, ydI3, ydI4;
        int CARD_WIDTH;
        int CARD_HEIGHT;

        BufferedImage cardsImage = null;

        public CardsPanel() {
            try {
                cardsImage = ImageIO.read(new File("karte.gif"));
                System.out.println(cardsImage);
                CARD_WIDTH = cardsImage.getWidth() / 13;
                CARD_HEIGHT = cardsImage.getHeight() / 4;
                System.out.println(CARD_WIDTH);
                System.out.println(CARD_HEIGHT);
                xgD1 = (int) CARD_WIDTH;
                xgD2 = (int) (2.*CARD_WIDTH);
                xgD3 = (int) (3.*CARD_WIDTH);
                xgD4 = (int) (4*CARD_WIDTH);
                ygD1 = ygD2 = ygD3 = ygD4 = MainFrame.HEIGHT - 400;
                repaint();

                Random random = new Random();
            } catch (IOException e) {
                e.getCause();
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            if(cardsImage==null ) return;
            xgI1 = 0;
            ygI1 = 0;
            xdI1 = CARD_WIDTH;
            ydI1 = CARD_HEIGHT;
            g2d.drawImage(cardsImage, xgD1, ygD1, xdD1, ydD1, xgI1, ygI1, xdI1, ydI1, null);
        }
    }

    class ButtonPanel extends JPanel {
        private JButton grBtn = new JButton("Veca");
        private JButton lsBtn = new JButton("Manja");
        private JButton eqBtn = new JButton("Ista");
        private JButton newGameBtn = new JButton("Nove igra");

        public ButtonPanel() {
            grBtn.addActionListener(new ButtonActionListener());
            lsBtn.addActionListener(new ButtonActionListener());
            eqBtn.addActionListener(new ButtonActionListener());
            newGameBtn.addActionListener(new ButtonActionListener());
            add(grBtn);
            add(lsBtn);
            add(eqBtn);
            add(newGameBtn);
        }

        class ButtonActionListener implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == grBtn) {
                } else if (e.getSource() == lsBtn) {
                } else if (e.getSource() == eqBtn) {
                } else if (e.getSource() == newGameBtn) {
                }
            }
        }

    }
}