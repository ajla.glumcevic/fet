package problem1;

import jdk.jfr.internal.tool.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class TestProblem1 {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
    }
}


class MainFrame extends JFrame {
    private static int WIDTH = 600;
    private static int HEIGHT = 400;
    private LinePanel linePanel = new LinePanel();
    private ButtonPanel buttonPanel = new ButtonPanel();

    public MainFrame() {
        setSize(new Dimension(WIDTH, HEIGHT));
        setLocation(WIDTH / 2, HEIGHT/ 2);
        setVisible(true);
        setTitle("Linija");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        add(linePanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    class LinePanel extends JPanel {
        private Point2D center = new Point2D.Double(MainFrame.WIDTH / 2., MainFrame.HEIGHT / 2.);
        private Point2D current = null;
        private double length = 0;
        private Color lineColor = Color.BLACK;
        private boolean move = false;

        public LinePanel() {
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    if (current != null) {
                        move = false;
                        return;
                    }
                    move = true;
                    current = e.getPoint();
                    length = Math.sqrt(Math.pow((current.getX() - center.getX()), 2) + Math.pow((current.getY() - center.getY()), 2));
                    repaint();
                }
            });

            addMouseMotionListener(new MouseAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                    super.mouseMoved(e);
                    if (!move) return;
                    if (current == null) return;
//                    current = getDrawingPoint(e.getPoint());
                    current = e.getPoint();

//                    treba ovdje neka logika sad idk
                }
//                Point getDrawingPoint(Point2D p){
//                    double x = p.getX() - 250;
//                    double y = p.getY() - 250;
//                    double angle = Math.abs(Math.atan(y / x));
//                    double finalX = x > 0 ? 250 + length* Math.cos(angle):250 - length* Math.cos(angle);
//                    double finalY = y > 0 ? 250 + length* Math.sin(angle):250 - length* Math.sin(angle);
//                    return new Point((int)finalX,(int)finalY);
//                }
            });


        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g.setColor(lineColor);
            if (current != null) {
                Line2D line = new Line2D.Double(center.getX(), center.getY(), current.getX(), current.getY());
                g2d.draw(line);
            }
            repaint();
        }
    }

    class ButtonPanel extends JPanel {
        private JButton yellow = new JButton("Zuta");
        private JButton blue = new JButton("Plava");
        private JButton red = new JButton("Crvena");

        public ButtonPanel() {
            yellow.addActionListener(new ColorListener());
            blue.addActionListener(new ColorListener());
            red.addActionListener(new ColorListener());
            add(yellow);
            add(blue);
            add(red);
        }

        private class ColorListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == blue) linePanel.lineColor = Color.BLUE;
                else if (e.getSource() == yellow) linePanel.lineColor = Color.YELLOW;
                else if (e.getSource() == red) linePanel.lineColor = Color.RED;
            }
        }
    }
}



