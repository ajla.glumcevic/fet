import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.lang.Math.*;

public class FirstProblem {
    public static void main(String[] args) {
        GameFrame gf = new GameFrame();
    }
}

class GameFrame extends JFrame {
    Color lineColor;

    public GameFrame() {
        setSize(500, 500);
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xPosition = tk.getScreenSize().width;
        int yPosition = tk.getScreenSize().height;
        setLocation(xPosition / 2 - 500 / 2, yPosition / 2 - 500 / 2);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        ColorSelectorPanel csp = new ColorSelectorPanel();
        add(csp, BorderLayout.SOUTH);

        LineDrawer linedrw = new LineDrawer();
        add(linedrw, BorderLayout.CENTER);
    }

    private class ColorSelectorPanel extends JPanel {
        public ColorSelectorPanel() {
            JButton yellowBtn = new JButton("Zuta");
            JButton blueBtn = new JButton("Plava");
            JButton redBtn = new JButton("Crvena");

            yellowBtn.addActionListener(new ColorListener(Color.YELLOW));
            blueBtn.addActionListener(new ColorListener(Color.BLUE));
            redBtn.addActionListener(new ColorListener(Color.RED));

            add(yellowBtn);
            add(blueBtn);
            add(redBtn);

        }
    }

    private class ColorListener implements ActionListener {
        Color color;

        public ColorListener(Color c) {
            color = c;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            lineColor = color;
        }

    }

    private class LineDrawer extends JPanel {
        private Point cursorPosition = null;
        private double distance;
        public LineDrawer() {
            addMouseListener(new MouseHandler());
            addMouseMotionListener(new MouseMotionHandler());
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D drawer = (Graphics2D) g;
            drawer.setColor(lineColor);
            if (cursorPosition != null) {
                Line2D line = new Line2D.Double(250, 250, cursorPosition.getX(), cursorPosition.getY());
                drawer.draw(line);
            }
        }

        private class MouseHandler extends MouseAdapter {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (cursorPosition == null) {
                    cursorPosition = e.getPoint();
                    int x = e.getX()-250;
                    int y = e.getY()-250;
                    distance = Math.sqrt((x*x) + (y*y));
                    repaint();
                }
            }

        }

        private class MouseMotionHandler implements MouseMotionListener {

            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (cursorPosition != null) {
                    cursorPosition = getDrawingPoint(e.getPoint());
                    repaint();
                }
                
            }

        }

        private Point getDrawingPoint(Point p){
            double x = p.getX() - 250;
            double y = p.getY() - 250;
            double angle = Math.abs(Math.atan(y / x));
            double finalX = x > 0 ? 250 + distance * Math.cos(angle):250 - distance * Math.cos(angle);
            double finalY = y > 0 ? 250 + distance * Math.sin(angle):250 - distance * Math.sin(angle);
            return new Point((int)finalX,(int)finalY);
        }
    }

}
