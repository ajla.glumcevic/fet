package animals;

public abstract class Animal implements Cloneable,Comparable<Animal>{
    private String name;
    private int age;

    protected Animal(String name, int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void display(){
        System.out.println("Name: " + this.getName());
        System.out.println("Age: " + this.getAge());
    }

    abstract public void makeSound();

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

   @Override
    public String toString(){
       return "\tName: " + this.getName()  + "\tAge: " + this.getAge();
    }

    @Override
    public int compareTo(Animal o) {
        if(this.age < o.age) return -1;
        else if(this.age > o.age) return 1;
        else return 0;
    }
}
