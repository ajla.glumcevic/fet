package animals;

import javax.swing.*;

public class Bird extends Animal implements Sound, Cloneable{
    private boolean hasWings;
    private Wings wings;

    public Bird(String name, int age, boolean hasWings){
        super(name, age);
        this.hasWings = hasWings;

        if(this.hasWings)
            this.wings = new Wings();
        else
            this.wings = null;
    }


    @Override
    public void makeSound(){
        System.out.println("piiiiiiiiiiii");
    }

    @Override
    public void display(){
        super.display();
        if(this.hasWings)
            System.out.println("Wings width: " + this.getWidth());
        else
            System.out.println("NO Wings.");
    }

    @Override
    public String toString(){
        return hasWings ? super.toString() + "\tWings width: " + getWidth() : super.toString();
    }

    class Wings{
        private int width;

        public Wings(){
                this.width = Integer.parseInt(JOptionPane.showInputDialog("Wings width", "0"));
        }
        public int getWidth(){
            return width;
        }
    }
    public int getWidth(){
        return hasWings ? wings.getWidth() : 0;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
