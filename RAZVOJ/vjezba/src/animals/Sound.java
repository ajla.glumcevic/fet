package animals;

public interface Sound {
    void makeSound();
}
