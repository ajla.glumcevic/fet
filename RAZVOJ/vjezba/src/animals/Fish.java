package animals;

public class Fish extends Animal implements Sound{
    private String livesIn;

    public Fish(String name, int age, String livesIn){
        super(name, age);
        this.livesIn = livesIn;
    }

    @Override
    public void makeSound(){
        System.out.println("ribibibibii");
    }

    @Override
    public String toString() {
        return super.toString() + "\tLives in: " + livesIn;
    }

    public String getLivesIn(){
        return livesIn;
    }
}
