package animals;

@FunctionalInterface
public interface Display {
    void display();
}
