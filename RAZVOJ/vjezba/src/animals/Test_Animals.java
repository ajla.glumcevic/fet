package animals;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test_Animals {
    public static void main(String[] args) {


        try{

            ArrayList<Animal> birds = new ArrayList<Animal>();
            birds.add(new Bird("Laki",4,true));
//            birds.add(new Bird("Riki",7,true));
//            birds.add(new Bird("Kozo",13,false));
            Bird b1 = new Bird("ajla", 7, false);
            Bird b2 = (Bird)b1.clone();
            System.out.println(b1);
            b2.setAge(188);
            System.out.println(b2);
//
//            Stream<Animal> streamBirds = birds.stream();
//            ArrayList<Animal> flyingBirds = (ArrayList<Animal>) streamBirds.filter(bird -> ((Bird)bird).getWidth() != 0).collect(Collectors.toList());
//            System.out.println("Number of Flyingbirds: " + flyingBirds.stream().count());
//            flyingBirds.forEach(System.out::println);
//
//
//            ArrayList<Animal> fishes = new ArrayList<Animal>();
//            fishes.add(new Fish("Nemo", 6, "Ocean"));
//            fishes.add(new Fish("Dora", 11, "Lake"));
//            Stream<Fish> streamFishes = fishes.stream().map(fish -> new Fish(fish.getName() + "FishyFish",fish.getAge(),((Fish)fish).getLivesIn() + " Water"));
//
//            System.out.println("ArrayList fishes: ");
//            fishes.forEach(System.out::println);
//            System.out.println("Stream streamFishes: ");
//            streamFishes.forEach(System.out::println);

//            this dont work
//            Stream<Fish> sortedFishes =(Stream<Fish>) streamFishes.sorted();
//            streamBirds = streamBirds.sorted();
//            streamFishes.forEach(System.out::println);
//            streamBirds.forEach(System.out::println);

        }
        catch(Exception e){
           e.getCause();
        }finally {
            System.out.println("Programm ended.");
        }



    }
}
