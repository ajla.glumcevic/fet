package other;

import java.io.IOException;

public class Exceptions {
    public static void main(String[] args) throws Base{

        int a = 2, b = 0;

        String s = new String("sinisa");
        assert s.isEmpty();

/***********************************************************************************************************************/
        // Throw baca throwable objekat, re nije validan objekat - NullPointerExp
//        RuntimeException re = null;
//        throw re;
/***********************************************************************************************************************/
//        try {
////            Gresku obradjuje u catchu
////            Ako nema catch blokova i main mora throws InterruptedException
//            interrupted();
//        } catch (InterruptedException e) {
//            System.out.println(e);
////            Gresku baca main
//            throw new RuntimeException();
//        } catch (RuntimeException e) {
//            System.out.println("Runtime catch blok.");
//        } catch (Exception e) {
//            System.out.println("Exception catch blok.");
//        }
////        Finally uvijek na kraju
//        finally {
//            System.out.println("Finally.");
//        }

/***********************************************************************************************************************/

//        try {
//            child();
//            base();
//        } catch (Child e) {
//            System.out.println("child.");
//            throw e;
//        } catch (Base e) {
//            System.out.println("base.");
//            throw e;
//        } finally {
//            System.out.println("Finally.");
//        }
//
//        try{
//            A aa = new A();
//            aa.f();
//        }catch (Exception e){
//            System.out.println(e.getCause());
//        }
    }

    /***********************************************************************************************************************/
//    InterruptedException (<- Exception)
    static void interrupted() throws InterruptedException{
        throw new InterruptedException("Interrupted exp.");
    }

    public static void child() throws Child{
        throw new Child();
    }
    public static void base() throws Base{
        throw new Base();
    }

/***********************************************************************************************************************/

}
//Mora nasljedjivat od Throwable
class Base extends Throwable{
}
class Child extends Base{
}

/***********************************************************************************************************************/

// U override metodama navesti sve, nijednu ili checked exceptione od nadredjenje klase
// Od ovog checked ne mogu se navodit nadredjene npr. InterruptedException -> Exception
class A extends Exception{
//    Arithmetic unchecked
//    IOEXception checked (<- Exception)
    public void f() throws ArithmeticException, IOException{
        assert 1==0;
        System.out.println("Base f");
    }
}
class B extends A{
    public void f() throws ArithmeticException{
        System.out.println("Base f");
    }
}

/***********************************************************************************************************************/
