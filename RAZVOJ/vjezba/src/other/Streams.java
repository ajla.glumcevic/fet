package other;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.*;

public class Streams {
    public static void main(String[] args) {

//        CREATE STREAM
/**********************************************************************/
//        OF
       Stream<Integer> s1 = Stream.of(1,2,3,4);
       Stream<Integer> s2 = Stream.of(new Integer[]{5,6,7,8});

//       FROM LIST
        List<Integer> l1 = new ArrayList<Integer>();
        Stream<Integer> s3 = l1.stream();

//        STREAM GENERATE
        Stream<Integer> s4 = Stream.generate(() -> {return new Integer(7);});
//        s4.forEach(System.out::println);

//        FROM STRING
        IntStream s5 = "abcd".chars();
        Stream<String> s6 = Stream.of("A$B$C".split("\\$"));
        s5.forEach(c -> System.out.println(c));



//    KONVERZIJA U COLLECTION
/**********************************************************************/
        List<Integer> l2 = s1.filter(i -> i%2==0).collect(Collectors.toList());

        Integer[] l3 = s2.toArray(Integer[]::new);


//   METODI
/**********************************************************************/
        Stream<Integer> tmp = Stream.of(1,2,3,2,3,7,7,8,9);

//        tmp.forEach(y -> System.out.println(y));

//        tmp.filter(i -> i%2==1).forEach(i -> System.out.println(i+1));

//        Stream<Integer> newStream = tmp.map(i -> i + 1);

//        Stream<Integer> newStream = tmp.sorted();
//        newStream.forEach(System.out::println);

//        List<Integer> tmpList = tmp.collect(Collectors.toList());

//        Stream<Integer> another = Stream.of(14,15,16);
//        Stream.concat(another, tmp).forEach(System.out::println);

//        Stream<Integer> newStream = Stream.of(new Integer[]{12,13,12});
//        newStream.distinct().forEach(System.out::println);

//        System.out.println(tmp.count());

//        System.out.println(tmp.allMatch(i -> i==1));
//        System.out.println(tmp.anyMatch(i -> i==1));
//        System.out.println(tmp.noneMatch(i -> i==1));

//        System.out.println( tmp.reduce(0,(x,y)->x+y));
        Stream<String> ss = Stream.of(new String[]{"Ajla", "Glumcevic"});
        System.out.println(ss.reduce((string1,string2)->string1+string2).get());
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(5);
        Stream<Integer> tok = arr.stream();
        arr = tok.filter(i -> i % 2 == 0).map(i -> (int)Math.pow(i,4)).collect(Collectors.toCollection(ArrayList::new));
        arr.forEach(System.out::println);
    }





}
