package other;

import java.util.ArrayList;
import java.util.List;

public class Generics {

    public static void main(String[] args) {

//        NE MOGU PRIMITIVNI
//        Stack<int> s = new Stack<int>();

//        GENERISE SE SAMO JEDNA INSTANCA GENERIC KLASE
        Stack<Number> nums = new Stack<>();
        Stack<Integer> ints = new Stack<>();
        System.out.println((nums instanceof Stack && nums instanceof Stack));

//        DODJELJIVANJE
        Stack raw = new Stack();
        Stack<Number> notrow = raw; // warning

        try {
            Number n = nums.pop();
        } catch (NullPointerException e) {
            System.out.println("nada");
        }

//        WILDCARD
        noCasting(new Stack<Number>());
//        noCasting(new Stack<Integer>());
        okCasting(new Stack<Integer>());

        gornjeOgranicenje(new Stack<Kid>());
        donjeOgranicenje(new Stack<Integer>(),new Stack<Number>());

        Stack<? super Number> s = new Stack<>();
        s.push(2);

    }

//    DOPUSTA READ IZ GENERICKOG
    public static void gornjeOgranicenje(Stack<? extends Parent> arg){
        Stack<Kid> kids = new Stack<>();
        Stack<? extends Parent> parents = kids;
//        kids.push(new Parent());
        System.out.println("? extends Parent pop()\npush(? extends Parent)\n");

        Stack<? extends Integer> ints = new Stack<>();
        Stack<? extends Number> nums = ints;
    }

//    DOPUSTA WRITE IZ GENERICKOG
    public static <Kid> void donjeOgranicenje(Stack<Kid> s1, Stack<? super Kid> s2){
        System.out.println("T ili iznad\n");
        System.out.println("? super Kid pop()\npush(? super Kid)\n");
    }


//    SOLVED WITH WILDCARD
    public static void noCasting(Stack<Number> s){
        System.out.println("Cant cast Stack<Anything> to Stack<Something>\n");
    }
    public static  void okCasting(Stack<? extends Number> s){
        System.out.println("Wildcard\n");
    }

//    EXTENDS KLASE & INTERFEJSI
    public static <T extends Number & Comparable> void max(Stack<T> s1, Stack<T> s2){
        ArrayList<Number> li = new ArrayList<Number>();
        li.add(2);
        li.add(2.5);
        T a = s1.pop();
        T b = s2.pop();
        if(a.compareTo(b) > 0) System.out.println(a);;
    }

}

/********************************************************************************************************/
class Ogranicenja<T>{
//    T t = new T();
//    T[] t = new T[6];
//    static T t;
//    if T instanceof
//    T t = (T)smth
//    catch T
//    overloading(T,T)
}
/********************************************************************************************************/
class Parent{}
class Kid extends Parent{}
/********************************************************************************************************/
class Stack<T>{

    class Node<T> {
       T element;
       Node<T> previous;

       public Node(T element){
           this.element = element;
       }
    }

    private Node<T> last = null;
    private long size = 0;

    public void push(T newElement){
        Node<T> tmp = last;
        last = new Node<>(newElement);
        last.previous = tmp;
        ++size;
    }

    public T pop() throws NullPointerException{
        if(last == null) throw new NullPointerException();
        else{
            T tmp = last.element;
            last = last.previous;
            --size;
            return tmp;
        }
    }

    public boolean isEmpty(){
        return last == null;
    }

    public long getSize(){
        return size;
    }


    public <T extends Number & Comparable> T max(){
        T max = (T)pop();
        while(!isEmpty()){
            T tmp = (T) pop();
            if(max.compareTo(tmp) < 0)
                max = tmp;
        }
        return max;
    }

}