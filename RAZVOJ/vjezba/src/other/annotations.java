package other;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


public class annotations {

    public static void main(String[] args) {
        method(' ');

        Integer i = 1;
        System.out.println(i instanceof Number);
        try {
//       DOHVATI SVE ANOTACIJE ZA MOJU KLASU
            Class klasa = MySubClass.class;
//       DOHVATI SVE ANOTACIJE ZA FIELD member
            Field field = klasa.getField("member");
//       DOHVATI SVE ANOTACIJE ZA METOD
            Method metod = klasa.getMethod("method");

//            Annotation[] annotations = klasa.getAnnotations();
//            Annotation[] annotations = field.getAnnotations();
            Annotation[] annotations = metod.getAnnotations();

            for (Annotation a : annotations) {
                if (a instanceof CustomAnn) {
                    CustomAnn cs = (CustomAnn) a;
                    System.out.println("Name: " + cs.name());
                }
            }

    }catch(NoSuchFieldException | NoSuchMethodException e){
        e.getCause();
    }
    }

//    @SuppressWarnings("deprecation")
    public static void method(char ch){
        System.out.println("Method: " + Character.isSpace(ch));
    }
}
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
//@Target(ElementType.FIELD)
//@Target(ElementType.METHOD)
@Inherited
@interface CustomAnn{
    public String name();
    public String value() default "DefaultValue";
}

//VRIJEDNOST SVIH ATRIBUTA SE MORA INICIJALIZIRATI UKOLIO NE POSTOJI DEFAULT
@CustomAnn(name="Name", value="Value")
class MyClass{
//    @CustomAnn(name = "Name")
    public String member = null;

//    @CustomAnn(name="name")
    public void method(){}
}

//OVA NASLJEDJUJE ANOTACIJE SA Inherited
class MySubClass extends MyClass{

}