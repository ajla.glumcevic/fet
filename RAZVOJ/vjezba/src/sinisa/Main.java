package sinisa;

import java.lang.annotation.*;
import java.sql.SQLOutput;

interface Mapa<K, V> {
    void dodaj(K kljuc, V vrijednost);

    V nadji(K kljuc);

}

@MojaAnotacija(
        imeStud = "Student",
        usmStud = "Ri",
        godStud = 3,
        izbPred = {"Klinicki inz", "Mehatronika"}
)

public class Main {
    public static void main(String[] args) {
        Mapa<String, Number> mapa = new MojaMapa<>();
        mapa.dodaj("prvi", 2);
        mapa.dodaj("drugi", 3.5);
        mapa.dodaj("treci", 8);
        mapa.dodaj("drugi", 1.1);
        stampajPoKljucu(mapa, "treci");
        stampajPoKljucu(mapa, "peti");
        stampajPoKljucu(mapa, "drugi");
        stampajPoKljucu(mapa, "sedmi");
//        System.out.println(prosjek(mapa));


        Class klasa = Main.class;
        Annotation ans[] = klasa.getAnnotations();
        for (Annotation an : ans) {
            if (an instanceof MojaAnotacija) {
                MojaAnotacija a = (MojaAnotacija) an;
                System.out.println(a.imeStud() + "je" + a.godStud() + " usmjerenja" + a.usmStud() + "i izabrao je predmete:");
                for (String s : a.izbPred()) {
                    System.out.print(s);
                }
            }
        }
    }

    static <K, V> void stampajPoKljucu(Mapa<K, V> mapa, K kljuc) {
        V vrijednost = mapa.nadji(kljuc);
        if (vrijednost == null) return;
        System.out.println(vrijednost);
    }

    static <K extends Comparable<K>, V extends Number> double prosjek(Mapa<K, V> mapa) {
        return ((MojaMapa<K, V>) mapa).prosjek();
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface MojaAnotacija {
    String imeStud();

    String usmStud();

    int godStud();

    String[] izbPred();
}

class MojaMapa<K extends Comparable<K>, V extends Number> implements Mapa<K, V> {
    private Node root = null;
    private int size = 0;

    class Node {
        K kljuc;
        V vrijednost;
        Node left = null;
        Node right = null;

        Node(K kljuc, V vrijednost) {
            this.kljuc = kljuc;
            this.vrijednost = vrijednost;
        }
    }

    @Override
    public void dodaj(K kljuc, V vrijednost) {
        if(this.root == null){
            this.root = new Node(kljuc,vrijednost);
            ++size;
        }
        else{
            Node tmp = root;
            while(tmp.left!=null || tmp.right!=null){
                int res = (kljuc).compareTo(tmp.kljuc);
                if(res == -1){
                    if(tmp.left==null){
                        tmp.left = new Node(kljuc,vrijednost);
                        ++size;
                        break;
                    }else {
                        tmp = tmp.left;
                    }
                }else if(res == 1){
                    if(tmp.right==null){
                        tmp.right = new Node(kljuc,vrijednost);
                        ++size;
                        break;
                    }else{
                        tmp = tmp.right;
                    }
                }else{
                    tmp.vrijednost = vrijednost;
                    break;
                }
            }
        }

    }


    @Override
    public V nadji(K kljuc) {
        Node tmp = root;
        while(tmp.kljuc != kljuc){
            int res = kljuc.compareTo(tmp.kljuc);
            if(res < 0){
                tmp = tmp.left;
            }else{
                tmp = tmp.right;
            }
        }
        return tmp.vrijednost;
    }


    public double prosjek() {
        double sum = 0;
        sum += racunaj(root);
        return sum / size;
    }

    private double racunaj(Node root) {
        if (root == null) return 0;
        return racunaj(root.left) + root.vrijednost.doubleValue() + racunaj(root.right);
    }
}

