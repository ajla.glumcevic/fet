package swap;


public class TestClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
//		A a = new A(0,0);
//		a.statA();
//		a.nonstatA();
//		
//		B.statB();
//		B b = new B(1,1);
//		b.nonstatB();

		B b1 = new B(0,0);
		B b2 = new B(1,1);
		B b3 = new B(2,2);
		System.out.println("b3 a: " + b3.getA());
		System.out.println("b3 b: " + b3.getB());

		
		System.out.println("Before swapBs b1 a: " + b1.getA());
		System.out.println("Before swapBs b2 a: " + b2.getA());
		System.out.println("Before swapBs b1 b: " + b1.getB());
		System.out.println("Before swapBs b2 b: " + b2.getB());
		swapBs2(b1,b2);
		System.out.println("After swapBs b1 a: " + b1.getA());
		System.out.println("After swapBs b2 a: " + b2.getA());
		System.out.println("After swapBs b1 b: " + b1.getB());
		System.out.println("After swapBs b2 b: " + b2.getB());
		
		changeB(b1);
		System.out.println("After change a: " + b1.getA());
		System.out.println("After change a: "+  b1.getA());
	}
	
	public static void statTestClass() {
		System.out.println("Stat TestClass");
	}
	
	public void nonstatTestClass() {
		System.out.println("nonStat TestClass");
	}
	
	
	static void changeB(B b1) {
		b1.setA(b1.getA()+100);
		b1.setB(b1.getB()+100);
	}
	
	static void swapBs2(B b1, B b2) {
		B tmpB = b1;
		b1 = b2;
		b2 = tmpB;
		
		System.out.println("In swapBs b1 a: " + b1.getA());
		System.out.println("In swapBs b2 a: " + b2.getA());
		System.out.println("In swapBs b1 b: " + b1.getB());
		System.out.println("In swapBs b2 b: " + b2.getB());
	}
}

class B{
	
	int a;
	int b;
	
	
	public static void statB() {
		System.out.println("Stat B");
	}
	
	
	public void nonstatB() {
		System.out.println("nonStat B");
	
	}
	
	public B(int x, int y) {
		a = x;
		b = y;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}
	
}