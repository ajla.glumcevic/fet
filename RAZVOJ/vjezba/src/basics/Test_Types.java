package basics;

public class Test_Types {

	public static void main(String[] args) {
		
		long n = 1234567890;
		long l = 123456789l;
		n = (int)l;
		System.out.println(n);
		
		// Not good
//		for(int n; n <0; n++) {
//			
//		}
		// Good
		for(int m; n <0; n++) {	
		}
		for(int m; n <0; n++) {
		}
		
		
		// primitivni po kopiji
		int[] niz = {1,2,3,4,5};
		for(int i: niz) {
			i+=1;
		}
		for(int i: niz) {
			System.out.println(i);
		}
		
		// String su immutable
		String s1 = "Ajla";
		String s2 = s1.concat("Glum");
		System.out.println(s2);
		
		System.out.println(s1.charAt(1) + ((Integer)(6+8)).toString());
		System.out.println(((Character)s1.charAt(1)).toString());

	
		
		
		
	}

}
