package basics;

import javax.swing.JOptionPane;

public class Test_JOP {

	public static void main(String[] args) {
		
		
		String input1 = (String) JOptionPane.showInputDialog(null, "Question to Double", "Title", JOptionPane.INFORMATION_MESSAGE) ;
		String input2 = (String) JOptionPane.showInputDialog(null, "Question to String", "Default") ;

//		 Component, Object, String, int, Icon, Object[] & Object – Input dialog with predefined options
		String[] options = {"1","2","3"};
		String input3 = (String) JOptionPane.showInputDialog(null, "Question to Int", "Title", JOptionPane.QUESTION_MESSAGE,null,options,options[0]) ;
		System.out.println(Integer.parseInt(input3) + Double.parseDouble(input1) + input2);

		

	}
	

}
