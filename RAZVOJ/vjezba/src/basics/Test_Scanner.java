package basics;

import java.util.Scanner;
public class Test_Scanner {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		
		System.out.println("Enter name, age, salary:");
		String name = sc.next();
		Integer age = sc.nextInt();
		Float salary = sc.nextFloat();
		System.out.println(name + age + salary.toString());
		
		
		System.out.println("Multiple lines: ");
		String line = sc.nextLine();
		String found = sc.findInLine("ajla");
		System.out.println(line);
		System.out.println(found);
		
	}

}
