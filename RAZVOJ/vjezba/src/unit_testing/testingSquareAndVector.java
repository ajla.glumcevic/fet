package unit_testing;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({testingSquareClass.class, testingVector.class})
public class testingSquareAndVector {
}
