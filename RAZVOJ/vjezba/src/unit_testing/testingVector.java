package unit_testing;

import org.junit.Test;

public class testingVector {

    @Test(expected = NullPointerException.class)
    public void popShouldReturnNull(){
        TestVector vector = new TestVector(5);
        vector.pop();
    }
}
class TestVector{
    Object[] elements;
    int size;
    public TestVector(int length){
        this.elements = new Object[length];
        this.size = 0;
    }
    public void push(Object o){
        this.elements[size++] = o;
    }
    public Object back(){
        return this.elements[size];
    }
    public Object pop(){
        if(this.size ==0) throw new NullPointerException();
        return this.elements[size--];
    }
}
