package unit_testing;

import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

public class testingSquareClass {
    @Test
    public void testSquare(){
        SquareClass sc = new SquareClass();
        int rez = sc.square(5);
        assertEquals(300, rez);
    }

    @Test
    public void testCountA(){
        SquareClass sc = new SquareClass();
        int rez = sc.countA("AAA");
        assertEquals(3, rez);
    }
}

class SquareClass{
    public int square(int x){
        return  x*x;
    }
    public int countA(String input){
        int count = 0;
        for(int i = 0; i < input.length() ; ++i){
            if(input.charAt(i)=='a' || input.charAt(i)=='A') ++count;
        }
        return count;
    }


}

