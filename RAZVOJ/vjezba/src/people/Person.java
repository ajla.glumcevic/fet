package people;


public abstract class Person {

	private Integer id;
	private String name;
	private Integer age;
	
	public Person(String name, int age) {
		this.id = 0;
		this.name = name;
		this.age = age;
	}
	
//	Getters
	public final int getId() {
		return id;
	}
	public final String getName() {
		return name;
	}
	public final int getAge() {
		return age;
	}
//	Setters
	protected void setId(int id) {
		this.id = id;
	}
	public final void setName(String newName) {
		 name = newName;
	}
	public final void setAge(Integer newAge) {
		age = newAge;
	}
	
	public abstract void printDailyActivities();
	
	@Override
	public String toString() {
		return "Name: " + name + "\tAge: " + age;
	}
		
}
