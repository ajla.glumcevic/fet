package people;

public class Teacher extends Person implements Comparable<Teacher>{
	
	private Double salary;
	
	public Teacher(String name, int age, double salary) {
		super(name, age);
		this.salary = salary;
		super.setId(1);

	}
	
//	Getters
	public Double getSalary() {
		return this.salary;
	}
	
//	Setters
	public void setSalary(Double newSalary) {
		salary = newSalary;
	}
	
	public void printDailyActivities() {
		System.out.println("Teach.");
	}
	
	@Override
	public String toString() {
		return super.toString() + "\tSalary: " + salary;
	}
	@Override
	public boolean equals(Object o) {
		if(this.getClass() == o.getClass()) {
			Teacher t = (Teacher)o;
			if(this.salary == t.salary) return true;
		}
		return false;		
	}
	@Override
	public int compareTo(Teacher other) {
		if(this.salary < other.salary) 
			return -1;
		else if(this.salary == other.salary ) 
			return 0;
		else 
			return 1;		
	}
}
