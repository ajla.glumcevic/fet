package people;

public class Student extends Person implements Comparable<Student>{

	private String major;
	private Integer grade;
	
	public Student(String name, int age, String major) {
		super(name, age);
		this.major = major;
		super.setId(2);
		}
	
//	Getters
	public String getMajor() {
		return this.major;
	}
	public int getGrade() {
		return grade;
	}
	
//	Setters
	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	public void printDailyActivities() {
		System.out.println("Study.");
	}
	
	@Override
	public String toString() {
		return super.toString() + "\tMajor: " + major;
	}
	@Override
	public boolean equals(Object o) {
		if(this.getClass() == o.getClass()) {
			Student s = (Student)o;
			if(this.major == s.major && this.grade == s.grade) return true;
		}
		return false;		
	}
	@Override
	public int compareTo(Student other) {
		if(grade < other.grade) 
			return -1;
		else if(grade == other.grade) 
			return 0;
		else 
			return 1;
	}
	
}
