package class_;


class Test{
	public static void main(String[] args) {


//		IMPLEMENTACIJA METODA IZ FUNKCIONALNOG INTERFEJSA
/**********************************************************************/
//		ANONIMNA KLASA
	    Functional func = new Functional() {
	    	int i = 0;
			@Override
			public void onlyOneMethod(int x) {
				System.out.println("Ovo je haman Anonimna klasa koja overridea metod funk.interfejsa?" + x);
			}
		};

//	    LAMDA :
	    Functional lamdica = (x) -> System.out.println("Ovo je sa lambdom " + x);


	}
}

/***********************************************************************************************************************/
	 interface Movable{
//	PUBLIC STATIC...
//		 no class members

//    	...FINAL
		 int i = 0;

//		 Java 8
//		 CAN BE OVERRIDEN
		 default void defaultMethod() {
//		 	i = 10; NO, ITS FINAL
		 	System.out.println("Default method.");
		 }
//		 STATIC NORMALNA
		 static void staticMethod() {
			 System.out.println("Static method.");
		 }
//		 ... ABSTRACT
		 void move(int x);
	 }


/***********************************************************************************************************************/
//	 FUNCTIONAL INTERFACE - samo jedan metod
@FunctionalInterface
	interface Functional{
		void onlyOneMethod(int x);
		default void defaultMethod(){
			System.out.println("default");
		}
    	static void staticMethod(String y){
			System.out.println(y);
		}
//		void anotherOne(); NO
}


/***********************************************************************************************************************/
//	// For Array.sort method
//	 interface Comparable{
//		int compareTo(Object other);
//	}

/***********************************************************************************************************************/
@FunctionalInterface
interface Display{
	public int show(String s1, String s2);
}
