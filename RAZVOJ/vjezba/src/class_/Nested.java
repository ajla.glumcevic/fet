package class_;


class Nested {
    public static void main(String[] args) {
       First f = new First();
       First.Second s = f.new Second();
       s.print();
       f.createLocalClass();
       f.exampleAnonimna();

//       STATIC
       Car audi = new Car( "Audi", "6");
       Car.Motor motor = new Car.Motor();
       motor.setType();
       System.out.println(motor.getType());

    }

}

/***********************************************************************************************************************/
    class First{
        public int x = 1;
        public int z = 0;
        public static int b = 888;

        void printX(){
            System.out.println(this.x);
        }

//       INNER
/**********************************************************************/
//        private class Second ACCESSIBLE ONLY IN FIRST
        public class Second{
            public int x = 2;
//            z = 10;   NO
//            static int y = 0;   NO
//            interface MyInterface{} NO, STATIC PO DEFAULTU
            final int y = 0;

            public Second(){
                First.this.x = 1555;
            }
            void printX(){
                System.out.println(this.x);
            }

            public void print(){
                System.out.println("First x: " + First.this.x + "Second x: " + this.x);
            }
        }

//        LOKALNA - DEFINIRANE UNUTAR METODA/IF/FOR
/**********************************************************************/
        public void createLocalClass(){
            class LocalClass{
               public LocalClass(){
                   System.out.println( First.this.x + "Local Class");
               }
            }
            LocalClass l = new LocalClass();
        }

//        STATIC
/**********************************************************************/
        public static class StaticClass{
           public void printStatic(){
//                System.out.println(First.this.b); NO
//                System.out.println(First.this.x); NO
               First f = new First();
               First.b = 1000;
               f.x = 1100;

            }
}

//      ANONIMNA - KORISTI SAMO JEDNOM
/**********************************************************************/
    public void exampleAnonimna(){
        Functional f = new Functional() {
            private int p = 100;
            @Override
            public void onlyOneMethod(int x) {
                System.out.println("Anonimna: " + (x+p));
            }
        };
        f.onlyOneMethod(10);
    }
}


/***********************************************************************************************************************/

class Car{
    private String name;
    String type;
    static String typeStat = "4 cilindra";

    public Car(String name, String type){
        this.name = name;
        this.type = type;
    }

    private String getName(){
        return name;
    }

    /**********************************************************************/
    static class Motor{
        String type;

        void setType(){
            if(Car.typeStat.equals("Audi"))
                this.type = "Audi Motor" + Car.typeStat;
            else
                this.type = "Whateva Motor";
        }

        public String getType(){
            return type;
        }
    }
}