package class_;

public class Lambda {

    public static void main(String[] args) {

//		IMPLEMENTACIJA METODA IZ FUNKCIONALNOG INTERFEJSA
/**********************************************************************/
//		ANONIMNA KLASA
            Functional func = new Functional() {
                @Override
                public void onlyOneMethod(int x) {
                    System.out.println("Ovo je haman Anonimna klasa koja overridea metod funk.interfejsa?" + x);
                }
            };
            func.onlyOneMethod(555);

//	    LAMDA
            Functional lamdica = (x) -> System.out.println("Ovo je sa lambdom " + x);
            lamdica.onlyOneMethod(8);

//      REF1 STATIC
            Functional ref1 = X::method1;
            ref1.onlyOneMethod(1);

//      REF2 CLASS, kako drugacije osim static u ovom kontekstu ??
            Functional ref2 = X::method1;
            ref2.onlyOneMethod(2);

//      REF3 OBJECT
            X x = new X(5);
            x.method2(2);
            Functional ref3 = x::method2;
            ref3.onlyOneMethod(3);

//      REF4 CONSTRUCTOR .. ne kontam sta mi vraca ovim
            Functional ref4 = X::new;
            ref4.onlyOneMethod(2);


            Display disp = String::indexOf;
            System.out.println(disp.show("String", "String"));
        }




}
/***********************************************************************************************************************/


class X{
    private int y = 0;
    public X(int y){
        this.y = y;
        System.out.println("Hej setovao si me na " + this.y);
    }
    static void method1(int x){
        System.out.println("X static method1." + x);
    }
    public void method2(int x){
        System.out.println("X method2." + x);
    }
}
