package class_;

public class Test_Inheritance {

	public static void main(String[] args) {
		
//		Manager m = new Manager(1000, "Emir");
//		m.setBonus(500);
//		System.out.println(m.getPlata());
//		
//		// Primjer Dinamickog Vezivanja(virtual u C++)
//		Employee e;
//		e = m; 
//		System.out.println(e.getPlata()); 
//		
//		System.out.println("\nEmployees array:");
//		Employee[] employees = new Employee[3];
//		employees[0] = m; // Employee ref na Manager objekat
//		employees[1] = new Manager(2000, "Ajla"); 
//		employees[2] = new Employee(3000, "Melina");
//		for(Employee employee : employees) {
//			System.out.println(employee.getIme() + " " + employee.getPlata());
//		}
//		
//		// Primjer Castanja
////		Manager m1 = employees[1]; compiletime error
//		if(employees[1] instanceof Manager) {
//			Manager m1 = (Manager)employees[1];
//		}
////		Manager m2 = (Manager)employees[2]; runtime error ClassCastException - downcast problem
//		
//		Manager[] managers = new Manager[3];
//		Employee[] employees2 = managers;
////		employees2[0] = new Employee(10, "Zlatan"); //runtime error ArrayStoreException (managers == employees2)
////		managers[0].setBonus(10000);
//		
//		
//		
//		// Primjer Object
//		Object o1 = new Employee(500, "Edin");
//		Object o2 = new Manager(900, "Lejla");
//		Manager o3 = new Manager(900, "Lejla");
//		if(o2.equals(o3) == true) System.out.println("Isti");
//		else System.out.println("Razliciti");
//		print(o3);
//		
//		//ArrayList
//		System.out.println("\nManager list:");
//		ArrayList<Manager> list = new ArrayList<Manager>();
//		list.add(m);
//		list.add(o3);
//		list.add(new Manager(699, "Emina"));
////		for(Manager manager: list) {
////			System.out.println(manager);
////		}
//		ListIterator<Manager> managerIt = list.listIterator(); 
//		System.out.println("Next: ");
//		while(managerIt.hasNext()) {
//			System.out.println(managerIt.next().getIme());
//		}
//		System.out.println("Previous: ");
//		while(managerIt.hasPrevious()) {
//			System.out.println(managerIt.previous().getIme());
//		}
//	}
//
//	static void print(Object o) {
//		System.out.println(o);
}
}
