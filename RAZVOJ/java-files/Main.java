package drugitry;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Broj[] brojevi = new Broj[5];
        Kompleksni[] kompleksni;
        brojevi[0] = new Kompleksni(2, -3);
        brojevi[1] = new Razlomak(4, 5);
        brojevi[2] = new Kompleksni(2, 6);
        brojevi[3] = new Razlomak(2, 3);
        brojevi[4] = new Kompleksni(2, -3);
        for (Broj br : brojevi) {
            br.clanovi();
            System.out.println("Vrijednost broja je " + br.value() + "\n");
        }
        stampajBrojeve(brojevi);
        kompleksni = izdvojiKompleksne(brojevi);
        Arrays.sort(kompleksni);
        stampajBrojeve(kompleksni);
        System.out.println(brojevi[0].saberi(brojevi[2]));
        System.out.println(brojevi[0].pomnozi(brojevi[4]));
        System.out.println(kompleksni[1].konjugirano());
        System.out.println(brojevi[0].equals(brojevi[1]));
        System.out.println(brojevi[0].equals(brojevi[4]));

//        Broj[] brojevi = new Broj[5];
//        Razlomak[] razlomci;
//        brojevi[0] = new Razlomak(6, 9);
//        brojevi[1] = new Razlomak(4, 5);
//        brojevi[2] = new Kompleksni(2, 6);
//        brojevi[3] = new Razlomak(2, 3);
//        brojevi[4] = new Kompleksni(2, -3);
//        for (Broj br : brojevi) {
//            br.clanovi();
//            System.out.println("Vrijednost broja je " + br.value() + "\n");
//        }
//        stampajBrojeve(brojevi);
//        razlomci = izdvojiRazlomke(brojevi);
//        Arrays.sort(razlomci);
//        stampajBrojeve(razlomci);
//        System.out.println(brojevi[0].saberi(brojevi[1]));
//        System.out.println(brojevi[0].pomnozi(brojevi[3]));
//        razlomci[0].skrati();
//        System.out.println(razlomci[0]);
//        System.out.println(brojevi[0].equals(brojevi[2]));
//        System.out.println(brojevi[0].equals(brojevi[3]));
    }
    static void stampajBrojeve(Broj[] brojevi) {
        for (Broj b : brojevi) {
            System.out.print(b.toString() + ',');
        }
        System.out.println();
    }

    static Razlomak[] izdvojiRazlomke(Broj[] brojevi) {
        Razlomak[] razlomci = new Razlomak[Razlomak.brojKreiranih];
        int counter = 0;
        for (Broj b : brojevi) {
            if (b instanceof Razlomak) razlomci[counter++] = (Razlomak) b;
        }
        return razlomci;
    }

    static Kompleksni[] izdvojiKompleksne(Broj[] brojevi) {
        Kompleksni[] kompleksni = new Kompleksni[Kompleksni.brojKreiranih];
        int counter = 0;
        for (Broj b : brojevi) {
            if (b instanceof Kompleksni) kompleksni[counter++] = (Kompleksni) b;
        }
        return kompleksni;
    }
}

abstract class Broj implements Comparable<Broj> {
    protected int a;
    protected int b;

    public Broj(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void clanovi() {
        System.out.print("Clanovi broja su (" + a + "," + b + ").");
    }

    public boolean equals(Broj b) {
        if (this.value() == b.value()) return true;
        return false;
    }

    abstract public double value();

    abstract public Broj saberi(Broj b);

    abstract public Broj pomnozi(Broj b);

    abstract public String toString();


    @Override
    public int compareTo(Broj k) {
        if (this.value() < k.value()) return -1;
        else if (this.value() == k.value()) return 0;
        else return 1;
    }
}
class Kompleksni extends Broj {
    public static int brojKreiranih = 0;

    public Kompleksni(int real, int imag) {
        super(real, imag);
        ++brojKreiranih;
    }

    public Kompleksni konjugirano() {
        return new Kompleksni(super.a, -super.b);
    }

    @Override
    public double value() {
        return Math.sqrt(super.a * super.a + super.b * super.b);
    }

    @Override
    public Broj saberi(Broj b) {
        return new Kompleksni(super.a + b.a, super.b + b.b);
    }

    @Override
    public Broj pomnozi(Broj b) {
        int real = super.a * b.a + super.b * b.b * (-1);
        int imag = super.a * b.b + super.b * b.a;
        return new Kompleksni(real, imag);
    }

    @Override
    public String toString() {
        if (super.b > 0) return super.a + "+" + super.b + "i";
        else return Integer.toString(super.a) + super.b + "i";
    }


}
class Razlomak extends Broj {
    public static int brojKreiranih = 0;

    public Razlomak(int br, int naz) {
        super(br, naz);
        ++brojKreiranih;
    }

    @Override
    public double value() {
        return (double) super.a / super.b;
    }

    @Override
    public Razlomak saberi(Broj b) {
        Razlomak r = (Razlomak) b;
        if (super.b == r.b) return new Razlomak(super.a + r.a, super.b);
        else {
            Razlomak res = new Razlomak(super.a * r.b + super.b * r.a, super.b * r.b);
            res.skrati();
            return res;
        }
    }

    @Override
    public Razlomak pomnozi(Broj b) {
        Razlomak r = (Razlomak) b;
        Razlomak res = new Razlomak(super.a * r.a, super.b * r.b);
        res.skrati();
        return res;
    }

    public void skrati() {
        int[] djelioci = {2, 3, 4, 5, 7, 11};
        for (int i : djelioci) {
            while (super.a % i == 0 && super.b % i == 0) {
                super.a /= i;
                super.b /= i;
            }
        }
    }

    @Override
    public String toString() {
        return super.a + "/" + super.b;
    }


}

