public class Razlomak extends Broj implements Comparable<Broj>{
    double brojnik;
    double nazivnik;

    public Razlomak(double br, double naz) {
        this.brojnik = br;
        this.nazivnik = naz;
    }

    @Override
    public int compareTo(Broj o) {
        Razlomak r = (Razlomak) o;
        if (this.value() < r.value()) return -1;
        else if (this.value() == r.value()) return 0;
        else return 1;
    }
    @Override
    public double value() {
        return brojnik / nazivnik;
    }

    @Override
    public void clanovi() {
        System.out.print("Clanovi broja su (" + this.brojnik + "," + this.nazivnik + ").");
    }

    @Override
    public Razlomak saberi(Broj broj) {
        Razlomak other = (Razlomak) broj;
        Razlomak rez;
        if (this.nazivnik == other.nazivnik) {
            rez = new Razlomak(this.brojnik + other.brojnik, this.nazivnik);
        }
        rez = new Razlomak(this.brojnik * other.nazivnik + other.brojnik * this.nazivnik, this.nazivnik * other.nazivnik);
        rez.skrati();
        return rez;
    }

    @Override
    public Razlomak pomnozi(Broj broj) {
        Razlomak other = (Razlomak) broj;
        return new Razlomak(this.brojnik * other.brojnik, this.nazivnik * other.nazivnik);
    }

    @Override
    public String toString() {
        return new String((int)this.brojnik + "/" + (int)this.nazivnik);
    }

    public void skrati(){
        while(this.brojnik%2==0 && this.nazivnik%2==0){
            this.brojnik/=2;
            this.nazivnik/=2;
        }
        while(this.brojnik%3==0 && this.nazivnik%3==0){
            this.brojnik/=3;
            this.nazivnik/=3;
        }
        while(this.brojnik%4==0 && this.nazivnik%4==0){
            this.brojnik/=4;
            this.nazivnik/=4;
        }
        while(this.brojnik%5==0 && this.nazivnik%2==5){
            this.brojnik/=5;
            this.nazivnik/=5;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Kompleksni) {
            Kompleksni k = (Kompleksni) o;
            return this.value() == k.value();
        } else if (o instanceof Razlomak) {
            Razlomak r = (Razlomak) o;
            return this.value() == r.value();
        }
        return false;
    }

}


