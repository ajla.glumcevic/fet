class Plain {
    public void fly(){
        System.out.println("Fly plain");
    }
}

class Boeing extends Plain{
   @Override
   public void fly(){
       System.out.println("Fly boeing");
   }
}

interface Car {
    public void drive();
}
public class Test{
    public static void main(String[] args) {
       Plain boeing = new Boeing();
       boeing.fly();

       Car car  = () -> System.out.println("drive");
       car.drive();
    }

}
