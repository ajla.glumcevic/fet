package drugitry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Duck {
    public static int podijeli(int a, int b) throws Exception {
        if (b == 0)
            throw new Exception("Dijeljenje sa 0");
        return a / b;
    }

    public static int pozovi2(int a, int b) throws Exception {
        return podijeli(a, b);
    }

    public static void pozovi1(int a, int b) {
        int temp;
        try {


            temp = pozovi2(a, b);
        } catch (Exception e) {
            System.out.println("Uhvaceno: " + e + " Generisem novu gresku");
//throw e; greska u kompajliranju
            throw new RuntimeException("Dogodila se greska");
        }
    }

    public static void main(String[] args) {
        String stream = Stream.of("A$B$C").toString();
        System.out.println(stream);
    }
}

class Ana{
    public static void anamarija(int u){
        System.out.println(u);
    }
}

interface Display{
    void display(int a);
}
