package queue;

import java.lang.annotation.*;

interface Queue<E>{
    void push(E element);
    E pop();
    boolean isEmpty();
}

@Anotacija(
    naslov = "Najbolja knjiga",
        usmStud = "RI",
        godStud = 3,
        literatura = {"Baze", "Razvoj"}
        )
public class Main {
    public static void main(String[] args) {
        Queue<Number> red = new MyQueue<>();
        red.push(2);
        red.push(3.6);
        red.push(8);
        red.push(1.1);
        while(!red.isEmpty()) printRoundedElement(red);
        red.push(2);
        red.push(3.6);
        red.push(8);
        red.push(1.1);
        System.out.println(prosjek(red));

        Class klasa = Main.class;
        Annotation[] ans = klasa.getAnnotations();
        for(Annotation a : ans){
            if(a instanceof Anotacija){
                Anotacija anotacija = (Anotacija)a;
                System.out.println(anotacija.naslov() + " se koristi kao literatura na "
                + anotacija.godStud() + " godini usmjerenja " + anotacija.usmStud() + " za predmete: " +
                anotacija.literatura()[0] + "," + anotacija.literatura()[1]);
            }
        }


    }

    static void printRoundedElement(Queue<Number> q){
        MyQueue<Number> red  = (MyQueue<Number>)q;
        Number n = red.pop();
        if(n instanceof Double){
            Double d = (Double)n;
            System.out.println(Math.round(d));
        }else{
            System.out.println(n);
        }
    }

    static Double prosjek(Queue<Number> q){
        MyQueue<Number> red  = (MyQueue<Number>)q;
        double sum = 0;
        int size = red.getSize();
        while(!red.isEmpty()){
            Number n = red.pop();
            if(n instanceof Double) sum+=(Double)n;
            else sum+=(Integer)n;
        }
        return sum/size;
    }
}
class MyQueue<E> implements Queue<E>{
    private int size = 0;
    private Node head = null;
    private Node tail = null;
    private class Node{
        E element;
        Node next;
        public Node(E element){
            this.element = element;
            this.next = null;
        }

    }

    @Override
    public void push(E element){
        if(isEmpty()) head = tail = new Node(element);
        else{
            tail.next = new Node(element);
            tail = tail.next;
        }
        ++size;
    }

    @Override
    public E pop(){
        if(isEmpty()) throw new NullPointerException();
        E returnElement = head.element;
        head = head.next;
        --size;
        return returnElement;
    }

    @Override
    public boolean isEmpty(){
        return head == null;
    }

    public int getSize(){
        return size;
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface Anotacija{
    String naslov();
    String usmStud();
    int godStud();
    String[] literatura();
}
