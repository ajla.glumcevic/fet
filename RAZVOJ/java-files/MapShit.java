import java.lang.annotation.*;

interface Mapa<K,V> {
    void dodaj(K key, V value);
    V nadji(K key);
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface MojaAnotacija {
    String imeStud();
    String usmStud();
    int godStudija();
    String[] izbPred();
}

@MojaAnotacija(
        imeStud = "Student Studentić",
        usmStud = "RI",
        godStudija = 3,
        izbPred = {"Klinički inženjering", "Mehatronika"}
)
public class MapShit {
    public static void main(String[] args) {
        Mapa<String, Number> mapa = new MojaMapa<>();
        mapa.dodaj("prvi",2);
        mapa.dodaj("drugi",3.5);
        mapa.dodaj("treci",8);
        mapa.dodaj("drugi",1.1);
        stampajPoKljucu(mapa,"treci");
        stampajPoKljucu(mapa,"peti");
        stampajPoKljucu(mapa,"drugi");
        stampajPoKljucu(mapa,"sedmi");
        System.out.println(prosjek(mapa));
        Class c = MapShit.class;
        Annotation annotation = c.getAnnotations()[0];
        MojaAnotacija sinisa = (MojaAnotacija) annotation;
        System.out.println(sinisa.imeStud());
    }

    private static <K extends Comparable,V extends Number> void stampajPoKljucu(Mapa<K, V> m, K k) {
        V val = m.nadji(k);
        if (val != null) {
            System.out.println(val);
        }
    }

    private static <K extends Comparable,V extends Number> double prosjek(Mapa<K, V> m) {
        return ((MojaMapa<K,V>)m).getAverage();
//        return m.getAverage();
    }
}

class Node<K extends Comparable,V extends Number> {
    public K key;
    public V value;
    public Node left;
    public Node right;
    public Node(K k, V v) {
        this.key = k;
        this.value = v;
    }
}

class MojaMapa<K extends Comparable,V extends Number> implements Mapa<K,V> {
    private Node root = null;
    private int size = 0;

    public double getAverage() {
        double sum = 0;
        sum += brrr(root);
        return sum / size;
    }

    private double brrr(Node root) {
        if (root == null) {
            return 0;
        }
        return brrr(root.left) + root.value.doubleValue() + brrr(root.right);
    }

    @Override
    public void dodaj(K key, V value) {
        Node newNode = new Node(key, value);
        if (root == null) {
            root = newNode;
        } else {
            addNode(root, newNode);
        }
        ++size;
    }

    @Override
    public V nadji(K key) {
        return findNode(root, key);
    }

    private V findNode(Node root, K key) {
        if (root == null) {
            return null;
        }
        if ((key).equals(root.key)) {
            return (V) root.value;
        }
        int r = (key).compareTo(root.key);
        if (r < 0) {
            return findNode(root.left, key);
        } else {
            return findNode(root.right, key);
        }
    }

    private void addNode(Node root, Node newNode) {
        int r = (newNode.key).compareTo(root.key);
        if (r < 0) {
            if (root.left == null) {
                root.left = newNode;
            } else {
                addNode(root.left, newNode);
            }
        } else if (r == 0) {
            root.value = newNode.value;
            --size;
        } else {
            if (root.right == null) {
                root.right = newNode;
            } else {
                addNode(root.right, newNode);
            }
        }
    }

    private void addNode1(Node root, Node newNode) {
        if (root == null) {
            root = newNode;
            return;
        }
        int r = (root.key).compareTo(newNode.key);
        if (r < 0) {
            root.left = new Node(newNode.key, newNode.value);
            addNode1(root.left, newNode);
        } else if (r == 0) {
            root.value = newNode.value;
        } else {
            root.right = new Node(newNode.key, newNode.value);
            addNode1(root.right, newNode);
        }
    }
}
