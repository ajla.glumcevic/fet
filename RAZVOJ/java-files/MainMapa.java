package drugitry;

interface Mapa<K, V> {
    void dodaj(K key, V value);

    V nadji(K key);

}

public class MainMapa {
    public static void main(String[] args) {
        Mapa<String, Number> mapa = new MojaMapa<>();
        mapa.dodaj("prvi", 2);
        mapa.dodaj("drugi", 3.5);
        mapa.dodaj("treci", 8);
        mapa.dodaj("drugi", 1.1);
        stampajPoKljucu(mapa, "treci");
        stampajPoKljucu(mapa, "peti");
        stampajPoKljucu(mapa, "drugi");
        stampajPoKljucu(mapa, "sedmi");
        System.out.println(prosjek(mapa));
    }

    static <K extends Comparable<K>, V extends Number> void stampajPoKljucu(Mapa<K, V> mapa, K key) {
        MojaMapa<K, V> mojaMapa = (MojaMapa<K, V>) mapa;
        V value = mojaMapa.nadji(key);
        if(value == null) return;
        System.out.println(value.doubleValue());
    }

    static <K extends Comparable<K>, V extends Number> Double prosjek(Mapa<K, V> mapa) {
        MojaMapa<K, V> mojaMapa = (MojaMapa<K, V>) mapa;
        return mojaMapa.prosjek();
    }
}

class MojaMapa<K extends Comparable<K>, V extends Number> implements Mapa<K, V> {
    Node root = null;
    private int size = 0;

    private class Node {
        K key;
        V value;
        Node left = null;
        Node right = null;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }


    @Override
    public void dodaj(K key, V value) {
        if (root == null) {
            root = new Node(key, value);
            ++size;
            return;
        }

        Node tmp = root;
        dodajNode(root, new Node(key, value));
    }

    public void dodajNode(Node node, Node newNode) {
        if (node == null) return;

        int res = (newNode.key).compareTo(node.key);
        if (res < 0) {
            if (node.left == null) {
                node.left = newNode;
                ++size;
            } else dodajNode(node.left, newNode);
        } else if (res > 0) {
            if (node.right == null) {
                node.right = newNode;
                ++size;
            } else dodajNode(node.right, newNode);
        } else {
            node.value = newNode.value;
        }
    }

    @Override
    public V nadji(K key) {
        Node tmp = root;
        return nadjiNode(tmp, key);
    }

    private V nadjiNode(Node node, K key) {
        if (node == null) return null;
        int res = (key).compareTo(node.key);
        if (res == 0) return node.value;
        else if (res < 0) {
            return nadjiNode(node.left, key);
        } else {
            return nadjiNode(node.right, key);
        }
    }

    public Double prosjek() {
        Double sum = 0.;
        sum += racunaj(root);
        return sum/ size;
    }

    private Double racunaj(Node node) {
        if (node == null) return 0.;
        return (node.value).doubleValue() + racunaj(node.left) + racunaj(node.right);
    }

}
