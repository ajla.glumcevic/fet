public class Kompleksni extends Broj implements Comparable<Broj> {
    int i, j;

    public Kompleksni(int i, int j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public int compareTo(Broj o) {
        Kompleksni k = (Kompleksni) o;
        if (this.value() < k.value()) return -1;
        else if (this.value() == k.value()) return 0;
        else return 1;
    }

    private double module() {
        double mod;
        mod = Math.sqrt(i * i + j * j);
        return mod;
    }

    @Override
    public void clanovi() {
        System.out.print("Clanovi broja su (" + this.i + "," + this.j + ").");
    }

    @Override
    public Kompleksni saberi(Broj broj) {
        Kompleksni other = (Kompleksni) broj;
        return new Kompleksni(this.i + other.i, this.j + other.j);
    }

    @Override
    public Kompleksni pomnozi(Broj broj) {
        Kompleksni other = (Kompleksni) broj;
        return new Kompleksni(this.i * other.i + this.j * other.j * (-1), this.i * other.j + this.j * other.i);
    }

    public Kompleksni konjugirano() {
        return new Kompleksni(this.i, -this.j);
    }

    @Override
    public double value() {
        return this.module();
    }


    @Override
    public String toString() {
        Integer i = this.i;
        Integer j = this.j;
        if (j < 0)
            return i.toString() + j.toString() + "i";
        else
            return i.toString() + "+" + j.toString() + "i";

    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Kompleksni) {
            Kompleksni k = (Kompleksni) o;
            return this.i == k.i && this.j == k.j;
        } else if (o instanceof Razlomak) {
            Razlomak r = (Razlomak) o;
            return this.module() == r.value();
        }
        return false;
    }

}
