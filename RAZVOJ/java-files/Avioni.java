import java.util.*;
import java.util.stream.Stream;

public class avioni {

    public static void main(String[] args) {
        AvionskiLet let1 = new AvionskiLet();
        System.out.println(let1);
        AvionskiLet let2 = new AvionskiLet(221, "Kirk", 2);
        System.out.println(let2);

        dodajPutnika(let2, null);
        dodajPutnika(let2, "Prvi Putnik");
        dodajPutnika(let2, "Drugi Putnik");
        dodajPutnika(let2, "Treci Putnik");
        System.out.println("\nLista putnika:");
        let2.ispisPutnika();

        System.out.println("\nPutnici: ");
        Iterator<String> iterator = let2.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
//
        AvionskiLet let3 = new AvionskiLet(222, "Picard", 7);
        AvionskiLet let4 = new AvionskiLet(222, "Jareway", 4);
        if (let3.equals(let4)) {
            System.out.println("let3 i let 4 su jednaki");
        } else {
            System.out.println("let3 i let4 nisu jednaki");
        }
        AvionskiLet let5 = new AvionskiLet(224, "Hock", 1);
        AvionskiLet let6 = new AvionskiLet(225, "Morgan", 14);
        if (let5.equals(let6)) {
            System.out.println(" let 5 i let6 su jednaki");
        } else {
            System.out.println("let 5 i let 6 isu jednaki");
        }
//
        List<AvionskiLet> listaLetova = new ArrayList<>();
        listaLetova.add(let1);
        listaLetova.add(let2);
        listaLetova.add(let3);
        listaLetova.add(let4);
        listaLetova.add(let5);
        listaLetova.add(let6);
        System.out.println("\nLista letova:");
        ispisiListu(listaLetova);
//
        System.out.println("\nSortirana lista: ");
        Collections.sort(listaLetova);
        ispisiListu(listaLetova);
    }

    /******************************************************************************************/

    public static void dodajPutnika(AvionskiLet al, String imePutnika) {
        try {
            al.dodajPutnika(imePutnika);
        } catch (IllegalArgumentException e) {
            System.out.println(e.toString());
        } catch (Exception e) {
            System.out.println("Exception uhvacen: " + e.getLocalizedMessage());
        }
    }

    public static void ispisiListu(List<AvionskiLet> lista) {
        lista.forEach(av -> System.out.println(av));
    }
}

/******************************************************************************************/

/******************************************************************************************/
class AvionskiLet implements Comparable<AvionskiLet> {

    class IteratorAL implements Iterator<String> {
        private int isAt = 0;

        @Override
        public boolean hasNext() {
            if (isAt == size) return false;
            return true;
        }

        @Override
        public String next() {
            return putnici[isAt++].toString();
        }

    }


    private int id;
    private String kapetan;
    private int capacity;
    private int size;
    private String[] putnici;

    public IteratorAL iterator() {
        return new IteratorAL();
    }

    public AvionskiLet() {
        this.id = 2;
        this.kapetan= "Jack Sparow";
        this.capacity = 10;
        this.size = 0;
        this.putnici = null;
    }

    public AvionskiLet(int id, String naziv, int capacity) {
        this.id = id;
        this.kapetan= naziv;
        this.capacity = capacity;
        this.size = 0;
        this.putnici = new String[capacity];
    }

    @Override
    public String toString() {
        return "Broj leta: " + this.id + " Kapetan: " + this.kapetan+ " Kapacitet leta: " + this.capacity;
    }

    public void dodajPutnika(String imePutnika) throws Exception {
        if (imePutnika == null) throw new IllegalArgumentException("NIje proslijedjeno ime");
        if (size == capacity) throw new Exception("Dosegnut kapacitet leta");
        this.putnici[size++] = new String(imePutnika);
    }

    public void ispisPutnika() {
        Stream.of(putnici).forEach(putnik -> System.out.println(putnik));
    }

    @Override
    public boolean equals(Object o) {
        AvionskiLet al = (AvionskiLet) o;
        return this.id == al.id;
    }

    @Override
    public int compareTo(AvionskiLet o) {
        if (this.capacity < o.capacity) return 1;
        else if (this.capacity == o.capacity) return 0;
        else return -1;
    }
}
