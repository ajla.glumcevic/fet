package prvi;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class magic_smth {

    public static boolean isMagicSquare(ArrayList<String[]> rows){
        int sum = 0;
        int sumDiagonalLeft = 0;
        int sumDiagonalRight = 0;
        for(int i = 0; i < rows.size(); ++i) {
            int sumOfRow = 0;
            int sumOfColumn = 0;

            for (int j = 0; j < rows.size(); ++j) {
                if (i == 0) {
                    sum += Integer.parseInt(rows.get(i)[j]);
                    sumDiagonalLeft += Integer.parseInt(rows.get(j)[j]);
                    sumDiagonalRight += Integer.parseInt(rows.get(j)[rows.size() - j - 1]);
                }
                sumOfRow += Integer.parseInt(rows.get(i)[j]);
                sumOfColumn += Integer.parseInt(rows.get(j)[i]);
            }

            if (sumOfRow != sum || sumOfColumn != sum) {
                return false;
            }
        }
        return sumDiagonalLeft == sum && sumDiagonalRight == sum;
    }

    public static void main(String[] args) {

        String line;
        String[] columns;
        ArrayList<String[]> rows = new ArrayList<String[]>(){};

        try(BufferedReader bf = new BufferedReader(new FileReader("Zad1a.txt"))){
        while( (line = bf.readLine()) != null){
            columns = line.split("\t");
            if(columns.length <= 1) continue;
            rows.add(columns);
        }
            System.out.println(isMagicSquare(rows));
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

    }



}
