package queue;

import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

public class Test_Queue {
    public static void main(String[] args) {

        ArrayList<Integer> arr = new ArrayList<Integer>(){};
        for(String s : args){
            arr.add(Integer.parseInt(s));
        }
        Queue<Integer> q = new Queue(arr);

        while(!q.isEmpty()){
            System.out.println(q.pop());
        }
    }
}

class Queue<T>{
    class Node<T>{
        T value;
        Node<T> next;
        Node(T value){
            this.value = value;
            this.next = null;
        }
    }
    Node<T> head = null;
    Node<T> tail = null;

    public Queue(ArrayList<T> array){
        for(T t : array){
            this.push(t);
        }

    }

    public boolean isEmpty(){
        return head == null;
    }

    public void push(T value){
        Node<T> newNode = new Node(value);
        if(isEmpty()){
           this.head = this.tail = newNode;
        }else{
           this.tail.next = newNode;
           this.tail = newNode;
        }
    }

    public T pop(){
        if(isEmpty()){
            throw new ArrayIndexOutOfBoundsException("Broooother.");
        }else{
            T tmpValue = this.head.value;
            if(this.head==this.tail)
                this.tail= this.head = this.head.next;
            else
                this.head = this.head.next;

            return tmpValue;
        }
    }

    public T end(){
        if(isEmpty()){
            return null;
        }else{
            return this.head.value;
        }
    }

}
