package stacks.test;
import stacks.zadaca2.MyStack;
import stacks.zadaca2.StackList;
import stacks.zadaca2.StackArray;

public class TestStack
{
    public static void main(String[] args)
    {
         MyStack a = new StackArray(2);
        a.push("String");
        a.push(new Integer(2));
        a.push(new Double(2.1));
        a.push(new Integer(10));
        while(!a.isEmpty())
            System.out.println(a.pop());

        try
        {
            a.pop();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        a = new StackList();
        a.push(new Integer(10));
        System.out.println(a.pop());
        a.pop();
    }
}