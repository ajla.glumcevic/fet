package stacks.zadaca2;

public interface MyStack {

    void push(Object obj);
    Object pop();
    boolean isEmpty();

}
