package stacks.zadaca2;

public class StackList implements MyStack{
    class Node{
        Object value;
        Node previous;
        public Node(Object obj){
            this.value = obj;
            this.previous = null;
        }
    }
    Node top;

    public StackList(){
       this.top = null;
    }
    @Override
    public boolean isEmpty(){
        return top == null;
    }

    @Override
    public void push(Object obj){
       if(isEmpty()){
          this.top = new Node(obj);
       }else{
          Node newNode = new Node(obj);
          newNode.previous = this.top;
          this.top = newNode;
       }
    }

    @Override
    public Object pop() {
        if(isEmpty())
            throw new RuntimeException("Empty stack!");
        else{
           Object returnValue = this.top.value;
           this.top = this.top.previous;
           return returnValue;
        }
    }
}
