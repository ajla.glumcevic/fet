package stacks.zadaca2;


public class StackArray implements MyStack{

    private Object[] objs;
    private int capacity;
    private int size;

    public StackArray(int capacity){
        this.capacity = capacity;
        objs = new Object[capacity];
    }

    @Override
    public boolean isEmpty(){
        return size == 0;
    }

    private boolean isFull(){
        return this.size >= this.capacity;
    }

    @Override
    public void push(Object obj){
        if(isFull()) {
            this.capacity *= 2;
            Object[] tmpObjs = new Object[this.capacity];
            for (int i = 0; i < this.objs.length; ++i) {
                tmpObjs[i] = this.objs[i];
            }
            this.objs = tmpObjs;
        }
            this.objs[size++] = obj;
    }

    @Override
    public Object pop(){
        if(isEmpty()){
            throw new RuntimeException("Empty stack!");
        }else{
           return this.objs[--this.size];
        }
    }
}
