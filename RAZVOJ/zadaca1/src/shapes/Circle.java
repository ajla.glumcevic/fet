package shapes;

public class Circle extends Shape{
    private double r;

    public Circle(double r){
        this.r = r;
    }

    @Override
    public double area(){
        return Math.pow(r,2.) * Math.PI;
    }
}
