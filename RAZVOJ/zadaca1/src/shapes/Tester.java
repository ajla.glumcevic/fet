package shapes;

public class Tester {
    public static void main(String[] args) {
        Shape[] a = new Shape[2];
        a[0] = new Circle(3);
        a[1] = new Rectangle(2,3);
        for(int k = 0; k < a.length; ++k){
            System.out.println(a[k].area());
        }
    }

}


