package polinom;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Polinom {

    private double[] coeficients;
    private int len;

    public Polinom(double[] coeficients){
        this.coeficients = coeficients;
        this.len = coeficients.length;
    }

    public void add(Polinom other){
        if(this.len  < other.len){
            double[] tmpCoeficients = new double[other.len];
            int diff = other.len - this.len - 1;
            for(int i = other.len - 1, j = this.len - 1; i > diff ; --i,--j )
                tmpCoeficients[i] = this.coeficients[j] + other.coeficients[i];
            for(int i = diff; i >=0 ; --i)
                tmpCoeficients[i] = other.coeficients[i];
            this.coeficients = tmpCoeficients;
        }else{
            int diff = this.len - other.len;
           for(int i = 0; i < other.len ; i++)
               this.coeficients[diff+i] = this.coeficients[diff+i] + other.coeficients[i];
        }
    }

    public void multiply(Polinom other){
        double[] tmpCoeficients = new double[this.len+other.len-1];

    }

    public double calculate(double x){
        double sum = 0.;
        int power = this.len - 1;
        for(double coeficient : coeficients){
            sum+= coeficient * Math.pow(x, power);
            power--;
        }
        return sum;
    }

    public void print(){
        for(double d : coeficients){
            System.out.print(d + " ");
        }

    }
//    @Override
//    public String toString() {
//    }
}
