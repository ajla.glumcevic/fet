package employees;


public class Employee {
	private String name;
	private Double salary;
	
	public Employee(String name, Double salary) {
		this.name = name;
		this.salary = salary;
	}

	public final String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}



	//override Object method
	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null) return false;
		if(this.getClass() != o.getClass()) return false;
		Employee emp = (Employee)o;
		return this.salary == emp.salary && (this.name).equals(emp.name);
	}
	
	@Override
	public String toString() {
		return "Ime: " + this.getName() + "\nPlata:" + this.getSalary();
	}
}
