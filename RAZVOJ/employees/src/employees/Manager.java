package employees;

public class Manager extends Employee{
	private Double bonus;
	
	public Manager(String name, Double salary) {
		super(name, salary);
		this.bonus = 0.;
	}
	@Override
	public Double getSalary() {
		return super.getSalary() + bonus;
	}
	public final void setBonus(Double bonus) {
		this.bonus = bonus;
	}
	
	@Override
	public boolean equals(Object o) {
		if(super.equals(o) == true) {
			Manager m = (Manager)o;
			if(this.bonus == m.bonus) return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return super.toString() +  "\nBonus: " + bonus;
 	}
}
