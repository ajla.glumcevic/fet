package geoobjects;

public class Circle extends GeoObject{

	private int radius;
	
	public Circle() {}
	
	public Circle(int radius) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return Math.PI * Math.pow(this.getRadius(), 2) ;
	}
	
	public double getVolume() {
		return 2 * this.getRadius() * Math.PI;
	}
	
	@Override
	public String toString() {
		return "Circle:\n" + super.toString() + "Radius: " + this.getRadius() + "\nArea: " + this.getArea() + "\n";
	}
}
