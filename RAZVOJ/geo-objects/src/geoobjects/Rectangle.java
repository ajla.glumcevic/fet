package geoobjects;

public class Rectangle extends GeoObject {

	private int width;
	private int height;
	
	public Rectangle() {}
	
	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	public double getArea() {
		return this.getWidth() * this.getHeight();
	}
	
	public double getVolume() {
		return 2 * this.getWidth() + 2* this.getHeight();
	}
	
	@Override
	public String toString() {
		return "Rectangle:\n" + super.toString() + "Width: " + this.getWidth() + "\nHeight: " + this.getHeight() + "\nArea: " + this.getArea() + "\n";
	}
	
	
}
