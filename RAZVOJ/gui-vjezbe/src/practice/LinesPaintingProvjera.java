package practice;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class LinesPaintingProvjera{
    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
    }
}

class MainFrame extends JFrame {
    private ScreenPanel screenPanel = null;
    private ButtonPanel buttonPanel = null;

    public MainFrame() {
        setVisible(true);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setTitle("Main Frame");

        Toolkit tk = Toolkit.getDefaultToolkit();
        int x = tk.getScreenSize().width;
        int y = tk.getScreenSize().height;
        setLocation(3 * x / 8, y / 4);
        setSize(new Dimension(x / 4, y / 2));

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                int returnValue = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit?", "Exit dialog.", JOptionPane.OK_CANCEL_OPTION);
                if (returnValue == 0) {
                    dispose();
                    System.exit(0);
                }
            }
        });

        setLayout(new BorderLayout());
        screenPanel = new ScreenPanel();
        add(screenPanel, BorderLayout.CENTER);

        buttonPanel = new ButtonPanel();
        add(buttonPanel, BorderLayout.SOUTH);


    }


    private class ScreenPanel extends JPanel {
        ArrayList<Line2D> lines = new ArrayList<Line2D>();
        Point2D currentPoint = null;
        Point2D lastPoint = null;
        boolean allowPainting = true;

        public ScreenPanel() {
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    if (!allowPainting) return;
                    if (e.getClickCount() >= 2) {
                        allowPainting = false;
                    }

                    currentPoint = e.getPoint();
                    if (lastPoint == null) {
                        lastPoint = currentPoint;
                        return;
                    } else {
                        Line2D line = new Line2D.Double(lastPoint, currentPoint);
                        if (checkLines(line) == true) return;
                        lines.add(line);
                        lastPoint = currentPoint;
                        repaint();
                    }

                }
            });

        }

        public void clearScreen() {
            lines.clear();
            currentPoint = null;
            lastPoint = null;
            allowPainting = true;
            repaint();
        }

        public boolean checkLines(Line2D other) {
            for (Line2D line : lines) {
            }
            return false;
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            for (Line2D line : lines)
                g2.draw(line);
        }
    }

    private class ButtonPanel extends JPanel {
        private JButton button = null;

        public ButtonPanel() {
            button = new JButton("New line");
            add(button, BorderLayout.SOUTH);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    screenPanel.clearScreen();
                }
            });
        }
    }

}