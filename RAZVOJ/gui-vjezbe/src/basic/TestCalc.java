package basic;

import javax.swing.*;
import java.awt.*;

//flow
//border, mijenja veliicnu u skladu velicine parenta
//grid
public class TestCalc {
    public static void main(String[] args) {
        CalculatorFrame frame = new CalculatorFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
//        frame.setResizable(false);
    }
}

class CalculatorFrame extends JFrame {
    public CalculatorFrame() {
//        setSize(500, 500);
        setTitle("Calculator");
        CalculatorPanel panel = new CalculatorPanel();
        add(panel);
//         preferredSize + setPreferredSize()
        pack();
    }
}

class CalculatorPanel extends JPanel {

    private JPanel panel;

    public CalculatorPanel() {
        setLayout(new BorderLayout());

        JButton display = new JButton("0.0.");
        display.setEnabled(false);
        add(display, BorderLayout.NORTH);

        panel = new JPanel();
        panel.setLayout(new GridLayout(4, 4));

        addButton("7");
        addButton("8");
        addButton("9");
        addButton("/");

        addButton("4");
        addButton("5");
        addButton("6");
        addButton("*");

        addButton("1");
        addButton("2");
        addButton("3");
        addButton("-");

        addButton("0");
        addButton(".");
        addButton("=");
        addButton("+");
        add(panel, BorderLayout.CENTER);

        setPreferredSize(new Dimension(800,800));
    }

    private void addButton(String label) {
        JButton button = new JButton(label);
        button.setSize(new Dimension(50,50));
        panel.add(button);

    }
}
