package basic;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

public class TesFrame {
    public static void main(String[] args) {
//        create custom frame
        CustomFrame cf = new CustomFrame();
    }
}

class CustomFrame extends JFrame{
    private static int WIDTH = 400;
    private static int HEIGHT = 400;


    CustomFrame(){
//        set frame to be visible
        setVisible(true);

//        DO_NOTHING_ON_CLOSE
//        DISPOSE_ON_CLOSE
//        set default operation when X is clicked
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

//        set dimension
        setSize(WIDTH, HEIGHT);

//        set location
        Toolkit tk = Toolkit.getDefaultToolkit();
        int screenWidth = tk.getScreenSize().width;
        int screenHeight = tk.getScreenSize().height;
        setLocation(screenWidth / 2 - WIDTH/2, screenHeight / 2 - HEIGHT/2);

//        set title
        setTitle("Not a hello world program.");

//        add(new CustomPanel());
        add(new DrawPanel());
    }
}

class DrawPanel extends JPanel{

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D  g2D = (Graphics2D) g;

        int leftX = 100, leftY = 100, width = 200, height = 150;
        Rectangle2D rectangle = new Rectangle2D.Double(leftX,leftY,width,height);

        g2D.draw(rectangle);

        Ellipse2D ellipse = new Ellipse2D.Double();
        ellipse.setFrame(rectangle);
        g2D.setColor(Color.RED);
        g2D.draw(ellipse);

        Line2D line = new Line2D.Double(leftX,leftY,leftX + width, leftY+height);
        g2D.draw(line);

//        ovo mi ne radi
//        Ellipse2D circle = new Ellipse2D.Double();
//        circle.setFrameFromCenter(rectangle.getCenterX(), rectangle.getCenterY(),
//                rectangle.getCenterX() + 75, rectangle.getY() + 75);
//        g2D.setColor(Color.YELLOW);
//        g2D.fill(circle);
    }
}


class CustomPanel extends JPanel{

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawString("Not a hello world program!", 0, 100);
    }
}