package basic;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class TestClock {
    public static void main(String[] args) {
        Clock c = new Clock();
        c.setVisible(true);
        c.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

class Clock extends JFrame {
    public Clock() {
        setSize(500, 500);
        add(new ClockPanel());
        add(new ClockTextFrame());
    }
}

class ClockTextFrame extends JFrame {
    public static final int DEFAULT_WIDTH = 300;
    public static final int DEFAULT_HEIGHT = 300;

    private JTextField textSati;
    private JTextField textMinute;
    private ClockPanel sat;

    public ClockTextFrame() {
        setTitle("Title");
        setLayout(new BorderLayout());

        JPanel panel = new JPanel();
        panel.add(new JLabel("Sati"));
        textSati = new JTextField("12", 3);
        panel.add(textSati);
        textSati.getDocument().addDocumentListener(new ClockFieldListener());

        panel.add(new JLabel("Minute"));
        textMinute = new JTextField("00", 3);
        panel.add(textMinute);
        textMinute.getDocument().addDocumentListener(new ClockFieldListener());

        add(panel, BorderLayout.SOUTH);

        sat = new ClockPanel();
        add(sat, BorderLayout.CENTER);
        pack();
    }

    private class ClockFieldListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            setClock();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            setClock();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {

        }
    }

    private void setClock() {
        try {
            int sati = Integer.parseInt(textSati.getText().trim());
            int minute = Integer.parseInt(textMinute.getText().trim());
            sat.postaviVrijeme(sati, minute);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class ClockPanel extends JPanel {
    private int RADIUS = 100;
    private double VELIKA_DUZINA = 0.8 * RADIUS;
    private double MALA_DUZINA = 0.6 * RADIUS;
    private double minute = 0;

    public ClockPanel() {
        setPreferredSize(new Dimension(2 * RADIUS + 1, 2 * RADIUS + 1));
    }

    public void paintComponent(Graphics2D g) {
//        crta kruznu granicu
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        Ellipse2D circle = new Ellipse2D.Double(0, 0, 2 * RADIUS, 2 * RADIUS);
        g2.draw(circle);

//        crta malu kazaljku
        double mala = Math.toRadians(90 - 360 * minute / 60);
        crtajKazaljku(g2, mala, MALA_DUZINA);

//        crta veliku kazaljku
        double velika = Math.toRadians(90 - 360 * minute / 60);
        crtajKazaljku(g2, velika, VELIKA_DUZINA);
    }

    public void crtajKazaljku(Graphics2D g2, double ugao, double duzina) {
        Point2D kraj = new Point2D.Double(
                RADIUS + duzina * Math.cos(ugao),
                RADIUS - duzina * Math.sin(ugao));
        Point2D centar = new Point2D.Double(RADIUS, RADIUS);
        g2.draw(new Line2D.Double(centar, kraj));
    }

    public void postaviVrijeme(int h, int m) {
        minute = h * 60 + m;
        repaint();
    }
}