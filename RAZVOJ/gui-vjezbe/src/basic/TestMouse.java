package basic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class TestMouse {
    public static void main(String[] args) {
        MouseFrame mf = new MouseFrame();
    }
}

class MouseFrame extends JFrame {

    public MouseFrame() {
        setTitle("Event programming.");
        setVisible(true);

        Toolkit tk = Toolkit.getDefaultToolkit();
        int screenWidth = tk.getScreenSize().width;
        int screenHeight = tk.getScreenSize().height;

        setSize(screenWidth / 2, screenHeight / 2);
        setLocation(screenWidth / 4, screenHeight / 4);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        add(new MousePanel());
    }
}

class MousePanel extends JPanel {
    private ArrayList<Rectangle2D> rectangles = new ArrayList<>();
    private Rectangle2D current = null;

    public MousePanel() {
        addMouseListener(new MouseHandler());
        addMouseMotionListener(new MouseMotionHandler());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        for (Rectangle2D r : rectangles) {
            g2.draw(r);
        }
    }

    public Rectangle2D find(Point2D p) {
        for (Rectangle2D r : rectangles) {
            if (r.contains(p)) return r;
        }
        return null;
    }

    public void addRectangle(Point2D p) {
        current = new Rectangle2D.Double(p.getX() - 5, p.getY() - 5, 10, 10);
        rectangles.add(current);
        repaint();
    }

    public void removeRectangle(Rectangle2D rectangle) {
        if (rectangle == null) return;
        if (rectangle == current) current = null;
        rectangles.remove(rectangle);
        repaint();
    }

    private class MouseHandler extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            int numClicks = e.getClickCount();
            if (numClicks >= 2 && (current = find(e.getPoint())) != null) {
                removeRectangle(current);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            current = find(e.getPoint());
            if (current == null) addRectangle(e.getPoint());
        }

    }

    private class MouseMotionHandler implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (current != null) current.setFrame(e.getX() - 5, e.getY() - 5, 10, 10);
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            current = find(e.getPoint());
            if (current == null) setCursor(Cursor.getDefaultCursor());
            else setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        }
    }
}
