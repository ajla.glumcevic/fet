package basic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TestEvent {
    public static void main(String[] args) {
//        add test frame
        TestFrame frame = new TestFrame();
    }
}


class TestFrame extends JFrame {

    public TestFrame() {
        setTitle("Event programming.");
        setVisible(true);

        Toolkit tk = Toolkit.getDefaultToolkit();
        int screenWidth = tk.getScreenSize().width;
        int screenHeight = tk.getScreenSize().height;

        setSize(screenWidth / 2, screenHeight / 2);
        setLocation(screenWidth / 4, screenHeight / 4);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        ButtonPanel bp = new ButtonPanel();
//        setFocusable(false);
//        bp.setFocusable(true);

        add(bp);
//        bp.requestFocus();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                int returnValue = JOptionPane.showConfirmDialog(null, "Goodbye", "Bye modal", JOptionPane.OK_CANCEL_OPTION);
                if (returnValue == 0) {
                    dispose();
                    System.exit(0);
                }
            }
        });
    }
}

class ButtonPanel extends JPanel implements ActionListener, KeyListener {
    JButton blueBtn = new JButton("Blue");
    JButton yellowBtn = new JButton("Yellow");
    JButton redBtn = new JButton("Red");

    public ButtonPanel() {
        add(blueBtn);
        add(yellowBtn);
        add(redBtn);
        blueBtn.addActionListener(this);
        blueBtn.addKeyListener(this);
        yellowBtn.addActionListener(this);
        yellowBtn.addKeyListener(this);
        redBtn.addActionListener(this);
        redBtn.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == blueBtn) setBackground(Color.BLUE);
        else if (e.getSource() == yellowBtn) setBackground(Color.YELLOW);
        else if (e.getSource() == redBtn) setBackground(Color.RED);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (keyCode == KeyEvent.VK_Y && e.isControlDown()) setBackground(Color.YELLOW);
        else if (keyCode == KeyEvent.VK_R && e.isControlDown()) setBackground(Color.RED);
        else if (keyCode == KeyEvent.VK_B && e.isControlDown()) setBackground(Color.BLUE);
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}