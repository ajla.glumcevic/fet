package basic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextArea {
    public static void main(String[] args) {
        TextAreaFrame frame = new TextAreaFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

class TextAreaFrame extends JFrame{
    public static final int DEFAULT_WIDTH = 300;
    public static final int DEFAULT_HEIGHT = 300;
    private JPanel buttonPanel;
    private JTextArea textArea;
    private JScrollPane scrollPane;

    public TextAreaFrame(){
        setTitle("tExt");
        setSize(DEFAULT_WIDTH,DEFAULT_HEIGHT);

        textArea = new JTextArea(10,40);
        scrollPane = new JScrollPane(textArea);

        add(scrollPane, BorderLayout.CENTER);

        buttonPanel = new JPanel();
        JButton insertButton = new JButton("Insert");
        buttonPanel.add(insertButton);
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.append("Dummy text for text area.");
            }
        });

        JButton wrapButton = new JButton("Wrap");
        wrapButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean wrap = !textArea.getLineWrap();
                textArea.setLineWrap(wrap);
                wrapButton.setText(wrap ? "No wrap" : "Wrap");
            }
        });

        buttonPanel.add(wrapButton);

        add(buttonPanel, BorderLayout.SOUTH);

    }
}
