package hw2;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Problem4 {
    public static void main(String[] args) {
        GeometricFrame geometricFrame = new GeometricFrame();
    }
}

class GeometricFrame extends JFrame {
    ButtonPanel bp;
    DrawingPanel dp;
    TextPanel tp;

    public GeometricFrame() {
        setTitle("Not a hello world program.");
        Toolkit tk = Toolkit.getDefaultToolkit();
        int screenWidth = tk.getScreenSize().width;
        int screenHeight = tk.getScreenSize().height;
        setSize(screenWidth / 2, screenHeight / 2);
        setLocation(screenWidth / 4, screenHeight / 4);
        setLayout(new BorderLayout());

        bp = new ButtonPanel();
        dp = new DrawingPanel();
        tp = new TextPanel();

        add(new ButtonPanel(), BorderLayout.NORTH);
        add(new DrawingPanel(), BorderLayout.CENTER);
        add(new TextPanel(), BorderLayout.SOUTH);

        setVisible(true);
    }

    class ButtonPanel extends JPanel {
        JButton circleBtn;
        JButton squareBtn;

        public ButtonPanel() {
            circleBtn = new JButton("Krug");
            squareBtn = new JButton("Kvadrat");

            circleBtn.addActionListener(new MouseButtonListener());
            squareBtn.addActionListener(new MouseButtonListener());

            add(circleBtn);
            add(squareBtn);
        }
    }

    class MouseButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            dp.circle = e.getSource() == bp.circleBtn;
            repaint();
        }
    }

    class DrawingPanel extends JPanel {
        boolean circle = false;
        double dimension = 0;

        public DrawingPanel() {
            JLabel label = new JLabel("Oblik:");
            label.setHorizontalAlignment(JLabel.CENTER);

            add(label);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D graphics2D = (Graphics2D) g;
            graphics2D.setColor(Color.RED);
            Rectangle2D rectangle2D = new Rectangle2D.Double(50, 50, dimension, dimension);
            if (circle) {
                Ellipse2D ellipse2D = new Ellipse2D.Double();
                ellipse2D.setFrame(rectangle2D);
                graphics2D.setColor(Color.MAGENTA);
                graphics2D.fill(ellipse2D);
            }
            repaint();
        }
    }

    class TextPanel extends JPanel {
        public TextPanel() {
            JLabel textLabel = new JLabel("Dimenzija:");
            JTextField textField = new JTextField();

            textField.getDocument().addDocumentListener(new TextDocumentListener());

            add(textLabel);
            add(textField);
        }
    }

    class TextDocumentListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void removeUpdate(DocumentEvent e) {

        }

        @Override
        public void changedUpdate(DocumentEvent e) {

        }
    }

}


